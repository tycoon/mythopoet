﻿
DROP TABLE IF EXISTS `flash_sale_item`;
CREATE TABLE `flash_sale_item` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `item_id` bigint(11) NOT NULL COMMENT '商品id',
  `price` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '抢购价',
  `discount` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '折扣',
  `stime` int(11) NOT NULL DEFAULT '0' COMMENT '抢购开始时间',
  `etime` int(11) NOT NULL DEFAULT '0' COMMENT '抢购结束时间',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


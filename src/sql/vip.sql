

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` bigint(11) NOT NULL auto_increment COMMENT '客户id',
  `userid` bigint(11) NOT NULL COMMENT '用户ID',
  `img` varchar(255) default NULL COMMENT '头像',
  `sex` int(1) default NULL COMMENT '性别',
  `birthday` int(11) default NULL COMMENT '生日',
  `realname` varchar(10) default NULL COMMENT '真实姓名',
  `mail` varchar(50) default NULL COMMENT '邮箱',
  `marriage` int(1) default NULL COMMENT '是否婚姻',
  `earning` int(11) default NULL COMMENT '月收入',
  `idtype` int(11) default NULL COMMENT '证件类型',
  `identity` varchar(50) default NULL COMMENT '证件号',
  `education` varchar(20) default NULL COMMENT '学历',
  `industry` varchar(50) default NULL COMMENT '行业',
  `level` int(1) NOT NULL COMMENT '会员级别',
  `memo` varchar(255) default NULL COMMENT '备注',
  `utime` int(11) NOT NULL COMMENT '创建时间',
  `ctime` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `customercard`;

CREATE TABLE `customercard` (
  `id` bigint(11) NOT NULL auto_increment COMMENT '主键',
  `code` varchar(20) NOT NULL COMMENT '密码',
  `level` int(11) NOT NULL COMMENT '会员级别',
  `state` int(1) NOT NULL default '0' COMMENT '0:未激活;1:已激活',
  `userid` int(11) default NULL COMMENT '用户ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `customerlevel`;

CREATE TABLE `customerlevel` (
  `id` int(2) NOT NULL COMMENT '级别',
  `level` int(2) NOT NULL COMMENT '编号',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `discount` float NOT NULL COMMENT '折扣',
  `img` varchar(255) NOT NULL COMMENT '会员图标',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARSET=utf8;


insert  into `customerlevel`(`id`,`level`,`name`,`discount`,`img`) values (1,0,'普通会员',1,'');
insert  into `customerlevel`(`id`,`level`,`name`,`discount`,`img`) values (2,1,'金牌会员',1,'');
insert  into `customerlevel`(`id`,`level`,`name`,`discount`,`img`) values (3,2,'铂金会员',0.98,'');
insert  into `customerlevel`(`id`,`level`,`name`,`discount`,`img`) values (4,3,'钻石会员',0.95,'');
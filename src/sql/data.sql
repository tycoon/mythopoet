INSERT INTO user (name,sex,ctime,utime) VALUES ('张三',0,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO user (name,sex,ctime,utime) VALUES ('李四',1,unix_timestamp(NOW()),unix_timestamp(NOW()));



INSERT INTO item (`name`,price,origin_price,discount,description,pic,ctime,utime) VALUES('碎花连衣裙',1000,1200,0.8,'测试专用001','/static/images/product.png',
                                                                                    unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item (`name`,price,origin_price,discount,description,pic,ctime,utime) VALUES('白色清纯衣衫',800,1000,0.8,'测试专用002','/static/images/product.png',
                                                                                    unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item (`name`,price,origin_price,discount,description,pic,ctime,utime) VALUES('制服一套',1500,1700,0.8,'测试专用003','/static/images/product.png',
                                                                                    unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item (`name`,price,origin_price,discount,description,pic,ctime,utime) VALUES('水手服',1900,2200,0.8,'测试专用003','/static/images/product.png',
                                                                                    unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item (`name`,price,origin_price,discount,description,pic,ctime,utime) VALUES('袜子',1200,1300,0.8,'测试专用003','/static/images/product.png',
                                                                                    unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO designer (name,initial,description,icon,pic,country,ctime,utime) VALUES ('Alex','A','潮流者','/static/images/designer3.png','/static/images/designer3.png','法国',unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO designer (name,initial,description,icon,pic,country,ctime,utime) VALUES ('Boy','B','舞者','/static/images/designer3.png','/static/images/designer3.png','阿拉伯',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO designer (name,initial,description,icon,pic,country,ctime,utime) VALUES ('HelloWorld','H','苦逼码农','/static/images/designer3.png','/static/images/designer3.png','陕西',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO designer (name,initial,description,icon,pic,country,ctime,utime) VALUES ('Hipop','H','猫猫系列','/static/images/designer3.png','/static/images/designer3.png','意大利',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO designer (name,initial,description,icon,pic,country,ctime,utime) VALUES ('XXXXX','X','煎饼果子来一套','/static/images/designer3.png','/static/images/designer3.png','朝鲜',
unix_timestamp(NOW()),
unix_timestamp(NOW()));


INSERT INTO item_designer(item_id,designer_id,ctime,utime) VALUES(1,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_designer(item_id,designer_id,ctime,utime) VALUES(2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_designer(item_id,designer_id,ctime,utime) VALUES(3,3,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_designer(item_id,designer_id,ctime,utime) VALUES(4,4,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_designer(item_id,designer_id,ctime,utime) VALUES(5,1,unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO category(name,pic,sort,ctime,utime) VALUES('手袋','../../static/images/category1.png',1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO category(name,pic,sort,ctime,utime) VALUES('美容','../../static/images/category2.png',2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO category(name,pic,sort,ctime,utime) VALUES('衣服','../../static/images/category1.png',3,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO category(name,pic,sort,ctime,utime) VALUES('鞋子','../../static/images/category1.png',4,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO category(name,pic,sort,ctime,utime) VALUES('裤子','../../static/images/category2.png',5,unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO item_cate(cate_id,item_id,ctime,utime) VALUES(1,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_cate(cate_id,item_id,ctime,utime) VALUES(2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_cate(cate_id,item_id,ctime,utime) VALUES(3,3,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_cate(cate_id,item_id,ctime,utime) VALUES(4,4,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_cate(cate_id,item_id,ctime,utime) VALUES(5,5,unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO tag (`name`,ctime,utime) VALUES ('男',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag (`name`,ctime,utime) VALUES ('女',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag (`name`,ctime,utime) VALUES ('性感',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag (`name`,ctime,utime) VALUES ('舒适',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag (`name`,ctime,utime) VALUES ('衣服',unix_timestamp(NOW()),unix_timestamp(NOW()));



INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (1,0,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (2,0,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (3,0,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (4,0,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (5,0,2,unix_timestamp(NOW()),unix_timestamp(NOW()));

INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (1,1,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (1,1,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (2,1,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (3,1,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (4,1,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (5,1,2,unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (1,2,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (2,2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (3,2,1,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (3,2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (4,2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag_relation (tid,type,tag_id,ctime,utime) VALUES (5,2,2,unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO item_prop(cate_id,content,ctime,utime) VALUES(1,'大小',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop(cate_id,content,ctime,utime) VALUES(2,'颜色',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop(cate_id,content,ctime,utime) VALUES(3,'型号',unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(1,'M',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(1,'L',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(1,'XL',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(2,'白色',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(2,'红色',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(3,'001',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO item_prop_value(prop_id,content,ctime,utime) VALUES(3,'002',unix_timestamp(NOW()),unix_timestamp(NOW()));


INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(1,'1:4:6',100,1200,1300,'/static/images/product.png','碎花连衣裙',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(1,'1:5:6',100,1300,1400,'/static/images/product.png','碎花连衣裙',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(1,'2:4:7',100,1400,1500,'/static/images/product.png','碎花连衣裙',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(2,'1:4:6',100,1500,1600,'/static/images/product.png','白色清纯衣衫',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(2,'2:4:6',100,1600,1700,'/static/images/product.png','白色清纯衣衫',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(3,'1:4:6',100,1700,1800,'/static/images/product.png','制服一套',
unix_timestamp(NOW()),
unix_timestamp(NOW()));
INSERT INTO item_sku(item_id,props,`count`,price,origin_price,pic,name,ctime,utime) VALUES(4,'1:4:6',100,1800,1900,'/static/images/product.png','水手服',
unix_timestamp(NOW()),
unix_timestamp(NOW()));


INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('极尽奢华','奢华，大方，展示高贵气质','/topic/demo','/static/images/topic/t1.jpg',
1,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('性感美丽','这么少的衣服能不性感吗','/topic/demo','/static/images/topic/t1.jpg',
1,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('屌丝气质','低调奢华上档次','/topic/demo','/static/images/topic/t1.jpg',
1,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('极尽奢华1','奢华，大方，展示高贵气质','/topic/demo','/static/images/topic/t1.jpg',
2,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('性感美丽1','这么少的衣服能不性感吗','/topic/demo','/static/images/topic/t1.jpg',
2,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('屌丝气质1','低调奢华上档次','/topic/demo','/static/images/topic/t1.jpg',
2,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('极尽奢华2','奢华，大方，展示高贵气质','/topic/demo','/static/images/topic/t1.jpg',
0,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('性感美丽2','这么少的衣服能不性感吗','/topic/demo','/static/images/topic/t1.jpg',
0,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO topic (name,description,url,pic,location,template_id,content,ctime,utime)VALUES('屌丝气质2','低调奢华上档次','/topic/demo','/static/images/topic/t1.jpg',
0,1,'',unix_timestamp(NOW()),unix_timestamp(NOW()));
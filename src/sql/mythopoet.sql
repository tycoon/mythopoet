CREATE DATABASE IF NOT EXISTS mythopoet;
use mythopoet;

CREATE TABLE IF NOT EXISTS user(
   id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT 'userId',
   name VARCHAR(100) NOT NULL COMMENT '用户名',
   sex TINYINT(1) NOT NULL DEFAULT '0' COMMENT '性别',
   phone VARCHAR(20) NOT NULL COMMENT '电话',
   password VARCHAR(50) NOT NULL DEFAULT '' COMMENT '密码',
   avatar VARCHAR(255) DEFAULT '' COMMENT '头像',
   source INT(4) COMMENT '登陆来源0=注册,1=QQ,2=微博',
   source_id BIGINT(11)  COMMENT '来源id',
   status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
   ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
   utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
   PRIMARY KEY (`id`),
   UNIQUE KEY (`phone`),
   UNIQUE KEY (`name`),
   UNIQUE KEY `idx_source` (`phone`,`source_id`,`source`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='用户信息';

CREATE TABLE IF NOT EXISTS wx_auth_user(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT 'userId',
  user_id BIGINT(11) default '0' COMMENT '用户ID',
  code VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'auth验证第一步需要的code',
  access_token VARCHAR(500) default '' COMMENT 'auth第二步请求的accessToken',
  expires_in BIGINT(11) default '0' COMMENT 'accesstoken过期时间',
  refresh_token VARCHAR(500) default '' COMMENT 'auth第二步请求的refreshToken',
  refresh_time INT(11) default '0' COMMENT 'accesstoken最近一次刷新时间',
  open_id VARCHAR(255) NOT NULL DEFAULT '' COMMENT '用户openid，用于支付',
  scope VARCHAR(255) DEFAULT '' COMMENT 'wx授权scope',
  union_id BIGINT(11)  default '0' COMMENT 'wx唯一id，只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段,获取用户信息',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  jsapi_token VARCHAR(500) default '' COMMENT '存储jsAPI用来获取ticket的token',
  jsapi_token_expires_in BIGINT(11) default '0' COMMENT 'jsapi_accesstoken过期时间',
  jsapi_token_refresh_time INT(11) default '0' COMMENT 'jsapi_accesstoken最近一次刷新时间',
  jsapi_ticket VARCHAR(500) default '' COMMENT '存储jsAPI的临时票据',
  jsapi_expires_in BIGINT(11) default '0' COMMENT 'jsapi_ticket过期时间',
  jsapi_refresh_time INT(11) default '0' COMMENT 'jsapi_ticket最近一次刷新时间',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='微信授权用户信息';

CREATE TABLE IF NOT EXISTS access_token(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  user_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  token VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'token',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='登陆token信息';

CREATE TABLE IF NOT EXISTS sms_verify(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  phone VARCHAR(100) NOT NULL DEFAULT '' COMMENT '手机号',
  captcha VARCHAR(10) NOT NULL DEFAULT '' COMMENT '短信验证码',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='手机号码短信验证码';

CREATE TABLE IF NOT EXISTS cnee_address(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  user_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  phone VARCHAR(30) NOT NULL DEFAULT '' COMMENT '收货人电话',
  province VARCHAR(30) NOT NULL DEFAULT '' COMMENT '省份',
  city VARCHAR(30) NOT NULL DEFAULT '' COMMENT '城市',
  county VARCHAR(30) NOT NULL DEFAULT '' COMMENT '县',
  `detail` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '描述',
  type INT(4) NOT NULL DEFAULT '0' COMMENT '类型0=普通 1=默认地址',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='收货地址信息';

CREATE TABLE IF NOT EXISTS item(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  view_id varchar(30) unique NOT NULL COMMENT '商品编号，用户查看的商品ID',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '商品名',
  price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  origin_price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品原价',
  discount FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品折扣',
  description VARCHAR(255) NOT NULL DEFAULT '' COMMENT '商品描述',
  size_desc VARCHAR(2000) DEFAULT '' COMMENT '该商品尺码描述',
  pic VARCHAR(800) NOT NULL DEFAULT '' COMMENT '图片',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品信息';

CREATE TABLE IF NOT EXISTS brand(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '品牌名称',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品品牌';

CREATE TABLE IF NOT EXISTS `item_brand` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `item_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `brand_id` bigint(11) NOT NULL DEFAULT '0' COMMENT '品牌ID',
  `status` int(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  `ctime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `utime` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_item` (`item_id`),
  KEY `idx_brand` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='品牌商品对应关系';

CREATE TABLE IF NOT EXISTS tag(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '标签名',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE idx_name (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品标签';

CREATE TABLE IF NOT EXISTS tag_relation(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  tid BIGINT(11)  NOT NULL DEFAULT '0' COMMENT '商品/分类/设计师ID',
  type int(4) NOT NULL default '0' COMMENT '类型0=商品,1=分类,2=设计师',
  tag_id BIGINT(11)  NOT NULL DEFAULT '0' COMMENT '标签ID',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE idx_tag_item (`tag_id`,`tid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='标签关系表(商品,分类,设计师等)';

CREATE TABLE IF NOT EXISTS designer(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '姓名',
  initial CHAR(2) NOT NULL DEFAULT '' COMMENT '首字母',
  description VARCHAR(255) NOT NULL DEFAULT '' COMMENT '设计师描述',
  icon VARCHAR(255) NOT NULL DEFAULT '' COMMENT '设计师icon',
  pic VARCHAR(255) NOT NULL DEFAULT '' COMMENT '图片',
  country VARCHAR(50) NOT NULL DEFAULT '' COMMENT '国家',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='设计师信息';

CREATE TABLE IF NOT EXISTS item_designer(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  item_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  designer_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '设计师ID',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_item (`item_id`),
  UNIQUE KEY idx_designer_item(`designer_id`,`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='设计师商品对应关系';

CREATE TABLE IF NOT EXISTS category(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  parent_cate_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '父类别ID',
  name VARCHAR(100) NOT NULL DEFAULT '' COMMENT '分类名称',
  pic VARCHAR(255) NOT NULL DEFAULT '' COMMENT '分类图标',
  layer INT(4) NOT NULL DEFAULT '0' COMMENT '第几层分类',
  sort INT(4) NOT NULL DEFAULT '0' COMMENT '排序',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品分类';

CREATE TABLE IF NOT EXISTS item_cate(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  cate_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
  item_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY (cate_id,item_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品分类关系';

CREATE TABLE IF NOT EXISTS item_prop(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  parent_prop_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '父级属性ID',
  cate_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  content VARCHAR(100) NOT NULL DEFAULT '' COMMENT '属性内容',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_uk_prop(`cate_id`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品属性';

CREATE TABLE IF NOT EXISTS item_prop_value(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  prop_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '所属属性ID',
  content VARCHAR(100) NOT NULL DEFAULT '' COMMENT '值内容',
  content_alias VARCHAR(255) DEFAULT '' COMMENT '值内容提示或别称',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_uk_content(`prop_id`,`content`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品属性值';

CREATE TABLE IF NOT EXISTS item_sku(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  item_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '商品ID',
  name VARCHAR(100) NOT NULL default '' COMMENT '商品名称，必要的冗余字段',
  props VARCHAR(255) NOT NULL DEFAULT '' COMMENT '属性值vid:vid:vid',
  count INT(4) NOT NULL DEFAULT '0' COMMENT '数量',
  price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  origin_price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品原价',
  pic VARCHAR(800) NOT NULL DEFAULT '' COMMENT '图片',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_item_props(`item_id`,`props`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='商品库存';

-- content如果太少可以改为Text/Blob
CREATE TABLE IF NOT EXISTS topic(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  name VARCHAR(255) NOT NULL DEFAULT '' COMMENT '专题名称',
  description VARCHAR(255) NOT NULL DEFAULT '' COMMENT '描述',
  url varchar(255) default '' comment '链接地址',
  pic varchar(255) default '' comment '图片',
  location INT(4) NOT NULL DEFAULT '0' COMMENT '展位0=默认位置1=头条2=轮播',
  position INT(4) NOT NULL DEFAULT '0' COMMENT '头条的顺序，0即没顺序',
  template_id int(4)  NOT NULL COMMENT '模板ID,模板是html模板',
  content varchar(20000) NOT NULL DEFAULT '0' COMMENT '模板内容JSON串',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='精品专题';


CREATE TABLE IF NOT EXISTS cart_detail(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  user_id BIGINT(11)  NOT NULL COMMENT '用户',
  item_id BIGINT(11)  NOT NULL COMMENT '商品ID',
  count INT(4) NOT NULL DEFAULT '0' COMMENT '商品数量',
  cart_price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '加入购物车时商品价格',
  origin_price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品原价',
  props VARCHAR(255) NOT NULL DEFAULT '' COMMENT '选择的属性vid:vid',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_user_item (`user_id`,`item_id`,`props`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='购物车详情';

CREATE TABLE IF NOT EXISTS orders(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  view_id varchar(30) unique NOT NULL COMMENT '订单编号，用户查看',
  user_id BIGINT(11)  NOT NULL COMMENT '用户ID',
  total_count INT(4) NOT NULL DEFAULT '0' COMMENT '总商品数量',
  amount FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '总订单价格',
  freight FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  pay_type INT(4) NOT NULL DEFAULT '0' COMMENT '支付类型0=微信支付 5=银行转账',
  pay_status INT(4) NOT NULL DEFAULT '0' COMMENT '支付状态0=未支付,1=发起支付,4=支付完成,5=支付失败,6=取消支付',
  address_id BIGINT(11)  NOT NULL COMMENT '收货地址ID',
  remark VARCHAR(255) NOT NULL DEFAULT '' COMMENT '备注',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=提交订单,1=预出仓(已经提案或付款),2=已出仓,4=完成 9=取消',
  express INT(4) NOT NULL DEFAULT '0' COMMENT '快递公司类型',
  express_no BIGINT(64) NOT NULL DEFAULT '0' COMMENT '快递单号',
  dispatch_time INT(11) NOT NULL DEFAULT '0' COMMENT '派送时间',
  recept_time INT(11) NOT NULL DEFAULT '0' COMMENT '收货时间',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  pay_time INT(11) NOT NULL DEFAULT '0' COMMENT '支付时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY idx_user (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='订单';

CREATE TABLE IF NOT EXISTS order_detail(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  order_id BIGINT(11)  NOT NULL COMMENT '订单ID',
  item_id BIGINT(11)  NOT NULL COMMENT '商品ID',
  count INT(4) NOT NULL DEFAULT '0' COMMENT '商品数量',
  price FLOAT(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  props VARCHAR(255) NOT NULL DEFAULT '' COMMENT '选择的属性vid:vid',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=提交订单,1=预出仓(到付的已经提案或已付款),2=已出仓,4=完成 9=取消',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_order_item (`order_id`,`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='订单详情';

CREATE TABLE IF NOT EXISTS favorites(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  user_id BIGINT(11)  NOT NULL COMMENT '用户ID',
  type INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=商品 1=设计师',
  tid BIGINT(11) NOT NULL DEFAULT '0' COMMENT '收藏的商品或设计师ID',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_user (`user_id`,`tid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='收藏详情';

CREATE TABLE IF NOT EXISTS pay_record(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  order_id BIGINT(11)  NOT NULL COMMENT '订单ID',
  pay_task_id BIGINT(11) NOT NULL DEFAULT '0' COMMENT '支付任务id',
  pay_type INT(4) NOT NULL DEFAULT '0' COMMENT '支付类型',
  pay_status INT(4) NOT NULL DEFAULT '0' COMMENT '支付状态',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY idx_user (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='支付记录';

CREATE TABLE IF NOT EXISTS order_record(
  id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  order_id BIGINT(11)  NOT NULL COMMENT '订单ID',
  user_id BIGINT(11)  NOT NULL COMMENT '订单ID',
  pay_status INT(4) NOT NULL COMMENT '支付状态',
  detail varchar(800) COMMENT '订单详情',
  remark varchar(100) COMMENT '备注',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY idx_user (`user_id`),
  KEY idx_order (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='订单记录';

CREATE TABLE IF NOT EXISTS sys_user(
   id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT 'userId',
   name VARCHAR(100) NOT NULL COMMENT '用户名',
   sex TINYINT(1) NOT NULL DEFAULT '0' COMMENT '性别',
   phone VARCHAR(20) NOT NULL COMMENT '电话',
   password VARCHAR(50) NOT NULL DEFAULT '' COMMENT '密码',
   description VARCHAR(255) NOT NULL DEFAULT '' COMMENT '描述',
   status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
   ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
   utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='系统用户信息';

INSERT INTO tag (`name`,ctime,utime) VALUES ('男',unix_timestamp(NOW()),unix_timestamp(NOW()));
INSERT INTO tag (`name`,ctime,utime) VALUES ('女',unix_timestamp(NOW()),unix_timestamp(NOW()));

ALTER TABLE item ADD COLUMN `note` VARCHAR(100) DEFAULT '' NOT NULL COMMENT '编辑笔记' AFTER `description`;

ALTER TABLE orders  ADD COLUMN `discount_type` INT(4) DEFAULT '0' NOT NULL COMMENT '0:无优惠，1:会员折扣，2:优惠券' AFTER `status`;

ALTER TABLE orders ADD COLUMN `discount_amount` FLOAT(10,2) DEFAULT '0.00' NOT NULL COMMENT '折扣金额' AFTER `status`;

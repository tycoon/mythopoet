DROP TABLE IF EXISTS `ticket`;

CREATE TABLE `ticket` (
  `id` bigint(11) NOT NULL default '1' COMMENT '券ID',
  `pword` varchar(20) NOT NULL default '' COMMENT '密码',
  `buylimit` int(11) NOT NULL COMMENT '购买额度',
  `cost` int(11) NOT NULL COMMENT '券值',
  `expdate` int(11) NOT NULL COMMENT '有效期',
  `isuse` tinyint(1) NOT NULL default '0' COMMENT '0:未使用；1:已使用',
  `orderid` int(11) NOT NULL COMMENT '订单号',
  `ctime` int(11) default NULL COMMENT '创建时间',
  `utime` int(11) default NULL COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `access_log`;
CREATE TABLE `access_log` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `source` varchar(50) NOT NULL COMMENT '来源',
  `source_id` bigint(11) NOT NULL COMMENT '编号',
  `status` int(4) NOT NULL COMMENT '类型（0=浏览，1=下单）',
  `ctime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8  COMMENT='浏览日志表';


package com.mythopoet.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtils {
	 public final static void pressImage(String pressImg, String targetImg, int x, int y, float alpha) {      
	        try {      
	            File img = new File(targetImg);      
	            Image src = ImageIO.read(img);      
	            int wideth = src.getWidth(null);      
	            int height = src.getHeight(null);      
	            BufferedImage image = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);      
	            Graphics2D g = image.createGraphics();      
	            g.drawImage(src, 0, 0, wideth, height, null);      
	            // 水印文件      
	            Image src_biao = ImageIO.read(new File(pressImg));      
	            int wideth_biao = src_biao.getWidth(null);      
	            int height_biao = src_biao.getHeight(null);      
	            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));      
	            g.drawImage(src_biao, (wideth - wideth_biao) / 2, (height - height_biao) / 2, wideth_biao, height_biao, null);      
	            // 水印文件结束      
	            g.dispose();      
	            ImageIO.write((BufferedImage) image, "jpg", img);      
	        } catch (Exception e) {      
	            e.printStackTrace();      
	        }      
	    } 
	 
	 public static void pressText(String pressText, String targetImg, String fontName, int fontStyle, Color color, int fontSize, int x, int y, float alpha) {      
	        try {      
	            File img = new File(targetImg);      
	            Image src = ImageIO.read(img);      
	            int width = src.getWidth(null);      
	            int height = src.getHeight(null);      
	            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);      
	            Graphics2D g = image.createGraphics();      
	            g.drawImage(src, 0, 0, width, height, null);      
	            g.setColor(color);      
	            g.setFont(new Font(fontName, fontStyle, fontSize));      
	            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));      
	            g.drawString(pressText, (width - (getLength(pressText) * fontSize)) / 2 + x, (height - fontSize) / 2 + y);      
	            g.dispose();      
	            ImageIO.write((BufferedImage) image, "jpg", img);      
	        } catch (Exception e) {      
	            e.printStackTrace();      
	        }      
	    }      
	     
	    /**    
	     * 缩放    
	     *     
	     * @param filePath    
	     *            图片路径    
	     * @param height    
	     *            高度    
	     * @param width    
	     *            宽度    
	     * @param bb    
	     *            比例不对时是否需要补白    
	     */     
	    public static void resize(String filePath, int height, int width, boolean bb) {      
	        try {      
	            double ratio = 0; // 缩放比例      
	            File f = new File(filePath);      
	            BufferedImage bi = ImageIO.read(f);      
	            Image itemp = bi.getScaledInstance(bi.getWidth(), bi.getHeight(), bi.SCALE_SMOOTH);      
	                 
	                  
	                BufferedImage image = new BufferedImage(bi.getWidth(), bi.getHeight(),      
	                        BufferedImage.TYPE_INT_RGB);      
	                Graphics2D g = image.createGraphics();      
	                g.setColor(Color.white);      
	                g.fillRect(0, 0, width, height); 
	                
	                if(bi.getWidth()/ bi.getHeight()>1.1)
	                     
	                    g.drawImage(itemp, 0, (height - itemp.getHeight(null)) / 2, itemp.getWidth(null), itemp.getHeight(null), Color.white, null);      
	                else     
	                    g.drawImage(itemp, (width - itemp.getWidth(null)) / 2, 0, itemp.getWidth(null), itemp.getHeight(null), Color.white, null);      
	                g.dispose();      
	                itemp = image;      
	                  
	            ImageIO.write((BufferedImage) itemp, "jpg", f);      
	        } catch (IOException e) {      
	            e.printStackTrace();      
	        }      
	    }      
	     
	    public static void main(String[] args) throws IOException {         
	        resize("e:\\pic", 500, 500, true);      
	    }      
	     
	    public static int getLength(String text) {      
	        int length = 0;      
	        for (int i = 0; i < text.length(); i++) {      
	            if (new String(text.charAt(i) + "").getBytes().length > 1) {      
	                length += 2;      
	            } else {      
	                length += 1;      
	            }      
	        }      
	        return length / 2;      
	    }      
}

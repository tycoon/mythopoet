package com.mythopoet.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.penuel.mythopoet.service.CategoryService;
import com.penuel.mythopoet.service.DesignerService;
import com.penuel.mythopoet.service.ItemService;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-mythopoet.xml")

public class FreeMarkerHtmlTest {	
	@Resource
	ItemService itemService;
	@Resource
	CategoryService ategoryService;
	@Resource
	DesignerService esignerService;
	private Configuration freemarker_cfg = null;
	
	private String dir = "E:/01_dev/project/03_mythopoetweb/mythopoet-web/src/main/webapp/WEB-INF/views/module";
	
	@Test
	public void getNewItem(){
		List a = itemService.newList(0,0,10);
		List b = ategoryService.getByTagId(1, 0, 10);
		List c = ategoryService.getByTagId(0, 0, 10);
		System.err.println("新品男装数据 ： "+a.size());
		System.err.println("分类男装数据 ： "+b.size());
		System.err.println("分类女数据 ： "+b.size());
	}
	
	
	
	public void testFreemarker() {
		Configuration cfg = new Configuration();
		List discountList = itemService.discountList(0,3,1);
		 
		try {
			// 从哪里加载模板文件
			cfg.setDirectoryForTemplateLoading(new File(dir));
			
			// 定义模版的位置，从类路径中，相对于FreemarkerManager所在的路径加载模版
			// cfg.setTemplateLoader(new ClassTemplateLoader(FreemarkerManager.class, "templates"))

			// 设置对象包装器
			cfg.setObjectWrapper(new DefaultObjectWrapper());

			// 设置异常处理器
			cfg
					.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

			// 定义数据模型
			Map root = new HashMap();
			root.put("abc", "世界，你好");
			root.put("discountList", discountList);

			// 通过freemarker解释模板，首先需要获得Template对象
			Template template = cfg.getTemplate("navbarTemplete.ftl");

			// 定义模板解释完成之后的输出
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(dir+"/navbar.ftl")));

			
			try {
				// 解释模板
				template.process(root, out);
			} catch (TemplateException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}

package com.mythopoet.service;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.service.ItemService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-mythopoet.xml")
public class ItemServiceTest {
	@Resource
	ItemService itemService;
	
	@Test
	public void testSearch(){
		
		 List<Item> itemList = itemService.searchByName("测试", 0, 10);
		 
		 System.out.println(itemList.size());
	}
}

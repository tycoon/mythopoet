package com.penuel.mythopoet.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.penuel.mythopoet.dao.FlashSaleItemDAO;
import com.penuel.mythopoet.dao.ItemBrandDAO;
import com.penuel.mythopoet.dao.ItemDesignerDAO;
import com.penuel.mythopoet.dao.ItemPropDAO;
import com.penuel.mythopoet.dao.ItemPropValueDAO;
import com.penuel.mythopoet.dao.ItemSkuDAO;
import com.penuel.mythopoet.model.Designer;
import com.penuel.mythopoet.model.FlashSaleItem;
import com.penuel.mythopoet.model.ItemProp;
import com.penuel.mythopoet.model.ItemPropValue;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.modelView.FlashSaleItemView;
import com.penuel.mythopoet.modelView.ItemSkuView;
import com.penuel.mythopoet.modelView.ItemView;

@Service
public class FlashSaleItemService {

	@Autowired
	private FlashSaleItemDAO flashSaleItemDAO;
	
    @Autowired
    private ItemPropValueDAO itemPropValueDAO;
    
    @Autowired
    private ItemSkuDAO itemSkuDAO;
    
    @Autowired
    private ItemBrandDAO itemBrandDAO;
    
    @Autowired
    private ItemPropDAO itemPropDAO;
    
	@Autowired
	private ItemDesignerDAO itemDesignerDAO;

	public FlashSaleItem getByItemId(Long itemId) {
		return flashSaleItemDAO.getByItemId(itemId);
	}
	
	public List<FlashSaleItem> getList(String stime){
		return flashSaleItemDAO.getList(stime);
	}

	public FlashSaleItem getById(Long id) {
		return flashSaleItemDAO.getById(id);
	}
	
	public List<FlashSaleItem> getActivityList(long activityId){
		return flashSaleItemDAO.getActivityList(activityId);
	}
	
	public int addFlashSaleItem(FlashSaleItem flashSaleItem){
		return flashSaleItemDAO.addFlashSaleItem(flashSaleItem);
	}
	
	public int updateFlashSaleItem(FlashSaleItem flashSaleItem){
		return flashSaleItemDAO.updateFlashSaleItem(flashSaleItem);
	}
	
	public int deleteById(Long id){
		return flashSaleItemDAO.deleteById(id);
	}
	
	public int deleteByActivityId(Long activityId){
		return flashSaleItemDAO.deleteByActivityId(activityId);
	}
	
	public int countItems(Long activityId) {
		return flashSaleItemDAO.countItems(activityId);
	}
	
	public List<ItemView> selectItemsList( Long activityId,int offset,int limit){
		return flashSaleItemDAO.list(activityId, offset, limit);
	}

	public FlashSaleItem getByActivityId(long activityId, long itemId) {
		return flashSaleItemDAO.getByActivityId(activityId,itemId);
	}
	
	
	public void fillSkuInfo(FlashSaleItemView itemView) {

        List<ItemSku> itemSkuList = itemSkuDAO.getByItemId(itemView.getId());
        if ( CollectionUtils.isEmpty(itemSkuList) ) {
            return;
        }
        //sku
        int totalSkuCount = 0;
        List<ItemSkuView> itemSkuViewList = new ArrayList<ItemSkuView>();
        ItemSkuView itemSkuView = null;
        List<Long> propIdList = null;
        for ( ItemSku itemSku : itemSkuList ) {
            itemSkuView = new ItemSkuView();
            List<Long> propsValueIds = parserPropValueIdFromItemSku(itemSku);
            List<ItemPropValue> itemPropValueList = itemPropValueDAO.getByIds(propsValueIds);
            BeanUtils.copyProperties(itemSku, itemSkuView);
            itemSkuView.setItemPropValueList(itemPropValueList);
            itemSkuViewList.add(itemSkuView);
            propIdList = getPropIdListByItemPropValues(itemPropValueList);
            totalSkuCount +=itemSku.getCount();
        }
        itemView.setItemSkuViewList(itemSkuViewList);
        itemView.setTotalSkuCount(totalSkuCount);
        //props
        List<ItemProp> itemPropList = itemPropDAO.getByIds(propIdList);
        itemView.setItemPropList(itemPropList);
    }
	
    private List<Long> parserPropValueIdFromItemSku(ItemSku itemSku) {

        ArrayList<Long> arrayList = new ArrayList<Long>();
        String[] props = itemSku.getProps().split(":");
        for ( String p : props ) {
            arrayList.add(NumberUtils.toLong(p));
        }
        return arrayList;
    }
    
    
    private List<Long> getPropIdListByItemPropValues(List<ItemPropValue> itemPropValues) {

        Set<Long> propIdSet = new HashSet<Long>();
        for ( ItemPropValue itemPropValue : itemPropValues ) {
            propIdSet.add(itemPropValue.getPropId());
        }
        List<Long> list = new ArrayList<Long>();
        list.addAll(propIdSet);
        return list;

    }
    
    public void fillBrandInfo(FlashSaleItemView itemView) {
        String brandName = getBranNameByItemId(itemView.getId());
        itemView.setBrandName(brandName);
    }

    public String getBranNameByItemId(long itemId) {
        return itemBrandDAO.getBrandNameByItem(itemId);
    }
    
	public void fillDesignerInfo(FlashSaleItemView itemView){
		Designer designer = itemDesignerDAO.getDesignerByItem(itemView.getId());
		itemView.setDesingerId(designer.getId());
		itemView.setDesingerDescription(designer.getDescription());
	}

	public List<FlashSaleItem> getActivityListPage(long activityId, int offset,
			int limit) {
		return flashSaleItemDAO.getActivityListPage(activityId,offset,limit);
	}

}

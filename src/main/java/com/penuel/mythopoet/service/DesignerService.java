package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.DesignerDAO;
import com.penuel.mythopoet.model.Designer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DesignerService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/14 上午9:07
 * Desc:
 */

@Service
public class DesignerService {

    @Autowired
    private DesignerDAO designerDAO;

    public Designer getById(Long designerId) {
        return designerDAO.getById(designerId);
    }

    public List<Designer> list(int offset, int limit) {

        return designerDAO.list(offset,limit);
    }

    public List<Designer> getByTagId(int tagId, int offset, int limit) {

        return designerDAO.getByTagId(tagId,offset,limit);
    }

    public List<Designer> searchByName(String key) {
        return designerDAO.searchByName(key);
    }
}
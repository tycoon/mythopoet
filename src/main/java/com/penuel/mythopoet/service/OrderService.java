package com.penuel.mythopoet.service;

import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.dao.OrdersDAO;
import com.penuel.mythopoet.model.*;
import com.penuel.mythopoet.modelView.OrderDetailView;
import com.penuel.mythopoet.modelView.OrderView;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * OrderService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:42
 * Desc:
 */

@Service
public class OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrdersDAO orderDAO;
    @Autowired
    private CneeAddressService cneeAddressService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private OrderRecordService orderRecordService;
    @Autowired
    private ItemService itemService;

    public List<Orders> listByUserIdAndStatus(Long userId, Integer status, int offset, int limit) {
        List<Orders> ordersList = orderDAO.getByUserIdAndStatus(userId, status, offset, limit);
        //deleteOut24HWaitPayOrder(ordersList);
        return ordersList;
    }

    public Orders getById(Long userId, Long orderId) {

        return orderDAO.getById(orderId, userId);
    }
    
    public Orders getByViewId(Long userId, String viewId) {

        return orderDAO.getByViewIdAndUserId(viewId, userId);
    }
    
    public Orders getByViewId( String viewId) {

        return orderDAO.getByViewId(viewId);
    }

    public Orders addOrder(Long userId, ItemSku itemSku, int count, long addressId, String remark, int payType,double amount,int discountType,double discountAmount) {

        Orders order = new Orders();
        order.setUserId(userId);
        Item item = itemService.getById(itemSku.getItemId());
        if ( item == null ){
            return order;
        }
        order.setAmount(amount);
        order.setTotalCount(1);
        order.setPayType(payType);
        order.setRemark(remark);
        order.setDiscountType(discountType);
        order.setDiscountAmount(discountAmount);
        if ( addressId == 0 ) {
            CneeAddress address = cneeAddressService.getDefault(userId);
            if ( address != null ) {
                addressId = address.getId();
            }
        }
        order.setAddressId(addressId);
        String orderIdView = getOrderIdView();
        order.setViewId(orderIdView);
        int result = orderDAO.insert(order);
        if ( result > 0 ) {
            orderRecordService.record(order,"新增");
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setItemId(itemSku.getItemId());
            orderDetail.setProps(itemSku.getProps());
            orderDetail.setCount(count);
            orderDetail.setOrderId(order.getId());
            orderDetail.setPrice(item.getPrice());
            orderDetailService.insert(orderDetail);
        }
        return order;
    }

    public Orders batchAddOrder(Long userId, List<CartDetail> cartDetailList, long addressId, String remark, int payType,double amount,int discountType,double discountAmount) {

        if ( CollectionUtils.isEmpty(cartDetailList) ) {
            return null;
        }

        Orders order = new Orders();
        order.setRemark(remark);
        order.setPayType(payType);
        order.setTotalCount(cartDetailList.size());
        order.setDiscountType(discountType);
        order.setDiscountAmount(discountAmount);
        if ( addressId == 0 ) {
            CneeAddress cneeAddress = cneeAddressService.getDefault(userId);
            if ( cneeAddress != null ) {
                addressId = cneeAddressService.getDefault(userId).getId();
            }
        }
        order.setAddressId(addressId);
        order.setAmount(amount);
        order.setUserId(userId);
        String orderIdView = getOrderIdView();
        order.setViewId(orderIdView);
        int result = orderDAO.insert(order);
        if ( result <= 0 ) {
            return order;
        }

        orderRecordService.record(order,"新增");
        List<OrderDetail> orderDetailList = new ArrayList<OrderDetail>();
        OrderDetail orderDetail;
        for ( CartDetail cartDetail : cartDetailList ) {
            orderDetail = new OrderDetail();
            orderDetail.setOrderId(order.getId());
            orderDetail.setPrice(cartDetail.getCurrentPrice());
            orderDetail.setCount(cartDetail.getCount());
            orderDetail.setProps(cartDetail.getProps());
            orderDetail.setItemId(cartDetail.getItemId());
            orderDetailList.add(orderDetail);
        }
        orderDetailService.batchInsert(orderDetailList);
        return order;
    }

    private double calTotalAmount(List<CartDetail> cartDetails) {

        if ( CollectionUtils.isEmpty(cartDetails) ) {
            return 0;
        }
        double amount = 0;
        for ( CartDetail cartDetail : cartDetails ) {
            amount += cartDetail.getCount() * cartDetail.getCurrentPrice();
        }
        return amount;
    }

    public List<Orders> getWaitRecOrderList(Long userId, int offset, int limit) {

        List<Orders> ordersList = orderDAO.getWaitRecOrderList(userId, offset, limit);
        //deleteOut24HWaitPayOrder(ordersList);  暂时取消删除24小时以外未支付的订单
        return ordersList;
    }

    public List<Orders> getAllByUserId(Long userId, int offset, int limit) {

        List<Orders> ordersList = orderDAO.getAllByUserId(userId, offset, limit);
        //deleteOut24HWaitPayOrder(ordersList);
        return ordersList;
    }

    public int getWaitRecOrderCount(Long userId) {

        return orderDAO.getWaitRecOrderCount(userId);
    }

    public int getAllCountByUserId(Long userId) {

        return orderDAO.getAllCountByUserId(userId);
    }

    public int getCountByUserIdAndStatus(Long userId, Integer status) {
        return orderDAO.getCountByUserIdAndStatus(userId, status);
    }

    public int payOrder(Orders order) {

        int result =  orderDAO.payOrder(order);
        if ( result > 0 ){
            orderRecordService.record(order,"支付");
        }
        return result;
    }

    /**
     * 填充orderView信息
     *
     * @param order
     * @return
     */
    public OrderView buildOrderViewByOrder(Orders order) {

        OrderView orderView = new OrderView();
        BeanUtils.copyProperties(order, orderView);
        CneeAddress address = cneeAddressService.getById(order.getUserId(), order.getAddressId());
        orderView.setCneeAddress(address);
        List<OrderDetail> orderDetailList = orderDetailService.getByOrderId(order.getId());
        List<OrderDetailView> orderDetailViewList = new ArrayList<OrderDetailView>();
        for ( OrderDetail orderDetail : orderDetailList ) {
            orderDetailViewList.add(orderDetailService.decorateOrderDetail(orderDetail));
        }
        orderView.setOrderDetailViewList(orderDetailViewList);
        return orderView;
    }

    public long updateAddress(long userId, long orderId, long addressId) {
        Orders order = orderDAO.getById(orderId,userId);
        if (order == null){
            return 0;
        }
        if ( addressId == 0 ) {
            CneeAddress cneeAddress = cneeAddressService.getDefault(userId);
            if ( cneeAddress != null ) {
                addressId = cneeAddressService.getDefault(userId).getId();
            }
        }
        int result  = orderDAO.updateAddress(userId, orderId, addressId);
        if ( result > 0 ){
            order.setAddressId(addressId);
            orderRecordService.record(order,"更新");
        }
        return addressId;
    }

    public int updateRemark(long userId, long orderId, String remark) {
        return orderDAO.updateRemark(userId, orderId, remark);
    }

    public int deleteById(Long userId, Long orderId) {
        Orders orders = orderDAO.getById(orderId,userId);
        if ( null == orders ){
            return 0;
        }
        int result = orderDAO.deleteById(orderId, userId);
        if ( result > 0 ){
            orders.setStatus(PoetConstants.Order_VIEW_DELETE);
            orderRecordService.record(orders,"删除");
        }
        return result;
    }

    /**
     * 过滤超过24小时，却没有付款的订单，将之删除order.status=10,
     * 会导致列表不一定返回20个
     *
     * @param orderList
     * @return
     */
    public void deleteOut24HWaitPayOrder(List<Orders> orderList) {
        if ( CollectionUtils.isEmpty(orderList) ) {
            return;
        }
        Date now = new Date();
        for ( int i = orderList.size() - 1; i >= 0; i-- ) {
            Orders order = orderList.get(i);
            if ( now.getTime() - order.getCtime().getTime() > 24 * 60 * 60 * 1000 && order.getPayStatus() == 0
                    && order.getStatus() == PoetConstants.Order_VIEW_WAIT_PAY ) {
                int result = orderDAO.updateOrderStatus(order.getId(), order.getUserId(), PoetConstants.Order_VIEW_DELETE);
                if ( result > 0 ) {
                    order.setStatus(PoetConstants.Order_VIEW_DELETE);
                    orderRecordService.record(order, "删除超过24H");
                }
                orderList.remove(i);
            }
        }
    }

    public String getOrderIdView() {
        Calendar cal = Calendar.getInstance();
        Date today = DateUtils.truncate(cal.getTime(), Calendar.DATE);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = DateUtils.truncate(cal.getTime(), Calendar.DATE);
        long orderIndex = orderDAO.todayOrderIndex(today, tomorrow);
        String orderIdView = generatePrepayOrderId(orderIndex+1);
        LOGGER.info("PayService.todayOrderIndex : today=" + today + ",tomorrow=" + tomorrow + ",orderIndex=" + orderIndex + ",orderIdView=" + orderIdView);
        return orderIdView;
    }

    public static String generatePrepayOrderId(long orderIndex) {
        String today = DateFormatUtils.format(Calendar.getInstance(), "yyyyMMdd");
        String orderIndexStr = orderIndex + "";
        if ( orderIndexStr.length() >= 7 ) {
            return today + orderIndexStr;
        }
        int size = 7 - orderIndexStr.length();
        String result = "";
        for ( int i = 0; i < size; i++ ) {
            result += "0";
        }
        return today + result + orderIndexStr;
    }

    public int updatePayType(long userId, long orderId, int payType) {
        Orders orders = orderDAO.getById(orderId,userId);
        if ( orders == null ){
            return 0;
        }
        int result = orderDAO.updatePayType(userId,orderId,payType);
        if ( result > 0 ){
            orders.setPayStatus(payType);
            orderRecordService.record(orders,"更新");
        }
        return result;
    }
}

package com.penuel.mythopoet.service;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.dao.ItemSkuDAO;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.OrderDetail;
import com.penuel.mythopoet.model.Orders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ItemSkuService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:09
 * Desc:
 */

@Service
public class ItemSkuService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemSkuService.class);

    @Autowired
    private ItemSkuDAO itemSkuDAO;
    @Autowired
    private OrderDetailService orderDetailService;

    public ItemSku getById(long skuId) {
        return itemSkuDAO.getById(skuId);
    }

    public ItemSku getAllSkuByItemAndProps(long itemId,String props) {
        return itemSkuDAO.getAllSkuByItemAndProps(itemId,props);
    }

    public ItemSku getValidSkuByItemAndProps(long itemId,String props) {
        return itemSkuDAO.getValidSkuByItemAndProps(itemId,props);
    }

    public int getSumSkuCountByItemId(long itemId) {

        Integer count =  itemSkuDAO.getSumSkuCountByItemId(itemId);
        return count == null?0:count;
    }

    public int reduceSku(Orders order) {
        List<OrderDetail> orderDetailList = orderDetailService.getByOrderId(order.getId());
        if ( null == orderDetailList ){
            return 0;
        }
        for (OrderDetail orderDetail : orderDetailList){
            ItemSku itemSku = itemSkuDAO.getValidSkuByItemAndProps(orderDetail.getItemId(),orderDetail.getProps());
            if ( itemSku == null || itemSku.getCount() < orderDetail.getCount() ){
                LOGGER.error("ItemSkuService.reduceSku Error: orderCount > skuCount, sku="+ JSONObject.toJSONString(itemSku));
            }
           itemSkuDAO.reduceSkuCount(itemSku.getId(),orderDetail.getCount());
        }
        return 0;
    }
}
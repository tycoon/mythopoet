package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.TagDAO;
import com.penuel.mythopoet.model.Tag;
import com.penuel.mythopoet.model.TagRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TagService
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/6/7 下午9:31
 * Desc:
 */

@Service
public class TagService {

    @Autowired
    private TagDAO tagDAO;

    public List<Tag> searchByKey(String key) {
        return tagDAO.searchByName(key);
    }

    public TagRelation getByTagAndItem(long tagId, long itemId) {
        return tagDAO.getByTagAndItem(tagId,itemId);
    }
}
package com.penuel.mythopoet.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.utils.Constants;
import com.penuel.mythopoet.utils.MD5Util;

@Service
public class AliPayService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AliPayService.class);
	/**
	 * 支付宝提供给商户的服务接入网关URL
	 */
	private static final String ALIPAY_GATEWAY = "https://mapi.alipay.com/gateway.do?";
	/**
     * 支付宝消息验证地址
     */
	private static final String HTTPS_VERIFY_URL = "https://mapi.alipay.com/gateway.do?service=notify_verify&";
	/**
	 * 页面跳转同步通知页面路径
	 */
	private static final String HSHS_RETRUN_URL = Constants.get("HOST_URL")+"/alicallback/recall";
	/**
	 * 服务器异步通知页面路径
	 */
	private static final String HSHS_NOTIFY_URL = Constants.get("HOST_URL")+"/alicallback/recalltwo";
	/*
	 * 商品展示URL
	 */
	private static final String HSHS_SHOW_URL = Constants.get("HOST_URL")+"/order/page";
	/**
	 * 签名类型
	 */
	private static final String sign_type = "MD5";

	/*
	 * 合作身份者ID，以2088开头由16位纯数字组成的字符串
	 */
	public static String partner = "2088811400131508";
	/*
	 * 收款支付宝账号，以2088开头由16位纯数字组成的字符串
	 */
	public static String seller_id = partner;
	/*
	 * 商户的私钥
	 */
	public static String security_key = "3a2j3dhsbzo8k35e6afap00jj5r1lfgo";

	/*
	 * 支付类型
	 */
	public static String payment_type = "1";

	/*
	 * 字符集
	 */
	public static String charset = "UTF-8";
	@Autowired
	private OrderService orderService;

	public String commitOrder(Long userId, Long orderId) {
		Orders o = orderService.getById(userId, orderId);
		// 1创建数据
		Map<String, String> datas = buildRequestData(o);
		// 2签名
		Map<String, String> datasWithSign = buildRequestSignData(datas);
		// 3形成表单
		List<String> keys = new ArrayList<String>(datasWithSign.keySet());
		StringBuffer sbHtml = new StringBuffer();

		sbHtml.append("<form id=\"alipaysubmit\" name=\"alipaysubmit\" action=\""
				+ ALIPAY_GATEWAY
				+ "_input_charset="
				+ charset
				+ "\" method=\"get\">");

		for (int i = 0; i < keys.size(); i++) {
			String name = (String) keys.get(i);
			String value = (String) datasWithSign.get(name);

			sbHtml.append("<input type=\"hidden\" name=\"" + name
					+ "\" value=\"" + value + "\"/>");
		}

		// submit按钮控件请不要含有name属性
		sbHtml.append("<input type=\"submit\" value=\"confirm\" style=\"display:none;\"></form>");
		sbHtml.append("<script>document.forms['alipaysubmit'].submit();</script>");

		return sbHtml.toString();
	}

	private Map<String, String> buildRequestData(Orders o) {
		// 商户订单号 商户网站订单系统中唯一订单号，必填
		String out_trade_no = o.getViewId();

		// 订单名称
		String subject = this.toCharEncoding("绘事后素购物-订单编号：" + out_trade_no);
		// 必填

		// 付款金额
		String total_fee = String.valueOf(o.getFreight() + o.getAmount());
		// 必填

		// 商品展示地址
		String show_url = HSHS_SHOW_URL;
		// 必填，需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

	

		// 把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "alipay.wap.create.direct.pay.by.user");
		sParaTemp.put("partner", partner);
		sParaTemp.put("seller_id", seller_id);
		sParaTemp.put("_input_charset", charset);
		sParaTemp.put("payment_type", payment_type);
		sParaTemp.put("notify_url", HSHS_NOTIFY_URL);
		sParaTemp.put("return_url", HSHS_RETRUN_URL);
		sParaTemp.put("out_trade_no", out_trade_no);
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		sParaTemp.put("show_url", show_url);
		return sParaTemp;

	}

	private Map<String, String> buildRequestSignData(
			Map<String, String> sParaTemp) {

		Map<String, String> sPara = paraFilter(sParaTemp);
		// 生成签名结果
		String sign = buildRequestSign(sPara);

		// 签名结果与签名方式加入请求提交参数组中
		sPara.put("sign", sign);
		sPara.put("sign_type", sign_type);

		return sPara;
	}

	private String buildRequestSign(Map<String, String> sParaTemp) {
		String strurl = createLinkString(sParaTemp); // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
		String content = strurl + security_key;
		String sign = "";
		if (sign_type.equals("MD5")) {
			sign = MD5Util.md5(content);
		}
		return sign;

	}

	private String createLinkString(Map<String, String> params) {

		List<String> keys = new ArrayList<String>(params.keySet());
		Collections.sort(keys);
		String prestr = "";
		for (int i = 0; i < keys.size(); i++) {
			String key = keys.get(i);
			String value = params.get(key);
			if (i == keys.size() - 1) {// 拼接时，不包括最后一个&字符
				prestr = prestr + key + "=" + value;
			} else {
				prestr = prestr + key + "=" + value + "&";
			}
		}
		return prestr;
	}

	/**
	 * 除去数组中的空值和签名参数
	 * 
	 * @param sArray 签名参数组
	 * @return 去掉空值与签名参数后的新签名参数组
	 */
	public static Map<String, String> paraFilter(Map<String, String> sArray) {

		Map<String, String> result = new HashMap<String, String>();

		if (sArray == null || sArray.size() <= 0) {
			return result;
		}

		for (String key : sArray.keySet()) {
			String value = sArray.get(key);
			if (value == null || value.equals("")
					|| key.equalsIgnoreCase("sign")
					|| key.equalsIgnoreCase("sign_type")) {
				continue;
			}
			result.put(key, value);
		}

		return result;
	}
	/**
	 * 转换字符集
	 * @param content 转换字符集的内容
	 * @return
	 */
	private String toCharEncoding(String content){
		
		String tmp ="";
		try {
			tmp = new String(content.getBytes(),
					charset);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("AliPayService.toCharEncoding ERROR,content ="+content);
		}
		return tmp;
		
	}
	
	/**
     * 根据反馈回来的信息，生成签名结果
     * @param Params 通知返回来的参数数组
     * @param sign 比对的签名结果
     * @return 生成的签名结果
     */
	private boolean getSignVeryfy(Map<String, String> Params, String sign) {
    	//过滤空值、sign与sign_type参数
    	Map<String, String> sParaNew = paraFilter(Params);
        //获取待签名字符串
        String preSignStr = createLinkString(sParaNew);
        //获得签名验证结果
        String content = preSignStr + security_key;
		String _sign = "";
        boolean isSign = false;
        if(sign_type.equals("MD5")){
        	_sign = MD5Util.md5(content);
        }
        return _sign.equals(sign);
    }
	
	/**
     * 验证消息是否是支付宝发出的合法消息
     * @param params 通知返回来的参数数组
     * @return 验证结果
     */
	 public boolean verify(Map<String, String> params) {

	        //判断responsetTxt是否为true，isSign是否为true
	        //responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
	        //isSign不是true，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
	    	String responseTxt = "true";
			if(params.get("notify_id") != null) {
				String notify_id = params.get("notify_id");
				responseTxt = verifyResponse(notify_id);
			}
		    String sign = "";
		    if(params.get("sign") != null) {sign = params.get("sign");}
		    boolean isSign = getSignVeryfy(params, sign);

	        //写日志记录（若要调试，请取消下面两行注释）
	        //String sWord = "responseTxt=" + responseTxt + "\n isSign=" + isSign + "\n 返回回来的参数：" + AlipayCore.createLinkString(params);
		    //AlipayCore.logResult(sWord);

	        if (isSign && responseTxt.equals("true")) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	 
	 /**
	    * 获取远程服务器ATN结果,验证返回URL
	    * @param notify_id 通知校验ID
	    * @return 服务器ATN结果
	    * 验证结果集：
	    * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空 
	    * true 返回正确信息
	    * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
	    */
	    private String verifyResponse(String notify_id) {
	        //获取远程服务器ATN结果，验证是否是支付宝服务器发来的请求

	        String veryfy_url = HTTPS_VERIFY_URL + "partner=" + partner + "&notify_id=" + notify_id;

	        return checkUrl(veryfy_url);
	    }

	    /**
	    * 获取远程服务器ATN结果
	    * @param urlvalue 指定URL路径地址
	    * @return 服务器ATN结果
	    * 验证结果集：
	    * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空 
	    * true 返回正确信息
	    * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
	    */
	    private  String checkUrl(String urlvalue) {
	        String inputLine = "";

	        try {
	            URL url = new URL(urlvalue);
	            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection
	                .getInputStream()));
	            inputLine = in.readLine().toString();
	        } catch (Exception e) {
	            e.printStackTrace();
	            inputLine = "";
	        }

	        return inputLine;
	    }

}

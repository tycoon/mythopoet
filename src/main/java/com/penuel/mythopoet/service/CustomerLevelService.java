package com.penuel.mythopoet.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.CustomerLevelDAO;
import com.penuel.mythopoet.model.CustomerLevel;

import java.util.List;

/**
 * CartDetailService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:16
 * Desc:
 */

@Service
public class CustomerLevelService {

    @Autowired
    private CustomerLevelDAO customerLevelDAO;
    
    public int addCustomerLevel(CustomerLevel customerLevel){
    	return customerLevelDAO.addCustomerLevel(customerLevel);
    }
    
    public CustomerLevel getByLevel(int level){
    	return customerLevelDAO.getByLevel(level);
    }
    
   
}
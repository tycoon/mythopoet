package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.OrderRecordDAO;
import com.penuel.mythopoet.model.OrderRecord;
import com.penuel.mythopoet.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * OrderRecordService
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/6/8 下午10:55
 * Desc:
 */

@Service
public class OrderRecordService {

    @Autowired
    private OrderRecordDAO orderRecordDAO;

    public void record(Orders orders, String remark) {
        OrderRecord orderRecord = new OrderRecord();
        orderRecord.setOrderId(orders.getId());
        orderRecord.setPayStatus(orders.getPayStatus());
        orderRecord.setStatus(orders.getStatus());
        orderRecord.setUserId(orders.getUserId());
        orderRecord.setDetail(OrderRecord.buildDetailByOrder(orders));
        orderRecord.setRemark(remark);
        orderRecordDAO.create(orderRecord);
    }

}
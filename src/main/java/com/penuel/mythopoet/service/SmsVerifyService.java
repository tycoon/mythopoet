package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.SmsVerifyDAO;
import com.penuel.mythopoet.model.SmsVerify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SmsVerifyService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/2 下午10:58
 * Desc:
 */

@Service
public class SmsVerifyService {

    @Autowired
    private SmsVerifyDAO smsVerifyDAO;

    public int creat(String phone, String captcha) {
        return smsVerifyDAO.insertOrUpdate(phone,captcha);
    }

    public SmsVerify getByPhone(String phone) {
        return smsVerifyDAO.getByPhone(phone);
    }
}
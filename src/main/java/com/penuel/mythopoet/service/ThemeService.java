package com.penuel.mythopoet.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.ThemeDAO;
import com.penuel.mythopoet.model.Theme;




@Service
public class ThemeService {

	@Autowired
	private ThemeDAO themeDAO;

    public int addTheme(Theme theme){
    	return themeDAO.addTheme(theme);
    }
    
    public int updateTheme(Theme theme){
    	return themeDAO.updateTheme(theme);
    }
     
    public Theme getById(Long id){
    	return themeDAO.getById(id);
    }
    
    public List<Theme> getList(){
    	return themeDAO.getList();
    }
    
    public int deleteById(long id){
    	return themeDAO.deleteById(id);
    }
}

package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.CartDetailDAO;
import com.penuel.mythopoet.dao.CustomerDAO;
import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.Customer;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemSku;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CartDetailService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:16
 * Desc:
 */

@Service
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;
    
    public int addCustomer(Customer customer){
    	return customerDAO.addCustomer(customer);
    }
    
    public int updateCustomer(Customer customer){
    	return customerDAO.updateCustomer(customer);
    }
    
    public Customer getByUserId(Long userId){
    	return customerDAO.getByUserId(userId);
    }
    
    public int updateLevel(int level,long id){
    	return customerDAO.updateLevel(level, id);
    }
   
}
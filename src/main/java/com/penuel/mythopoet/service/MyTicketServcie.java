package com.penuel.mythopoet.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.MyTicketDAO;
import com.penuel.mythopoet.model.MyTicket;
import com.penuel.mythopoet.model.Ticket;


@Service
public class MyTicketServcie {

	@Autowired
	private MyTicketDAO myTicketDAO;
	
	public Ticket getActivityTicket(){
		return myTicketDAO.getActivityTicket();
	}
	
	public int addMyTicket(MyTicket myTicket){
		return myTicketDAO.addMyTicket(myTicket);
	}
	
	public List<Ticket> getMyTicket(long userId){
		return myTicketDAO.getMyTicket(userId);
	}
	
	public int giveTicket(long userId){
		Ticket ticket = this.getActivityTicket();
		if(ticket == null){
			return 0;
		}
		MyTicket myTicket = new MyTicket();
		myTicket.setTicketId(ticket.getId());
		myTicket.setUserId(userId);
		return this.addMyTicket(myTicket);
	}
	

}

package com.penuel.mythopoet.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.dao.WxAuthDAO;
import com.penuel.mythopoet.model.WxAuthUser;
import com.penuel.mythopoet.utils.Constants;
import com.penuel.mythopoet.utils.HttpClientUtil;
import com.penuel.mythopoet.utils.WxPayUtil;

@Service
public class WxAuthWithoutUserService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WxAuthWithoutUserService.class);

	/** 获取code url */
	public static final String CODE_URL = WxAuthService.CODE_URL;
	public static final String CODE_RECALL_URL = Constants.get("HOST_URL")
			+ "/authWithoutLogin/wxcode";// TODO 上线更改
	/** 获取 accesstoken url */
	public static final String ACCESS_TOKEN_URL = WxAuthService.ACCESS_TOKEN_URL;
	/** 获取 openid url */
	public static final String REFRESH_ACCESS_TOKEN_URL = WxAuthService.REFRESH_ACCESS_TOKEN_URL;
	/** 获取 jsapi_ticket url */
	public static final String REFRESH_JSAPI_TICKET_URL = WxAuthService.REFRESH_JSAPI_TICKET_URL;
	/**
	 * 获取 jsapi_accesstoken url
	 * http://mp.weixin.qq.com/wiki/15/54ce45d8d30b6bf6758f68d2e95bc627.html
	 */
	public static final String REFRESH_JSAPI_ACCESSTOKEN_URL = WxAuthService.REFRESH_JSAPI_ACCESSTOKEN_URL;

	@Autowired
	private WxAuthDAO wxAuthDAO;

	public int insert(WxAuthUser wxAuthUser) {
		return wxAuthDAO.insert(wxAuthUser);

	}

	public WxAuthUser getByOpenId(String openId) {

		return wxAuthDAO.getByOpenId(openId);

	}

	/**
	 * @param
	 * @return { { "access_token":"ACCESS_TOKEN", "expires_in":7200,
	 *         "refresh_token":"REFRESH_TOKEN", "openid":"OPENID",
	 *         "scope":"SCOPE", "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL" }
	 */
	public WxAuthUser getAccessToken(String code, Long userId) {
		String accessTokenUrl = genrateAccessTokenUrl(code);
		String content = HttpClientUtil.sendGetRequest(accessTokenUrl, "UTF-8");
		LOGGER.info("WxAuthService.getAccessToken content = " + content);
		JSONObject obj = JSONObject.parseObject(content);
		WxAuthUser wxAuthUser = this.getByOpenId(obj.getString("openid"));
		if (wxAuthUser == null) {
			wxAuthUser = new WxAuthUser();
			wxAuthUser.setCode(code);
			wxAuthUser.setUserId(userId);
			wxAuthUser.setAccessToken(obj.getString("access_token"));
			wxAuthUser.setExpiresIn(obj.getLongValue("expires_in"));
			wxAuthUser.setRefreshToken(obj.getString("refresh_token"));
			wxAuthUser.setOpenId(obj.getString("openid"));
			wxAuthUser.setScope(obj.getString("scope"));
			wxAuthUser.setUnionId(obj.getLongValue("unionid"));
			this.insert(wxAuthUser);

		}else{
			wxAuthUser.setCode(code);
			wxAuthUser.setUserId(userId);
			wxAuthUser.setAccessToken(obj.getString("access_token"));
			wxAuthUser.setExpiresIn(obj.getLongValue("expires_in"));
			wxAuthUser.setRefreshToken(obj.getString("refresh_token"));
			wxAuthUser.setOpenId(obj.getString("openid"));
			wxAuthUser.setScope(obj.getString("scope"));
			wxAuthUser.setUnionId(obj.getLongValue("unionid"));
			wxAuthDAO.updateAccessToken(wxAuthUser);
		}
		return wxAuthUser;

	}

	/**
	 * @param wxAuthUser
	 * @return { "access_token":"ACCESS_TOKEN", "expires_in":7200,
	 *         "refresh_token":"REFRESH_TOKEN", "openid":"OPENID",
	 *         "scope":"SCOPE" }
	 */
	public int refreshAccessToken(WxAuthUser wxAuthUser) {

		String accessTokenUrl = refreshAccessTokenUrl(wxAuthUser
				.getRefreshToken());
		String content = HttpClientUtil.sendGetRequest(accessTokenUrl, "UTF-8");
		JSONObject obj = JSONObject.parseObject(content);
		int errcode = obj.getIntValue("errcode");
		LOGGER.info("WxAuthService.refreshAccessToken content = " + content
				+ ",errcode=" + errcode);
		if (errcode == 40001 || errcode == 41001) {// invalid credential,
													// access_token is invalid
													// or not latest
			getAccessToken(wxAuthUser.getCode(), wxAuthUser.getUserId());
		}
		wxAuthUser.setAccessToken(obj.getString("access_token"));
		wxAuthUser.setExpiresIn(obj.getLongValue("expires_in"));
		wxAuthUser.setRefreshToken(obj.getString("refresh_token"));
		wxAuthUser.setOpenId(obj.getString("openid"));
		wxAuthUser.setScope(obj.getString("scope"));
		return wxAuthDAO.updateAccessToken(wxAuthUser);
	}

	public int refreshJsapiAccessToken(WxAuthUser wxAuthUser) {
		String content = HttpClientUtil.sendGetSSLRequest(
				getRefreshJsapiAccesstokenUrl(), "UTF-8");
		LOGGER.info("WxAuthService.refreshJsapiAccessToken content = "
				+ content + ",authUser=" + JSONObject.toJSONString(wxAuthUser));
		JSONObject obj = JSONObject.parseObject(content);
		int errcode = obj.getIntValue("errcode");
		if (errcode == 0) {// OK
			String access_token = obj.getString("access_token");
			long expires_in = obj.getLongValue("expires_in");
			wxAuthUser.setJsapiToken(access_token);
			wxAuthUser.setJsapiTokenExpiresIn(expires_in);
			return wxAuthDAO.updateJsapiToken(wxAuthUser);
		} else {
			return 0;
		}
	}

	public int refreshJsapiTicket(WxAuthUser wxAuthUser) {
		String jsapiTicketUrl = refreshJsapiTicket(wxAuthUser.getJsapiToken());
		String content = HttpClientUtil.sendGetRequest(jsapiTicketUrl, "UTF-8");
		LOGGER.info("WxAuthService.refreshJsapiTicket content = " + content);
		JSONObject obj = JSONObject.parseObject(content);
		int errcode = obj.getIntValue("errcode");
		String errmsg = obj.getString("errmsg");
		if (errcode == 40001 || errcode == 41001 || errcode == 42001) {
			refreshJsapiAccessToken(wxAuthUser);
			jsapiTicketUrl = refreshJsapiTicket(wxAuthUser.getJsapiToken());
			content = HttpClientUtil.sendGetRequest(jsapiTicketUrl, "UTF-8");
			obj = JSONObject.parseObject(content);
			errcode = obj.getIntValue("errcode");
		}
		if (errcode != 0) {
			LOGGER.error("WxAuthService.refreshJsapiTicket Error: errcode="
					+ errcode + ",errmsg=" + errmsg);
			return 0;
		}
		wxAuthUser.setJsapiTicket(obj.getString("ticket"));
		wxAuthUser.setJsapiExpiresIn(obj.getLongValue("expires_in"));
		return wxAuthDAO.updateJsapiTicket(wxAuthUser);
	}

	public SortedMap<Object, Object> fillJsapiTicketParam(String jsapiTicket,
			String url) {

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
		parameters.put("noncestr", PayService.genNonceStr());
		parameters.put("jsapi_ticket", jsapiTicket);
		long timestamp = System.currentTimeMillis() / 1000;
		parameters.put("timestamp", timestamp);
		parameters.put("url", url);
		String sign = WxPayUtil.createSha1Sign(parameters);
		parameters.put("sign", sign);
		parameters.put("appId", Constants.get("APP_ID"));
		return parameters;
	}

	private String genrateCodeUrl(String state) {

		try {
			String redirect_url = URLEncoder.encode(CODE_RECALL_URL, "UTF-8");
			return String.format(CODE_URL, Constants.get("APP_ID"),
					redirect_url, "code", "snsapi_base", state);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("WxAuthService.genrateCodeUrl Error:", e);
		}
		return null;
	}

	private String genrateAccessTokenUrl(String code) {

		return String.format(ACCESS_TOKEN_URL, Constants.get("APP_ID"),
				Constants.get("APP_AUTH_SECRET"), code, "authorization_code");
	}

	/**
	 * @param accessToken
	 * @return
	 */
	private String refreshAccessTokenUrl(String accessToken) {

		return String.format(REFRESH_ACCESS_TOKEN_URL, Constants.get("APP_ID"),
				"refresh_token", accessToken);
	}

	private String refreshJsapiTicket(String accessToken) {

		return String.format(REFRESH_JSAPI_TICKET_URL, accessToken);
	}

	public String authorizeURL(Long userId,String currentUrl) {
		StringBuffer state = new StringBuffer();
		//state.append(userId).append("@").append(currentUrl);
		state.append("");
		return genrateCodeUrl(state.toString());
	}

	public String getRefreshJsapiAccesstokenUrl() {
		return String.format(REFRESH_JSAPI_ACCESSTOKEN_URL,
				Constants.get("APP_ID"), Constants.get("APP_AUTH_SECRET"));
	}

	public int clearCodeByOpenId(String openId) {
		return wxAuthDAO.clearCodeByOpenId(openId);
	}
}
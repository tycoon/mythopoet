package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.CartDetailDAO;
import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemSku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CartDetailService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:16
 * Desc:
 */

@Service
public class CartDetailService {

    @Autowired
    private CartDetailDAO cartDetailDAO;
    @Autowired
    private ItemService itemService;

    public List<CartDetail> getByUserId(Long userId,int offset,int limit) {

        return cartDetailDAO.getByUserId(userId,offset,limit);
    }

    public int addItem(Long userId, ItemSku itemSku,int count) {
        CartDetail cartDetail = cartDetailDAO.getByUserAndItemAndProps(userId,itemSku.getItemId(),itemSku.getProps());
        Item item = itemService.getById(itemSku.getItemId());
        if ( cartDetail == null ){
            cartDetail = new CartDetail();
            cartDetail.setUserId(userId);
            cartDetail.setCount(count);
            cartDetail.setItemId(itemSku.getItemId());
            cartDetail.setOriginPrice(item.getOriginPrice());
            cartDetail.setCartPrice(item.getPrice());
            cartDetail.setProps(itemSku.getProps());
            return cartDetailDAO.insert(cartDetail);
        }
        if ( cartDetail.getStatus() == 0 ){//购物车已存在正常的商品
            cartDetail.setCount(cartDetail.getCount()+count);
        }else{
            cartDetail.setCount(count);

        }
        cartDetail.setCartPrice(item.getPrice());
        cartDetail.setOriginPrice(item.getOriginPrice());
        cartDetail.setStatus(0);
        return cartDetailDAO.update(cartDetail);
    }

    public CartDetail getById(long cartDetailId,long userId) {

        return cartDetailDAO.getById(cartDetailId,userId);
    }

    public int update(CartDetail cartDetail) {
        if ( cartDetail == null ) {
            return 0;
        }
        if ( cartDetail.getCount() <= 0 ){
            return cartDetailDAO.deleteById(cartDetail.getId());
        }else{
            return cartDetailDAO.update(cartDetail);
        }
    }

    public int cleanCart(Long userId) {

        return cartDetailDAO.deleteByUserId(userId);
    }

    public int deleteByIds(List<Long> cartDetailIdList) {

        return cartDetailDAO.deleteByIds(cartDetailIdList);
    }
    
    public int getItemAmount(long userId,long itemId){
    	return cartDetailDAO.getItemAmount(userId,itemId);
    }
}
package com.penuel.mythopoet.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.ActivityDAO;
import com.penuel.mythopoet.model.Activity;




@Service
public class ActivityService {

	@Autowired
	private ActivityDAO activityDAO;

    public int addActivity(Activity activity){
    	if(activity.getState() == 0){
    		activityDAO.updateActivityStateByThemeId(activity.getThemeId());
    	}
	    return activityDAO.addActivity(activity);
    }
	    
	    
    public int updateActivity(Activity activity){
    	if(activity.getState() == 0){
    		activityDAO.updateActivityStateByThemeId(activity.getThemeId());
    	}
    	return activityDAO.updateActivity(activity);
    }
	    
	   
	public Activity getById(Long id){
		return activityDAO.getById(id);
	}
	     
	public List<Activity> getByThemeId(Long themeId){
		return activityDAO.getByThemeId(themeId);
	}

	public Activity getDefaltActivity(Long themeId){
		return activityDAO.getDefaltActivity(themeId);
	}


	public int toActivityPrice(long activityId) {
		return activityDAO.toActivityPrice(activityId);
	}
    
	public int toItemPrice(long activityId) {
		return activityDAO.toItemPrice(activityId);
	}


	public int startActivityByWeight(long themeId, int weight) {
		return activityDAO.startActivityByWeight(themeId,weight);
	}
	
	public int deleteById(Long id){
		return activityDAO.deleteById(id);
	}
	
	public int deleteByThemeId(Long themeId){
		return activityDAO.deleteByThemeId(themeId);
	}
}

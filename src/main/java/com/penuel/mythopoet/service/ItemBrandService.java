package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.ItemBrandDAO;
import com.penuel.mythopoet.model.ItemBrand;
import com.penuel.mythopoet.modelView.ItemView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ItemBrandService
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/5/30 下午11:26
 * Desc:
 */

@Service
public class ItemBrandService {

    @Autowired
    private ItemBrandDAO itemBrandDAO;

    public void fillBrandInfo(ItemView itemView) {
        String brandName = getBranNameByItemId(itemView.getId());
        itemView.setBrandName(brandName);
    }

    public String getBranNameByItemId(long itemId) {
        return itemBrandDAO.getBrandNameByItem(itemId);
    }
}
package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.CategoryDAO;
import com.penuel.mythopoet.model.Category;
import com.penuel.mythopoet.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * CategoryService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/16 上午8:48
 * Desc:
 */

@Service
public class CategoryService {


    @Autowired
    private CategoryDAO categoryDAO;

    public List<Category> list(int offset, int pageSize) {
        return categoryDAO.list(offset,pageSize);
    }

    public List<Item> getItemsByCateId(Long categoryId,int offset,int limit) {

        return categoryDAO.getItemsByCateId(categoryId,offset,limit);
    }

    public List<Category> getByTagId(int tagId, int offset, int limit) {

        return categoryDAO.getByTagId(tagId,offset,limit);
    }

}
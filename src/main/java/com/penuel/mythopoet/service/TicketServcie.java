package com.penuel.mythopoet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.TicketDAO;
import com.penuel.mythopoet.model.Ticket;

@Service
public class TicketServcie {

	@Autowired
	private TicketDAO ticketDAO;

	/**
	 * 通过券密码获取券信息
	 * 
	 * @param pword
	 *            券的数字密码
	 * @return
	 */
	public Ticket getByPword(String pword) {

		return ticketDAO.getByPword(pword);

	}

	/**
	 * 券更新为已经被使用  微信
	 * 
	 * @param orderId
	 *            订单编号
	 * @param pword
	 *            券的数字密码
	 * @return
	 */

	public int haveUsed(Long orderId, String pword) {
		return ticketDAO.haveUsed(orderId, pword);
	}

	/**
	 * 券更新为已经被使用  支付宝
	 * 
	 * @param orderId
	 *            订单编号
	 * @param pword
	 *            券的数字密码
	 * @return
	 */

	/*public int haveUsed(Long orderId) {
		return ticketDAO.haveUsed(orderId);
	}*/

	/**
	 * 讲券与订单号建立关联    支付宝
	 * 
	 * @param orderId
	 * @param pword
	 * @return
	 */
	public int haveOrderId(Long orderId, String pword) {
		return ticketDAO.haveOrderId(orderId, pword);
	}

}

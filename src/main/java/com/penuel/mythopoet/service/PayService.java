package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.OrdersDAO;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.model.PayResult;
import com.penuel.mythopoet.utils.Constants;
import com.penuel.mythopoet.utils.WxPayUtil;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 * PayService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/22 上午7:34
 * Desc:
 */

@Service
public class PayService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayService.class);

    /** 发起预支付请求 */
    public static final String PRE_PAY_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    /** 接收微信支付异步通知回调地址 */
    public static final String PAY_RECALL_URL = Constants.get("HOST_URL")+"/pay/recall/";
    /** 关闭支付订单1:商户订单支付失败需要生成新单号重新发起支付2:系统下单后，用户支付超时，系统退出不再受理，避免用户继续 */
    public static final String PAY_CLOSE_ORDER = "https://api.mch.weixin.qq.com/pay/closeorder";
    /** 主动查询订单状态，完成流程 */
    public static final String PAY_ORDER_QUERY = "https://api.mch.weixin.qq.com/pay/orderquery";

    //公众账号APPID
    public static final String APP_ID = Constants.get("APP_ID");   /// wx51473b3f12948341  wx35bdf35ad4d75772;
    //43b9bb299fc87abe7c66747761bd4165
    //5a020c23542d8b4bb21b1f8f16615fd9
    public static final String APP_SECRET = Constants.get("APP_SECRET");//商户号的API安全->密钥  // 
    public static final String APP_AUTH_SECRET = Constants.get("APP_AUTH_SECRET");//公众号的密钥
    //商户号
    public static final String MCH_ID = Constants.get("MCH_ID");
    //device_info = "WEB" 设备号

    public static String genNonceStr() {

        return UUID.randomUUID().toString().substring(0, 20);
    }

    public PayResult prePay(Orders order, String userIp, String openId) {

        SortedMap<Object, Object> param = fillPrePayParam(order, userIp, openId);
        String xmlParam = WxPayUtil.getRequestParamXml(param);
        String result = WxPayUtil.httpsRequest(PRE_PAY_URL, "POST", xmlParam);
        LOGGER.info("PayService.prePay = " + result + ",xmlParam=" + xmlParam);
        Map<String, String> map = WxPayUtil.doXMLParse(result);
        return parseMap4PayResult(map, 0);
    }

    public PayResult orderQuery(Orders order) {

        SortedMap<Object, Object> param = fillOrderQueryParam(order);
        String xmlParam = WxPayUtil.getRequestParamXml(param);
        String result = WxPayUtil.httpsRequest(PAY_ORDER_QUERY, "POST", xmlParam);
        LOGGER.info("PayService.orderQuery = " + result + ",xmlParam=" + xmlParam);
        Map<String, String> map = WxPayUtil.doXMLParse(result);
        return parseMap4PayResult(map, 1);
    }

    /**
     * 解析微信返回的xml数据
     *
     * @param map
     * @param type 0=prepay 1=orderquery
     * @return
     */
    private PayResult parseMap4PayResult(Map<String, String> map, int type) {

        PayResult payResult = new PayResult();
        payResult.setReturn_code(map.get("return_code"));
        payResult.setReturn_msg(map.get("return_msg"));
        if ( !"SUCCESS".equals(payResult.getReturn_code()) ) {
            return payResult;
        }
        payResult.setRequestSuccess(true);
        payResult.setAppid(map.get("appid"));
        payResult.setMch_id(map.get("mch_id"));
        payResult.setDevice_info(map.get("device_info"));
        payResult.setNonce_str(map.get("nonce_str"));
        payResult.setSign(map.get("sign"));
        payResult.setResult_code(map.get("result_code"));
        payResult.setErr_code(map.get("err_code"));
        payResult.setErr_code_des(map.get("err_code_des"));
        if ( !"SUCCESS".equals(payResult.getResult_code()) ) {
            return payResult;
        }
        payResult.setTradeSuccess(true);
        payResult.setTrade_type(map.get("trade_type"));
        payResult.setPrepay_id(map.get("prepay_id"));
        payResult.setCode_url(map.get("code_url"));
        if ( type == 1 ){
            payResult.setIs_subscribe(map.get("is_subscribe"));
            payResult.setTrade_state(map.get("trade_state"));
            payResult.setTransaction_id(map.get("transaction_id"));
            payResult.setOut_trade_no(map.get("out_trade_no"));
            payResult.setTrade_state_desc(map.get("trade_state_desc"));
            payResult.setAttach(map.get("attach"));
        }
        return payResult;
    }

    private SortedMap<Object, Object> fillPrePayParam(Orders order, String userIp, String openId) {

        SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
        parameters.put("appid", Constants.get("APP_ID"));
        parameters.put("mch_id", Constants.get("MCH_ID"));
        parameters.put("device_info", "WEB");
        parameters.put("nonce_str", genNonceStr());
        parameters.put("body", "绘事后素-订单编号"+order.getViewId());
        parameters.put("detail", order.getId());
        parameters.put("out_trade_no", order.getViewId()); //String(32)
        parameters.put("total_fee", getIntAmountByDouble(order.getAmount()));//注意，金额只能为整数
        parameters.put("spbill_create_ip", userIp);
        parameters.put("notify_url", PAY_RECALL_URL);
        parameters.put("trade_type", "JSAPI");
        parameters.put("openid", openId);
        String sign = WxPayUtil.createSign(parameters);
        parameters.put("sign", sign);
        return parameters;
    }

    private SortedMap<Object, Object> fillOrderQueryParam(Orders order) {

        SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
        parameters.put("appid", Constants.get("APP_ID"));
        parameters.put("mch_id", Constants.get("MCH_ID"));
        parameters.put("device_info", "WEB");
        parameters.put("nonce_str", genNonceStr());
        parameters.put("out_trade_no", order.getViewId());
        String sign = WxPayUtil.createSign(parameters);
        parameters.put("sign", sign);
        return parameters;
    }

    public SortedMap<Object, Object> fillPayParam4JS(String prepay_id) {

        SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
        parameters.put("appId", Constants.get("APP_ID"));
        parameters.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000));
        parameters.put("nonceStr", genNonceStr());
        parameters.put("package", "prepay_id=" + prepay_id);
        parameters.put("signType", "MD5");
        String sign = WxPayUtil.createSign(parameters);
        parameters.put("paySign", sign);
        return parameters;
    }

    /**
     * 转换为微信的支付金额，单位分；即把.00去掉，并*100
     *
     * @param amount
     * @return
     */
    public static int getIntAmountByDouble(double amount) {

        amount = amount * 100;
        return (int) amount;
    }

    public void paySuccess(HttpServletRequest request, HttpServletResponse response) throws Exception {

        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ( (len = inStream.read(buffer)) != -1 ) {
            outSteam.write(buffer, 0, len);
        }
        LOGGER.info("~~~~~~~~~~~~~~~~付款成功~~~~~~~~~");
        outSteam.close();
        inStream.close();
        String result = new String(outSteam.toByteArray(), "utf-8");//获取微信调用我们notify_url的返回信息
        Map<String, String> map = WxPayUtil.doXMLParse(result);
        for ( Object keyValue : map.keySet() ) {
            System.out.println(keyValue + "=" + map.get(keyValue));
        }
        if ( map.get("result_code").toString().equalsIgnoreCase("SUCCESS") ) {
            //TODO 对数据库的操作
            response.getWriter().write(setXML("SUCCESS", ""));   //告诉微信服务器，我收到信息了，不要在调用回调action了
            LOGGER.info("-------------" + setXML("SUCCESS", ""));
        }
    }

    public static String setXML(String return_code, String return_msg) {

        return "<xml><return_code><![CDATA[" + return_code
                + "]]></return_code><return_msg><![CDATA[" + return_msg
                + "]]></return_msg></xml>";
    }

}
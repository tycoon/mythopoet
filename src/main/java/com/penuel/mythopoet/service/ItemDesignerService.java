package com.penuel.mythopoet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.ItemDesignerDAO;
import com.penuel.mythopoet.model.Designer;
import com.penuel.mythopoet.modelView.ItemView;


@Service
public class ItemDesignerService {
	@Autowired
	private ItemDesignerDAO itemDesignerDAO;

	public void fillDescriptionInfo(ItemView itemView) {
		String description = getDescriptionByItem(itemView.getId());
		itemView.setDesingerDescription(description);
	}

	private String getDescriptionByItem(long itemId) {
		return itemDesignerDAO.getDescriptionByItem(itemId);
	}
	
	public void fillDesignerInfo(ItemView itemView){
		Designer designer = itemDesignerDAO.getDesignerByItem(itemView.getId());
		itemView.setDesingerId(designer.getId());
		itemView.setDesingerDescription(designer.getDescription());
	}
}

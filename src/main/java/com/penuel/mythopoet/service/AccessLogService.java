package com.penuel.mythopoet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.AccessLogDAO;
import com.penuel.mythopoet.model.AccessLog;



/**
 * ItemService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/14 上午7:57
 * Desc:
 */

@Service
public class AccessLogService {

    @Autowired
    private AccessLogDAO accessLogDao;

    public int insertAccessLog(AccessLog accessLog){
    	return accessLogDao.insert(accessLog);
    }
}
package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.ItemPropValueDAO;
import com.penuel.mythopoet.model.ItemPropValue;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * ItemPropValueService Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/29 上午8:39
 * Desc:
 */

@Service
public class ItemPropValueService {

    @Autowired
    private ItemPropValueDAO itemPropValueDAO;

    public List<ItemPropValue> getByIds(List<Long> idList) {

        return itemPropValueDAO.getByIds(idList);
    }

    public String getPropValueByIdStr(String propsIdStr){
        List<Long> idList = new ArrayList<Long>();
        String[] propsIds= propsIdStr.split(":");
        for ( String s : propsIds ){
            idList.add(NumberUtils.toLong(s));
        }
        return splicePropValue(getByIds(idList));
    }

    public String splicePropValue(List<ItemPropValue> itemPropValues) {

        StringBuffer sb = new StringBuffer();
        for ( int i = 0; i < itemPropValues.size(); i++ ) {
            if ( i != 0 ){
                sb.append(" ");
            }
            sb.append(itemPropValues.get(i).getContent());
        }
        return sb.toString();
    }

}

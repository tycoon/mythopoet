package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.UserDAO;
import com.penuel.mythopoet.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * UserService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/28 上午8:21
 * Desc:
 */

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public User getUser(Long userId) {

        return userDAO.get(userId);
    }

    public int regist(User user) {

        return userDAO.insert(user);
    }

    public User getUserByPhone(String phone) {

        return userDAO.getByPhone(phone);
    }

    public User getUserByName(String name) {
        return userDAO.getByName(name);
    }


    public int updatePwd(long id, String password) {
        return userDAO.updatePwd(id,password);
    }
}
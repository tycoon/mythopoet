package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.DesignerDAO;
import com.penuel.mythopoet.dao.FavoritesDAO;
import com.penuel.mythopoet.dao.ItemDAO;
import com.penuel.mythopoet.model.*;
import com.penuel.mythopoet.modelView.FavoritesView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * FavoriteService Created with mythopoet. User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午9:00 Desc:
 */

@Service
public class FavoriteService {

	@Autowired
	private FavoritesDAO favoritesDAO;
	@Autowired
	private ItemDAO itemDAO;
	@Autowired
	private ItemSkuService itemSkuService;
	@Autowired
	private DesignerDAO designerDAO;
	@Autowired
	private ItemBrandService itemBrandService;

	public List<Favorites> listByUserId(Long userId, int type, int offset,
			int limit) {
		return favoritesDAO.getByUserId(userId, type, offset, limit);
	}

	public int add(Favorites favorites) {

		return favoritesDAO.insert(favorites);
	}

	public Favorites getByUserIdAndTidAndType(Long userId, Long tid, int type) {

		return favoritesDAO.getByUserIdAndTidAndType(userId, tid, type);
	}

	public int deleteById(Long favoriteId, Long userId) {

		return favoritesDAO.deleteById(favoriteId, userId);
	}

	public Favorites getById(Long favoriteId, Long userId) {

		return favoritesDAO.getById(favoriteId, userId);
	}

	public List<FavoritesView> fillItemInfoByFavorites(
			List<Favorites> favoritesList) {
		if (CollectionUtils.isEmpty(favoritesList)) {
			return null;
		}
		List<FavoritesView> favoritesViewList = new ArrayList<FavoritesView>();
		FavoritesView favoritesView;
		Item item;
		for (Favorites favorites : favoritesList) {// TODO 优化
			item = itemDAO.getById(favorites.getTid());
			if (item != null) {
				favoritesView = new FavoritesView();
				int count = itemSkuService.getSumSkuCountByItemId(favorites
						.getTid());
				favoritesView.setSkuCount(count);
				BeanUtils.copyProperties(favorites, favoritesView);
				String name = itemBrandService
						.getBranNameByItemId(item.getId());
				favoritesView.setItemBrandName(name);
				favoritesView.setItemPrice(item.getPrice());
				favoritesView.setName(item.getName());
				favoritesView.setPic(item.getPic());
				favoritesViewList.add(favoritesView);
			}
		}
		return favoritesViewList;
	}

	public List<FavoritesView> fillDesignerInfoByFavorites(
			List<Favorites> favoritesList) {
		if (CollectionUtils.isEmpty(favoritesList)) {
			return null;
		}
		List<FavoritesView> favoritesViewList = new ArrayList<FavoritesView>();
		FavoritesView favoritesView;
		Designer designer;
		for (Favorites favorites : favoritesList) {// TODO 优化
			designer = designerDAO.getById(favorites.getTid());
			if (designer == null) {
				continue;
			}
			favoritesView = new FavoritesView();
			BeanUtils.copyProperties(favorites, favoritesView);
			favoritesView.setName(designer.getName());
			favoritesView.setPic(designer.getPic());
			favoritesView.setIcon(designer.getIcon());
			favoritesView.setDesingerCountry(designer.getCountry());
			favoritesViewList.add(favoritesView);
		}
		return favoritesViewList;
	}

	public Favorites getByTidAndUserId(Long tid, int type, Long userId) {

		return favoritesDAO.getByTidAndUserId(tid, type, userId);
	}
}
package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.CneeAddressDAO;
import com.penuel.mythopoet.model.CneeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AddressService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/18 上午10:31
 * Desc:
 */
@Service
public class CneeAddressService {

    @Autowired
    private CneeAddressDAO addressDAO;

    public List<CneeAddress> list(Long userId, int offset, int limit) {

        return addressDAO.listByUserId(userId, offset, limit);
    }

    public CneeAddress getById(Long userId, Long addressId) {

        return addressDAO.getById(userId, addressId);
    }

    public int setDefault(Long userId, Long addressId) {

        addressDAO.cancelDefault(userId);
        return addressDAO.setDefaultById(userId, addressId);
    }

    public int insertOrUpdate(CneeAddress address) {

        if ( address.getType() == 1 ) {
            addressDAO.cancelDefault(address.getUserId());
        }
        if ( address.getId() > 0 ) {
            return addressDAO.update(address);
        }
        return addressDAO.insert(address);
    }

    public int deleteById(Long userId, Long addressId) {

        return addressDAO.deleteById(userId, addressId);
    }

    public CneeAddress getDefault(long userId) {

        return addressDAO.getDefaultByUserId(userId);
    }
}
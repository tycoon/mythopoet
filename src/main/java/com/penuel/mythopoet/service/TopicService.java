package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.TopicDAO;
import com.penuel.mythopoet.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TopicService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/9 上午12:16
 * Desc:
 */

@Service
public class TopicService {

    @Autowired
    private TopicDAO topicDAO;

    public List<Topic> list(int offset, int limit, int location) {
        return topicDAO.list(location, offset, limit);
    }

    public List<Topic> headlineList(int offset, int limit, int location) {
        return topicDAO.headlineList(location, offset, limit);
    }
}
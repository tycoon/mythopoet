package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.ItemPropValueDAO;
import com.penuel.mythopoet.dao.OrderDetailDAO;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemPropValue;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.OrderDetail;
import com.penuel.mythopoet.modelView.OrderDetailView;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * OrderDetailService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/14 上午8:27
 * Desc:
 */

@Service
public class OrderDetailService {

    @Autowired
    private OrderDetailDAO orderDetailDAO;
    @Autowired
    private ItemSkuService itemSkuService;
    @Autowired
    private ItemPropValueService itemPropValueService;
    @Autowired
    private ItemBrandService itemBrandService;
    @Autowired
    private ItemService itemService;

    public int insert(OrderDetail orderDetail) {

        return orderDetailDAO.insert(orderDetail);
    }

    public int batchInsert(List<OrderDetail> orderDetailList) {

        return orderDetailDAO.batchInsert(orderDetailList);
    }

    public List<OrderDetail> getByOrderId(long orderId) {

        return orderDetailDAO.getByOrderId(orderId);
    }

    public OrderDetailView decorateOrderDetail(OrderDetail orderDetail) {

        OrderDetailView orderDetailView = new OrderDetailView();
        BeanUtils.copyProperties(orderDetail, orderDetailView);
        Item item = itemService.getById(orderDetail.getItemId());
        if ( null != item ) {//产品更改逻辑，不取库存商品的这些信息，取原商品信息
            orderDetailView.setItemName(item.getName());
            orderDetailView.setPic(item.getPic());
        }
        ItemSku itemSku = itemSkuService.getAllSkuByItemAndProps(orderDetail.getItemId(), orderDetail.getProps());
        orderDetailView.setItemSku(itemSku);
        List<String> propStrList = Arrays.asList(orderDetail.getProps().split(":"));
        List<Long> propIdList = new ArrayList<Long>();
        for ( String propString : propStrList ) {
            propIdList.add(NumberUtils.toLong(propString));
        }
        List<ItemPropValue> itemPropValueList = itemPropValueService.getByIds(propIdList);
        orderDetailView.setItemPropValueList(itemPropValueList);
        String propValues = itemPropValueService.splicePropValue(itemPropValueList);
        orderDetailView.setPropsValue(propValues);
        orderDetailView.setBrandName(itemBrandService.getBranNameByItemId(orderDetail.getItemId()));
        return orderDetailView;
    }
}
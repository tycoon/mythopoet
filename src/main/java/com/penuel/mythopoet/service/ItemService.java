package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.ItemDAO;
import com.penuel.mythopoet.dao.ItemPropDAO;
import com.penuel.mythopoet.dao.ItemPropValueDAO;
import com.penuel.mythopoet.dao.ItemSkuDAO;
import com.penuel.mythopoet.model.*;
import com.penuel.mythopoet.modelView.ItemSkuView;
import com.penuel.mythopoet.modelView.ItemView;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * ItemService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/14 上午7:57
 * Desc:
 */

@Service
public class ItemService {

    @Autowired
    private ItemDAO itemDAO;

    @Autowired
    private ItemSkuDAO itemSkuDAO;

    @Autowired
    private ItemPropValueDAO itemPropValueDAO;

    @Autowired
    private ItemPropDAO itemPropDAO;

    public Item getById(Long id) {

        return itemDAO.getById(id);
    }

    public List<ItemView> hotList(int offset, int limit, long tagId) {

        return itemDAO.hotList(offset, limit, tagId);
    }

    public List<Item> newList(int offset, int limit, long tagId) {

        return itemDAO.newList(offset, limit, tagId);
    }

    public List<Item> discountList(int offset, int limit, long tagId) {

        return itemDAO.discountList(offset, limit, tagId);
    }

    public List<Item> getByItemIds(List<Long> ids) {

        return itemDAO.getByIds(ids);
    }

    public void fillSkuInfo(ItemView itemView) {

        List<ItemSku> itemSkuList = itemSkuDAO.getByItemId(itemView.getId());
        if ( CollectionUtils.isEmpty(itemSkuList) ) {
            return;
        }
        //sku
        int totalSkuCount = 0;
        List<ItemSkuView> itemSkuViewList = new ArrayList<ItemSkuView>();
        ItemSkuView itemSkuView = null;
        List<Long> propIdList = null;
        for ( ItemSku itemSku : itemSkuList ) {
            itemSkuView = new ItemSkuView();
            List<Long> propsValueIds = parserPropValueIdFromItemSku(itemSku);
            List<ItemPropValue> itemPropValueList = itemPropValueDAO.getByIds(propsValueIds);
            BeanUtils.copyProperties(itemSku, itemSkuView);
            itemSkuView.setItemPropValueList(itemPropValueList);
            itemSkuViewList.add(itemSkuView);
            propIdList = getPropIdListByItemPropValues(itemPropValueList);
            totalSkuCount +=itemSku.getCount();
        }
        itemView.setItemSkuViewList(itemSkuViewList);
        itemView.setTotalSkuCount(totalSkuCount);
        //props
        List<ItemProp> itemPropList = itemPropDAO.getByIds(propIdList);
        itemView.setItemPropList(itemPropList);
    }

    private List<Long> parserPropValueIdFromItemSku(ItemSku itemSku) {

        ArrayList<Long> arrayList = new ArrayList<Long>();
        String[] props = itemSku.getProps().split(":");
        for ( String p : props ) {
            arrayList.add(NumberUtils.toLong(p));
        }
        return arrayList;
    }

    private List<Long> getPropIdListByItemPropValues(List<ItemPropValue> itemPropValues) {

        Set<Long> propIdSet = new HashSet<Long>();
        for ( ItemPropValue itemPropValue : itemPropValues ) {
            propIdSet.add(itemPropValue.getPropId());
        }
        List<Long> list = new ArrayList<Long>();
        list.addAll(propIdSet);
        return list;

    }

    public List<Item> searchByName(String key,int offset,int limit) {

        return itemDAO.findByName(key,offset,limit);
    }

    public List<Item> getAllByDesignerId(Long designerId) {

        return itemDAO.getAllByDesingerId(designerId);
    }

    public List<Item> getByDesignerId(Long designerId,int offset,int limit) {

        return itemDAO.getByDesingerId(designerId,offset,limit);
    }

    public List<Item> getAllByTagId(long tagId) {
        return itemDAO.getAllByTagId(tagId);
    }

    public List<Item> getItemsByCateIdAndTagId(long categoryId, long tagId, int offset, int limit) {
        return itemDAO.getByCateIdAndTagId(categoryId,tagId,offset,limit);
    }

    public List<Item> getByDesignerIdAndTagId(long designerId, long tagId, int offset, int limit) {
        return itemDAO.getByDesignerIdAndTagId(designerId,tagId,offset,limit);
    }
    
    public List<Item> getSingleByTagId(long tagId,int offset,int limit){
    	return itemDAO.getSingleByTagId(tagId, offset, limit);
    }
}
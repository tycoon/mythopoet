package com.penuel.mythopoet.service;

import com.penuel.mythopoet.dao.AccessTokenDAO;
import com.penuel.mythopoet.model.AccessToken;
import com.penuel.mythopoet.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * AccessTokenService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/2 下午10:48
 * Desc:
 */

@Service
public class AccessTokenService {

    @Autowired
    private AccessTokenDAO accessTokenDAO;

    public AccessToken createByUser(User user) {
        AccessToken accessToken = new AccessToken();
        String uuid = UUID.randomUUID().toString();
        accessToken.setUserId(user.getId());
        accessToken.setToken(uuid);
        accessTokenDAO.insertOrUpdate(accessToken);
        return accessToken;
    }

    public AccessToken getByUserId(long userId) {

        return accessTokenDAO.getByUserId(userId);
    }
}
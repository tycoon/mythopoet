package com.penuel.mythopoet.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.penuel.mythopoet.dao.CustomerCardDAO;
import com.penuel.mythopoet.model.CustomerCard;

import java.util.List;

/**
 * CartDetailService Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:16
 * Desc:
 */

@Service
public class CustomerCardService {

    @Autowired
    private CustomerCardDAO customerCardDAO;
    
    public int addCustomerCard(CustomerCard customerCard){
    	return customerCardDAO.addCustomerCard(customerCard);
    }
    
    public int useCustomerCard(CustomerCard customerCard){
    	return customerCardDAO.useCustomerCard(customerCard);
    }
    
    public CustomerCard getUnUseCode(String code){
    	return customerCardDAO.getUnUseCode(code);
    }
    
   
}
package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * Order Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:42
 * Desc:
 */

public class Orders {

    private long id;
    private String viewId;
    private long userId;
    private int totalCount;
    private double amount;
    private double freight;
    private int payType;//payType 0=微信支付 5=银行转账
    private int payStatus;//0=未支付,1=支付中,4=支付完成,5=支付失败,6=取消支付,7=转入退款
    private long addressId;
    private String remark;
    private int express;//快递公司
    private long expressNo;
    private Date dispatchTime;
    private Date receptTime;
    private Date payTime;
    private int status; //状态0=提交订单,1=预出仓(已经提案或付款),2=已出仓,4=完成 9=取消 10=已删除，不显示在列表里
    private Date ctime;
    private Date utime;
    private int discountType; //折扣类型  1会员折扣  2代金券减免
    private double discountAmount; //折扣金额

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getFreight() {
        return freight;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getExpress() {
        return express;
    }

    public void setExpress(int express) {
        this.express = express;
    }

    public long getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(long expressNo) {
        this.expressNo = expressNo;
    }

    public Date getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(Date dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public Date getReceptTime() {
        return receptTime;
    }

    public void setReceptTime(Date receptTime) {
        this.receptTime = receptTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

	public int getDiscountType() {
		return discountType;
	}

	public void setDiscountType(int discountType) {
		this.discountType = discountType;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	
    
    
}
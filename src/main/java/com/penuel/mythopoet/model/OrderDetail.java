package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * OrderDetail Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:44
 * Desc:
 */

public class OrderDetail {

    private long id;
    private long orderId;
    private long itemId;
    private int count;
    private double price;
    private String props;
    private int status;//状态0=提交订单,1=预出仓(已经提案或付款),2=已出仓,4=完成 9=取消
    private Date ctime;
    private Date utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProps() {
        return props;
    }

    public void setProps(String props) {
        this.props = props;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }
}
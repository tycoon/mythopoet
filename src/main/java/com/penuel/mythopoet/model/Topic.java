package com.penuel.mythopoet.model;

import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.utils.FormatterUtil;

import java.util.Date;

/**
 * topic Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:28
 * Desc:
 */

public class Topic {

    private long id;
    private String name;
    private String description;
    private String url;
    private String pic;
    private int location;//展位0=默认位置1=头条2=轮播
    private int position;//头条的顺序 0=不排序
    private int status;
    private Date ctime;
    private Date utime;

    public static int LOCATION_VIEWPAGER=2;
    public static int LOCATION_HEAD=1;
    public static int LOCATION_DEFAULT=0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPic() {
        return FormatterUtil.decoratePicPrefix(pic);
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getPosition() {

        return position;
    }

    public void setPosition(int position) {

        this.position = position;
    }


}
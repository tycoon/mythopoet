package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * WxAuthUser Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/24 上午8:41
 * Desc: 微信授权用户信息
 */

public class WxAuthUser {

    private long id;
    private long userId;
    private String code;
    private String accessToken;
    private long expiresIn;
    private String refreshToken;
    private Date refreshTime;
    private String openId;
    private String scope;
    private long unionId;//unionId
    private String jsapiToken;
    private long jsapiTokenExpiresIn;
    private Date jsapiTokenRefreshTime;
    private String jsapiTicket;
    private long jsapiExpiresIn;
    private Date jsapiRefreshTime;
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public long getUserId() {

        return userId;
    }

    public void setUserId(long userId) {

        this.userId = userId;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getAccessToken() {

        return accessToken;
    }

    public void setAccessToken(String accessToken) {

        this.accessToken = accessToken;
    }

    public long getExpiresIn() {

        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {

        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {

        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {

        this.refreshToken = refreshToken;
    }

    public String getOpenId() {

        return openId;
    }

    public void setOpenId(String openId) {

        this.openId = openId;
    }

    public String getScope() {

        return scope;
    }

    public void setScope(String scope) {

        this.scope = scope;
    }

    public long getUnionId() {

        return unionId;
    }

    public void setUnionId(long unionId) {

        this.unionId = unionId;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    public Date getCtime() {

        return ctime;
    }

    public void setCtime(Date ctime) {

        this.ctime = ctime;
    }

    public Date getUtime() {

        return utime;
    }

    public void setUtime(Date utime) {

        this.utime = utime;
    }

    public Date getRefreshTime() {

        return refreshTime;
    }

    public void setRefreshTime(Date refreshTime) {

        this.refreshTime = refreshTime;
    }

    public String getJsapiTicket() {

        return jsapiTicket;
    }

    public void setJsapiTicket(String jsapiTicket) {

        this.jsapiTicket = jsapiTicket;
    }

    public long getJsapiExpiresIn() {

        return jsapiExpiresIn;
    }

    public void setJsapiExpiresIn(long jsapiExpiresIn) {

        this.jsapiExpiresIn = jsapiExpiresIn;
    }

    public Date getJsapiRefreshTime() {

        return jsapiRefreshTime;
    }

    public void setJsapiRefreshTime(Date jsapiRefreshTime) {

        this.jsapiRefreshTime = jsapiRefreshTime;
    }

    public String getJsapiToken() {

        return jsapiToken;
    }

    public void setJsapiToken(String jsapiToken) {

        this.jsapiToken = jsapiToken;
    }

    public long getJsapiTokenExpiresIn() {

        return jsapiTokenExpiresIn;
    }

    public void setJsapiTokenExpiresIn(long jsapiTokenExpiresIn) {

        this.jsapiTokenExpiresIn = jsapiTokenExpiresIn;
    }

    public Date getJsapiTokenRefreshTime() {

        return jsapiTokenRefreshTime;
    }

    public void setJsapiTokenRefreshTime(Date jsapiTokenRefreshTime) {

        this.jsapiTokenRefreshTime = jsapiTokenRefreshTime;
    }
}
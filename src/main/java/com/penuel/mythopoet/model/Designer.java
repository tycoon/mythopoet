package com.penuel.mythopoet.model;

import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.utils.FormatterUtil;

import java.util.Date;

/**
 * Designer Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:18
 * Desc: 设计师
 */

public class Designer {
    
    private long id;
    private String name;
    private String initial;
    private String description;
    private String icon;
    private String pic;
    private String country;
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getIcon() {
        return FormatterUtil.decoratePicPrefix(icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPic() {
        return FormatterUtil.decoratePicPrefix(pic);
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }
}
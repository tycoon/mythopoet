package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * Tag Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/29 下午11:21
 * Desc: 标签类
 */

public class Tag {
    private long id;
    private String name;
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    public Date getCtime() {

        return ctime;
    }

    public void setCtime(Date ctime) {

        this.ctime = ctime;
    }

    public Date getUtime() {

        return utime;
    }

    public void setUtime(Date utime) {

        this.utime = utime;
    }
}
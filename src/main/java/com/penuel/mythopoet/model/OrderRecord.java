package com.penuel.mythopoet.model;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * OrderRecord Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:53
 * Desc:
 *
 CREATE TABLE IF NOT EXISTS order_record(
 id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
 order_id BIGINT(11)  NOT NULL COMMENT '订单ID',
 user_id BIGINT(11)  NOT NULL COMMENT '订单ID',
 pay_status INT(4) NOT NULL COMMENT '支付状态',
 detail varchar(500) COMMENT '订单详情',
 status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
 ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
 utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
 PRIMARY KEY (`id`),
 KEY idx_user (`user_id`),
 KEY idx_order (`order_id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET = utf8 COMMENT='订单记录';
 */

public class OrderRecord {

    private long id;
    private long orderId;
    private long userId;
    private int payStatus;
    private int status;
    private String detail;
    private String remark;
    private Date ctime;
    private Date utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static String buildDetail(String key,String value){
        JSONObject obj = new JSONObject();
        obj.put(key,value);
        return obj.toString();
    }

    public static String buildDetailByOrder(Orders orders){
        JSONObject obj = new JSONObject();
        obj.put("id",orders.getId());
        obj.put("viewId",orders.getViewId());
        obj.put("addressId",orders.getAddressId());
        obj.put("amount",orders.getAmount());
        obj.put("payStatus",orders.getPayStatus());
        obj.put("payType",orders.getPayType());
        obj.put("status",orders.getStatus());
        obj.put("payTime",orders.getPayTime());
        obj.put("receptTime",orders.getReceptTime());
        obj.put("freight",orders.getFreight());
        obj.put("dispatchTime",orders.getDispatchTime());
        obj.put("expressNo",orders.getExpressNo());
        return obj.toString();
    }
}
package com.penuel.mythopoet.model;

import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.utils.FormatterUtil;

import java.util.Date;

/**
 * Category Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:22
 * Desc: 分类
 */

public class Category {

    private long id;
    private long parentCateId;
    private String name;
    private String pic;
    private int layer;
    private int sort;
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParentCateId() {
        return parentCateId;
    }

    public void setParentCateId(long parentCateId) {
        this.parentCateId = parentCateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return FormatterUtil.decoratePicPrefix(pic);
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }
}
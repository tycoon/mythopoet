package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * Brand
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/5/30 下午11:23
 * Desc:
 */

public class Brand {
    private long id;
    private String name;
    private Date ctime;
    private Date utime;
    private int status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
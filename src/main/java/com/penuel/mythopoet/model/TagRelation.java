package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * TagRelation Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/29 下午11:22
 * Desc:
 */

public class TagRelation {

    private long id;
    private long tagId;
    private long tid;
    private int type;//类型0=商品,1=分类,2=设计师
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public long getTagId() {

        return tagId;
    }

    public void setTagId(long tagId) {

        this.tagId = tagId;
    }

    public long getTid() {

        return tid;
    }

    public void setTid(long tid) {

        this.tid = tid;
    }

    public int getType() {

        return type;
    }

    public void setType(int type) {

        this.type = type;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    public Date getCtime() {

        return ctime;
    }

    public void setCtime(Date ctime) {

        this.ctime = ctime;
    }

    public Date getUtime() {

        return utime;
    }

    public void setUtime(Date utime) {

        this.utime = utime;
    }
}
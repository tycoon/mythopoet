package com.penuel.mythopoet.model;

import java.util.Date;

/**
 * Favorites Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午8:48
 * Desc:
 */

public class Favorites {

    private long id;
    private long userId;
    private int type;//0=商品 1=设计师
    private long tid;
    private int status;
    private Date ctime;
    private Date utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTid() {
        return tid;
    }

    public void setTid(long tid) {
        this.tid = tid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }
}
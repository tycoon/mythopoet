package com.penuel.mythopoet.model;

/**
 * PrePayResult Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/23 上午8:17
 * Desc: http://pay.weixin.qq.com/wiki/doc/api/index.php?chapter=9_1 预支付结果
 */

public class PayResult {
    private String return_code;//通信SUCCESS/FAIL，交易结果需要result_code来判断
    private String return_msg;//返回信息，如非空，为错误原因
    /**以下字段在return_code为SUCCESS的时候有返回*/
    private String appid;
    private String mch_id;
    private String device_info;
    private String nonce_str;
    private String sign;
    private String result_code;//SUCCESS/FAIL 业务结果
    private String err_code;//错误码
    private String err_code_des;//错误返回的信息描述
    /**以下字段在return_code 和result_code都为SUCCESS的时候有返回*/
    private String trade_type;//调用接口提交的交易类型，取值如下：JSAPI，NATIVE，APP
    private String prepay_id;//微信生成的预支付回话标识
    private String code_url;//trade_type为NATIVE是有返回，可将该参数值生成二维码展示出来进行扫码支付
    //
    private boolean requestSuccess = false;
    private boolean tradeSuccess = false;
    //orderquery参数
    private String is_subscribe;//Y/N 是否关注公众号
    private String trade_state;//交易结果--SUCCESS—支付成功;REFUND—转入退款;NOTPAY—未支付;CLOSED—已关闭;REVOKED—已撤销;USERPAYING--用户支付中;PAYERROR--支付失败
    private String transaction_id;//微信支付订单号
    private String out_trade_no;//商户订单号
    private String trade_state_desc;//对当前查询订单状态的描述和下一步操作的指引
    private String attach;//商家数据包，原样返回


    public String getReturn_code() {

        return return_code;
    }

    public void setReturn_code(String return_code) {

        this.return_code = return_code;
    }

    public String getReturn_msg() {

        return return_msg;
    }

    public void setReturn_msg(String return_msg) {

        this.return_msg = return_msg;
    }

    public String getAppid() {

        return appid;
    }

    public void setAppid(String appid) {

        this.appid = appid;
    }

    public String getMch_id() {

        return mch_id;
    }

    public void setMch_id(String mch_id) {

        this.mch_id = mch_id;
    }

    public String getDevice_info() {

        return device_info;
    }

    public void setDevice_info(String device_info) {

        this.device_info = device_info;
    }

    public String getNonce_str() {

        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {

        this.nonce_str = nonce_str;
    }

    public String getSign() {

        return sign;
    }

    public void setSign(String sign) {

        this.sign = sign;
    }

    public String getResult_code() {

        return result_code;
    }

    public void setResult_code(String result_code) {

        this.result_code = result_code;
    }

    public String getErr_code() {

        return err_code;
    }

    public void setErr_code(String err_code) {

        this.err_code = err_code;
    }

    public String getErr_code_des() {

        return err_code_des;
    }

    public void setErr_code_des(String err_code_des) {

        this.err_code_des = err_code_des;
    }

    public String getTrade_type() {

        return trade_type;
    }

    public void setTrade_type(String trade_type) {

        this.trade_type = trade_type;
    }

    public String getPrepay_id() {

        return prepay_id;
    }

    public void setPrepay_id(String prepay_id) {

        this.prepay_id = prepay_id;
    }

    public String getCode_url() {

        return code_url;
    }

    public void setCode_url(String code_url) {

        this.code_url = code_url;
    }

    public boolean isRequestSuccess() {

        return requestSuccess;
    }

    public void setRequestSuccess(boolean requestSuccess) {

        this.requestSuccess = requestSuccess;
    }

    public boolean isTradeSuccess() {

        return tradeSuccess;
    }

    public void setTradeSuccess(boolean tradeSuccess) {

        this.tradeSuccess = tradeSuccess;
    }

    public String getIs_subscribe() {

        return is_subscribe;
    }

    public void setIs_subscribe(String is_subscribe) {

        this.is_subscribe = is_subscribe;
    }

    public String getTrade_state() {

        return trade_state;
    }

    public void setTrade_state(String trade_state) {

        this.trade_state = trade_state;
    }

    public String getTransaction_id() {

        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {

        this.transaction_id = transaction_id;
    }

    public String getOut_trade_no() {

        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {

        this.out_trade_no = out_trade_no;
    }

    public String getTrade_state_desc() {

        return trade_state_desc;
    }

    public void setTrade_state_desc(String trade_state_desc) {

        this.trade_state_desc = trade_state_desc;
    }

    public String getAttach() {

        return attach;
    }

    public void setAttach(String attach) {

        this.attach = attach;
    }
}
package com.penuel.mythopoet.model;

import java.util.Date;

public class Theme {
	
	private long id;
	private String themeName;
	private String themeDescribe;
	private int state;
	private Date ctime;
	private Date utime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getThemeName() {
		return themeName;
	}
	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}
	public String getThemeDescribe() {
		return themeDescribe;
	}
	public void setThemeDescribe(String themeDescribe) {
		this.themeDescribe = themeDescribe;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public Date getUtime() {
		return utime;
	}
	public void setUtime(Date utime) {
		this.utime = utime;
	}
	
	
	
}

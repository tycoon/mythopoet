package com.penuel.mythopoet.constants;

/**
 * PoetConstants Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/17 上午8:44
 * Desc:
 */

public class PoetConstants {

    /**
     * 页面订单状态信息
     */
    public static final int Order_VIEW_ALL = -1;//全部
    public static final int Order_VIEW_WAIT_PAY = 0;//待支付
    public static final int Order_VIEW_WAIT_DEL = 1;//待发货
    public static final int Order_VIEW_WAIT_REC = 2;//待收货
    public static final int Order_VIEW_COMPLETE = 4;//已完成
    public static final int Order_VIEW_CANCEL= 9;//已取消
    public static final int Order_VIEW_DELETE= 10;//已删除

    /**支付类型*/
    public static final int PAY_STATUS_WX_WEB = 1;

    /**Request Constants*/
    public static final String COOKIE_TOKEN="token";
    public static final String COOKIE_USER_ID="userId";

    /**Ali ONS IMG PRE*/
//    public static final String ALI_IMG_PRE = "http://mythopoetms.oss-cn-beijing.aliyuncs.com";
    public static final String ALI_IMG_PRE = "http://7xjd0u.com2.z0.glb.qiniucdn.com";

}
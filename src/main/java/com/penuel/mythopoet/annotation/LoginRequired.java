package com.penuel.mythopoet.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * LoginRequired Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/5/3 下午2:45
 * Desc:
 */

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginRequired {
    ResponseResult result() default ResponseResult.HTML;
}

package com.penuel.mythopoet.annotation;

/**
 * ResponseResult Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/5/3 下午2:44
 * Desc:
 */

public enum ResponseResult {
    HTML, JSON
}

package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.Item;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static org.apache.ibatis.jdbc.SqlBuilder.*;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

/**
 * CartDetailDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/16 上午9:09
 * Desc:
 */

@Component
public interface CartDetailDAO {

    static final String INSERT_KEY = " id,user_id,item_id,count,cart_price,origin_price,props,status,ctime,utime ";
    static final String SELECT_KEY = " id,user_id,item_id,count,cart_price,origin_price,props,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " cart_detail ";

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where status =0 and user_id=#{userId} order by utime desc limit #{offset},#{limit}")
    List<CartDetail> getByUserId(@Param("userId") long userId,@Param("offset") int offset,@Param("limit") int limit);

    @Select(" select "+SELECT_KEY+" from "+TABEL+" where user_id=#{userId} and item_id=#{itemId} and props=#{props}")
    CartDetail getByUserAndItemAndProps(@Param("userId")Long userId,@Param("itemId") long itemId, @Param("props") String props);

    @Insert("insert into "+TABEL+" ( "+INSERT_KEY+") values (null,#{userId},#{itemId},#{count},#{cartPrice},#{originPrice},#{props},0,unix_timestamp(NOW()),unix_timestamp(NOW()))")
    @Options(useGeneratedKeys = true)
    int insert(CartDetail cartDetail);

    @Update("update "+TABEL+" set count=#{count},cart_price=#{cartPrice},origin_price=#{originPrice},status=#{status}," +
                    "utime=unix_timestamp(NOW()) where id=#{id}")
    int update(CartDetail cartDetail);

    @Select("select "+SELECT_KEY+" from "+TABEL+" where id=#{id} and user_id=#{userId}")
    CartDetail getById(@Param("id")long id,@Param("userId") long userId);

    @Update("update "+TABEL+" set count=0,status=1,utime=unix_timestamp(NOW()) where id=#{id} and status = 0 ")
    int deleteById(@Param("id")long id);


    @Update("update "+TABEL+" set count=0,status=1,utime=unix_timestamp(NOW()) where user_id=#{userId} and status = 0")
    int deleteByUserId(@Param("userId") Long userId);

    @UpdateProvider(type = SqlProvider.class, method = "deleteByIds")
    int deleteByIds(@Param("idList") List<Long> ids);
    
    @Select("select count from "+ TABEL +" where user_id=#{userId} and item_id=#{itemId} and status =0")
    int getItemAmount(@Param("userId") long userId,@Param("itemId") long itemId);
    
    public static class SqlProvider {
        public String deleteByIds(Map<String, Object> params) {

            BEGIN();
            UPDATE(TABEL);
            SET("count = 0");
            SET("status = 1");
            SET("utime = unix_timestamp(NOW())");
            WHERE("status = 0 ");
            WHERE(ListParamDaoHelper.preparedInSql(params, "idList", "id", true));

            return SQL();
        }


    }
}

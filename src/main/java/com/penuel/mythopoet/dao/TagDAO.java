package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Tag;
import com.penuel.mythopoet.model.TagRelation;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TagDAO
 * User: penuel (lipengpeng@meituan.com)
 * Date: 15/6/7 下午9:32
 * Desc:
 */

@Component
public interface TagDAO {

    static final String TAG_SELECT_KEY = " id,name,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TAG_TABEL = " tag ";

    static final String TAG_RELATION_SELECT_KEY = " id,tid,type,tag_id,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TAG_RELATION_TABEL = " tag_relation ";

    @Select(" select " + TAG_SELECT_KEY + " from " + TAG_TABEL + " where name like \"%\"#{key}\"%\" and status = 0")
    List<Tag> searchByName(@Param("key") String key);

    @Select( " select "+TAG_RELATION_SELECT_KEY+" from "+TAG_RELATION_TABEL +" where tag_id=#{tagId} and tid=#{itemId} and type = 0" )
    TagRelation getByTagAndItem(@Param("tagId")long tagId,@Param("itemId") long itemId);

    /*
     id BIGINT(11)  NOT NULL AUTO_INCREMENT COMMENT '主键',
  tid BIGINT(11)  NOT NULL DEFAULT '0' COMMENT '商品/分类/设计师ID',
  type int(4) NOT NULL default '0' COMMENT '类型0=商品,1=分类,2=设计师',
  tag_id BIGINT(11)  NOT NULL DEFAULT '0' COMMENT '标签ID',
  status INT(4) NOT NULL DEFAULT '0' COMMENT '状态0=正常,1=删除',
  ctime INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  utime INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
     */
}

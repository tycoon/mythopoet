package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.SmsVerify;
import com.penuel.mythopoet.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * SmsVerifyDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/2 下午11:05
 * Desc:
 */

@Component
public interface SmsVerifyDAO {

    static final String KEY = " id,phone,captcha,status,ctime,utime ";
    static final String TABEL = " sms_verify ";

    @Insert("insert into "+TABEL+" ("+KEY+") values " +
            "(null,#{phone},#{captcha},0,unix_timestamp(NOW()),unix_timestamp(NOW())) "+
            "on duplicate key update utime=unix_timestamp(NOW()),captcha=#{captcha} ")
    public int insertOrUpdate(@Param("phone")String phone,@Param("captcha")String captcha);

    @Select("select id,phone,captcha,from_unixtime(ctime) ctime,from_unixtime(utime) utime from" + TABEL + " where phone=#{phone}")
    SmsVerify getByPhone(@Param("phone") String phone);

}

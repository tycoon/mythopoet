package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.WxAuthUser;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * WxAuthUserDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/24 下午11:49
 * Desc:
 */

@Component
public interface WxAuthUserDAO {

    static final String INSERT_KEY = " id,user_id,code,access_token,refresh_token,refresh_time,expires_in,open_id,scope,union_id,status,ctime,utime ";
    static final String SELECT_KEY = " id,user_id,code,access_token,refresh_token,from_unixtime(refresh_time) refresh_time,expires_in,open_id,scope,union_id," +
            "jsapi_token,jsapi_token_expires_in,from_unixtime(jsapi_token_refresh_time) jsapi_token_refresh_time,jsapi_ticket,jsapi_expires_in,from_unixtime(jsapi_refresh_time) jsapi_refresh_time,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " wx_auth_user ";

    @Select("select" + SELECT_KEY + "from" + TABEL + " where user_id=#{userId}")
    WxAuthUser getByUserId(@Param("userId")long userId);
    @Select("select" + SELECT_KEY + "from" + TABEL + " where open_id=#{openId} ")
	WxAuthUser getByOpenId(@Param("openId") String openId);
    
    
    @Insert("insert into "+TABEL+" ("+INSERT_KEY+") values (null,#{userId},#{code},#{status},unix_timestamp(NOW()),unix_timestamp(NOW()))" +
                    " on duplicate key update utime=unix_timestamp(NOW()),status=0,code=#{code}")
    @Options(useGeneratedKeys = true)
    int insert(WxAuthUser wxAuthUser);
    
    @Insert("insert into "
			+ TABEL
			+ " ("
			+ INSERT_KEY
			+ ") values (null,#{userId},#{code}, #{accessToken},#{refreshToken},unix_timestamp(NOW()),#{expiresIn},#{openId},#{scope},#{unionId},#{status},unix_timestamp(NOW()),unix_timestamp(NOW()))"
			+ " on duplicate key update utime=unix_timestamp(NOW()),status=0,code=#{code}")
	@Options(useGeneratedKeys = true)
	int add(WxAuthUser wxAuthUser);


    @Update("update "+TABEL+" set user_id=#{userId},code=#{code},access_token=#{accessToken},refresh_token=#{refreshToken},refresh_time=unix_timestamp(NOW()),expires_in=#{expiresIn},open_id=#{openId},scope=#{scope},union_id=#{unionId},utime=unix_timestamp(NOW()) where open_id=#{openId}")
    int updateAccessToken(WxAuthUser wxAuthUser);

    @Update("update "+TABEL+" set jsapi_ticket=#{jsapiTicket},jsapi_expires_in=#{jsapiExpiresIn},jsapi_refresh_time=unix_timestamp(NOW()),utime=unix_timestamp(NOW()) where user_id=#{userId}")
    int updateJsapiTicket(WxAuthUser wxAuthUser);

    @Update("update "+TABEL+" set jsapi_token=#{jsapiToken},jsapi_token_expires_in=#{jsapiTokenExpiresIn},jsapi_token_refresh_time=unix_timestamp(NOW()),utime=unix_timestamp(NOW()) where user_id=#{userId}")
    int updateJsapiToken(WxAuthUser wxAuthUser);

    @Update("update"+TABEL+" set code = '' ,utime=unix_timestamp(NOW()) where user_id = #{userId} ")
    int clearCodeByUserId(@Param("userId") long userId);
}

package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.ItemBrand;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * ItemBrandDAO
 * User: penuel (lipengpeng@meituan.com)
 * Date: 15/5/30 下午11:27
 * Desc:
 */

@Component
public interface ItemBrandDAO {


    static final String SELECT_KEY = " id,item_id,brand_id,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " item_brand ";

    @Select(" select name from brand where id in (select brand_id from " + TABEL + " where item_id =#{itemId} and status = 0) and status = 0")
    String getBrandNameByItem(@Param("itemId")long itemId);

}

package com.penuel.mythopoet.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.penuel.mythopoet.model.Customer;

public interface CustomerDAO {
	static final String INSERT_KEY = " id,userid,level,ctime,utime ";
    static final String SELECT_KEY = " id,userid,img,sex,from_unixtime(birthday) birthday,realname,mail,marriage,earning,idtype,identity,education,industry,industry,level,memo,area,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " customer ";
    
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{userId},#{level},unix_timestamp(NOW()),unix_timestamp(NOW())) " +
            "on duplicate key update utime=unix_timestamp(NOW())")
    @Options(useGeneratedKeys = true)
    int addCustomer(Customer customer);
    
    
    @Update(" update "+ TABEL +" set img=#{img},sex=#{sex},birthday=unix_timestamp(#{birthday}),area=#{area},realname=#{realname},mail=#{mail},marriage=#{marriage}," +
            "earning=#{earning},idtype=#{idtype},identity=#{identity},education=#{education},industry=#{industry},level=#{level},memo=#{memo},utime=unix_timestamp(NOW()) " +
            "where id=#{id} and userid=#{userId}")
    int updateCustomer(Customer customer);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where userid=#{userId}")
    Customer getByUserId(@Param("userId") Long userId);
    
    @Update(" update "+ TABEL +" set level=#{level} where id=#{id}")
    int updateLevel(@Param("level")int level,@Param("id") long id);
    
    
    
}

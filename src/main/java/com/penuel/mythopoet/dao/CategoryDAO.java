package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Category;
import com.penuel.mythopoet.model.Item;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * CategoryDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/16 上午8:52
 * Desc:
 */

@Component
public interface CategoryDAO {

    static final String SELECT_KEY = " id,parent_cate_id,name,pic,layer,sort,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " category ";

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where status =0 order by sort limit #{offset},#{limit}")
    List<Category> list(@Param("offset")int offset,@Param("limit") int limit);

    @Select("select a.* from (select item.* from item join item_cate on item.id=item_cate.item_id  and item.status=0 and item_cate.cate_id=#{cateId} and item_cate.status=0) a where a.id in ( select item_id from  item_sku b where b.status=0 and b.count > 0 ) limit #{offset},#{limit}")
    List<Item> getItemsByCateId(@Param("cateId") Long cateId,@Param("offset")int offset,@Param("limit") int limit);

    @Select("select"+SELECT_KEY+"from"+TABEL+" where id in (select tid from tag_relation where tag_id=#{tagId} and status=0 and type=1) and status=0 order by utime desc limit #{offset},#{limit}")
    List<Category> getByTagId(@Param("tagId")int tagId,@Param("offset") int offset,@Param("limit") int limit);

}

package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.OrderRecord;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Component;

/**
 * OrderRecordDAO
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/6/8 下午10:47
 * Desc:
 */

@Component
public interface OrderRecordDAO {

    static final String INSERT_KEY = " order_id,user_id,pay_status,status,detail,remark,ctime ";
    static final String TABEL = " order_record ";

    @Insert("insert into"+TABEL+"("+INSERT_KEY+") values (#{orderId},#{userId},#{payStatus},#{status},#{detail},#{remark},unix_timestamp(NOW()))")
    public void create(OrderRecord orderRecord);

}
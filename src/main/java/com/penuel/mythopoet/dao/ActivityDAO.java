package com.penuel.mythopoet.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.penuel.mythopoet.model.Activity;





public interface ActivityDAO {
	static final String INSERT_KEY = " id,theme_id,activity_name,state,weight,starttime,endtime,ctime,utime ";
    static final String SELECT_KEY = " id,theme_id,activity_name,state,weight,from_unixtime(starttime) starttime,from_unixtime(endtime) endtime,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " activity ";
    
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{themeId},#{activityName},#{state},#{weight},unix_timestamp(#{startTime}),unix_timestamp(#{endTime}),unix_timestamp(NOW()),unix_timestamp(NOW())) ")
    @Options(useGeneratedKeys = true)
    int addActivity(Activity activity);
    
    
    @Update(" update "+ TABEL +" set theme_id=#{themeId},activity_name=#{activityName},weight=#{weight}," +
            "starttime=unix_timestamp(#{startTime}),endtime=unix_timestamp(#{endTime}),utime=unix_timestamp(NOW()) " +
            "where id=#{id}")
    int updateActivity(Activity activity);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where id=#{id}")
    Activity getById(@Param("id") Long id);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where theme_id=#{themeId} order by weight asc ")
    List<Activity> getByThemeId(@Param("themeId") Long themeId);

    @Update(" update "+ TABEL +" set state=1 ,utime=unix_timestamp(NOW()) where theme_id=#{themeId}")
    int updateActivityStateByThemeId(@Param("themeId") long themeId);

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where theme_id=#{themeId} and state=0 limit 1")
	Activity getDefaltActivity(@Param("themeId") Long themeId);

    @Update(" update item a INNER JOIN flash_sale_item s ON a.id=s.item_id SET" +
            " a.price=s.activity_price,a.discount=s.activity_discount,utime=unix_timestamp(NOW()) where s.activity_id = #{activityId} ")
	int toActivityPrice(@Param("activityId") long activityId);
    
    @Update(" update item a INNER JOIN flash_sale_item s ON a.id=s.item_id SET" +
            " a.price=s.price,a.discount=s.discount,utime=unix_timestamp(NOW()) where s.activity_id = #{activityId} ")
	int toItemPrice(@Param("activityId") long activityId);
    
    @Delete(" delete from " + TABEL + " where id = #{id} " )
    int deleteById(@Param("id") long id);
    
    @Delete(" delete from " + TABEL + " where theme_id = #{themeId} " )
    int deleteByThemeId(@Param("themeId") long themeId);

    @Update(" update activity set state = 0 ,utime=unix_timestamp(NOW()) where theme_id=#{themeId} and weight>#{weight} order by weight asc limit 1")
	int startActivityByWeight(@Param("themeId")long themeId, @Param("weight")int weight);
    
    
}

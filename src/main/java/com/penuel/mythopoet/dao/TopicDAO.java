package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Topic;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TopicDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/9 上午12:18
 * Desc:
 */


@Component
public interface TopicDAO {

    static final String KEY = " id,name,description,url,pic,location,position,status,ctime,utime ";
    static final String TABEL = " topic ";

    @Select("select " + KEY + " from " + TABEL + " where status = 0 and location=#{location} order by utime desc limit #{offset},#{limit}")
    List<Topic> list(@Param("location") int location,@Param("offset") int offset, @Param("limit") int limit);

    @Select("select " + KEY + " from " + TABEL + " where status = 0 and location=1 order by position desc, utime desc limit #{offset},#{limit}")
    List<Topic> headlineList(@Param("location") int location,@Param("offset") int offset, @Param("limit") int limit);
}

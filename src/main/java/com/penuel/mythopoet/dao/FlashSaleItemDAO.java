package com.penuel.mythopoet.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.LEFT_OUTER_JOIN;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.penuel.mythopoet.model.FlashSaleItem;
import com.penuel.mythopoet.modelView.ItemView;





@Component
public interface FlashSaleItemDAO {
	static final String INSERT_KEY = " id,item_id,activity_id,price,discount,origin_price,activity_price,activity_discount,ctime,utime ";
    static final String SELECT_KEY = " id,item_id,activity_id,price,discount,origin_price,activity_price,activity_discount,from_unixtime(stime) stime,from_unixtime(etime) etime,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " flash_sale_item ";
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where activity_id = #{activityId} order by ctime desc ")
    List<FlashSaleItem> getActivityList(@Param("activityId") long activityId);
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where activity_id = #{activityId} order by ctime desc limit #{offset},#{limit} ")
	List<FlashSaleItem> getActivityListPage(long activityId, int offset,int limit);
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where stime = #{stime} ")
    List<FlashSaleItem> getList(@Param("stime") String stime);
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where id = #{id}")
	FlashSaleItem getById(@Param("id") long id);    
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where item_id = #{itemId}")
	FlashSaleItem getByItemId(@Param("itemId") long itemId);    
    
	@Update(" update " + TABEL + " set activity_price=#{activityPrice},activity_discount=#{activityDiscount},utime=unix_timestamp(NOW()) where id=#{id}")
	int updateFlashSaleItem(FlashSaleItem flashSaleItem);
    
    @Select("select "+SELECT_KEY+" from "+TABEL +" where activity_id = #{activityId} and item_id=#{itemId}")
	FlashSaleItem getByActivityId( @Param("activityId") long activityId,@Param("itemId") long itemId);
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{itemId},#{activityId},#{price},#{discount},#{originPrice},#{activityPrice},#{activityDiscount},unix_timestamp(NOW()),unix_timestamp(NOW())) ")
    @Options(useGeneratedKeys = true)
    int addFlashSaleItem(FlashSaleItem flashSaleItem);

    @Select("select count(1) from "+TABEL +" where activity_id = #{activityId} ")
	int countItems(Long activityId);
    
    @Delete(" delete from " + TABEL + " where id = #{id} " )
    int deleteById(@Param("id") long id);
    
    @Delete(" delete from " + TABEL + " where activity_id = #{activityId} " )
    int deleteByActivityId(@Param("activityId") long activityId);
    
    @SelectProvider(type = SqlProvider.class, method = "selectItemsSql")
	List<ItemView> list(@Param("activityId") Long activityId,@Param("offset") int offset, @Param("limit") int limit);
    
	public static class SqlProvider {
		public String selectItemsSql(Map<String, Object> params) {
			BEGIN();
			SELECT(" a.id,a.name,g.activity_price as price,a.origin_price,g.activity_discount as discount,a.description,a.note,a.pic,a.status,from_unixtime(a.ctime) ctime,from_unixtime(a.utime) utime ,a.view_id,a.size_desc");
			SELECT(" b.cate_id ");
			SELECT(" c.designer_id ");
			SELECT(" d.brand_id ");
			SELECT(" e.count ");
			SELECT(" f.name as dname ");
			FROM("  flash_sale_item g ");
			LEFT_OUTER_JOIN(" item a on g.item_id=a.id");
            LEFT_OUTER_JOIN(" item_cate b on a.id=b.item_id");
            LEFT_OUTER_JOIN(" item_designer c on a.id=c.item_id");
            LEFT_OUTER_JOIN(" item_brand d on a.id=d.item_id");
            LEFT_OUTER_JOIN(" (select sku.item_id ,sum(sku.count) count from  item_sku sku where status=0 group by sku.item_id) e on a.id = e.item_id");
            LEFT_OUTER_JOIN(" designer f on  c.designer_id = f.id");

			WHERE("g.activity_id=#{activityId}");


			ORDER_BY(" a.utime desc limit #{offset},#{limit}");

			return SQL();
		}
		
	}




    
}

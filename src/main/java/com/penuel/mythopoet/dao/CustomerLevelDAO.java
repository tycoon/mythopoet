package com.penuel.mythopoet.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.penuel.mythopoet.model.CustomerLevel;


public interface CustomerLevelDAO {
	
	static final String INSERT_KEY = " id,level,name,discount,img ";
    static final String SELECT_KEY = " id,level,name,discount,img ";
    static final String TABEL = " customerlevel ";
    
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{level},#{name},#{discount},#{img}) ")
    @Options(useGeneratedKeys = true)
    int addCustomerLevel(CustomerLevel customerLevel);
    
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where level=#{level} ")
    CustomerLevel getByLevel(@Param("level") int level);
    
    
    
    
    
}

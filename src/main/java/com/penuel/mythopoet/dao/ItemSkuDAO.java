package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.ItemSku;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ItemSkuDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/17 上午8:10
 * Desc:
 */

@Component
public interface ItemSkuDAO {

    static final String SELECT_KEY = " id,item_id,name,props,count,price,origin_price,pic,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " item_sku ";

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where status =0 and id=#{skuId}")
    ItemSku getById(@Param("skuId") long skuId);

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where item_id=#{itemId} and props=#{props}")
    ItemSku getAllSkuByItemAndProps(@Param("itemId") long itemId,@Param("props") String props);

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where status =0 and item_id=#{itemId} and props=#{props}")
    ItemSku getValidSkuByItemAndProps(@Param("itemId") long itemId,@Param("props") String props);

    @Select("select "+SELECT_KEY+" from "+TABEL+" where item_id=#{itemId} and status=0")
    List<ItemSku> getByItemId(@Param("itemId")long itemId);

    @Select("select sum(count) from "+TABEL+" where item_id=#{itemId} and status=0")
    Integer getSumSkuCountByItemId(@Param("itemId")long itemId);

    @Update("update"+TABEL+"set count = count - #{count},utime=unix_timestamp(NOW()) where id=#{id}")
    int reduceSkuCount(@Param("id")long id,@Param("count") int count);
}

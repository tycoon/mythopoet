package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Orders;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * OrderDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/19 下午5:38
 * Desc:
 */

@Component
public interface OrdersDAO {

    static final String INSERT_KEY = " id,view_id,user_id,total_count,amount,freight,address_id,remark,pay_type,status,ctime,utime,discount_type,discount_amount ";
    static final String SELECT_KEY = " id,view_id,user_id,total_count,amount,freight,pay_type,pay_status,address_id,remark,express,express_no," +
            "from_unixtime(dispatch_time) dispatch_time,from_unixtime(recept_time) recept_time,from_unixtime(pay_time) pay_time," +
            "status,from_unixtime(ctime) ctime,from_unixtime(utime) utime,discount_type,discount_amount ";
    static final String TABEL = " orders ";


    @Insert("insert into "+TABEL+" ( "+INSERT_KEY+") values (null,#{viewId},#{userId},#{totalCount},#{amount},#{freight},#{addressId},#{remark},#{payType},0," +
                    "unix_timestamp(NOW()),unix_timestamp(NOW()),#{discountType},#{discountAmount})")
    @Options(useGeneratedKeys = true)
    int insert(Orders order);

    @Select(" select"+SELECT_KEY+"from"+TABEL+"where id=#{id} and user_id=#{userId}")
    Orders getById(@Param("id")Long id, @Param("userId") Long userId);
    
    @Select(" select"+SELECT_KEY+"from"+TABEL+"where id=#{id} and user_id=#{userId}")
    Orders getByViewIdAndUserId(@Param("view_id")String view_id, @Param("userId") Long userId);
    
    @Select(" select"+SELECT_KEY+"from"+TABEL+"where view_id=#{view_id}")
    Orders getByViewId(@Param("view_id")String view_id);

    @Select(" select"+SELECT_KEY+"from"+TABEL+"where status=#{status} and user_id=#{userId} order by utime desc limit #{offset},#{limit} ")
    List<Orders> getByUserIdAndStatus(@Param("userId")Long userId,@Param("status") Integer status,@Param("offset") int offset,@Param("limit") int limit);

    @Select(" select"+SELECT_KEY+"from"+TABEL+"where status>1 and status <4 and user_id=#{userId} order by utime desc limit #{offset},#{limit} ")
    List<Orders> getWaitRecOrderList(@Param("userId")Long userId,@Param("offset") int offset,@Param("limit") int limit);

    @Select(" select"+SELECT_KEY+"from"+TABEL+"where user_id=#{userId} and status!=10 order by utime desc limit #{offset},#{limit} ")
    List<Orders> getAllByUserId(@Param("userId")Long userId,@Param("offset") int offset,@Param("limit") int limit);

    @Select(" select count(*) from"+TABEL+"where status=#{status} and user_id=#{userId} ")
    int getCountByUserIdAndStatus(@Param("userId")Long userId,@Param("status") Integer status);

    @Select(" select count(*) from"+TABEL+"where status>1 and status <4 and user_id=#{userId}")
    int getWaitRecOrderCount(@Param("userId")Long userId);

    @Select(" select count(*) from"+TABEL+"where user_id=#{userId}  and status!=10 ")
    int getAllCountByUserId(@Param("userId")Long userId);

    @Update("update"+TABEL+"set pay_type=#{payType},pay_status=#{payStatus},status=#{status},pay_time=unix_timestamp(NOW()),utime=unix_timestamp(NOW()) where id=#{id}")
    int payOrder(Orders order);

    @Update("update"+TABEL+"set address_id=#{addressId},utime=unix_timestamp(NOW()) where user_id = #{userId} and id=#{id}")
    int updateAddress(@Param("userId") long userId, @Param("id") long id,@Param("addressId") long addressId);

    @Update("update"+TABEL+"set remark=#{remark},utime=unix_timestamp(NOW()) where user_id = #{userId} and id=#{id}")
    int updateRemark(@Param("userId") long userId, @Param("id") long id,@Param("remark") String remark);

    @Select(" select count(*) from"+TABEL+"where from_unixtime(ctime) between #{today} and #{tomorrow} ")
    long todayOrderIndex(@Param("today") Date today,@Param("tomorrow") Date tomorrow);

    @Update("update"+TABEL+"set status=10, utime=unix_timestamp(NOW()) where user_id = #{userId} and id=#{id}")
    int deleteById(@Param("id") Long id, @Param("userId") Long userId);

    @Update("update"+TABEL+"set view_id=#{viewId}, utime=unix_timestamp(NOW()) where id=#{id}")
    int updateOrderIdView(@Param("id")long id,@Param("viewId") String viewId);

    @Update("update"+TABEL+"set status=#{status}, utime=unix_timestamp(NOW()) where user_id = #{userId} and id=#{id}")
    int updateOrderStatus(@Param("id")long id,@Param("userId") long userId,@Param("status") int status);

    @Update("update"+TABEL+"set pay_type=#{payType}, utime=unix_timestamp(NOW()) where user_id = #{userId} and id=#{id}")
    int updatePayType(@Param("userId")long userId, @Param("id")long id, @Param("payType")int payType);
}

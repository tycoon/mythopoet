package com.penuel.mythopoet.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.penuel.mythopoet.model.Ticket;

@Component
public interface TicketDAO {
	static final String INSERT_KEY = " id,pword,buylimit buyLimit,cost,expdate expDate,isuse isUse,orderid orderId,ctime,utime ";
    static final String SELECT_KEY = " id,pword,buylimit buyLimit,cost,from_unixtime(expdate) expDate,isuse isUse,orderid orderId,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " ticket ";
    
    @Select("select "+SELECT_KEY+" from "+TABEL+" where pword=#{pword} and 1=1")
    Ticket getByPword(@Param("pword") String pword);    
    
    @Update("update "+TABEL+" set isuse=1,orderid=#{orderId},utime=unix_timestamp(NOW()) where pword=#{pword} and isuse=0")
    int haveUsed(@Param("orderId") Long orderId,@Param("pword") String pword);
    
   /* @Update("update "+TABEL+" set isuse=1,utime=unix_timestamp(NOW()) where orderid=#{orderId} and isuse=0")
    int haveUsed(@Param("orderId") Long orderId);*/

    @Update("update "+TABEL+" set orderid=#{orderId},utime=unix_timestamp(NOW()) where pword=#{pword} and isuse=0")
    int haveOrderId(@Param("orderId") Long orderId,@Param("pword") String pword);
}

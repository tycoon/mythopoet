package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Favorites;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FavoritesDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/27 下午9:49
 * Desc:
 */

@Component
public interface FavoritesDAO {

    static final String SELECT_KEY = " id,user_id,type,tid,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String INSERT_KEY = " id,user_id,type,tid,status,ctime,utime ";
    static final String TABEL = " favorites ";

    @Insert("insert into "+TABEL+" ("+INSERT_KEY+") values (null,#{userId},#{type},#{tid},#{status},unix_timestamp(NOW()),unix_timestamp(NOW()))" +
                    " on duplicate key update utime=unix_timestamp(NOW()),status=0")
    @Options(useGeneratedKeys = true)
    int insert(Favorites favorites);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where user_id=#{userId} and type=#{type} and status = 0 limit #{offset},#{limit}")
    List<Favorites> getByUserId(@Param("userId")Long userId,@Param("type")int type,@Param("offset") int offset,@Param("limit") int limit);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where user_id=#{userId} and tid=#{tid} and type=#{type} and status = 0")
    Favorites getByUserIdAndTidAndType(@Param("userId")Long userId, @Param("tid")Long tid,@Param("type") int type);

    @Update("update"+TABEL+"set status=1 where id=#{id} and user_id=#{userId}")
    int deleteById(@Param("id")Long id,@Param("userId") Long userId);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where id=#{id} and user_id=#{userId} and status = 0")
    Favorites getById(@Param("id")Long id,@Param("userId") Long userId);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where tid=#{tid} and user_id=#{userId} and status = 0 and type=#{type}")
    Favorites getByTidAndUserId(@Param("tid") long tid, @Param("type") int type, @Param("userId") Long userId);
}

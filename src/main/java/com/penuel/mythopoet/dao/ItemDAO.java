package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.ItemView;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static org.apache.ibatis.jdbc.SqlBuilder.*;

/**
 * ItemDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/14 上午8:02
 * Desc:
 */

@Component
public interface ItemDAO {

    static final String SELECT_KEY = " id,view_id,name,price,origin_price,discount,description,note,size_desc,pic,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String ITEM_SELECT_KEY = " item.id,item.view_id,item.name,item.price,item.origin_price,item.discount,item.description,item.note,item.size_desc,item.pic,item.status,from_unixtime(item.ctime) ctime,from_unixtime(item.utime) utime ";
    static final String TABEL = " item ";

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where id =#{id} and status=0")
    Item getById(@Param("id") Long id);

    @Select(" select distinct item.id,item.view_id,item.name,item.price,item.origin_price,item.discount,item.description,item.note,item.size_desc,item.pic,item.status,from_unixtime(item.ctime) ctime,from_unixtime(item.utime) utime from " + TABEL + " where id =#{id}")
    Item getSkuItemById(@Param("id") Long id);

    /**获取有库存的商品*/
    @Select("select distinct a.id,a.view_id,a.name,a.price,a.origin_price,a.discount,a.description,a.note,a.size_desc,a.pic,a.status,from_unixtime(a.ctime) ctime,from_unixtime(a.utime) utime, a.ordered_count from (select "+ITEM_SELECT_KEY+", tmp.ordered_count ordered_count from " + TABEL + " left join ( select ifnull(item_id,0) item_id, ifnull(sum(count),0) ordered_count " +
                    "from order_detail where status=1 order by ordered_count desc limit #{offset},#{limit} ) tmp on item.id=tmp.item_id and item.status=0 join tag_relation t on item.id=t.tid and t.type=0 and t.tag_id=#{tagId}) a join " +
                    "item_sku b on a.id=b.item_id and b.status=0 and a.status = 0 and b.count > 0")
    List<ItemView> hotList(@Param("offset") int offset, @Param("limit") int limit, @Param("tagId") long tagId);


    /**获取有库存的商品*/
    @Select("select distinct item.id,item.view_id,item.name,item.price,item.origin_price,item.discount,item.description,item.note,item.size_desc,item.pic,item.status,from_unixtime(item.ctime) ctime,from_unixtime(item.utime) utime from" + TABEL + "join item_sku b on item.id=b.item_id and item.status=0 and b.status=0 and b.count > 0 join tag_relation t on item.id=t.tid and t.type=0 and t.tag_id=#{tagId} order by item.ctime desc limit #{offset},#{limit}")
    List<Item> newList(@Param("offset") int offset, @Param("limit") int limit, @Param("tagId") long tagId);

    /**获取有库存的商品*/
    @Select("select distinct item.id,item.view_id,item.name,item.price,item.origin_price,item.discount,item.description,item.note,item.size_desc,item.pic,item.status,from_unixtime(item.ctime) ctime,from_unixtime(item.utime) utime from " + TABEL + " join item_sku b on item.id=b.item_id and item.discount < 1 and item.status=0 and b.status=0 and b.count > 0 join tag_relation t on item.id=t.tid and t.type=0 and t.tag_id=#{tagId} order by item.discount limit #{offset},#{limit}")
    List<Item> discountList(@Param("offset") int offset, @Param("limit") int limit, @Param("tagId") long tagId);

    @SelectProvider(type = SqlProvider.class, method = "getByIds")
    List<Item> getByIds(@Param("idList") List<Long> ids);

    @Select("select distinct item.id,item.view_id,item.name,item.price,item.origin_price,item.discount,item.description,item.note,item.size_desc,item.pic,item.status,from_unixtime(item.ctime) ctime,from_unixtime(item.utime) utime from"+TABEL+" join item_sku b on item.id=b.item_id and item.status=0 and b.status=0 and b.count > 0 " +
                    "and item.name like \"%\"#{key}\"%\" order by utime desc limit #{offset},#{limit}")
    List<Item> findByName(@Param("key") String key,@Param("offset") int offset,@Param("limit") int limit);

    @Select("select " + SELECT_KEY + " from " + TABEL + " where status=0 and id in (select item_id from item_designer where designer_id=#{designerId} and status=0) order by utime desc")
    List<Item> getAllByDesingerId(@Param("designerId") Long designerId);

    @Select("select  distinct a.id,a.view_id,a.name,a.price,a.origin_price,a.discount,a.description,a.note,a.size_desc,a.pic,a.status,from_unixtime(a.ctime) ctime,from_unixtime(a.utime) utime  from (select " + SELECT_KEY + " from " + TABEL + " where status=0 and id in (select item_id from item_designer" +
                    " where designer_id=#{designerId} and status=0)) a join item_sku b on a.id=b.item_id and b.status=0 and b.count > 0 order by utime desc limit #{offset},#{limit}")
    List<Item> getByDesingerId(@Param("designerId") Long designerId,@Param("offset") int offset,@Param("limit") int limit);

    @Select("select " + SELECT_KEY + " from " + TABEL + " where status=0 and id in (select tid from tag_relation where tag_id=#{tagId} and status=0 and type=0 ) order by utime desc")
    List<Item> getAllByTagId(@Param("tagId") long tagId);

    public static class SqlProvider {
        public String getByIds(Map<String, Object> params) {

            BEGIN();
            SELECT(SELECT_KEY);
            FROM(TABEL);
            WHERE("status=0");
            WHERE(ListParamDaoHelper.preparedInSql(params, "idList", "id", true));

            return SQL();
        }
    }

    @Select("select"+SELECT_KEY+"from"+TABEL+" where id in (select tid from tag_relation tr join item_cate ic " +
                    "on tr.tag_id=#{tagId} and tr.status=0 and tr.type=0 and ic.cate_id=#{cateId} and ic.status=0 and ic.item_id=tr.tid) and status=0 order by utime desc limit #{offset},#{limit}")
    List<Item> getByCateIdAndTagId(@Param("cateId") long cateId,@Param("tagId") long tagId,@Param("offset") int offset,@Param("limit") int limit);


    @Select("select"+SELECT_KEY+"from"+TABEL+" where id in (select tid from tag_relation tr join item_designer ic " +
                    "on tr.tag_id=#{tagId} and tr.status=0 and tr.type=0 and ic.designer_id=#{designerId} and ic.status=0 and ic.item_id=tr.tid) and status=0 order by utime desc limit #{offset},#{limit}")
    List<Item> getByDesignerIdAndTagId(@Param("designerId")long designerId,@Param("tagId") long tagId,@Param("offset") int offset,@Param("limit") int limit);
    
    @Select("select " + SELECT_KEY + " from " + TABEL + " where status=0 and id in (select tid from tag_relation where tag_id=#{tagId} and status=0 and type=0 ) order by utime desc limit #{offset},#{limit}")
    List<Item> getSingleByTagId(@Param("tagId") long tagId,@Param("offset") int offset,@Param("limit") int limit);
}

package com.penuel.mythopoet.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.penuel.mythopoet.model.Theme;




public interface ThemeDAO {
	static final String INSERT_KEY = " id,theme_name,theme_describe,state,ctime,utime ";
    static final String SELECT_KEY = " id,theme_name,theme_describe,state,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " theme ";
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{themeName},#{themeDescribe},#{state},unix_timestamp(NOW()),unix_timestamp(NOW())) ")
    @Options(useGeneratedKeys = true)
    int addTheme(Theme theme);
    
    @Update(" update "+ TABEL +" set theme_name=#{themeName},theme_describe=#{themeDescribe},state=#{state},utime=unix_timestamp(NOW()) " +
            "where id=#{id}")
    int updateTheme(Theme theme);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where id=#{id}")
    Theme getById(@Param("id") Long id);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL )
	List<Theme> getList();
    
    @Delete(" delete from " + TABEL + " where id = #{id} " )
    int deleteById(@Param("id") long id);
}

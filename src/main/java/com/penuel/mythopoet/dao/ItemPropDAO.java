package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.ItemProp;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static org.apache.ibatis.jdbc.SqlBuilder.*;

/**
 * ItemPropDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/18 下午11:18
 * Desc:
 */

@Component
public interface ItemPropDAO {

    static final String SELECT_KEY = " id,parent_prop_id,cate_id,content,status,from_unixtime(ctime) ctime," +
            "from_unixtime(utime) utime ";
    static final String TABEL = " item_prop ";

    @SelectProvider(type = SqlProvider.class, method = "getByIds")
    List<ItemProp> getByIds(@Param("idList") List<Long> ids);

    public static class SqlProvider {
        public String getByIds(Map<String, Object> params) {

            BEGIN();
            SELECT(SELECT_KEY);
            FROM(TABEL);
            WHERE("status=0");
            WHERE(ListParamDaoHelper.preparedInSql(params, "idList", "id", true));

            return SQL();
        }


    }
}

package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.CneeAddress;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * AddressDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/18 上午10:32
 * Desc:
 */
@Component
public interface CneeAddressDAO {

    static final String KEY = " id,user_id,name,phone,detail,province,city,county,type,status,ctime,utime ";
    static final String TABEL = " cnee_address ";

    @Select(" select " + KEY + " from " + TABEL + " where status =0 and user_id=#{userId} order by type desc limit #{offset},#{limit}")
    List<CneeAddress> listByUserId(@Param("userId")Long userId,@Param("offset") int offset,@Param("limit") int limit);

    @Select(" select " + KEY + " from " + TABEL + " where status =0 and user_id=#{userId} and id=#{addressId}")
    CneeAddress getById(@Param("userId")Long userId,@Param("addressId") Long addressId);

    @Update(" update "+ TABEL +" set name=#{name},phone=#{phone},detail=#{detail},type=#{type}," +
                    "province=#{province},city=#{city},county=#{county},status=#{status},utime=unix_timestamp(NOW()) " +
                    "where id=#{id} and user_id=#{userId}")
    int update(CneeAddress address);

    @Insert(" insert into "+TABEL+" ("+KEY+") values (null,#{userId},#{name},#{phone},#{detail},#{province},#{city},#{county},#{type},0," +
                    "unix_timestamp(NOW()),unix_timestamp(NOW()))")
    @Options(useGeneratedKeys = true)
    int insert(CneeAddress address);

    @Delete("delete from "+TABEL+" where user_id=#{userId} and id=#{addressId}")
    int deleteById(@Param("userId")Long userId,@Param("addressId") Long addressId);

    @Select(" select "+KEY+" from "+TABEL+" where user_id=#{userId} and status =0 and type = 1 ")
    CneeAddress getDefaultByUserId(@Param("userId") long userId);

    @Update("update "+ TABEL+" set type=0,utime=unix_timestamp(NOW()) where type=1 and user_id=#{userId}")
    int cancelDefault(@Param("userId")long userId);

    @Update("update "+ TABEL+" set type=1, utime=unix_timestamp(NOW()) where user_id=#{userId} and id=#{id}")
    int setDefaultById(@Param("userId") long userId,@Param("id") long id);
}

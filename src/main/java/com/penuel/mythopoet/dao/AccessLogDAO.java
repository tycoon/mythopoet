package com.penuel.mythopoet.dao;


import com.penuel.mythopoet.model.AccessLog;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.modelView.ItemView;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static org.apache.ibatis.jdbc.SqlBuilder.*;

/**
 * ItemDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/14 上午8:02
 * Desc:
 */

@Component
public interface AccessLogDAO {

    static final String INSERT_KEY = " id,source,url,ctime ";
    static final String TABEL = " access_log ";
    
    
    @Insert("insert into "+TABEL+" ( "+INSERT_KEY+") values (null,#{source},#{url}," +
            "unix_timestamp(NOW()))")
    @Options(useGeneratedKeys = true)
    int insert(AccessLog accessLog);
}

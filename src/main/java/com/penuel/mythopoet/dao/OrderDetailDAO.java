package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.OrderDetail;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * OrderDetailDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/14 上午8:31
 * Desc:
 */

@Component
public interface OrderDetailDAO {


    static final String INSERT_KEY = " order_id,item_id,count,price,props,status,ctime,utime ";
    static final String SELECT_KEY = " id,order_id,item_id,count,price,props,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " order_detail ";


    @Insert("insert into "+TABEL+" ( "+INSERT_KEY+") values (#{orderId},#{itemId},#{count},#{price},#{props},0," +
                    "unix_timestamp(NOW()),unix_timestamp(NOW()))")
    @Options(useGeneratedKeys = true)
    int insert(OrderDetail orderDetail);

    @InsertProvider(type = SqlProvider.class, method = "batchInsert")
    int batchInsert(List<OrderDetail> orderDetailList);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where order_id=#{orderId} and status=0")
    List<OrderDetail> getByOrderId(@Param("orderId")long orderId);

    class SqlProvider {
        //批量插入
        public String batchInsert(Map<String, List<OrderDetail>> map) {
            List<OrderDetail> orderDetailList = map.get("list");
            StringBuilder sb = new StringBuilder();
            sb.append(" INSERT INTO "+TABEL+" ("+INSERT_KEY+") VALUES ");
            MessageFormat messageFormat = new MessageFormat("(#'{'list[{0}].orderId},#'{'list[{0}].itemId}," +
                                                                    "#'{'list[{0}].count},#'{'list[{0}].price},#'{'list[{0}].props}," +
                                                                    "#'{'list[{0}].status},unix_timestamp(NOW()),unix_timestamp(NOW()))");
            for (int i = 0; i < orderDetailList.size(); i++) {
                sb.append(messageFormat.format(new Object[] { i }));
                if (i < orderDetailList.size() - 1) {
                    sb.append(",");
                }
            }
            sb.append(" on duplicate key update utime=values(utime) ");

            return sb.toString();
        }
    }
}

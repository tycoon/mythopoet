package com.penuel.mythopoet.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.penuel.mythopoet.model.CustomerCard;


public interface CustomerCardDAO {
	static final String INSERT_KEY = " id,code,level,state,userid ";
    static final String SELECT_KEY = " id,code,level,state,userid ";
    static final String TABEL = " customercard ";
    
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{code},#{level},#{state},#{userId}) ")
    @Options(useGeneratedKeys = true)
    int addCustomerCard(CustomerCard customerCard);
    
    
    @Update(" update "+ TABEL +" set state='1',userid=#{userId}" +
            " where code=#{code} and state='0'")
    int useCustomerCard(CustomerCard customerCard);
    
    @Select(" select " + SELECT_KEY + " from " + TABEL + " where code=#{code}")
    CustomerCard getUnUseCode(@Param("code") String code);
    
    
    
    
    
}

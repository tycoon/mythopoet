package com.penuel.mythopoet.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.penuel.mythopoet.model.Designer;




@Component
public interface ItemDesignerDAO {

	static final String INSERT_KEY = " id, item_id, designer_id, status, ctime, utime ";
    static final String TABEL = "item_designer";

    
    @Select(" select description from designer where id in (select designer_id from " + TABEL + " where item_id =#{itemId} and status = 0) and status = 0")
    String getDescriptionByItem(@Param("itemId")long itemId);

    @Select(" select * from designer where id in (select designer_id from " + TABEL + " where item_id =#{itemId} and status = 0) and status = 0")
    Designer getDesignerByItem(@Param("itemId")long itemId);
}

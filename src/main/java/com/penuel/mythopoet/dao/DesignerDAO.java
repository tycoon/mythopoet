package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.Designer;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * DesignerDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/17 上午8:33
 * Desc:
 */

@Component
public interface DesignerDAO { 

    static final String SELECT_KEY = " id,name,initial,description,icon,pic,country,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " designer ";

    @Select(" select " + SELECT_KEY + " from " + TABEL + " where status =0 and id=#{designerId}")
    Designer getById(@Param("designerId") Long designerId);

    @Select("select "+SELECT_KEY+" from "+TABEL+" where status = 0 order by name limit #{offset},#{limit}")
    List<Designer> list(@Param("offset")int offset,@Param("limit") int limit);

    @Select("select"+SELECT_KEY+"from"+TABEL+" where id in (select tid from tag_relation where tag_id=#{tagId} and status = 0 and type=2) and status = 0 order by name limit #{offset},#{limit}")
    List<Designer> getByTagId(@Param("tagId")int tagId,@Param("offset") int offset,@Param("limit") int limit);

    @Select("select "+SELECT_KEY+" from "+TABEL+" where name like \"%\"#{key}\"%\" and status = 0 ")
    List<Designer> searchByName(@Param("key") String key);
}

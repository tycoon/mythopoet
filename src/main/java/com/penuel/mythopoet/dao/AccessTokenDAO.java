package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.AccessToken;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * AccessTokenDAO Created with mythopoet.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/4/3 上午12:30
 * Desc:
 */

@Component
public interface AccessTokenDAO {

    static final String INSERT_KEY = " id,user_id,token,status,ctime,utime ";
    static final String SELECT_KEY = " id,user_id,token,status,from_unixtime(ctime) ctime,from_unixtime(utime) utime ";
    static final String TABEL = " access_token ";

    @Insert("insert into " + TABEL + " (" + INSERT_KEY + ") values " +
            "(null,#{userId},#{token},0,unix_timestamp(NOW()),unix_timestamp(NOW())) " +
            "on duplicate key update utime=unix_timestamp(NOW()),token=#{token} ")
    @Options(useGeneratedKeys = true)
    public int insertOrUpdate(AccessToken accessToken);

    @Select("select"+SELECT_KEY+"from"+TABEL+"where user_id=#{userId} order by utime desc limit 1")
    AccessToken getByUserId(@Param("userId")long userId);
}

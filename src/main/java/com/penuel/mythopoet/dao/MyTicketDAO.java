package com.penuel.mythopoet.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.penuel.mythopoet.model.MyTicket;
import com.penuel.mythopoet.model.Ticket;


@Component
public interface MyTicketDAO {
	static final String INSERT_KEY = " id,userid,tid,ctime,utime ";
    static final String SELECT_KEY = " id,userid,tid,ctime,utime ";
    static final String TABEL = " my_ticket ";
    
    @Select(" select id,pword,buylimit buyLimit,cost,from_unixtime(expdate) expDate,isuse isUse,orderid orderId,from_unixtime(ctime) ctime,from_unixtime(utime) utime from ticket where isuse = 0 and cost = 1000 and source = 1 and sysdate() < from_unixtime(expDate) and id not in (select tid from my_ticket) limit 0,1 ")
    Ticket getActivityTicket();
    
    @Insert(" insert into " + TABEL + " (" + INSERT_KEY + " ) values (null,#{userId},#{ticketId},unix_timestamp(NOW()),unix_timestamp(NOW())) ")
    @Options(useGeneratedKeys = true)
    int addMyTicket(MyTicket myTicket);
    
    
    @Select(" select id,pword,buylimit buyLimit,cost,from_unixtime(expdate) expDate,isuse isUse,orderid orderId,from_unixtime(ctime) ctime,from_unixtime(utime) utime from ticket where id=(select tid from my_ticket where userid=#{userId}) ")
    List<Ticket> getMyTicket(@Param("userId")long userId);
     
}

package com.penuel.mythopoet.dao;

import com.penuel.mythopoet.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

/**
 * UserDAO Createdmythopoetsimple.
 * User: penuel (lipengpeng@ndpmedia.com)
 * Date: 15/3/28 上午8:33
 * Desc:
 */

@Component
public interface UserDAO {

    static final String KEY = " id,name,sex,phone,password,avatar,source,source_id,status,ctime,utime ";
    static final String TABEL = " user ";

    @Select("select" + KEY + "from" + TABEL + " where id=#{userId}")
    User get(@Param("userId") Long userId);

    @Insert(" insert into " + TABEL + " (" + KEY + " ) values (null,#{name},#{sex},#{phone},#{password},#{avatar},#{source},#{sourceId},#{status},unix_timestamp(NOW()),unix_timestamp(NOW())) " +
            "on duplicate key update utime=unix_timestamp(NOW())")
    @Options(useGeneratedKeys = true)
    int insert(User user);

    @Select("select " + KEY + " from " + TABEL + " where phone=#{phone} and status=0")
    User getByPhone(@Param("phone") String phone);

    @Select("select " + KEY + " from " + TABEL + " where name=#{name}")
    User getByName(@Param("name") String name);

    @Update("update"+TABEL+"set password=#{password} where id=#{id}")
    int updatePwd(@Param("id")long id,@Param("password") String password);
}

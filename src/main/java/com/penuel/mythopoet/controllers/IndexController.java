package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.model.Customer;
import com.penuel.mythopoet.model.User;
import com.penuel.mythopoet.model.Topic;
import com.penuel.mythopoet.service.CustomerService;
import com.penuel.mythopoet.service.OrderService;
import com.penuel.mythopoet.service.UserService;
import com.penuel.mythopoet.service.TopicService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * IndexController Created with simple.mythopoeter: penuel (penuel.leo@gmail.com)
 * Date: 15/3/27 上午12:10
 * Desc:
 */

@Controller
@RequestMapping("")
public class IndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;
    @Autowired
    private TopicService topicService;
    @Autowired
    private CustomerService customerService;


    @RequestMapping("")
    public String index(HttpServletRequest request, Model model) {
        List<Topic> slides = null;
        try {
            slides = topicService.list(0, 20, 2); //轮播专题
        } catch (Exception e) {
            LOGGER.error("TopicController.list Exception:pageNum=" + 1 + ",pageSize=" + 20, e);
        }
        model.addAttribute("slides", slides);
        
        boolean isWeixin = false;
		String ua = request.getHeader("user-agent");
		if (ua!=null&&ua.contains("MicroMessenger")) {
			isWeixin = true;
		}
		model.addAttribute("isWeixin", isWeixin);
        return "home/index";
    }

    @RequestMapping("/new")
    public String newItems(HttpServletRequest request, Model model) {

        return "home/new";
    }

    @RequestMapping( "/mine" )
    @LoginRequired
    public String userCenter(Model model, @CookieValue( value = "", defaultValue = "0",required = false) Long userId) {

        try {
            if ( userId == null || userId == 0 ) {
                return "redirect:/user/login";
            }
            int waitPayCount = orderService.getCountByUserIdAndStatus(userId, 0);
            int waitRecCount = orderService.getWaitRecOrderCount(userId);
            int waitDelCount = orderService.getCountByUserIdAndStatus(userId, 1);
            model.addAttribute("waitPayCount", waitPayCount);
            model.addAttribute("waitRecCount", waitRecCount);
            model.addAttribute("waitDelCount", waitDelCount);
            User user = userService.getUser(userId);
            Customer customer = customerService.getByUserId(userId);
            model.addAttribute("customer", customer);
            model.addAttribute("user", user);
        } catch ( Exception e ) {
            LOGGER.error("OrderController.myOrderPage Error:userId=" + userId, e);
        }
        return "mine/mine";
    }

    @RequestMapping( "{userId}" )
    public String user(Model model, @PathVariable Long userId) {

        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        return "test/user";
    }

    @RequestMapping("/size/guide/{tagId}")
    public String size(HttpServletRequest request, Model model, @PathVariable Long tagId) {
        if(tagId == 1){
            return "item/size/female";
        }else{
            return "item/size/male";
        }
    }

}
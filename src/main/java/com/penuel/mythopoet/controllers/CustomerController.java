package com.penuel.mythopoet.controllers; 

import com.penuel.mythopoet.model.AccessToken;
import com.penuel.mythopoet.model.Customer;
import com.penuel.mythopoet.model.CustomerCard;
import com.penuel.mythopoet.model.CustomerLevel;
import com.penuel.mythopoet.model.SmsVerify;
import com.penuel.mythopoet.model.User;
import com.penuel.mythopoet.service.AccessTokenService;
import com.penuel.mythopoet.service.CustomerCardService;
import com.penuel.mythopoet.service.CustomerService;
import com.penuel.mythopoet.service.SmsVerifyService;
import com.penuel.mythopoet.service.UserService;
import com.penuel.mythopoet.service.WxAuthService;
import com.penuel.mythopoet.utils.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * UserController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午9:02
 * Desc:
 */

@Controller 
@RequestMapping("/customer")
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerCardService customerCardService;
    
    @RequestMapping(value = "/info/{userId}")
    public String getInfo(Model model, @PathVariable long userId){
    	User user = userService.getUser(userId);
    	Customer customer = customerService.getByUserId(userId);
      	model.addAttribute("user", user);
    	model.addAttribute("customer", customer);
    	return "mine/customer/update";
    }
    
    @RequestMapping(value = "/level/{userId}")
    public String getLevel(Model model, @PathVariable long userId){
    	User user = userService.getUser(userId);
    	Customer customer = customerService.getByUserId(userId);
      	model.addAttribute("user", user);
    	model.addAttribute("customer", customer);
    	return "mine/mylevel";
    }
    
    @RequestMapping(value = "/getvip/{userId}")
    public String getvip(Model model, @PathVariable long userId){
    	User user = userService.getUser(userId);
    	model.addAttribute("user", user);
    	return "mine/getvip";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json;charset=utf-8") 
    @ResponseBody
    public String updateCustomer(Model model, @RequestParam(value = "userId") long userId,
    		@RequestParam(value = "sex") int sex,
    		@RequestParam(value = "area") String area,
    		@RequestParam(value = "realname") String realname,
    		@RequestParam(value = "birthday") String birthday,
    		@RequestParam(value = "mail") String mail,@RequestParam(value = "industry") String industry) throws ParseException {
    		Customer customer = customerService.getByUserId(userId);
    		customer.setRealname(realname);
    		customer.setSex(sex);
    		customer.setIndustry(industry);
    		customer.setMail(mail);
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		Date date = sdf.parse(birthday);
    		customer.setBirthday(date);
    		customer.setArea(area);
    		int result = customerService.updateCustomer(customer);
    		if(result > 0){
    			return ResponseUtil.result(0, "修改成功", "");
    		}else{
    			return ResponseUtil.result(1, "修改失败", "");
    		}
    }
    
    @RequestMapping(value = "/exchangecode", method = RequestMethod.POST, produces = "application/json;charset=utf-8") 
    @ResponseBody
    public String exchangeCode(Model model, @RequestParam(value = "userId") long userId,@RequestParam(value = "code") String code) {
    		Customer customer = customerService.getByUserId(userId);
    		String str = null;
    		if(customer.getLevel() == 1){
    			str = "金牌";
    		}else if(customer.getLevel() == 2){
    			str = "铂金";
    		}else if(customer.getLevel() == 3){
    			str = "钻石";
    		}
    		CustomerCard customerCard = customerCardService.getUnUseCode(code);
    		if(null == customerCard){
    			return ResponseUtil.result(1, "请输入正确的激活码！", "");
    		}
    		if(customerCard.getState() == 1){
    			return ResponseUtil.result(1, "此VIP卡已被使用！", "");
    		}
    		if(customerCard.getLevel() <= customer.getLevel()){
    			return ResponseUtil.result(1, "您已经是"+ str +"会员啦，无需再次激活", "");
    		}
    		int result = customerService.updateLevel(customerCard.getLevel(), customer.getId());
    		if(result > 0){
    			customerCard.setUserId(userId);
    			customerCardService.useCustomerCard(customerCard);
    			return ResponseUtil.result(0, "激活成功", "");
    		}else{
    			return ResponseUtil.result(1, "激活失败", "");
    		}
    }
    
    @RequestMapping(value = "/getlevel", method = RequestMethod.POST, produces = "application/json;charset=utf-8") 
    @ResponseBody
    public String getLevelByUserId(Model model, @CookieValue( "userId" ) Long userId) throws ParseException {
    		Customer customer = customerService.getByUserId(userId);


    		if(customer != null){
    			return ResponseUtil.result(0, "查询成功", customer.getLevel());
    		}else{
    			return ResponseUtil.result(1, "查询失败", "");
    		}
    }
}

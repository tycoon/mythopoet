package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.model.Designer;
import com.penuel.mythopoet.model.Favorites;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.DesignerView;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.DesignerService;
import com.penuel.mythopoet.service.FavoriteService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.utils.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * DesignerController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:49
 * Desc:
 */

@Controller
@RequestMapping("designer")
public class DesignerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DesignerController.class);

    @Autowired
    private DesignerService designerService;
    @Autowired
    private FavoriteService favoriteService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private ItemBrandService itemBrandService;

    @RequestMapping( "list" )
    public String list(HttpServletRequest request, Model model) {

        return "find/designer";
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String designers(Model model, @RequestParam(value = "tagId", defaultValue = "0") int tagId,
            @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {

        List<Designer> designerList;
        try {
            if ( tagId == 0 ){
                designerList = designerService.list((pageNum - 1) * pageSize, pageSize);
            }else {
                designerList = designerService.getByTagId(tagId, (pageNum - 1) * pageSize, pageSize);
            }
        } catch ( Exception e ) {
            LOGGER.error("DesignerController.designers Error:tagId=" + tagId + ",pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取设计师列表失败", null);
        }
        return ResponseUtil.result(0, "OK", designerList);
    }

    @RequestMapping("detail")
    public String designer(HttpServletRequest request, Model model) {

        return "find/designer_detail";
    }

    @RequestMapping(value = "/{designerId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String detail(Model model, @PathVariable Long designerId, @CookieValue(value = "userId", required = false) Long userId) {

        DesignerView designerView;
        try {
            Designer designer = designerService.getById(designerId);
            if ( designer == null ) {
                return ResponseUtil.result(1, "获取设计师信息失败,请重试", "");
            }
            designerView = new DesignerView();
            BeanUtils.copyProperties(designer, designerView);
            if ( userId != null ) {
                Favorites favorites = favoriteService.getByTidAndUserId(designerId, 1, userId);
                if ( favorites != null ) {
                    designerView.setFavoritesId(favorites.getId());
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("DesignerController.detail Error:designerId=" + designerId, e);
            return ResponseUtil.result(1, "获取设计师信息失败", "");
        }
        return ResponseUtil.result(0, "OK", designerView);
    }

    @RequestMapping(value = "/{designerId}/items", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String items(Model model, @PathVariable Long designerId, @RequestParam( value = "pageNum", defaultValue = "1" ) int pageNum,
            @RequestParam( value = "pageSize", defaultValue = "20" ) int pageSize,@RequestParam(value = "tagId",defaultValue = "0") int tagId) {

        List<ItemView> itemViewList = new ArrayList<ItemView>();
        try {
            List<Item> itemList;
            if ( tagId == 0 ) {
                itemList = itemService.getByDesignerId(designerId, (pageNum - 1) * pageSize, pageSize);
            }else{
                itemList = itemService.getByDesignerIdAndTagId(designerId,tagId, (pageNum - 1) * pageSize, pageSize);
            }
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("DesignerController.detail Error:designerId=" + designerId, e);
            return ResponseUtil.result(1, "获取设计师的产品列表失败", "");
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }

    // 设计师详情页面
    @RequestMapping( value = "/detail/{designerId}" )
    public String detailPage(HttpServletRequest request, Model model, @PathVariable Long designerId,
            @CookieValue( value = "userId", required = false ) Long userId) {

        DesignerView designerView = null;
        try {
            Designer designer = designerService.getById(designerId);
            if ( designer == null ) {
                designerView = null;
            }
            designerView = new DesignerView();
            BeanUtils.copyProperties(designer, designerView);
            if ( userId != null ) {
                Favorites favorites = favoriteService.getByTidAndUserId(designerId, 1, userId);
                if ( favorites != null ) {
                    designerView.setFavoritesId(favorites.getId());
                }
            }
            List<ItemView> itemViewList = new ArrayList<ItemView>();
            List<Item> itemList = itemService.getAllByDesignerId(designerId);
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
            model.addAttribute("itemList", itemViewList);
        } catch ( Exception e ) {
            LOGGER.error("DesignerController.detail Error:designerId=" + designerId, e);
        }
        model.addAttribute("detail", designerView);
        return "find/designer_detail";
    }

}
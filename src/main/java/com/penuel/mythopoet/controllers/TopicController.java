package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.model.Topic;
import com.penuel.mythopoet.service.TopicService;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


/**
 * TopicController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:46
 * Desc:
 */

@Controller
@RequestMapping("topic")
public class TopicController {

   /* private static final Logger LOGGER = LoggerFactory.getLogger(TopicController.class);

    @Autowired
    private TopicService topicService;*/


   /* @RequestMapping(value = "list", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String list(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                       @RequestParam(value = "pageSize", defaultValue = "20") int pageSize,
                       @RequestParam(value = "location", defaultValue = "0") int location) {
        List<Topic> topics = null;
        try {
            int offset = (pageNum - 1) * pageSize;
            if ( location == Topic.LOCATION_HEAD ){
                topics = topicService.headlineList(offset,pageSize,location);
            }else {
                topics = topicService.list(offset, pageSize, location);
            }
        } catch (Exception e) {
            LOGGER.error("TopicController.list Exception:pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取数据失败", null);
        }
        return ResponseUtil.result(0, "OK", topics);
    }*/

    @RequestMapping("/demo")
    public String demo(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/topic";
    }
    @RequestMapping("/1")
    public String hefu(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/hefu";
    }
    @RequestMapping("/2")
    public String shoe(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/shoe";
    }
    @RequestMapping("/3")
    public String sexy(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/sexy";
    }
    @RequestMapping("/4")
    public String sporty(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/hshs";
    }
    @RequestMapping("/5")
    public String mad(HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/mad";
    }   

    @RequestMapping("{id}")
    public String hshsid(@PathVariable Long id,HttpServletRequest request, Model model) {
    	checkWeixin(request,model);
        return "topic/hshs"+id;
    }
    
    private void checkWeixin(HttpServletRequest request, Model model){
    	boolean isWeixin = false;
		String ua = request.getHeader("user-agent");
		if (ua!=null&&ua.contains("MicroMessenger")) {
			isWeixin = true;
		}
		model.addAttribute("isWeixin", isWeixin);
    }
}
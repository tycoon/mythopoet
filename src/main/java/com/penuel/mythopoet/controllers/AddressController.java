package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.model.CneeAddress;
import com.penuel.mythopoet.service.CneeAddressService;
import com.penuel.mythopoet.utils.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * AddressController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:44
 * Desc:
 */

@Controller
@RequestMapping("/address")
@LoginRequired
public class AddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);

    @Autowired
    private CneeAddressService cneeAddressService;

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json;charset=utf-8") @ResponseBody
    public String list(Model model, @RequestParam("userId") Long userId, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        LOGGER.info("AddressController.list userId=", +userId + ",pageNum=" + pageNum + ",pageSize=" + pageSize);
        List<CneeAddress> addressList = cneeAddressService.list(userId, (pageNum - 1) * pageSize, pageSize);
        return ResponseUtil.result(0, "OK", addressList);
    }

    @RequestMapping( value = "/{addressId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String detail(Model model, @RequestParam( "userId" ) Long userId, @PathVariable Long addressId) {
        LOGGER.info("AddressController.detail userId=", +userId + ",addressId=" + addressId);
        CneeAddress address = cneeAddressService.getById(userId, addressId);
        return ResponseUtil.result(0, "OK", address);
    }

    @RequestMapping( value = "/update", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String update(Model model, @RequestParam( "userId" ) Long userId, @RequestParam( "name" ) String name, @RequestParam( "phone" ) String phone,
            @RequestParam( "province" ) String province, @RequestParam( value = "city", defaultValue = "" ) String city,
            @RequestParam( "county" ) String county, @RequestParam( "detail" ) String detail,
            @RequestParam( value = "isDefault", defaultValue = "false" ) boolean isDefault,
            @RequestParam( value = "addressId", defaultValue = "0" ) long addressId) {

        if ( StringUtils.isBlank(name) || StringUtils.isBlank(phone) || StringUtils.isBlank(province) || StringUtils
                .isBlank(county) || StringUtils.isBlank(detail) ) {
            return ResponseUtil.result(1, "信息不完整，请继续填写", "");
        }
        LOGGER.info("AddressController.update userId=",
                    +userId + ",name=" + name + ",phone=" + phone + ",province=" + province + ",city=" + city + ",country=" + county + ",detail=" + detail
                            + ",isDefault=" + isDefault + ",addressId=" + addressId);
        CneeAddress address = new CneeAddress();
        address.setId(addressId);
        address.setCity(city);
        address.setCounty(county);
        address.setDetail(detail);
        address.setName(name);
        address.setPhone(phone);
        address.setUserId(userId);
        address.setProvince(province);
        address.setType(isDefault ? 1 : 0);
        int result = cneeAddressService.insertOrUpdate(address);
        return ResponseUtil.result(0, "OK", address);
    }

    @RequestMapping( value = "/delete/{addressId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String delete(Model model, @RequestParam( "userId" ) Long userId, @PathVariable Long addressId) {
        LOGGER.info("AddressController.delete userId="+userId+",addressId="+addressId);
        int result = cneeAddressService.deleteById(userId, addressId);
        return ResponseUtil.result(0, "OK", "");
    }

    @RequestMapping( value = "/default/{addressId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String edit(Model model, @RequestParam( "userId" ) Long userId, @PathVariable Long addressId) {

        LOGGER.info("AddressController.edit userId="+userId+",addressId="+addressId);
        int result = cneeAddressService.setDefault(userId, addressId);
        return ResponseUtil.result(0, "OK", result);
    }

    /*地址管理*/
    @RequestMapping( value = "" )
    public String manage(HttpServletRequest request, Model model) {

        return "mine/address/list";
    }

    @RequestMapping( value = "/add" )
    public String add(HttpServletRequest request, Model model) {

        return "mine/address/add";
    }

    @RequestMapping( value = "/update/{userId}/{addressId}" )
    public String updatePage(HttpServletRequest request, Model model, @PathVariable Long userId, @PathVariable Long addressId) {
        CneeAddress address = cneeAddressService.getById(userId, addressId);
        model.addAttribute("address", address);
        return "mine/address/update";
    }

}
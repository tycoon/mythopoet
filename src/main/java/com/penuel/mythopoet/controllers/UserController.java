package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.model.AccessToken;
import com.penuel.mythopoet.model.Customer;
import com.penuel.mythopoet.model.SmsVerify;
import com.penuel.mythopoet.model.User;
import com.penuel.mythopoet.service.AccessTokenService;
import com.penuel.mythopoet.service.CustomerService;
import com.penuel.mythopoet.service.MyTicketServcie;
import com.penuel.mythopoet.service.SmsVerifyService;
import com.penuel.mythopoet.service.UserService;
import com.penuel.mythopoet.service.WxAuthService;
import com.penuel.mythopoet.utils.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.Date;

/**
 * UserController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 上午9:02
 * Desc:
 */

@Controller @RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private AccessTokenService accessTokenService;
    @Autowired
    private SmsVerifyService smsVerifyService;
    @Autowired
    private WxAuthService wxAuthService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private MyTicketServcie myTicketServcie;

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8") @ResponseBody
    public String list(Model model, @PathVariable long userId) {

        User user = userService.getUser(userId);
        if ( user == null ) {
            return ResponseUtil.result(1, "获取失败，用户不存在", "");
        }
        return ResponseUtil.result(0, "获取成功", user);
    }
    @Transactional
    @RequestMapping(value = "/reg", method = RequestMethod.POST, produces = "application/json;charset=UTF-8") @ResponseBody
    public String regist(Model model, @RequestParam(value = "phone") String phone, @RequestParam(value = "captcha") String captcha,
            @RequestParam(value = "password") String password, @RequestParam(value = "repassword") String repassword, @RequestParam( "name" ) String name) {

        if ( StringUtils.isBlank(password) || StringUtils.isBlank(repassword)|| StringUtils.isBlank(name) || !password.equals(repassword) ) {
            return ResponseUtil.result(1, "用户名,密码不能为空或者两次输入的密码不一致", "");
        }
        if ( !FormatterUtil.isMobiPhoneNum(phone) ) {
            return ResponseUtil.result(1, "手机号码不合法，请检查重试", "");
        }
        User user = userService.getUserByPhone(phone);
        if ( user != null ) {
            return ResponseUtil.result(1, "手机号码已注册，请直接登陆", "");
        }
        user = userService.getUserByName(name);
        if ( user != null ) {
            return ResponseUtil.result(1, "用户名已被使用，请更改", "");
        }
        SmsVerify smsVerify = smsVerifyService.getByPhone(phone);
        boolean expired = DateUtils.addMinutes(smsVerify.getUtime(), 5).compareTo(new Date()) == -1;
        if ( null == smsVerify || expired || !smsVerify.getCaptcha().equals(captcha) ) {
            return ResponseUtil.result(1, "验证码错误或已过期，请重新获取", "");
        }
        String md5Pwd = DigestUtils.md5DigestAsHex(password.getBytes());
        user = new User();
        user.setPhone(phone);
        user.setPassword(md5Pwd);
        user.setName(name);
        int result = userService.regist(user);
        if ( result > 0 ) {
        	user = userService.getUserByPhone(phone);
        	Customer customer = new Customer();
        	customer.setUserId(user.getId());
        	customer.setLevel(0);
        	myTicketServcie.giveTicket(user.getId());
        	result = customerService.addCustomer(customer);
        	if(result > 0){
        		 return ResponseUtil.result(0, "OK", user);
        	}else{
        		 return ResponseUtil.result(1, "注册失败", user);
        	}
        } else {
            return ResponseUtil.result(1, "注册失败", user);
        }
    }

    public static void main(String[] args) {

        System.out.println(MD5Util.md5("12312312312").length());
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json;charset=utf-8") @ResponseBody
    public String login(Model model, @RequestParam(value = "phone") String phone, @RequestParam(value = "password") String password) {

        User user;
        try {
            if (StringUtils.isBlank(password) ) {
                return ResponseUtil.result(1, "用户名或手机号不能为空", "");
            }
            user = userService.getUserByPhone(phone);
            if ( user == null ) {
                user = userService.getUserByName(phone);
                if ( user == null || user.getStatus() != 0 ) {
                    return ResponseUtil.result(2, "用户不存在或非法，请重试", "");
                }
            }
            String md5Pwd = DigestUtils.md5DigestAsHex(password.getBytes());
            if ( !user.getPassword().equals(md5Pwd) ) {
                return ResponseUtil.result(3, "密码错误，请重试", "");
            }
            AccessToken accessToken = accessTokenService.createByUser(user);
            if ( accessToken == null || accessToken.getId() == 0 ) {
                return ResponseUtil.result(4, "登陆失败，请重试", "");
            }
            user.setToken(accessToken.getToken());
            //登陆成功后，将wxAuthUser的code清空，等待重新授权，防止同一微信不同用户登陆，导致微信授权用户不一致。支付失败。
            wxAuthService.clearWxUserCodeByUserId(user.getId());
        } catch ( Exception e ) {
            LOGGER.error("login Exception:phone=" + phone + ",password=***", e);
            return ResponseUtil.result(5, "登陆错误，请重试", "");
        }
        return ResponseUtil.result(0, "OK", user);
    }

    @RequestMapping( value = "/captcha", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String captcha(Model model, @RequestParam( "phone" ) String phone) {

        try {
            if ( !FormatterUtil.isMobiPhoneNum(phone) ) {
                return ResponseUtil.result(2, "手机号码格式错误，请检查!", "");
            }
            int validCount = NumberUtils.toInt(CacheUtil.get("valid_code_" + phone, CacheUtil.VALID_CODE_CACHE));
            if ( validCount > 5 ) {
                return ResponseUtil.result(1, "短信验证码太频繁，请10分钟后再试!", "");
            }
            int totalCount = NumberUtils.toInt(CacheUtil.get("total_valid_count", CacheUtil.TOTAL_COUNT_SMS_CACHE));
            if ( totalCount > 1000 ) {
                return ResponseUtil.result(1, "今天发送验证码过于频繁，请明天再试!", "");
            }
            String captcha = SmsUtil.getValidCode(4);
            int result = smsVerifyService.creat(phone, captcha);
            if ( result > 0 ) {
                boolean smsResult = SmsUtil.send(phone, "您的短信验证码是:" + captcha + "【绘事后素】");
                if ( smsResult ) {
                    CacheUtil.set("valid_code_" + phone, (validCount + 1) + "", CacheUtil.VALID_CODE_CACHE);
                    CacheUtil.set("total_valid_count", (totalCount + 1) + "", CacheUtil.TOTAL_COUNT_SMS_CACHE);
                    return ResponseUtil.result(0, "验证码已发送，请注意查收!", captcha);
                } else {
                    return ResponseUtil.result(1, "短信验证码服务错误，请重试!", captcha);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("UserController.captcha Error:phone=" + phone, e);
        }
        return ResponseUtil.result(1, "验证码发送失败，请重试！", "");
    }

    @RequestMapping( value = "/pwd/forgot", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String forgotPwd(Model model, @RequestParam( "phone" ) String phone, @RequestParam( "password" ) String password,
            @RequestParam( "repassword" ) String repassword, @RequestParam( "captcha" ) String captcha) {

        try {
            if ( !FormatterUtil.isMobiPhoneNum(phone) ) {
                return ResponseUtil.result(2, "手机号码格式错误，请检查!", "");
            }
            SmsVerify smsVerify = smsVerifyService.getByPhone(phone);
            boolean expired = DateUtils.addMinutes(smsVerify.getUtime(), 5).compareTo(new Date()) == -1;
            if ( null == smsVerify || expired || !smsVerify.getCaptcha().equals(captcha) ) {
                return ResponseUtil.result(1, "验证码错误或已过期，请重新获取", "");
            }
            User user = userService.getUserByPhone(phone);
            if ( user == null || user.getStatus() != 0 ) {
                return ResponseUtil.result(1, "该手机号未注册或非法", "");
            }
            if ( StringUtils.isBlank(password) || StringUtils.isBlank(repassword) || !password.equals(repassword) ){
                return ResponseUtil.result(1, "密码不能为空或者两次输入的密码不一致", "");
            }
            String md5Pwd = DigestUtils.md5DigestAsHex(password.getBytes());
            int result = userService.updatePwd(user.getId(),md5Pwd);
            if ( result > 0 ){
                return ResponseUtil.result(0, "密码修改成功！", "");
            }
        } catch ( Exception e ) {
            LOGGER.error("UserController.captcha Error:phone=" + phone, e);
        }
        return ResponseUtil.result(1, "密码修改失败，请重试！", "");
    }

    /**
     * 页面渲染
     */
    @RequestMapping( value = "/login", method = RequestMethod.GET )
    public String login(HttpServletRequest request, Model model) {

        return "user/login";
    }

    @RequestMapping( "/register" )
    public String register(HttpServletRequest request, Model model) {

        return "user/register";
    }
    @RequestMapping( value = "/password", method = RequestMethod.GET )
    public String password(HttpServletRequest request, Model model) {

        return "user/psw";
    }
}
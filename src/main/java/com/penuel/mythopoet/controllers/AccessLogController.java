package com.penuel.mythopoet.controllers;


import com.penuel.mythopoet.model.AccessLog;
import com.penuel.mythopoet.service.AccessLogService;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AddressController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:44
 * Desc:
 */

@Controller
@RequestMapping("/access")
public class AccessLogController {

    @Autowired
    private AccessLogService accessLogService;

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8") @ResponseBody
    public String add(Model model,@RequestParam( value = "url" , defaultValue = "") String url, @RequestParam( value = "source" , defaultValue = "") String source,
    		HttpServletRequest request, HttpServletResponse response) {
//    	 boolean flag = false;
//         Cookie[] cookies = request.getCookies();
//         for(int i = 0; i < cookies.length; i++){
//         	Cookie cookie =  cookies[i];
//         	if("source".equals(cookie.getName())){
//         		if(source.equals(cookie.getValue())){
//         			flag = true;
//         		}       		
//         	}
//         }
//         if(flag == false){
//         	Cookie c = new Cookie("source",source);
//     		c.setMaxAge(60*60*24);
//     		c.setPath("/");
//     		c.setDomain("hshs-fashion.com");
//     		c.setDomain("localhost");
//     		response.addCookie(c);
     		AccessLog accessLog = new AccessLog();	   		
           	accessLog.setSource(source);            
		    accessLog.setUrl(url);
		    accessLogService.insertAccessLog(accessLog);
//         }
         return ResponseUtil.result(0, "OK", null);
    }


}
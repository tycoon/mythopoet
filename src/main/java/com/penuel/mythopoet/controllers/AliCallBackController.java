package com.penuel.mythopoet.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.service.AliPayService;
import com.penuel.mythopoet.service.ItemSkuService;
import com.penuel.mythopoet.service.OrderDetailService;
import com.penuel.mythopoet.service.OrderService;
import com.penuel.mythopoet.service.PayService;
import com.penuel.mythopoet.service.TicketServcie;

@Controller
@RequestMapping("alicallback")
public class AliCallBackController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AliCallBackController.class);

	@Autowired
	private PayService payService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired
	private ItemSkuService itemSkuService;
	@Autowired
	private AliPayService aliPayService;
	
/*	@Autowired
	private TicketServcie ticketServcie;*/
	
	@RequestMapping("/recall")
	public String alirecall(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		LOGGER.info("AliCallBackController。alirecall alipay return callback");
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			
			params.put(name, valueStr);
		}
	
		try {
			if(aliPayService.verify(params)){
				LOGGER.info("AliCallBackController.alirecall verify OK");
				String out_trade_no = request.getParameter("out_trade_no");
				String trade_status = request.getParameter("trade_status");
				Orders order = orderService.getByViewId(out_trade_no);
				if (order != null) {
					if (out_trade_no != null && out_trade_no.equals(order.getViewId())) {
						if ("TRADE_SUCCESS".equals(trade_status)
								|| "TRADE_FINISHED".equals(trade_status)) {// 支付成功
							response.getWriter().write("success");
							 //ticketServcie.haveUsed(order.getId());//将券状态设置为已经使用
							
						}
					}
	
				}
			}else{
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/order/page";
	}
	
	@RequestMapping("/recalltwo")
	public String alirecall_two(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		LOGGER.info("AliCallBackController.alirecall_two alipay notify callback");
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			
			params.put(name, valueStr);
		}
		
		try {
			if(aliPayService.verify(params)){
				LOGGER.info("AliCallBackController.alirecall_two verify OK");
				String out_trade_no = request.getParameter("out_trade_no");
				String trade_status = request.getParameter("trade_status");
				Orders order = orderService.getByViewId(out_trade_no);
				if (order != null) {
					if (out_trade_no != null && out_trade_no.equals(order.getViewId())) {
						// 0=未支付,1=支付中,4=支付完成,5=支付失败,6=取消支付,7=转入退款
						if ("TRADE_SUCCESS".equals(trade_status)
								|| "TRADE_FINISHED".equals(trade_status)) {// 支付成功
							if (order.getPayStatus() < 4 && order.getStatus() < 4) {
								order.setPayStatus(4);
								order.setStatus(1);
								itemSkuService.reduceSku(order);// 支付成功减少库存
								response.getWriter().write("success");
								//ticketServcie.haveUsed(order.getId());
							}
						} else {
							order.setPayStatus(5);
						}
						orderService.payOrder(order);
	
					}
	
				}
			}else{
				response.getWriter().write("fail");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	@RequestMapping("/pay")
	public String pay(@RequestParam("userId") Long userId,
			@RequestParam("orderId") Long orderId, HttpServletRequest request,HttpServletResponse response,
			Model model) {
		boolean isWeixin = false;
		String ua = request.getHeader("user-agent");
		if(ua.contains("MicroMessenger")){
			isWeixin = true;
		}

		String htmlForm = aliPayService.commitOrder(userId, orderId);
		request.setAttribute("htmlForm", htmlForm);
		request.setAttribute("isWeixin", isWeixin);
		return "order/pay_ali";
	}

	@RequestMapping("/notify")
	public String alinotify(Model model, HttpServletRequest request,
			HttpServletResponse response) {

		Orders order;
		try {
			String out_trade_no = request.getParameter("out_trade_no");
			String trade_status = request.getParameter("trade_status");
			order = orderService.getByViewId(out_trade_no);

			if (order != null) {
				if (out_trade_no != null && out_trade_no.equals("")) {
					// 0=未支付,1=支付中,4=支付完成,5=支付失败,6=取消支付,7=转入退款
					if ("TRADE_SUCCESS".equals(trade_status)
							|| "TRADE_FINISHED".equals(trade_status)) {// 支付成功
						if (order.getPayStatus() < 4 && order.getStatus() < 4) {
							order.setPayStatus(4);
							order.setStatus(1);
							itemSkuService.reduceSku(order);// 支付成功减少库存
							response.getWriter().write("SUCCESS");
						}
					} else {
						order.setPayStatus(5);
					}
					orderService.payOrder(order);

				}

			}

		} catch (Exception e) {

		}
		return "redirect:/new";
	}

}

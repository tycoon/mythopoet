package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.model.Designer;
import com.penuel.mythopoet.model.Favorites;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.FavoritesView;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.DesignerService;
import com.penuel.mythopoet.service.FavoriteService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.utils.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * FavoritesController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:51
 * Desc:
 */

@Controller
@RequestMapping("/favorites")
@LoginRequired
public class FavoritesController {


    private static final Logger LOGGER = LoggerFactory.getLogger(FavoritesController.class);

    @Autowired
    private FavoriteService favoriteService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private DesignerService designerService;
    @Autowired
    private ItemBrandService itemBrandService;

    /**
     * 获取订单列表
     *
     * @param model
     * @param userId   用户ID
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String list(Model model, @RequestParam("userId") Long userId,@RequestParam(value = "type",defaultValue = "0") int type,
                       @RequestParam(value = "pageNum",defaultValue = "1") int pageNum, @RequestParam(value = "pageSize",defaultValue = "20") int pageSize) {
        List<FavoritesView> favoritesViewList;
        try {
            List<Favorites>  favoritesList = favoriteService.listByUserId(userId,type,(pageNum - 1) * pageSize, pageSize);
            if ( type == 1 ){
                favoritesViewList = favoriteService.fillDesignerInfoByFavorites(favoritesList);
            }else{
                favoritesViewList = favoriteService.fillItemInfoByFavorites(favoritesList);
            }
        } catch (Exception e) {
            LOGGER.error("FavoritesController.list Error:userId=" + userId + ",pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取收藏列表失败", null);
        }
        return ResponseUtil.result(0, "OK", favoritesViewList);
    }

    /**
     *  添加收藏
     * @param model
     * @param userId
     * @param type
     * @param tid
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String add(Model model, @RequestParam("userId") Long userId,
            @RequestParam(value = "type",defaultValue = "0") int type, @RequestParam("tid") Long tid) {
        try {
            Favorites favorites = favoriteService.getByUserIdAndTidAndType(userId,tid,type);
            if ( favorites != null ){
                return ResponseUtil.result(1, "您已经收藏过它了", null);
            }
            favorites = new Favorites();
            favorites.setUserId(userId);
            favorites.setTid(tid);
            favorites.setType(type);
            int result = favoriteService.add(favorites);
            if ( result > 0 ){
                return ResponseUtil.result(0, "恭喜您，收藏成功", favorites);
            }
            return ResponseUtil.result(1, "收藏失败，请重试", null);
        } catch (Exception e) {
            LOGGER.error("FavoritesController.add Error:userId=" + userId + ",userId=" + userId + ",tid=" + tid+",type="+type, e);
            return ResponseUtil.result(1, "添加收藏失败", null);
        }
    }


    @RequestMapping(value = "{favoritesId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String detail(Model model, @RequestParam("userId") Long userId,
            @PathVariable Long favoritesId) {
        try {
            Favorites favorites = favoriteService.getById(favoritesId,userId);
            if ( favorites == null ){
                return ResponseUtil.result(1, "该收藏不存在", null);
            }
            if ( favorites.getType() == 0 ){
                Item item = itemService.getById(favorites.getTid());
                ItemView itemView = new ItemView();
                BeanUtils.copyProperties(item,itemView);
                itemService.fillSkuInfo(itemView);
                itemBrandService.fillBrandInfo(itemView);
                return ResponseUtil.result(0, "OK", itemView);
            }else{
                Designer designer = designerService.getById(favorites.getTid());
                return ResponseUtil.result(0, "OK", designer);
            }
        } catch (Exception e) {
            LOGGER.error("FavoritesController.add Error:userId=" + userId + ",userId=" + userId, e);
            return ResponseUtil.result(1, "获取收藏详情失败", null);
        }
    }

    /**
     * 删除收藏
     * @param model
     * @param favoritesId
     * @param userId
     * @return
     */
    @RequestMapping(value = "{favoritesId}/delete", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String delete(Model model,@PathVariable Long favoritesId, @RequestParam("userId") Long userId) {
        try {
            int result = favoriteService.deleteById(favoritesId,userId);
            if ( result > 0 ){
                return ResponseUtil.result(0, "删除成功", "");
            }
            return ResponseUtil.result(1, "删除失败，请重试", null);
        } catch (Exception e) {
            LOGGER.error("FavoritesController.delete Error:userId=" + userId + ",userId=" + userId, e);
            return ResponseUtil.result(1, "删除收藏失败", null);
        }
    }


    @RequestMapping( value = "" )
    public String listPage(HttpServletRequest request, Model model) {

        return "mine/favorites";
    }

}

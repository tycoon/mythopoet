package com.penuel.mythopoet.controllers;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.model.Designer;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.Tag;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.modelView.Page;
import com.penuel.mythopoet.service.DesignerService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.service.TagService;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * SearchController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/29 下午11:57
 * Desc:
 */

@Controller @RequestMapping( "/search" )
public class SearchController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private DesignerService designerService;
    @Autowired
    private TagService tagService;
    @Autowired
    private ItemBrandService itemBrandService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    @RequestMapping( value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String search(Model model, @RequestParam( value = "key", defaultValue = "" ) String key,
            @RequestParam( value = "pageNum", defaultValue = "1" ) int pageNum, @RequestParam( value = "pageSize", defaultValue = "20" ) int pageSize) {

        List<ItemView> itemViewList = new ArrayList<ItemView>();
        Page page = new Page(pageNum,pageSize);
        try {
            //List<Item> itemList = searchByKey(key,page);
            List<Item> itemList = itemService.searchByName(key, pageNum, pageSize);
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item,itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("SearchController.search Error:key=" + key + ",pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "搜索失败", null);
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }

    @RequestMapping( value = "index" )
    public String index(Model model, HttpServletRequest request, @RequestParam( value = "key", defaultValue = "" ) String key) {
    	request.setAttribute("key", key);
        return "search/list";
    }


    private List<Item> searchByKey(String key,Page page){

        /**煞笔产品 搜索非要把设计师的商品都搜出来，又不出分页规则，只能全部取出来，等数据量大了这是个大坑。*/
        List<Item> itemList = itemService.searchByName(key, 0, Integer.MAX_VALUE);
        if ( CollectionUtils.isEmpty(itemList) ) {
            itemList = new ArrayList<Item>();
        }
        List<Designer> designerList = designerService.searchByName(key);
        if ( !CollectionUtils.isEmpty(designerList) ) {
            /** 煞笔产品 搜索非要把设计师的商品都搜出来，又不出分页规则，只能全部取出来，再limit，也懒得做批量优化了。*/
            for ( Designer designer : designerList ) {
                List<Item> designerItemList = itemService.getAllByDesignerId(designer.getId());
                itemList.addAll(designerItemList);
            }
        }

        /**搜索tag*/
        List<Tag> tagList = tagService.searchByKey(key);
        if ( !CollectionUtils.isEmpty(tagList) ) {
            for ( Tag tag : tagList ) {
                List<Item> tagItemList = itemService.getAllByTagId(tag.getId());
                itemList.addAll(tagItemList);
            }
        }

        itemList = uniqItem(itemList);
        page.setTotalCount(itemList.size());
        /** 取消分页，全量加载，产品的要求，需要把搜索结果全部展现出来 */
        int offset = (page.getCurPageNum() - 1) * page.getPageSize();
        int maxCount = page.getCurPageNum() * page.getPageSize();
        LOGGER.info("SearchController.search Info:key=" + key + ",page=" + JSONObject.toJSONString(page)  + ",but itemList.size=" + itemList.size());
        if ( itemList.size() > maxCount ) {
            itemList = itemList.subList(offset, maxCount);
        } else if ( itemList.size() <= offset ) {
            itemList = new ArrayList<Item>();
        } else {
            itemList = itemList.subList(offset, itemList.size());
        }

        return itemList;
    }

    private List<Item> uniqItem(List<Item> itemList) {
        if ( CollectionUtils.isEmpty(itemList) ) {
            return itemList;
        }
        List<Long> itemIdList = new ArrayList<Long>();
        for ( int i = itemList.size() - 1; i >= 0; i-- ) {
            Item item = itemList.get(i);
            if ( itemIdList.contains(item.getId()) ) {
                itemList.remove(i);
            } else {
                itemIdList.add(item.getId());
            }
        }
        return itemList;
    }
}
package com.penuel.mythopoet.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.OrderDetail;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.service.AliPayService;
import com.penuel.mythopoet.service.ItemSkuService;
import com.penuel.mythopoet.service.OrderDetailService;
import com.penuel.mythopoet.service.OrderService;
import com.penuel.mythopoet.service.PayService;
import com.penuel.mythopoet.service.TicketServcie;
import com.penuel.mythopoet.utils.ResponseUtil;

@Controller
@RequestMapping("alipay")
@LoginRequired
public class AliPayController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AliPayController.class);

	@Autowired
	private PayService payService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired
	private ItemSkuService itemSkuService;
	@Autowired
	private AliPayService aliPayService;


	@RequestMapping(value = "/prepay", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String prepay(HttpServletRequest request, Model model,
			@CookieValue("userId") Long userId,
			@RequestParam("orderId") Long orderId,
			@RequestParam(value = "payType", defaultValue = "0") int payType) {

		Orders order;
		try {
			order = orderService.getById(userId, orderId);
			List<OrderDetail> orderDetailList = orderDetailService
					.getByOrderId(orderId);
			if (order == null || CollectionUtils.isEmpty(orderDetailList)) {
				return ResponseUtil.result(1, "该订单不存在", "");
			}
			if (order.getAddressId() == 0) {
				return ResponseUtil.result(1, "未选择收货地址，请确认", "");
			}
			for (OrderDetail orderDetail : orderDetailList) {
				ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(
						orderDetail.getItemId(), orderDetail.getProps());
				if (itemSku == null
						|| itemSku.getCount() < orderDetail.getCount()) {
					int haveCount = itemSku == null ? 0 : itemSku.getCount();
					return ResponseUtil.result(1, "下手太慢了，【" + itemSku.getName()
							+ "】商品已被其他人抢先支付了，剩余" + haveCount + "件", null);
				}
			}

			return ResponseUtil.result(0, "OK", "");

		} catch (Exception e) {
			LOGGER.error("OrderController.detail Error:userId=" + userId
					+ ",ordrId=" + orderId, e);
			return ResponseUtil.result(1, "支付失败", null);
		}
	}

	@RequestMapping("/pay")
	public String pay(@RequestParam("userId") Long userId,
			@RequestParam("orderId") Long orderId,
			@RequestParam("pword") String pword, HttpServletRequest request,
			Model model) {
		boolean isWeixin = false;
		String ua = request.getHeader("user-agent");
		if (ua!=null&&ua.contains("MicroMessenger")) {
			isWeixin = true;
		}

		String htmlForm = aliPayService.commitOrder(userId, orderId);
		model.addAttribute("htmlForm", htmlForm);
		model.addAttribute("isWeixin", isWeixin);
		
		return "order/pay_ali";
	}

}

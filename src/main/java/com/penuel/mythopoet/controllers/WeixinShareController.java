package com.penuel.mythopoet.controllers;

import java.util.Date;
import java.util.SortedMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.model.WxAuthUser;
import com.penuel.mythopoet.service.WxAuthService;
import com.penuel.mythopoet.utils.ResponseUtil;

@Controller
@RequestMapping("/share")
//@LoginRequired
public class WeixinShareController {
	 private static final Logger LOGGER = LoggerFactory.getLogger(WeixinShareController.class);

	    @Autowired
	    private WxAuthService wxAuthService;

	    /**
	     * 获取wx code的url回调地址
	     *
	     * @param model
	     * @param code  返回需要的code,每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期<br/>
	     *              若用户禁止授权，则重定向后不会带上code参数
	     * @param state orderId@userId 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
	     * @return 
	     */
	    @RequestMapping(value = "/wxcode", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	    public String wxcode(Model model, @RequestParam(value = "code", defaultValue = "") String code,
	            @RequestParam(value = "state", defaultValue = "") String state,HttpServletRequest request, HttpServletResponse response) {

	        long userId = 0;
	        String currentUrl = "/";
	        try {
	        	String[] stateArray = state.split("@");
	            userId = NumberUtils.toLong(stateArray[1]);
	            currentUrl = stateArray[0];
	            if ( StringUtils.isBlank(code) ) {
	                return "redirect:"+currentUrl;
	            }
	            LOGGER.info("wxcode:code=" + code + ",state=" + state);
	            WxAuthUser wxAuthUser = wxAuthService.getAccessToken(code, userId);
				return "redirect:"+currentUrl;
	        } catch ( Exception e ) {
	            LOGGER.error("WeixinShareController.wxcode Error:code=" + code + ",state=" + state, e);
	        }
	        return "redirect:"+currentUrl;
	    }

	    @RequestMapping( value = "/ticket", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
	    @ResponseBody
	    public String jsapiTicket(Model model, @CookieValue( "userId" ) Long userId,@RequestParam("currentUrl") String currentUrl) {

	        try {
	           WxAuthUser wxAuthUser = wxAuthService.getByUserId(userId);
	            if ( wxAuthUser == null ){
	                String authUrl = wxAuthService.authorizeURL(userId, currentUrl);
	                return ResponseUtil.result(1001, "微信授权", authUrl);
	            }

	            boolean refreshTicket = false;
	            long millSecond = (new Date()).getTime() - wxAuthUser.getJsapiRefreshTime().getTime();
	            if ( StringUtils.isBlank(wxAuthUser.getJsapiTicket()) ){
	                refreshTicket = true;
	            }else if( millSecond + 5*60*1000 > wxAuthUser.getJsapiExpiresIn()*1000 ){
	                refreshTicket = true;
	            }
	            LOGGER.info("WeixinShareController.jsapiTicket.wxAuthUser ="+JSONObject.toJSONString(wxAuthUser)+",millSecond="+millSecond);
	            if ( refreshTicket ){
	                wxAuthService.refreshJsapiTicket(wxAuthUser);
	            }
	            SortedMap<Object, Object> map = wxAuthService.fillJsapiTicketParam(wxAuthUser.getJsapiTicket(), currentUrl);
	            LOGGER.info("WeixinShareController.jsapiTicket  result="+ JSONObject.toJSONString(map));
	            return ResponseUtil.result(0,"OK",map);
	        } catch ( Exception e ) {
	            LOGGER.error("WeixinShareController.jsapiTicket Error:userId=" + userId , e);
	            return ResponseUtil.result(1, "获取jsAPI初始化参数失败", null);
	        }
	    }

}

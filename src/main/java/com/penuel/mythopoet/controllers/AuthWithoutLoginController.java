package com.penuel.mythopoet.controllers;

import java.util.Date;
import java.util.SortedMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.model.WxAuthUser;
import com.penuel.mythopoet.service.WxAuthWithoutUserService;
import com.penuel.mythopoet.utils.ResponseUtil;

@Controller
@RequestMapping("/authWithoutLogin")
public class AuthWithoutLoginController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AuthWithoutLoginController.class);

	@Autowired
	private WxAuthWithoutUserService wxAuthService;

	/**
	 * 获取wx code的url回调地址
	 *
	 * @param model
	 * @param code
	 *            返回需要的code,每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期<br/>
	 *            若用户禁止授权，则重定向后不会带上code参数
	 * @param state userId,currentUrl
	 * @return  返回当前页面
	 */
	@RequestMapping(value = "/wxcode", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	public String wxcode(Model model,
			@RequestParam(value = "code", defaultValue = "") String code,
			@RequestParam(value = "state", defaultValue = "") String state,
			HttpServletRequest request, HttpServletResponse response) {

		long userId = -1;
		String currentUrl="/";
		try {
			
			//String[] stateArray = state.split("@");
            //userId = NumberUtils.toLong(stateArray[0]);
            //currentUrl = stateArray[1];
			
            if (StringUtils.isBlank(code)) {
            	return "redirect:"+currentUrl;
			}
			WxAuthUser wxAuthUser = wxAuthService.getAccessToken(code, userId);
			boolean flag = false;
			Cookie[] cookies = request.getCookies();
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if ("openId".equals(cookie.getName())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				Cookie c = new Cookie("openId", wxAuthUser.getOpenId());
				c.setMaxAge(1000); // set expire time to 1000 sec
				c.setPath("/");
				response.addCookie(c); // put cookie in response
				LOGGER.info("authWithoutLogin.wxcode : add cookie openId ="+wxAuthUser.getOpenId());
			}

			return "redirect:"+currentUrl;
		} catch (Exception e) {
			LOGGER.error("AuthWithoutLoginController.wxcode Error:code=" + code
					+ ",state=" + state, e);
		}
		return "redirect:"+currentUrl;
	}

	@RequestMapping(value = "/ticket", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String jsapiTicket(Model model, @RequestParam("openId") String openId,@RequestParam("userId") long userId,@RequestParam("currentUrl") String currentUrl) {

		try {
			WxAuthUser wxAuthUser;
			if (openId==null||openId.trim().equals("")) {
				String authUrl = wxAuthService.authorizeURL(userId,currentUrl);
				return ResponseUtil.result(1001, "微信授权", authUrl);
			}else{
				wxAuthUser = wxAuthService.getByOpenId(openId);
				if(wxAuthUser==null){
					String authUrl = wxAuthService.authorizeURL(userId,currentUrl);
					return ResponseUtil.result(1001, "微信授权", authUrl);
				}
			}

			boolean refreshTicket = false;
			long millSecond = (new Date()).getTime()
					- wxAuthUser.getJsapiRefreshTime().getTime();
			if (StringUtils.isBlank(wxAuthUser.getJsapiTicket())) {
				refreshTicket = true;
			} else if (millSecond + 5 * 60 * 1000 > wxAuthUser
					.getJsapiExpiresIn() * 1000) {
				refreshTicket = true;
			}
			LOGGER.info("jsapiTicket.wxAuthUser ="
					+ JSONObject.toJSONString(wxAuthUser) + ",millSecond="
					+ millSecond);
			if (refreshTicket) {
				wxAuthService.refreshJsapiTicket(wxAuthUser);
			}
			SortedMap<Object, Object> map = wxAuthService.fillJsapiTicketParam(
					wxAuthUser.getJsapiTicket(), currentUrl);
			LOGGER.info("AuthController.jsapiTicket  result="
					+ JSONObject.toJSONString(map));
			return ResponseUtil.result(0, "OK", map);
		} catch (Exception e) {
			LOGGER.error("AuthController.jsapiTicket Error:userId=" + userId, e);
			return ResponseUtil.result(1, "获取jsAPI初始化参数失败", null);
		}
	}

}
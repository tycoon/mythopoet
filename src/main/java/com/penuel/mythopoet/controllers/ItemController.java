package com.penuel.mythopoet.controllers;  

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.penuel.mythopoet.model.AccessLog;
import com.penuel.mythopoet.model.Favorites;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.TagRelation;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.AccessLogService;
import com.penuel.mythopoet.service.FavoriteService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemDesignerService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.service.TagService;
import com.penuel.mythopoet.utils.ResponseUtil;

/**
 * ItemController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:45
 * Desc:
 */
@Controller
@RequestMapping("item")
public class ItemController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    private ItemService itemService;
    @Autowired
    private FavoriteService favoriteService;
    @Autowired
    private ItemBrandService itemBrandService;
    @Autowired
    private ItemDesignerService itemDesignerService;
    @Autowired
    private TagService tagService;
    @Autowired
    private AccessLogService accessLogService;
    
    @RequestMapping( "detail/{id}" )
    public String detailPage(@PathVariable Long id,@CookieValue( value = "userId",required = false) Long userId, HttpServletRequest request, Model model) {

        ItemView itemView = null;
        try {
            if ( null == id ) {
                return ResponseUtil.result(1, "无此商品001", "");
            }
            Item item = itemService.getById(id);
            itemView = new ItemView();
            BeanUtils.copyProperties(item, itemView);
            itemService.fillSkuInfo(itemView);
            itemBrandService.fillBrandInfo(itemView);
            itemDesignerService.fillDesignerInfo(itemView);
            if ( userId != null ) {
                Favorites favorites = favoriteService.getByTidAndUserId(id, 0, userId);
                if ( favorites!=null ){
                    itemView.setFavoritesId(favorites.getId());
                }
            }
            TagRelation tagRelation = tagService.getByTagAndItem(1, id);
            if ( null != tagRelation ){
                itemView.setSexTagId(1);
            }else{
                itemView.setSexTagId(2);
            }
        } catch ( Exception e ) {
            LOGGER.error("ItemController.detail Error:itemId=" + id, e);
            return "/item/error";
        }
        model.addAttribute("detail", itemView);
        return "item/detail";
    }


    @RequestMapping( "detail/{id}/{source}" )
    public String detailPage1(@PathVariable Long id, @PathVariable String source,
    		@CookieValue( value = "userId",required = false) Long userId, 
    		HttpServletRequest request, HttpServletResponse response,Model model) {      
        
        
        return "redirect:/item/detail/"+id;
    }
    @RequestMapping( value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String detail(Model model, @RequestParam( value = "id",defaultValue = "0") Long id) {

        ItemView itemView = null;
        try {
            if ( null == id  || id == 0) {
                return ResponseUtil.result(1, "无此商品001", "");
            }
            Item item = itemService.getById(id);
            itemView = new ItemView();
            BeanUtils.copyProperties(item, itemView);
            itemService.fillSkuInfo(itemView);
            itemBrandService.fillBrandInfo(itemView);
            itemDesignerService.fillDescriptionInfo(itemView);
        } catch ( Exception e ) {
            LOGGER.error("ItemController.detail Error:itemId=" + id, e);
        }
        return ResponseUtil.result(0, "OK", itemView);
    }

    /**
     * 热销
     *
     * @param model
     * @return
     */
    @RequestMapping( value = "list/hot", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String hotList(Model model, @RequestParam( value = "pageNum",defaultValue = "1") int pageNum, @RequestParam( value = "pageSize" ,defaultValue = "20") int pageSize,
            @RequestParam( value = "tagId" ,defaultValue = "1") long tagId) {

        List<ItemView> itemList;
        try {
            itemList = itemService.hotList((pageNum - 1) * pageSize, pageSize, tagId);
            if ( !CollectionUtils.isEmpty(itemList) ){
                for ( ItemView itemView : itemList ){
                    itemService.fillSkuInfo(itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemDesignerService.fillDescriptionInfo(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("ItemController.hotList Error:pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取热销列表失败", null);
        }
        return ResponseUtil.result(0, "OK", itemList);
    }

    /**
     * 新品
     *
     * @param model
     * @return
     */
    @RequestMapping( value = "/list/new", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String newList(Model model, @RequestParam( value = "pageNum",defaultValue = "1") int pageNum, @RequestParam( value = "pageSize",defaultValue = "20") int pageSize,
            @RequestParam( value = "tagId" ,defaultValue = "1") long tagId) {

        List<ItemView> itemViewList = new ArrayList<ItemView>();
        try {
            List<Item> itemList = itemService.newList((pageNum - 1) * pageSize, pageSize, tagId);
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemService.fillSkuInfo(itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemDesignerService.fillDescriptionInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("ItemController.newList Error:pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取新品列表失败", null);
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }

    /**
     * 折扣
     *
     * @param model
     * @return
     */
    @RequestMapping( value = "/list/discount", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String discountList(Model model, @RequestParam( value = "pageNum",defaultValue = "1") int pageNum, @RequestParam( value = "pageSize",defaultValue = "20") int pageSize,
            @RequestParam( value = "tagId" ,defaultValue = "1") long tagId) {

        List<ItemView> itemViewList = new ArrayList<ItemView>();
        try {
            List<Item> itemList = itemService.discountList((pageNum - 1) * pageSize, pageSize,tagId);
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemService.fillSkuInfo(itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemDesignerService.fillDescriptionInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("ItemController.discountList Error:pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取折扣列表失败", null);
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }


}
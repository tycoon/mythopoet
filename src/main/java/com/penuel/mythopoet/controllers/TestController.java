package com.penuel.mythopoet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * TestController Created with mythopoet.
 * User: Morry (morryxia@gmail.com)
 * Date: 15/4/8 下午午8:46
 * Desc:
 */

// todo 这是发现controller 吗？＝ ＝
@Controller
@RequestMapping("/test")
public class TestController {

  @RequestMapping("/color")
  public String colorTest(HttpServletRequest request, Model model){
      return "test/color";
  }
}

package com.penuel.mythopoet.controllers;   

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.CneeAddress;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.modelView.CartDetailView;
import com.penuel.mythopoet.modelView.OrderView;
import com.penuel.mythopoet.service.*;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CartController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:44
 * Desc:
 */
@Controller
@RequestMapping( "/cart" )
public class CartController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    @Autowired
    private CartDetailService cartDetailService;
    @Autowired
    private ItemSkuService itemSkuService;
    @Autowired
    private ItemPropValueService itemPropValueService;
    @Autowired
    private ItemBrandService itemBrandService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private CneeAddressService cneeAddressService;

    @RequestMapping( value = "" )
    public String index(HttpServletRequest request, Model model) {

        return "cart/cart";
    }

    @RequestMapping( value = "/{userId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String list(HttpServletRequest request, Model model, @PathVariable Long userId,@RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize",defaultValue = "20") int pageSize) {
        List<CartDetailView> cartDetailViewList = new ArrayList<CartDetailView>();
        try {
            List<CartDetail> cartDetailList = cartDetailService.getByUserId(userId,(pageNum-1)*pageSize,pageSize);
            if ( !CollectionUtils.isEmpty(cartDetailList) ){
                CartDetailView cartDetailView;
                for ( CartDetail cartDetail:cartDetailList ){
                    cartDetailView = new CartDetailView();
                    BeanUtils.copyProperties(cartDetail,cartDetailView);
                    cartDetailView.setPropsValue(itemPropValueService.getPropValueByIdStr(cartDetailView.getProps()));
                    ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(cartDetailView.getItemId(),cartDetailView.getProps());
                    Item item = itemService.getById(itemSku.getItemId());
                    if ( null != item ) {//产品更改逻辑，不取库存商品的这些信息，取原商品信息
                        cartDetailView.setItemName(item.getName());
                        cartDetailView.setCurrentPrice(item.getPrice());
                        cartDetailView.setPic(item.getPic());
                        cartDetailView.setOriginPrice(item.getOriginPrice());
                    }
                    if ( itemSku == null  ){
                        cartDetailView.setSkuCount(0);
                    }else {
                        cartDetailView.setSkuCount(itemSku.getCount());
                    }
                    cartDetailView.setBrandName(itemBrandService.getBranNameByItemId(cartDetail.getItemId()));
                    cartDetailViewList.add(cartDetailView);
                }
            }
        }catch ( Exception e ){
            LOGGER.error("CartController.list ERROR:userId="+userId,e);
            return ResponseUtil.result(1, "获取购物车详情失败", null);
        }
        return ResponseUtil.result(0, "OK", cartDetailViewList);
    }

    /**
     * 从商品详情页新增一个商品到购物车
     *
     * @param request
     * @param model
     * @param userId
     * @param props
     * @param itemId
     * @param count
     * @return
     */
    @RequestMapping( value = "/{userId}/addItem", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String addCart(HttpServletRequest request, Model model,
            @PathVariable Long userId,
            @RequestParam( value = "props", defaultValue = "" ) String props,
            @RequestParam( value = "itemId", defaultValue = "0" ) long itemId,
            @RequestParam( value = "count", defaultValue = "1" ) int count) {

        ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(itemId, props);
        if ( null == itemSku || itemSku.getCount() < count ) {
            return ResponseUtil.result(1, "添加失败，库存不足", "");
        }
        int result = cartDetailService.addItem(userId, itemSku, count);
        if ( result > 0 ) {
            return ResponseUtil.result(0, "添加成功", "");
        } else {
            return ResponseUtil.result(1, "添加失败", "");
        }
    }

    /**
     * 更新购物车商品信息
     *
     * @param request
     * @param model
     * @param userId
     * @param cartDetailId 用户Id
     * @param count        最终购买的数量
     * @return
     */
    @RequestMapping( value = "/{userId}/update", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String updateCart(HttpServletRequest request, Model model,
            @PathVariable Long userId,
            @RequestParam( value = "cartDetailId", defaultValue = "0" ) long cartDetailId,
            @RequestParam( value = "count", defaultValue = "1" ) int count) {

        CartDetail cartDetail = cartDetailService.getById(cartDetailId,userId);
        if ( null == cartDetail ) {
            return ResponseUtil.result(2, "商品已经被移除，请重试", "");
        }
        ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(cartDetail.getItemId(), cartDetail.getProps());
        if ( null == itemSku || itemSku.getCount() < count ) {
            return ResponseUtil.result(3, "更新失败，库存不足", "");
        }
        Item item = itemService.getById(itemSku.getItemId());
        cartDetail.setCount(count);
        cartDetail.setCartPrice(item.getPrice());
        cartDetail.setOriginPrice(item.getOriginPrice());
        int result = cartDetailService.update(cartDetail);
        if ( result > 0 ) {
            return ResponseUtil.result(0, "操作成功", "");
        } else {
            return ResponseUtil.result(1, "操作失败，请重试", "");
        }
    }

    @RequestMapping( value = "/{userId}/clean", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String cleanCart(HttpServletRequest request, Model model,
            @PathVariable Long userId) {

        cartDetailService.cleanCart(userId);
        return ResponseUtil.result(0, "清空购物车成功", "");
    }
    
    @RequestMapping( value = "/getorder", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) 
    @ResponseBody
    public String getOrder(Model model, @CookieValue( "userId" ) Long userId, 
    		@RequestParam( "cartDetailIds" ) String cartDetailIds,
    		@RequestParam( value = "addressId" ,defaultValue = "0") Long addressId) {
    	List<CartDetailView> cartDetailViewList = new ArrayList<CartDetailView>();
    	CneeAddress cneeAddress = null;
        try{
	        List<String> cartDetailIdStringList = Arrays.asList(cartDetailIds.split(","));
	        if ( CollectionUtils.isEmpty(cartDetailIdStringList) ) {
	            LOGGER.info("OrderController.getorder : have no item");
	            return "";
	        }
	        String errMsg = "";
	        LOGGER.info("OrderController.getorder userId=" + userId + ",cartDetailIds=" + cartDetailIds );
	        CartDetailView cartDetailView;
	        for ( String cartDetailIdString : cartDetailIdStringList ) {
	            CartDetail cartDetail = cartDetailService.getById(NumberUtils.toLong(cartDetailIdString), userId);
	            if ( cartDetail == null ) {
	                errMsg = "用户信息异常，请退出重新登陆";
	                break;
	            } else {              
	                cartDetailView = new CartDetailView();
	                BeanUtils.copyProperties(cartDetail,cartDetailView);
	                cartDetailView.setPropsValue(itemPropValueService.getPropValueByIdStr(cartDetailView.getProps()));
	                ItemSku itemS = itemSkuService.getValidSkuByItemAndProps(cartDetailView.getItemId(),cartDetailView.getProps());
	                Item item = itemService.getById(itemS.getItemId());
	                if(addressId == 0){
	                	cneeAddress = cneeAddressService.getDefault(userId);
	                }else{
	                	cneeAddress = cneeAddressService.getById(userId, addressId);
	                }
	                 
	                if(cneeAddress != null){
	                	cartDetailView.setCneeAddress(cneeAddress);
	                }
	                if ( null != item ) {//产品更改逻辑，不取库存商品的这些信息，取原商品信息
	                    cartDetailView.setItemName(item.getName());
	                    cartDetailView.setCurrentPrice(item.getPrice());
	                    cartDetailView.setPic(item.getPic());
	                    cartDetailView.setOriginPrice(item.getOriginPrice());
	                }
	                if ( itemS == null  ){
	                    cartDetailView.setSkuCount(0);
	                }else {
	                    cartDetailView.setSkuCount(itemS.getCount());
	                }
	                cartDetailView.setBrandName(itemBrandService.getBranNameByItemId(cartDetail.getItemId()));
	                cartDetailViewList.add(cartDetailView);     
	            }
	        }
	    }catch ( Exception e ){
	        LOGGER.error("CartController.list ERROR:userId="+userId,e);
	        return ResponseUtil.result(1, "获取购物车详情失败", null);
	    }
        model.addAttribute("cartDetailIds", cartDetailIds);
	    return ResponseUtil.result(0, "OK", cartDetailViewList);
    }
    
    @RequestMapping( value = "/getSorder", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) 
    @ResponseBody
    public String getPay(Model model, @CookieValue( "userId" ) Long userId, 
    		@RequestParam( "itemId" ) long itemId,@RequestParam( "count" ) int count,
    		@RequestParam( "props" ) String props) {
    	List<CartDetailView> cartDetailViewList = new ArrayList<CartDetailView>();
    	CneeAddress cneeAddress = null;
        try{
	        CartDetailView cartDetailView = null;
            if ( userId == null ) {
                return ResponseUtil.result(1, "用户信息异常，请退出重新登陆", null);
            } else {    
            	Item item = itemService.getById(itemId);
                cartDetailView = new CartDetailView();
                cartDetailView.setPropsValue(itemPropValueService.getPropValueByIdStr(props));
                ItemSku itemS = itemSkuService.getValidSkuByItemAndProps(itemId,props);
                
                cneeAddress = cneeAddressService.getDefault(userId);
               
                 
                if(cneeAddress != null){
                	cartDetailView.setCneeAddress(cneeAddress);
                }
                if ( null != item ) {//产品更改逻辑，不取库存商品的这些信息，取原商品信息
                    cartDetailView.setItemName(item.getName());
                    cartDetailView.setCurrentPrice(item.getPrice());
                    cartDetailView.setCartPrice(item.getPrice());
                    cartDetailView.setPic(item.getPic());
                    cartDetailView.setOriginPrice(item.getOriginPrice());
                    cartDetailView.setCount(count);
                }
                if ( itemS == null  ){
                    cartDetailView.setSkuCount(0);
                }else {
                    cartDetailView.setSkuCount(itemS.getCount());
                }
                cartDetailView.setBrandName(itemBrandService.getBranNameByItemId(itemId));
                cartDetailViewList.add(cartDetailView);     
            }
	        
	    }catch ( Exception e ){
	        LOGGER.error("CartController.list ERROR:userId="+userId,e);
	        return ResponseUtil.result(1, "获取购物车详情失败", null);
	    }
	    return ResponseUtil.result(0, "OK", cartDetailViewList);
    }
    
    @RequestMapping( value = "/selectedcart")
    public String selectedcart(Model model) {
        return "/cart/getorder";
    }
}
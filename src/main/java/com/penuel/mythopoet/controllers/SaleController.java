package com.penuel.mythopoet.controllers; 

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.model.Customer;
import com.penuel.mythopoet.model.CustomerLevel;
import com.penuel.mythopoet.model.MyTicket;
import com.penuel.mythopoet.model.Ticket;
import com.penuel.mythopoet.service.CustomerLevelService;
import com.penuel.mythopoet.service.CustomerService;
import com.penuel.mythopoet.service.MyTicketServcie;
import com.penuel.mythopoet.service.TicketServcie;
import com.penuel.mythopoet.utils.ResponseUtil;

@Controller
@RequestMapping("/sale")
@LoginRequired
public class SaleController {

	@Autowired
	TicketServcie ticketServcie;
	@Autowired
	CustomerService customerService;
	@Autowired
	CustomerLevelService customerLevelService;
	@Autowired
	MyTicketServcie myTicketServcie;

	@RequestMapping(value = "/ticket/check", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String checkTicket(@RequestParam("pword") String pword,
			HttpServletRequest request, Model model) {
		Ticket t = ticketServcie.getByPword(pword);
		if(t==null){
			return ResponseUtil.result(3, "请输入正确的激活码！", null);
		}
		if (t.getIsUse() == 1) {
			return ResponseUtil.result(1, "此券已经被使用！", null);
		}

		if (t.getExpDate().compareTo(new Date()) < 0) { // -1 过期
			
			return ResponseUtil.result(2, "此券已经过期！", null);
		}
		
		return ResponseUtil.result(0, "校验成功！", t);	
	}

	@RequestMapping(value = "/ticket")
	public String ticket(HttpServletRequest request, Model model,@CookieValue("userId") Long userId) {

		return "order/ticket";
	}
	
	@RequestMapping(value = "/discount", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String discount(HttpServletRequest request, Model model, @CookieValue("userId") Long userId) {
		Customer customer = customerService.getByUserId(userId);
		CustomerLevel customerLevel = customerLevelService.getByLevel(customer.getLevel());
		if(null == customerLevel){
			return ResponseUtil.result(1, "账户异常！", null);
		}
		return ResponseUtil.result(0, "", customerLevel);
	}
	
	@RequestMapping(value = "/myTicket", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String myTicket(HttpServletRequest request, Model model, @CookieValue("userId") Long userId) {
		List<Ticket> list = myTicketServcie.getMyTicket(userId);
		if(list == null){
			return ResponseUtil.result(1, "优惠券信息异常！", null);
		}
		return ResponseUtil.result(0, "", list);
	}
	
	@RequestMapping(value = "/coupon")
	public String coupon(HttpServletRequest request, Model model,@CookieValue("userId") Long userId) {

		return "mine/coupon";
	}

}

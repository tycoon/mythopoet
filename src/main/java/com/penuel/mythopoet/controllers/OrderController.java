package com.penuel.mythopoet.controllers;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.modelView.OrderView;
import com.penuel.mythopoet.service.*;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * OrderController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:51
 * Desc:
 */

@Controller
@RequestMapping( "order" )
@LoginRequired
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private ItemSkuService itemSkuService;
    @Autowired
    private CartDetailService cartDetailService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private CneeAddressService cneeAddressService;
    @Autowired
    private UserService userService;
    @Autowired
	private TicketServcie ticketServcie;

    /**
     * 获取订单列表
     *
     * @param model
     * @param userId   用户ID
     * @param status   状态 {@see PoetConstants}
     *                 -1=全部，0=待支付 1=待收货 4=已完成 9=已取消
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping( value = "/list", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String list(Model model, @CookieValue( "userId" ) Long userId, @RequestParam( value = "status", defaultValue = "0" ) int status,
            @RequestParam( "pageNum" ) int pageNum, @RequestParam( "pageSize" ) int pageSize) {

        List<OrderView> orderViewList = null;
        try {
            List<Orders> orderList;
            LOGGER.info("OrderController.list userId=" + userId + ",status=" + status + ",pageNum=" + pageNum + ",pageSize=" + pageSize);
            if ( status == PoetConstants.Order_VIEW_WAIT_REC ) {
                orderList = orderService.getWaitRecOrderList(userId, (pageNum - 1) * pageSize, pageSize);
            } else if ( status == PoetConstants.Order_VIEW_ALL ) {
                orderList = orderService.getAllByUserId(userId, (pageNum - 1) * pageSize, pageSize);
            } else {
                orderList = orderService.listByUserIdAndStatus(userId, status, (pageNum - 1) * pageSize, pageSize);
            }
            if ( !CollectionUtils.isEmpty(orderList) ) {
                orderViewList = new ArrayList<OrderView>();
                OrderView orderView;
                for ( Orders orders : orderList ) {
                    orderView = orderService.buildOrderViewByOrder(orders);
                    orderViewList.add(orderView);
                }
            }
            LOGGER.info("OrderController.list Sucess: userId=" + userId + ",status=" + status + ",pageNum=" + pageNum + ",pageSize=" + pageSize+",orderViewList="+JSONObject.toJSONString(orderViewList));
        } catch ( Exception e ) {
            LOGGER.error("OrderController.list Error:userId=" + userId + ",status=" + status + ",pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取订单列表失败", null);
        }
        return ResponseUtil.result(0, "OK", orderViewList);
    }

    @RequestMapping( value = "/{orderId}", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String detail(Model model, @CookieValue( "userId" ) Long userId, @PathVariable Long orderId) {

        OrderView orderView;
        try {
            LOGGER.info("OrderController.detail userId=" + userId + ",orderId=" + orderId);
            Orders order = orderService.getById(userId, orderId);
            orderView = orderService.buildOrderViewByOrder(order);
        } catch ( Exception e ) {
            LOGGER.error("OrderController.detail Error:userId=" + userId + ",orderId=" + orderId, e);
            return ResponseUtil.result(1, "获取订单详情失败", null);
        }
        return ResponseUtil.result(0, "OK", orderView);
    }


    @RequestMapping( value = "/{orderId}/delete", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String deleteOrder(Model model, @CookieValue( "userId" ) Long userId, @PathVariable Long orderId) {

        try {
            LOGGER.info("OrderController.delete userId=" + userId + ",orderId=" + orderId);
            int result = orderService.deleteById(userId, orderId);
            if ( result < 1 ){
                return ResponseUtil.result(1, "删除失败", null);
            }
        } catch ( Exception e ) {
            LOGGER.error("OrderController.detail Error:userId=" + userId + ",orderId=" + orderId, e);
            return ResponseUtil.result(1, "删除订单失败", null);
        }
        return ResponseUtil.result(0, "OK", null);
    }

    /**
     * 立即购买，生成订单
     *
     * @param model
     * @param userId
     * @param itemId
     * @param count
     * @param props
     * @param payType 0=微信支付 5=银行转账
     * @return
     */
    @RequestMapping( value = "/add", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String addOrder(Model model, @CookieValue( "userId" ) Long userId, @RequestParam( "itemId" ) long itemId,
            @RequestParam( value = "count", defaultValue = "1" ) int count, @RequestParam( "props" ) String props,
            @RequestParam( value = "amount", defaultValue = "" ) double amount,
            @RequestParam( value = "addressId", defaultValue = "0" ) long addressId, @RequestParam( value = "remark", defaultValue = "" ) String remark,
            @RequestParam( value = "payType", defaultValue = "-1" ) int payType,
            @RequestParam( value = "discountType", defaultValue = "0" ) int discountType,
            @RequestParam( value = "discountAmount", defaultValue = "0" ) double discountAmount) {

        OrderView orderView;
        try {
            LOGGER.info("OrderController.addOrder userId=" + userId + ",itemId=" + itemId + ",count=" + count + ",props=" + props + ",addressId=" + addressId
                                + ",remark=" + remark);
            ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(itemId, props);
            if ( itemSku == null || itemSku.getCount() < count ) {
                int haveCount = itemSku == null ? 0 : itemSku.getCount();
                return ResponseUtil.result(1, "下手太慢了，商品已被抢光了，剩余" + haveCount + "件", null);
            }
            Orders order = orderService.addOrder(userId, itemSku, count, addressId, remark, payType,amount,discountType,discountAmount);
            orderView = orderService.buildOrderViewByOrder(order);
            LOGGER.info("OrderController.addOrder OK userId=" + userId + ",itemId=" + itemId + ",count=" + count + ",props=" + props + ",addressId=" + addressId
                                + ",remark=" + remark + ",order=" + JSONObject.toJSONString(orderView));
        } catch ( Exception e ) {
            LOGGER.info("OrderController.addOrder userId=" + userId + ",itemId=" + itemId + ",count=" + count + ",props=" + props + ",addressId=" + addressId
                                + ",remark=" + remark, e);
            return ResponseUtil.result(1, "添加订单失败", null);
        }
        return ResponseUtil.result(0, "OK", orderView);
    }

    /**
     * 选择购物车商品，提交订单
     *
     * @param model
     * @param userId
     * @param cartDetailIds 购物车条目IDs,用,分割
     * @param addressId
     * @param remark
     * @param payType 0=微信支付 5=银行转账
     * @return
     */
    @RequestMapping( value = "/submit", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String addOrder(Model model, @CookieValue( "userId" ) Long userId, 
    		@RequestParam( "cartDetailIds" ) String cartDetailIds,
            @RequestParam( value = "addressId", defaultValue = "0" ) long addressId,
            @RequestParam( value = "pword", defaultValue = "" ) String pword,
            @RequestParam( value = "remark", defaultValue = "" ) String remark,
            @RequestParam( value = "amount", defaultValue = "" ) double amount,
            @RequestParam( value = "payType", defaultValue = "-1" ) int payType,
            @RequestParam( value = "discountType", defaultValue = "0" ) int discountType,
            @RequestParam( value = "discountAmount", defaultValue = "0" ) double discountAmount) {

        OrderView orderView;
        try {
            List<String> cartDetailIdStringList = Arrays.asList(cartDetailIds.split(","));
            if ( CollectionUtils.isEmpty(cartDetailIdStringList) ) {
                return ResponseUtil.result(1, "未选择任何商品", "");
            }
            String errMsg = "";
            LOGGER.info("OrderController.submit userId=" + userId + ",cartDetailIds=" + cartDetailIds + ",addressId=" + addressId + ",remark=" + remark);
            List<CartDetail> cartDetailList = new ArrayList<CartDetail>();
            List<Long> cartDetailIdList = new ArrayList<Long>();
            for ( String cartDetailIdString : cartDetailIdStringList ) {
                CartDetail cartDetail = cartDetailService.getById(NumberUtils.toLong(cartDetailIdString), userId);
                if ( cartDetail == null ) {
                    errMsg = "用户信息异常，请退出重新登陆";
                    break;
                } else {
                    ItemSku itemSku = itemSkuService.getValidSkuByItemAndProps(cartDetail.getItemId(), cartDetail.getProps());
                    if ( itemSku == null || itemSku.getCount() < cartDetail.getCount() ) {
                        int haveCount = itemSku == null ? 0 : itemSku.getCount();
                        Item item = itemService.getById(cartDetail.getItemId());
                        errMsg = "下手太慢了，商品【" + item.getName() + "】选择的类型已被抢光了，剩余" + haveCount + "件";
                        break;
                    } else {
                        Item item = itemService.getById(itemSku.getItemId());
                        cartDetail.setCurrentPrice(item.getPrice());
                        cartDetailList.add(cartDetail);
                        cartDetailIdList.add(NumberUtils.toLong(cartDetailIdString));
                    }
                }
            }
            if ( StringUtils.isNotBlank(errMsg) ) {
                return ResponseUtil.result(2, errMsg, "");
            }
            Orders order = orderService.batchAddOrder(userId, cartDetailList, addressId, remark, payType,amount,discountType,discountAmount);
            orderView = orderService.buildOrderViewByOrder(order);
            if ( order != null && order.getId() > 0 ) {//下单成功，清空购物车
                cartDetailService.deleteByIds(cartDetailIdList);
                if(pword!=null||!pword.equals("")){	
                	ticketServcie.haveUsed(order.getId(),pword);//将券状态设置为已经使用
                	LOGGER.info("OrderController.submit  订单号："+order.getId()+"券密码:"+pword+" 已使用");
                }
            }
            LOGGER.info(
                    "OrderController.submit userId=" + userId + ",cartDetailIds=" + cartDetailIds + ",addressId=" + addressId + ",remark=" + remark + ",order="
                            + JSONObject.toJSONString(orderView));
        } catch ( Exception e ) {
            LOGGER.info("OrderController.submit userId=" + userId + ",cartDetailIds=" + cartDetailIds + ",addressId=" + addressId + ",remark=" + remark, e);
            return ResponseUtil.result(1, "获取订单详情失败", null);
        }
        return ResponseUtil.result(0, "OK", orderView);
    }

    @RequestMapping( value = "/{orderId}/updateAddress", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String updateAddress(Model model, @PathVariable Long orderId, @CookieValue( "userId" ) Long userId,
            @RequestParam( value = "addressId", defaultValue = "0" ) long addressId) {

        OrderView orderView;
        try {
            Orders order = orderService.getById(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""));
            if ( order != null ) {
                addressId = orderService.updateAddress(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""), addressId);
                order.setAddressId(addressId);
            }
            orderView = orderService.buildOrderViewByOrder(order);
        } catch ( Exception e ) {
            LOGGER.error("OrderController.updateAddress Error:userId=" + userId + ",orderId=" + orderId + ",addressId=" + addressId, e);
            return ResponseUtil.result(1, "更新订单地址失败", null);
        }
        return ResponseUtil.result(0, "OK", orderView);
    }

    @RequestMapping( value = "/{orderId}/updateRemark", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String updateRemark(Model model, @PathVariable Long orderId, @CookieValue( "userId" ) Long userId,
            @RequestParam( value = "remark", defaultValue = "" ) String remark) {

        OrderView orderView;
        try {
            LOGGER.info("OrderController.updateRemark userId=" + userId + ",orderId=" + orderId + ",remark=" + remark);
            Orders order = orderService.getById(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""));
            if ( order != null ) {
                orderService.updateRemark(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""), remark);
                order.setRemark(remark);
            }
            orderView = orderService.buildOrderViewByOrder(order);
        } catch ( Exception e ) {
            LOGGER.error("OrderController.updateRemark Error:userId=" + userId + ",orderId=" + orderId + ",remark=" + remark, e);
            return ResponseUtil.result(1, "更新订单备注失败", null);
        }
        return ResponseUtil.result(0, "OK", orderView);
    }

    /**
     *
     * @param model
     * @param orderId
     * @param userId
     * @param payType 0=微信支付  1=支付宝 5=银行转账
     * @return
     */
    @RequestMapping( value = "/{orderId}/updatePayType", method = RequestMethod.POST, produces = "application/json;charset=utf-8" ) @ResponseBody
    public String updatePayType(Model model, @PathVariable Long orderId, @CookieValue( "userId" ) Long userId,
            @RequestParam( value = "payType", defaultValue = "0" ) int payType) {

        try {
            LOGGER.info("OrderController.updatePayType userId=" + userId + ",orderId=" + orderId + ",payType=" + payType);
            if ( payType !=0 && payType!=1 && payType!=5 ){
                return ResponseUtil.result(1, "不支持的付款方式", null);
            }
            Orders order = orderService.getById(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""));
            if ( order != null ) {
                orderService.updatePayType(NumberUtils.toLong(userId + ""), NumberUtils.toLong(orderId + ""), payType);
            }
        } catch ( Exception e ) {
            LOGGER.error("OrderController.updatePayType Error:userId=" + userId + ",orderId=" + orderId + ",payType=" + payType, e);
            return ResponseUtil.result(1, "更新订单失败", null);
        }
        return ResponseUtil.result(0, "OK", null);
    }

    @RequestMapping( value = "/page" )
    public String myOrderPage(HttpServletRequest request, Model model) {

        return "mine/orders";
    }


    @RequestMapping("/refund")
    public String refund(HttpServletRequest request, Model model) {
        return "order/refund";
    }

}
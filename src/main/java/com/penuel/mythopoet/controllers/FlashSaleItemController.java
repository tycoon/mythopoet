package com.penuel.mythopoet.controllers; 

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.penuel.mythopoet.dao.ItemDesignerDAO;
import com.penuel.mythopoet.model.Activity;
import com.penuel.mythopoet.model.FlashSaleItem;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.FlashSaleItemView;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.ActivityService;
import com.penuel.mythopoet.service.FlashSaleItemService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemDesignerService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.service.ThemeService;
import com.penuel.mythopoet.utils.ResponseUtil;

@Controller
@RequestMapping("/flashSale")
public class FlashSaleItemController {

	@Autowired
	FlashSaleItemService flashSaleItemServcie;
	@Autowired
	ThemeService themeService;
	@Autowired
	ActivityService activityService;
	@Autowired
	ItemService itemService;
    @Autowired
    private ItemBrandService itemBrandService;
    @Autowired
    private ItemDesignerService itemDesignerService;
	@Autowired
	private ItemDesignerDAO itemDesignerDAO;
	
	@RequestMapping(value = "list", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String list(Model model,@RequestParam(value = "stime") String stime) {
        List<ItemView> itemViews = new ArrayList<ItemView>();
        List<FlashSaleItem> list = flashSaleItemServcie.getList(stime);
 
        for(FlashSaleItem flashSaleItem: list){
        	
        		 Item item = itemService.getById(flashSaleItem.getItemId());
        		 ItemView itemView = new ItemView();
                 BeanUtils.copyProperties(item, itemView);
                 itemService.fillSkuInfo(itemView);
                 itemBrandService.fillBrandInfo(itemView);
                 itemDesignerService.fillDesignerInfo(itemView);
                 itemView.setStime(flashSaleItem.getStime());
                 itemView.setEtime(flashSaleItem.getEtime());
                 itemView.setPrice(flashSaleItem.getPrice());
                 itemViews.add(itemView);
        	
        }
        return ResponseUtil.result(0, "OK", itemViews);
    }
	
	@RequestMapping(value = "check", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String check(Model model,@RequestParam(value = "itemId") Long itemId) {
        FlashSaleItem flashSaleItem = flashSaleItemServcie.getByItemId(itemId);
        Date date = new Date();
        if(date.after(flashSaleItem.getStime())==true && date.after(flashSaleItem.getEtime())==false){
        	return ResponseUtil.result(0, "OK", null);
        }else{
        	return ResponseUtil.result(1, "FAIL", null);
        }
    }
	
    @RequestMapping("")
    public String panicBuy(HttpServletRequest request, Model model) {

        return "topic/panic_buying";
    }
    
	@RequestMapping(value = "theme/{id}", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String theme(Model model,@PathVariable Long id) {
        List<Activity> list = activityService.getByThemeId(id);
        if(list != null){
        	return ResponseUtil.result(0, "OK", list);
        }else{
        	return ResponseUtil.result(1, "FAIL", null);
        }
    }
	
	@RequestMapping(value = "activityItems", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String activityItems(Model model,
    		@RequestParam( value = "activityId") long activityId, 
    		@RequestParam( value = "pageNum",defaultValue = "1") int pageNum, 
    		@RequestParam( value = "pageSize",defaultValue = "20") int pageSize,
            @RequestParam( value = "tagId" ,defaultValue = "0") long tagId) {
        List<FlashSaleItem> list = flashSaleItemServcie.getActivityListPage(activityId,(pageNum - 1) * pageSize, pageSize);
        List<FlashSaleItemView> viewList = new ArrayList<FlashSaleItemView>();
        if(list == null){
        	return ResponseUtil.result(1, "FAIL", null);
        }
        for(FlashSaleItem flashSaleItem: list){
        	
	   		Item item = itemService.getById(flashSaleItem.getItemId());
	   		FlashSaleItemView itemView = new FlashSaleItemView();
            BeanUtils.copyProperties(item, itemView);
            flashSaleItemServcie.fillSkuInfo(itemView);
            flashSaleItemServcie.fillBrandInfo(itemView);
            flashSaleItemServcie.fillDesignerInfo(itemView);
            itemView.setStime(flashSaleItem.getStime());
            itemView.setEtime(flashSaleItem.getEtime());
            itemView.setPrice(flashSaleItem.getPrice());
            viewList.add(itemView);
        }
    	return ResponseUtil.result(0, "OK", viewList);
    }
	
	


}

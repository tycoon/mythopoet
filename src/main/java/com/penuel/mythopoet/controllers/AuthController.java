package com.penuel.mythopoet.controllers;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.model.Orders;
import com.penuel.mythopoet.model.WxAuthUser;
import com.penuel.mythopoet.service.WxAuthService;
import com.penuel.mythopoet.utils.ResponseUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.SortedMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AuthController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/23 下午10:54
 * Desc:
 */

@Controller
@RequestMapping("/auth")
@LoginRequired
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private WxAuthService wxAuthService;

    /**
     * 获取wx code的url回调地址
     *
     * @param model
     * @param code  返回需要的code,每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期<br/>
     *              若用户禁止授权，则重定向后不会带上code参数
     * @param state orderId@userId 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     * @return /order/{orderId}
     */
    @RequestMapping(value = "/wxcode", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public String wxcode(Model model, @RequestParam(value = "code", defaultValue = "") String code,
            @RequestParam(value = "state", defaultValue = "") String state,HttpServletRequest request, HttpServletResponse response) {

        long userId = 0;
        long orderId = 0;
        try {
            if ( StringUtils.isBlank(code) ) {
                return "redirect:/pay/order/" + orderId + "?showwxpaytitle=1&errMsg=" + "用户禁止微信授权";
            }
            LOGGER.info("wxcode:code=" + code + ",state=" + state);
            String[] stateArray = state.split("@");
            userId = NumberUtils.toLong(stateArray[1]);
            orderId = NumberUtils.toLong(stateArray[0]);
            WxAuthUser wxAuthUser = wxAuthService.getAccessToken(code, userId);
            return "redirect:/pay/order/" + orderId + "?showwxpaytitle=1";//返回到订单支付页面
        } catch ( Exception e ) {
            LOGGER.error("AuthController.wxcode Error:code=" + code + ",state=" + state, e);
        }
        return "redirect:/pay/order/" + "/" + orderId + "?showwxpaytitle=1&errMsg=" + "微信授权失败";
    }

    @RequestMapping( value = "/ticket", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String jsapiTicket(Model model, @CookieValue( "userId" ) Long userId,@RequestParam("currentUrl") String currentUrl) {

        try {
           WxAuthUser wxAuthUser = wxAuthService.getByUserId(userId);
            if ( wxAuthUser == null ){
                String authUrl = wxAuthService.authorizeURL(userId, -100L);//-100表示是jsAPI获取ticket的时候需要授权，获取accesstoken
                return ResponseUtil.result(1001, "微信授权", authUrl);
            }

            boolean refreshTicket = false;
            long millSecond = (new Date()).getTime() - wxAuthUser.getJsapiRefreshTime().getTime();
            if ( StringUtils.isBlank(wxAuthUser.getJsapiTicket()) ){
                refreshTicket = true;
            }else if( millSecond + 5*60*1000 > wxAuthUser.getJsapiExpiresIn()*1000 ){
                refreshTicket = true;
            }
            LOGGER.info("jsapiTicket.wxAuthUser ="+JSONObject.toJSONString(wxAuthUser)+",millSecond="+millSecond);
            if ( refreshTicket ){
                wxAuthService.refreshJsapiTicket(wxAuthUser);
            }
            SortedMap<Object, Object> map = wxAuthService.fillJsapiTicketParam(wxAuthUser.getJsapiTicket(), currentUrl);
            LOGGER.info("AuthController.jsapiTicket  result="+ JSONObject.toJSONString(map));
            return ResponseUtil.result(0,"OK",map);
        } catch ( Exception e ) {
            LOGGER.error("AuthController.jsapiTicket Error:userId=" + userId , e);
            return ResponseUtil.result(1, "获取jsAPI初始化参数失败", null);
        }
    }


}
package com.penuel.mythopoet.controllers;

import com.penuel.mythopoet.model.Category;
import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.CategoryService;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.utils.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * CategoryController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:50
 * Desc:
 */

@Controller @RequestMapping("category")
public class CategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ItemBrandService itemBrandService;
    @Autowired
    private ItemService itemService;

    @RequestMapping( "/list" )
    public String list(Model model) {

        return "find/category";
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8") @ResponseBody
    public String categorys(Model model, @RequestParam(value = "tagId", defaultValue = "1") int tagId,
            @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {

        List<Category> categoryList;
        try {
            categoryList = categoryService.getByTagId(tagId, (pageNum - 1) * pageSize, pageSize);
            model.addAttribute("categoryList", categoryList);
        } catch ( Exception e ) {
            LOGGER.error("CategoryController.list Error:tagId=" + tagId + ",pageNum=" + pageNum + ",pageSize=" + pageSize, e);
            return ResponseUtil.result(1, "获取分类列表失败", null);
        }
        return ResponseUtil.result(0, "OK", categoryList);
    }

    @RequestMapping("/{categoryId}/items")
    public String items(@PathVariable Long categoryId, Model model) {

        
            model.addAttribute("categoryId", categoryId);
       
        return "find/item_list";
    }

    @RequestMapping(value = "/{categoryId}/itemss" , method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    @ResponseBody
    public String itemsByPage(@PathVariable Long categoryId, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize", defaultValue = "20") int pageSize, @RequestParam( value = "tagId", defaultValue = "0" ) long tagId, Model model) {

    	List<ItemView> itemViewList = new ArrayList<ItemView>();
        try {
        	
            List<Item> itemList;
            if ( tagId == 0 ) {
                itemList = categoryService.getItemsByCateId(categoryId, (pageNum - 1) * pageSize, pageSize);
            } else {
                itemList = itemService.getItemsByCateIdAndTagId(categoryId, tagId, (pageNum - 1) * pageSize, pageSize);
            }
            if ( !CollectionUtils.isEmpty(itemList) ) {
                ItemView itemView;
                for ( Item item : itemList ) {
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("CategoryController.itemsByPage Error:categoryId=" + categoryId, e);
            ResponseUtil.result(1, "ERROR", null);
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }

    @RequestMapping( "/list/3" )
    public String listThree(HttpServletRequest request, Model model) {

        return "find/item_list_3";
    }

}
package com.penuel.mythopoet.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.modelView.ItemView;
import com.penuel.mythopoet.service.ItemBrandService;
import com.penuel.mythopoet.service.ItemService;
import com.penuel.mythopoet.utils.ResponseUtil;


/**
 * ItemController Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:45
 * Desc:
 */
@Controller
@RequestMapping("single")
public class SingleProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SingleProductController.class);

    @Autowired
    private ItemService itemService;
    @Autowired
    private ItemBrandService itemBrandService;

    
    @RequestMapping( "/list" )
    public String list(Model model) {

        return "find/allitem";
    }
    
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String getSingle(Model model, @RequestParam( value = "pageNum", defaultValue = "1" ) int pageNum,
            @RequestParam( value = "pageSize", defaultValue = "20" ) int pageSize,@RequestParam(value = "tagId",defaultValue = "2") int tagId) {

        List<ItemView> itemViewList = new ArrayList<ItemView>();
        try {
            List<Item> itemList;
            
            itemList = itemService.getSingleByTagId(tagId, (pageNum - 1) * pageSize, pageSize);
            
            if ( !CollectionUtils.isEmpty(itemList) ){
                ItemView itemView;
                for ( Item item : itemList ){
                    itemView = new ItemView();
                    BeanUtils.copyProperties(item, itemView);
                    itemBrandService.fillBrandInfo(itemView);
                    itemViewList.add(itemView);
                }
            }
        } catch ( Exception e ) {
            LOGGER.error("SingleProductController.detail Error" , e);
            return ResponseUtil.result(1, "获取全部单品列表失败", "");
        }
        return ResponseUtil.result(0, "OK", itemViewList);
    }


}
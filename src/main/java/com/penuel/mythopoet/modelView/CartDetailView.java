package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.CartDetail;
import com.penuel.mythopoet.model.CneeAddress;

/**
 * CartDetailView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/29 上午8:22
 * Desc:
 */

public class CartDetailView extends CartDetail{

    private String propsValue;
    private String brandName;
    private String itemName;
    private int skuCount;//库存数量，需要和购物车的数量比较
    private String pic;
    private CneeAddress cneeAddress;
    private int allCount;
    private double allPrice;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getPropsValue() {

        return propsValue;
    }

    public void setPropsValue(String propsValue) {

        this.propsValue = propsValue;
    }

    public String getItemName() {

        return itemName;
    }

    public void setItemName(String itemName) {

        this.itemName = itemName;
    }

    public int getSkuCount() {

        return skuCount;
    }

    public void setSkuCount(int skuCount) {

        this.skuCount = skuCount;
    }

    public String getPic() {

        return pic;
    }

    public void setPic(String pic) {

        this.pic = pic;
    }

	public CneeAddress getCneeAddress() {
		return cneeAddress;
	}

	public void setCneeAddress(CneeAddress cneeAddress) {
		this.cneeAddress = cneeAddress;
	}

	public int getAllCount() {
		return allCount;
	}

	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	public double getAllPrice() {
		return allPrice;
	}

	public void setAllPrice(double allPrice) {
		this.allPrice = allPrice;
	}
    
    
    
}
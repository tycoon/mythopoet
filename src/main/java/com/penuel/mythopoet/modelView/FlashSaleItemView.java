package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.Item;
import com.penuel.mythopoet.model.ItemProp;
import com.penuel.mythopoet.utils.FormatterUtil;

import java.util.Date;
import java.util.List;

/**
 * ItemView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/14 上午8:50
 * Desc:
 */

public class FlashSaleItemView extends Item {
    private int orderedCount;
    private List<ItemSkuView> ItemSkuViewList;
    private List<ItemProp> itemPropList;
    private long favoritesId;
    private String pic;
    private String brandName;
    private int totalSkuCount;
    private int sexTagId;
    private String desingerDescription;
    private long desingerId;
    private Date stime;
    private Date etime;

    public int getOrderedCount() {
        return orderedCount;
    }

    public void setOrderedCount(int orderedCount) {
        this.orderedCount = orderedCount;
    }

    public List<ItemSkuView> getItemSkuViewList() {

        return ItemSkuViewList;
    }

    public void setItemSkuViewList(List<ItemSkuView> itemSkuViewList) {

        ItemSkuViewList = itemSkuViewList;
    }

    public List<ItemProp> getItemPropList() {

        return itemPropList;
    }

    public void setItemPropList(List<ItemProp> itemPropList) {

        this.itemPropList = itemPropList;
    }

    public long getFavoritesId() {

        return favoritesId;
    }

    public void setFavoritesId(long favoritesId) {

        this.favoritesId = favoritesId;
    }

    public String getPic() {
        return FormatterUtil.decoratePicPrefix(pic);
    }

    public void setPic(String pic) {

        this.pic = pic;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getTotalSkuCount() {
        return totalSkuCount;
    }

    public void setTotalSkuCount(int totalSkuCount) {
        this.totalSkuCount = totalSkuCount;
    }

    public int getSexTagId() {
        return sexTagId;
    }

    public void setSexTagId(int sexTagId) {
        this.sexTagId = sexTagId;
    }

	public String getDesingerDescription() {
		return desingerDescription;
	}

	public void setDesingerDescription(String desingerDescription) {
		this.desingerDescription = desingerDescription;
	}

	public long getDesingerId() {
		return desingerId;
	}

	public void setDesingerId(long desingerId) {
		this.desingerId = desingerId;
	}

	public Date getStime() {
		return stime;
	}

	public void setStime(Date stime) {
		this.stime = stime;
	}

	public Date getEtime() {
		return etime;
	}

	public void setEtime(Date etime) {
		this.etime = etime;
	}
    
    
}
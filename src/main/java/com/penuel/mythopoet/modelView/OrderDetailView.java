package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.ItemPropValue;
import com.penuel.mythopoet.model.ItemSku;
import com.penuel.mythopoet.model.OrderDetail;

import java.util.List;

/**
 * OrderDetailView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/19 下午10:22
 * Desc:
 */

public class OrderDetailView extends OrderDetail{

    private ItemSku itemSku;
    private List<ItemPropValue> itemPropValueList;
    private String propsValue;
    private String itemName;
    private String pic;
    private Double price;
    private String brandName;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public ItemSku getItemSku() {

        return itemSku;
    }

    public void setItemSku(ItemSku itemSku) {

        this.itemSku = itemSku;
    }

    public List<ItemPropValue> getItemPropValueList() {

        return itemPropValueList;
    }

    public void setItemPropValueList(List<ItemPropValue> itemPropValueList) {

        this.itemPropValueList = itemPropValueList;
    }

    public String getPropsValue() {

        return propsValue;
    }

    public void setPropsValue(String propsValue) {

        this.propsValue = propsValue;
    }

    public String getItemName() {

        return itemName;
    }

    public void setItemName(String itemName) {

        this.itemName = itemName;
    }

    public String getPic() {

        return pic;
    }

    public void setPic(String pic) {

        this.pic = pic;
    }
}
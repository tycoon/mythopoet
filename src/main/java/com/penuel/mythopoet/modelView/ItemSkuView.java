package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.ItemPropValue;
import com.penuel.mythopoet.model.ItemSku;

import java.util.List;

/**
 * ItemSkuView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/18 下午10:41
 * Desc:
 */

public class ItemSkuView extends ItemSku {

    private List<ItemPropValue> itemPropValueList;
    private String pic;

    public List<ItemPropValue> getItemPropValueList() {

        return itemPropValueList;
    }

    public void setItemPropValueList(List<ItemPropValue> itemPropValueList) {

        this.itemPropValueList = itemPropValueList;
    }

    public String getPic() {

        return pic;
    }

    public void setPic(String pic) {

        this.pic = pic;
    }
}
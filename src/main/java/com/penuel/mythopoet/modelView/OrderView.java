package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.CneeAddress;
import com.penuel.mythopoet.model.Orders;

import java.util.List;

/**
 * OrderView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/19 下午10:18
 * Desc:
 */

public class OrderView extends Orders {
    private List<OrderDetailView> orderDetailViewList;
    private CneeAddress cneeAddress;

    public List<OrderDetailView> getOrderDetailViewList() {

        return orderDetailViewList;
    }

    public void setOrderDetailViewList(List<OrderDetailView> orderDetailViewList) {

        this.orderDetailViewList = orderDetailViewList;
    }

    public CneeAddress getCneeAddress() {

        return cneeAddress;
    }

    public void setCneeAddress(CneeAddress cneeAddress) {

        this.cneeAddress = cneeAddress;
    }
}
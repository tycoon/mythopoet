package com.penuel.mythopoet.modelView;

/**
 * Page
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/6/10 上午11:26
 * Desc:
 */

public class Page {

    private int curPageNum;//当前第几页
    private int totalCount;//总共多少个
    private int pageSize;//每页多少个
    private int totalPageNum;//共多少页

    public Page(int curPageNum, int pageSize) {
        if ( pageSize <= 0 ) {
            pageSize = 20;
        }
        if ( curPageNum <= 0 ) {
            curPageNum = 1;
        }
        this.curPageNum = curPageNum;
        this.pageSize = pageSize;
    }

    public Page(int curPageNum, int pageSize, int totalCount) {
        this(curPageNum, pageSize);
        this.totalCount = totalCount;
        this.totalPageNum = (totalCount - 1) / pageSize + 1;
    }

    public Page() {}

    public int getCurPageNum() {
        return curPageNum;
    }

    public void setCurPageNum(int curPageNum) {
        this.curPageNum = curPageNum;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        this.totalPageNum = (totalCount - 1) / pageSize + 1;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPageNum() {
        return totalPageNum;
    }

    public void setTotalPageNum(int totalPageNum) {
        this.totalPageNum = totalPageNum;
    }
}
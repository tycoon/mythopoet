package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.Favorites;

/**
 * FavoritesView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/28 下午9:39
 * Desc:
 */

public class FavoritesView extends Favorites {

    private String icon;
    private String pic;
    private String name;
    private String itemBrandName;
    private double itemPrice;
    private String desingerCountry;
    private int skuCount;

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

    public String getPic() {

        return pic;
    }

    public void setPic(String pic) {

        this.pic = pic;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getItemBrandName() {
        return itemBrandName;
    }

    public void setItemBrandName(String itemBrandName) {
        this.itemBrandName = itemBrandName;
    }

    public double getItemPrice() {

        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {

        this.itemPrice = itemPrice;
    }

    public String getDesingerCountry() {

        return desingerCountry;
    }

    public void setDesingerCountry(String desingerCountry) {

        this.desingerCountry = desingerCountry;
    }

    public int getSkuCount() {

        return skuCount;
    }

    public void setSkuCount(int skuCount) {

        this.skuCount = skuCount;
    }
}
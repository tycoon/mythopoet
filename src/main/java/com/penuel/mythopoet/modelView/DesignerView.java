package com.penuel.mythopoet.modelView;

import com.penuel.mythopoet.model.Designer;

/**
 * DesignerView Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/28 下午10:35
 * Desc:
 */

public class DesignerView extends Designer {

    private long favoritesId;
    private String icon;
    private String pic;

    public long getFavoritesId() {

        return favoritesId;
    }

    public void setFavoritesId(long favoritesId) {

        this.favoritesId = favoritesId;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

    public String getPic() {

        return pic;
    }

    public void setPic(String pic) {

        this.pic = pic;
    }
}
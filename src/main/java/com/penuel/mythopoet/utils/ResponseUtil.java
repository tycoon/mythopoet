package com.penuel.mythopoet.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * ResponseUtil Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/3/31 下午11:49
 * Desc:
 */

public class ResponseUtil {

    public static String result(int code, String msg, Object data) {

        return resultJSON(code, msg, data).toString();
    }

    public static JSONObject resultJSON(int code, String msg, Object data) {

        JSONObject json = new JSONObject();
        json.put("code", code);
        json.put("msg", msg);
        json.put("data", JSON.toJSON(data));
        return json;
    }
}
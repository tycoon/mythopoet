package com.penuel.mythopoet.utils;

import com.penuel.mythopoet.constants.PoetConstants;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FormatterUtil Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/23 上午11:12
 * Desc:
 */

public class FormatterUtil {

    public static long timeFormatter(String strTime) {

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(strTime));
        } catch ( ParseException e ) {

        }
        long time = c.getTimeInMillis();
        time /= 1000;
        return time;
    }

    public static long timeFormatterAddOneDay(String strTime) {

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(strTime));
            c.add(c.DATE, 1);
        } catch ( ParseException e ) {

        }
        long time = c.getTimeInMillis();
        time /= 1000;
        return time;
    }

    public static long getTimeBeforeOneWeek() {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DATE, c.get(Calendar.DATE) - 7);
        long time = c.getTimeInMillis();
        time /= 1000;
        return time;
    }

    public static long getNow() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        long time = c.getTimeInMillis();
        time /= 1000;
        return time;
    }

    public static boolean isMobiPhoneNum(String telNum) {

        if ( StringUtils.isBlank(telNum) ) {
            return false;
        }
        String regex = "^((13[0-9])|(15[0-9])|(17[0-9])|(18[0-9]))\\d{8}$";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(telNum);
        return m.matches();
    }

    public static Integer getTodayLeftTime() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        long now = c.getTimeInMillis() / 1000;
        c.set(Calendar.DATE, c.get(Calendar.DATE) + 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        long todayEnd = c.getTimeInMillis() / 1000;
        long result = todayEnd - now;
        return (int) result;
    }

    public static String decoratePicPrefix(String pic) {

        if ( StringUtils.isBlank(pic) || pic.contains(PoetConstants.ALI_IMG_PRE) ) {
            return pic;
        }
        StringBuffer sb = new StringBuffer();
        String[] picArray = pic.split(",");
        for ( String s : picArray ) {
            sb.append(",");
            sb.append(PoetConstants.ALI_IMG_PRE + s);
        }
        return sb.toString().substring(1);
    }
}
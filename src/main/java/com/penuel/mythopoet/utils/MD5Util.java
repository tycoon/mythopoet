package com.penuel.mythopoet.utils;

import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * MD5Util Created with mythopoet. User: penuel (penuel.leo@gmail.com) Date:
 * 15/4/2 下午11:54 Desc:
 */

public class MD5Util {
	/**
	 * 微信支付加密，支付宝支付加密
	 * @param content
	 * @return
	 */
	public static String md5(String content) {
		MessageDigest md;
		try {
			// 生成一个MD5加密计算摘要
			md = MessageDigest.getInstance("MD5");
			// 计算md5函数
			md.update(content.getBytes("UTF-8"));
			// digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
			// BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
			content = new BigInteger(1, md.digest()).toString(16);
			return content;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}
	/**
	 * 顺丰加密
	 * @param content
	 * @param charset
	 * @return
	 */
	public static byte[] md5(String content, String charset) {

		MessageDigest md = null;
		try {
			// 生成一个MD5加密计算摘要
			md = MessageDigest.getInstance("MD5");
			// 计算md5函数

			md.update(content.getBytes(charset));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException nae) {
			nae.printStackTrace();
		}
		return md.digest();
	}

	public static void main(String[] args) {
		String a = "appid=wx51473b3f12948341&body=Mythopoet:12-3-1200.0-1&detail=12&device_info=WEB&mch_id=1231131202&nonce_str=0d18cfef-aca0-4208-9&notify_url=http://www.hshs-fashion.com/pay/recall&openid=oLwIQt6xNaGJ0HGLsRQ_abOulv_k&out_trade_no=12&spbill_create_ip=123.138.35.2&total_fee=120000&trade_type=JSAPI&key=a7f66265072709007c9e8b216aeab20c";
		System.out.println(md5(a));
		System.out.println(DigestUtils.md5DigestAsHex(a.getBytes()));
		System.out.println(UUID.randomUUID().toString());
	}

}
package com.penuel.mythopoet.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Random;

/**
 * SmsUtil Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/1 上午8:24
 * Desc: 短信服务
 */

public class SmsUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsUtil.class);

    /*
    http://221.179.180.158:9007/QxtSms/QxtFirewall?OperID=ceshi10&OperPass=hshs&SendTime=&ValidTime=&AppendID=1234&DesMobile=18618484029&Content=123abc&ContentType=8
     */
    public static final String SMS_SEND_URL = "http://221.179.180.158:9007/QxtSms/QxtFirewall";
    public static final String OperId = "hshs";
    public static final String OperPass = "hshs66";

    public static void main(String[] args) {
        send("18618484029","test111");
    }

    public static boolean send(String phone, String msg) {

        try {
            String params = getSmsUrlGetParam(phone, msg);
            String result = HttpClientUtil.sendGetRequest(SMS_SEND_URL + params, "UTF-8");
            Map<String,String> resultMap = WxPayUtil.doXMLParse(result);
            String code = resultMap.get("code");
            if ( "03".equals(code) ){
                return true;
            }else{
                LOGGER.error("SmsUtil.send Error:phone=" + phone + ",msg=" + msg+",smsResult="+ result);
            }
        } catch ( Exception e ) {
            LOGGER.error("SmsUtil.send Error:phone=" + phone + ",msg=" + msg, e);
        }
        return false;
    }

    private static String getSmsUrlGetParam(String mobile, String msg) throws UnsupportedEncodingException {

        StringBuffer sb = new StringBuffer();
        sb.append("?OperID=" + OperId);
        sb.append("&OperPass=" + OperPass);
        sb.append("&SendTime=");
        sb.append("&ValidTIme=");
        sb.append("&AppendID=66");
        sb.append("&DesMobile=" + mobile);
        sb.append("&Content=" + URLEncoder.encode(msg, "GBK"));
        sb.append("&ContentType=15");
        return sb.toString();
    }

    public static String getValidCode(int len) {

        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        while ( stringBuffer.length() < len ) {
            stringBuffer.append(random.nextInt(10));
        }
        return stringBuffer.toString();
    }

}
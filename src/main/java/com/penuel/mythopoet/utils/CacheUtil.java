package com.penuel.mythopoet.utils; 

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * CacheUtil Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/5/12 下午7:25
 * Desc:
 */

public class CacheUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheUtil.class);

    public static Cache<String, String> VALID_CODE_CACHE = CacheBuilder.newBuilder()
                                                                     .expireAfterWrite(10, TimeUnit.MINUTES)
                                                                     .build();

    public static Cache<String, String> TOTAL_COUNT_SMS_CACHE = CacheBuilder.newBuilder()
                                                                      .expireAfterWrite(24, TimeUnit.HOURS)
                                                                      .build();

    public static void set(String key, String value, Cache<String, String> cache) {

        cache.put(key, value);
    }

    public static String get(String key, Cache<String, String> cache) {

        String value = null;
        try {
            value = cache.get(key, new Callable<String>() {

                @Override public String call() throws Exception {

                    return "";
                }
            });
        } catch ( ExecutionException e ) {
            LOGGER.error("CacheUtil.get(" + key + ") Error:", e);
        }
        return value;
    }
}
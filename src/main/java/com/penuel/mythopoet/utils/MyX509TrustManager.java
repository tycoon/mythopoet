package com.penuel.mythopoet.utils;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * MyX509TrustManager Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/22 上午8:07
 * Desc:
 */

public class MyX509TrustManager implements X509TrustManager {

    @Override public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override public X509Certificate[] getAcceptedIssuers() {

        return new X509Certificate[0];
    }
}
package com.penuel.mythopoet.utils;

import com.penuel.mythopoet.service.PayService;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * WxPayUtil Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/4/22 上午8:01
 * Desc: 微信支付工具
 */

public class WxPayUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(WxPayUtil.class);

    /**
     * 发送https请求
     *
     * @param requestUrl    请求地址
     * @param requestMethod 请求方式（GET、POST）
     * @param data          提交的数据
     * @return 返回微信服务器响应的信息
     */
    public static String httpsRequest(String requestUrl, String requestMethod, String data) {

        try {
            // 创建SSLContext对象，并使用我们指定的信任管理器初始化
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new java.security.SecureRandom());
            // 从上述SSLContext对象中得到SSLSocketFactory对象
            SSLSocketFactory ssf = sslContext.getSocketFactory();
            URL url = new URL(requestUrl);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(ssf);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            conn.setRequestMethod(requestMethod);
            conn.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            // 当outputStr不为null时向输出流写数据
            if ( null != data ) {
                OutputStream outputStream = conn.getOutputStream();
                // 注意编码格式
                outputStream.write(data.getBytes("UTF-8"));
                outputStream.close();
            }
            // 从输入流读取返回内容
            InputStream inputStream = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String str = null;
            StringBuffer buffer = new StringBuffer();
            while ( (str = bufferedReader.readLine()) != null ) {
                buffer.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            inputStream = null;
            conn.disconnect();
            return buffer.toString();
        } catch ( ConnectException ce ) {
            LOGGER.error("httpsRequest连接超时：{}", ce);
        } catch ( Exception e ) {
            LOGGER.error("httpsRequest请求异常：{}", e);
        }
        return null;
    }

    /**
     * 生成xml request参数
     *
     * @param parameters
     * @return
     */
    public static String getRequestParamXml(SortedMap<Object, Object> parameters) {

        StringBuffer sb = new StringBuffer();
        sb.append("<xml>");
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while ( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());
            if ( "attach".equalsIgnoreCase(k) || "body".equalsIgnoreCase(k) || "sign".equalsIgnoreCase(k) ) {
                sb.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");
            } else {
                sb.append("<" + k + ">" + v + "</" + k + ">");
            }
        }
        sb.append("</xml>");
        return sb.toString();
    }

    /**
     * 按照微信支付生成sign
     *
     * @param parameters
     * @return
     */
    public static String createSign(SortedMap<Object, Object> parameters) {

        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while ( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());
            if ( null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k) ) {
                sb.append(k + "=" + v + "&");
            }
        }
        sb.append("key=" + Constants.get("APP_SECRET"));//xxxxxx换成你的API密钥
        String sign = MD5Util.md5(sb.toString()).toUpperCase();
        LOGGER.info("WxPayUtil.createSign StringA="+sb.toString()+",sign="+sign);
        return sign;
    }

    public static String createSha1Sign(SortedMap<Object, Object> parameters) {

        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while ( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = String.valueOf(entry.getKey());
            String v = String.valueOf(entry.getValue());
            if ( null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k) ) {
                sb.append(k + "=" + v + "&");
            }
        }
        String params = sb.toString();
        params = params.substring(0,params.length()-1);
        String sign = SHA1(params);
        LOGGER.info("WxPayUtil.createSha1Sign StringA="+sb.toString()+",sign="+sign);
        return sign;
    }

    public static String SHA1(String decript) {
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("SHA-1");
            digest.update(decript.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) {

        System.out.println(MD5Util.md5(
                "appid=wx51473b3f12948341&body=Mythopoet:12-3-1200.0-1&detail=12&device_info=WEB&mch_id=1231131202&nonce_str=0d18cfef-aca0-4208-9&notify_url=http://www.hshs-fashion.com/pay/recall&openid=oLwIQt6xNaGJ0HGLsRQ_abOulv_k&out_trade_no=12&spbill_create_ip=123.138.35.2&total_fee=120000&trade_type=JSAPI&key=a7f66265072709007c9e8b216aeab20c").toUpperCase());
    }

    /**
     * 解析xml,返回第一级元素键值对。如果第一级元素有子节点，则此节点的值是子节点的xml数据。
     *
     * @param strxml
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static Map<String,String> doXMLParse(String strxml) {

        Map<String,String> m = new HashMap<String,String>();
        InputStream in = null;
        try {
            strxml = strxml.replaceFirst("encoding=\".*\"", "encoding=\"UTF-8\"");

            if ( null == strxml || "".equals(strxml) ) {
                return null;
            }

            in = new ByteArrayInputStream(strxml.getBytes("UTF-8"));
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(in);
            Element root = doc.getRootElement();
            List list = root.getChildren();
            Iterator it = list.iterator();
            while ( it.hasNext() ) {
                Element e = (Element) it.next();
                String k = e.getName();
                String v = "";
                List children = e.getChildren();
                if ( children.isEmpty() ) {
                    v = e.getTextNormalize();
                } else {
                    v = WxPayUtil.getChildrenText(children);
                }

                m.put(k, v);
            }
            //关闭流
            in.close();
        } catch ( Exception e ) {
            LOGGER.error("WxPayUtil.doXMLParse Error:strXml=" + strxml, e);
        } finally {
            if ( in!=null ){
                try {
                    in.close();
                } catch ( IOException e ) {}
            }
        }

        return m;
    }

    /**
     * 获取子结点的xml
     *
     * @param children
     * @return String
     */
    public static String getChildrenText(List children) {

        StringBuffer sb = new StringBuffer();
        if ( !children.isEmpty() ) {
            Iterator it = children.iterator();
            while ( it.hasNext() ) {
                Element e = (Element) it.next();
                String name = e.getName();
                String value = e.getTextNormalize();
                List list = e.getChildren();
                sb.append("<" + name + ">");
                if ( !list.isEmpty() ) {
                    sb.append(WxPayUtil.getChildrenText(list));
                }
                sb.append(value);
                sb.append("</" + name + ">");
            }
        }

        return sb.toString();
    }

}
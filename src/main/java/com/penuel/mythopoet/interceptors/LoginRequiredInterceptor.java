package com.penuel.mythopoet.interceptors;

import com.alibaba.fastjson.JSONObject;
import com.penuel.mythopoet.annotation.LoginRequired;
import com.penuel.mythopoet.annotation.ResponseResult;
import com.penuel.mythopoet.constants.PoetConstants;
import com.penuel.mythopoet.model.AccessToken;
import com.penuel.mythopoet.service.AccessTokenService;
import com.penuel.mythopoet.utils.RequestUtil;
import com.penuel.mythopoet.utils.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * LoginRequiredInterceptor Created with mythopoet.
 * User: penuel (penuel.leo@gmail.com)
 * Date: 15/5/3 下午2:40
 * Desc: 登陆拦截器
 */

public class LoginRequiredInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginRequiredInterceptor.class);

    @Autowired
    private AccessTokenService accessTokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        try {
            LoginRequired loginRequired = priorityTokenValid(handler);
            if ( null != loginRequired ) {
                String token = getValueFromTokenOrRequest(PoetConstants.COOKIE_TOKEN, request);
                String userId = getValueFromTokenOrRequest(PoetConstants.COOKIE_USER_ID, request);

                //验证token 和 acctId的合法性 即账号和总店
                boolean isLogin = isLegalUser(userId, token);

                if ( !isLogin ) {
                    RequestUtil.clearCookie(request, response);
                    if ( ResponseResult.JSON == loginRequired.result() ) {
                        response.setCharacterEncoding("UTF-8");
                        response.setContentType("application/json");
                        PrintWriter printWriter = response.getWriter();
                        JSONObject jo = ResponseUtil.resultJSON(101, "用户不合法或已失效，请重新登陆", null);
                        printWriter.write(jo.toJSONString());
                        printWriter.flush();
                    } else {
                    	String uri = request.getRequestURI();
                    	if(uri.equals("")){
                    		response.sendRedirect("/user/login");
                    	}else{
                    		response.sendRedirect("/user/login?redirect="+uri);
                    	}
                        
                    }
                    return false;
                }

                //                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
                //                response.setHeader("Pragma", "no-cache");
                //                response.setHeader("Expires", "0");
            }
        } catch ( Exception e ) {
            LOGGER.error("LoginRequiredInterceptor.preHandle error:", e);
        }
        return true;
    }

    private boolean isLegalUser(String cookieUserId, String cookieAccessToken) {
        if ( cookieUserId == null || NumberUtils.toLong(cookieUserId) < 1 ) {
            return false;
        }
        AccessToken accessToken = accessTokenService.getByUserId(NumberUtils.toLong(cookieUserId));
        if ( accessToken == null || !accessToken.getToken().equals(cookieAccessToken) ) {
            return false;
        }
        return true;
    }

    private LoginRequired priorityTokenValid(Object handler) {

        LoginRequired loginRequired = null;
        if ( null != handler && handler instanceof HandlerMethod ) {
            HandlerMethod method = (HandlerMethod) handler;
            LoginRequired methodAnnotion = method.getMethod().getAnnotation(LoginRequired.class);
            LoginRequired clazzAnnotion = method.getMethod().getDeclaringClass().getAnnotation(LoginRequired.class);
            if ( null != methodAnnotion ) {
                loginRequired = methodAnnotion;
            } else if ( null != clazzAnnotion ) {
                loginRequired = clazzAnnotion;
            }
        }
        return loginRequired;
    }

    private String getValueFromTokenOrRequest(String key, HttpServletRequest request) {

        String value = RequestUtil.getCookieValue(request, key);
        if ( StringUtils.isBlank(value) ) {
            value = request.getParameter(key);
        }
        return value;
    }

}
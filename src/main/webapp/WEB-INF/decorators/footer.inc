﻿<!-- 页面的启动脚本文件 start -->
<#if page.properties['page.javascript']?exists>
    <#assign staticPath = page.properties['page.staticPath']>
        <script type="text/javascript"
                data-main="${staticPath}/js/page${page.properties['page.javascript']}.js?ver=547d8677"
                src="/static/js/lib/r.js"></script>
</#if>
<!--  页面的启动脚本文件 end -->
<script type="text/javascript" src="${staticPath}/js/page/home/access.js?ver=547f8681"></script>

</body>
</html>
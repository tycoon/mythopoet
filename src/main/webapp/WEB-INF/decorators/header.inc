﻿<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no">
    <meta name="format-detection" content="telephone=no" />

    <#-- 环境判断，基础变量设置 -->
    <#assign staticPath = "/static">
    <title>${title}</title>
    <meta name="description" content="绘事后素官方网站是国内首家专做全球顶级设计师时尚的新媒体类电商与推广平台，创始人王紫菲为80末新生代时尚怪咖。这里汇集了国际范围内知名的顶级设计师品牌以及当下最具潜力的新兴设计师品牌之产品，国际顶级的买手资源。电商融合杂志的美感，以新媒体的形式承载“可见即可买”的新型购物体验。挑剔的眼光、拒绝平庸的态度，将最顶级的设计师时尚真正地带入中国移动互联网大潮下每一个人的生活中。" />
    <meta name="keywords" content="绘事后素，王紫菲，全球顶级设计师品牌，设计师品牌，设计师，设计师时尚，新兴优秀的设计师，可见即可买，限量版，艺术与时尚的跨界。" />    <script type="text/javascript" src="${staticPath}/js/lib/jquery-1.11.2.min.js?ver=547f8682"></script>

    <#-- 页面的启动脚本配置文件，需要给定个变量判断是否开发环境 start -->
    <script type="text/javascript">
      var VM = {};
      VM.BOOTSTAMP = new Date().getTime();

      VM.STATIC_URL = "${staticPath}/js/";

    </script>
    <#-- 页面的启动脚本配置文件 end -->

    <!-- 原先引入的脚本文件 start -->
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="${staticPath}/images/projectavatar.png">
    <link rel="icon" href="${staticPath}/images/projectavatar.png" size="16x16 32x32">
    <!-- 原先引入的脚本文件 end -->

    <!-- 各个页面公用的全局css样式 start -->
    <link rel="stylesheet" type="text/css" href="${staticPath}/css/global.css?ver=547f8682">
    <!-- 各个页面公用的全局css样式 end -->

    <!-- 当前页面的引入的样式 start -->
    <#if page.properties['page.css']?exists>
    <link rel="stylesheet" href="${staticPath}/css/pages${page.properties['page.css']}.css?ver=547f8682" />
    </#if>
    <!-- 当前页面的引入的样式 end -->

    <script>
    var _hmt = _hmt || [];
    (function() {
      var hm = document.createElement("script");
      hm.src = "//hm.baidu.com/hm.js?3fa0326ec1851816dd957ffb28f0e1b7";
      var s = document.getElementsByTagName("script")[0]; 
      s.parentNode.insertBefore(hm, s);
    })();
    </script>
  </head>
  <body>
    <img src="/static/images/avatar2.png" style="height:0;display: block;">
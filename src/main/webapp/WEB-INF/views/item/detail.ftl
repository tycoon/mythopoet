<#include "detail_config.ftl">

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>商品详情</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<#if detail??>
<div class="product" id="j-item-wrapper" data-id="${detail.id}">
  <div class="product-intro cf">
<#--     <div class="produce-share">
      <i class="icon i-share"></i>
    </div> -->
    <div class="product-info">
      <p class="produce-name" style="font-family:'Bitter';margin-bottom:2px;font-size:1.3em;"><a href="/designer/detail/${detail.desingerId}">${detail.brandName!''}</a></p>
      <p class="produce-name">${detail.name}</p>
      <p>
        <#if detail.discount lt 1>
          <span class="produce-price price line-through">¥${detail.originPrice?string(",##0.00")}</span>
		  <span class="produce-price price discount-price">${(100-detail.discount*100)?string(",##0")}% OFF </span><br>
		   <span class="produce-price price discount-price">¥${detail.price?string(",##0.00")}</span>
          <#-- <span class="limit">全球限售10件</span> -->
        <#else>
          <span class="produce-price price">¥${detail.price?string(",##0.00")}</span>
        </#if>
      </p> 
      <#if detail.totalSkuCount==0>
        <p class="tc-red">该商品暂时没有库存</p>  
      </#if>    
    </div>
  </div> 

  <div class="product-images hide" id="j-slides">
    <#list detail.pic?split(",") as pic>
    <div><img class="product-img j-product-img" src="${pic}"></div>
    </#list>
  </div>
  <div id="j-desc" class="hide">
   <#if detail.note != ''> 
     <h3 class="tc-main-gray">编辑笔记</h3>
     <p class="product-description">${detail.note}</p>
   </#if> 
    <h3 class="tc-main-gray">商品详情</h3>
    <p class="product-description">${detail.description}</p>
    <#if detail.sizeDesc??>
	    <h3 class="tc-main-gray" >尺码指导 <a href="/size/guide/${detail.sexTagId!'1'}" <div class="border-btn fr" style="padding: 5px 10px;margin-left: 20px;font-size: 12px; position: relative; top: -8px;">详细尺码指导</div></a>
			</h3>
		<p class="product-description">${detail.sizeDesc}</p>
	</#if>
	<h3 class="tc-main-gray">设计师简介</h3>
    <p class="product-description">${detail.desingerDescription}</p>
    <h3 class="tc-main-gray">寄送信息</h3>
    <p class="product-description">您的订单将会从绘事后素直接送达您手中，所有商品将由顺丰快递全程负责，方便您随时跟踪。</p>
    <h3 class="tc-main-gray">免费退货换货服务</h3>
    <p class="product-description">绘事后素为您提供7天免费退换货服务。强烈建议您在签收商品后的7天内与我们的客服联系沟通并将商品退还至绘事后素总部。因卫生起见，耳环及高级珠宝类饰品，内衣及泳衣，折扣商品恕无法退换。<a href='/order/refund'>点击这里</a>，了解我们的退货政策。</p>
    <h3 class="tc-main-gray">客服提醒</h3> 
    <p class="product-description">强烈建议您直接联系我们的客服电话，以便确认您的尺码或任何其它需求。我们的客服电话随时恭候为您服务：<a href="tel:010-65008611">010-65008611</a></p>
  </div>

 


<div class="pursue-info">
  <a href="tel:010-65008611" class="tel" style="border-right:solid 1px white"><i class="icon i-communicate"></i>客服</a>
  <#if detail.favoritesId == 0>
    <div class="fav" id="j-fav" data-id="${detail.favoritesId}"><i class="icon i-fav" id="j-fav-icon"></i><span id="j-fav-text">收藏</span></div>
  <#else>
    <div class="fav" id="j-fav" data-id="${detail.favoritesId}"><i class="icon i-faved" id="j-fav-icon"></i><span id="j-fav-text" style="color:#d37eff">已收藏</span></div>
  </#if>
  <div class="cart j-show-mask" id="j-add-to-cart">加入购物车</div>
  <div class="buy j-show-mask" id="j-buy-now">马上出手</div>
</div>

<!-- 购买的遮罩层 -->
<div class="select-mask" id="j-select-mask">
  <div class="select-bg" id="j-select-area">
    <div class="item-info cf">
      <p class="fr price">¥<span id="j-sku-price">${detail.price}</span></p>
      <div>
        <img class="item-thumb fl" src="${detail.pic?split(",")[0]}">
        <p class="select-name">${detail.name}</p>
        <#-- <p>请选择尺码与颜色</p> -->
        <p>库存:<span id="j-sku-stock"></span></p>
      </div>
    </div>
    <div class="select-info cf" id="j-select-info">
    </div>
    <div class="confirm-btn" id="j-confirm" data-type="cart">确定</div>
  </div>
</div>
</#if>
<#include "pay_config.ftl">

<div class="banner">
  <a class="fl" href="/new"><i class="icon i-back"></i></a>
  <span>女装</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<a href="/address">
  <div class="address">
    <i class="i-address icon"></i>
    <div class="address-text">
      <p class="receiver"><span class="tc-light-gray">收货人：</span><span class="receiver-name">魏运昌</span> <span class="receiver-phone fr">13912345678</span></p>
      <p>北京市海淀区学院路40号27号楼1单元508室</p>
    </div>
  </div>
</a>

<div class="item-wrapper">
  <div class="info-wrapper"> 
    <img class="item-thumb fl" src="/static/images/product.png">
    <div class="item-info">
      <p>bread n butter面包黄油品牌女装钉圆领短袖上衣女士套装</p>
      <p class="tc-light-gray">规格：黑色 XL</p>
      <div class="count-box cf">
        <div class="minuse-btn">－</div>
        <div><input class="no-border count-input" type="text" value="1"></div>
        <div class="plus-btn">＋</div>
      </div>      
    </div>
  </div>

  <div class="trans-fee">
    <p>运费：<span class="fr tc-dark-gray tc-bold">¥8.00</span></p>
  </div>
</div>

<div class="pay-wrapper cf">
  <p class="tc-light-gray pay-title">支付方式</p>
  <div>
    <i class="icon i-wechat-pay fl"></i>
    <div class="pay-text">
      <p class="wechat-pay">微信支付</p>
      <p class="tc-light-gray">方便快捷，及时到账</p>
    </div>
    <i class="icon i-check-circle"></i>
  </div>
</div>

<div class="order-pay cf">
  <div class="fr pay-btn">马上支付</div>
  <div class="order-info">
    <p class="cf">
      <span class="fl">共<span class="total-count">1</span>件商品</span>
      <span class="fr">合计：¥<span class="total-price">4756</span>元</span>
    </p>
  </div>
</div>
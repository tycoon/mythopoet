<#include "error_config.ftl">

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>商品详情</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="error-box">
	<div class="error-img icon"></div>
	<p>您查看的商品已售完下架</p>
</div>

<#include "category_config.ftl">
<!-- 搜索遮罩层 -->
<div class="search-mask" id="j-search-mask">
  <div class="search-top">
    <div class="search-form" id="j-search-form">
      <img class="search-form-bg" usemap="#searchmap" src="/static/images/searchForm.png">
      <map name="searchmap" id="searchmap">
        <area shape="rect" coords="200,0,250,40" id="j-search-magnify" />
      </map>

      <input type="text" class="input-border-gray search-input" id="j-search-input" placeholder="请输入搜索的关键字">
      <span class="close-search" id="j-close-serach">关闭</span>
    </div> 
    <ul class="alpha-index cf" id="j-alpha-index">
      <li><a href="#num">#</a></li>
      <li><a href="#A">A</a></li>
      <li><a href="#B">B</a></li>
      <li><a href="#C">C</a></li>
      <li><a href="#D">D</a></li>
      <li><a href="#E">E</a></li>
      <li><a href="#F">F</a></li>
      <li><a href="#G">G</a></li>
      <li><a href="#H">H</a></li>
      <li><a href="#I">I</a></li>
      <li><a href="#J">J</a></li>
      <li><a href="#K">K</a></li>
      <li><a href="#L">L</a></li>
      <li><a href="#M">M</a></li>
      <li><a href="#N">N</a></li>
      <li><a href="#O">O</a></li>
      <li><a href="#P">P</a></li>
      <li><a href="#Q">Q</a></li>
      <li><a href="#R">R</a></li>
      <li><a href="#S">S</a></li>
      <li><a href="#T">T</a></li>
      <li><a href="#U">U</a></li>
      <li><a href="#V">V</a></li>
      <li><a href="#W">W</a></li>
      <li><a href="#X">X</a></li>
      <li><a href="#Y">Y</a></li>
      <li><a href="#Z">Z</a></li>
    </ul> 
  </div>
</div>
<!-- 页面 -->
<div class="find-nav">
  <div class="gender-select-wrapper cf" id="j-gender-select">
    <div class="current"><span class="dot-left">·</span><span>·</span></div>
    <div class="gender-item selected j-tag" data-gender='her' data-tag-id="1" id="j-her">For Her</div>
    <div class="gender-item not-selected j-tag" data-gender='him' data-tag-id="2" id="j-him">For Him</div>
  </div>
  <div class="tabs cf">
    <div class="nav-item j-nav j-designer" data-type="designer">
      <p class="nav-name">设计师</p>  
    </div>    
    <div class="nav-item j-nav j-single" data-type="single">
      <p class="nav-name">所有单品</p>  
    </div>  
    <div class="nav-item selected j-nav j-category" data-type="category">
      <p class="nav-name">品类</p>
    </div>
  </div>
</div>

<div class="search-az" id="j-go-search"><img class="az-img" src="/static/images/az.png"></div>
<div class="main-content cf" id="j-main">

</div>
<div class="loading-box">
	<span class="loading-icon"></span>
</div>
<#include "../widgets/footer.ftl">

<#include "designer_detail_config.ftl">
<title>设计师-${detail.name!''}</title>

<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>设计师</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<#if detail??>
<div class="designer-bg">
  <div class="designer-intro cf">
    <img class="designer-img" src="${detail.pic}">
    <#assign descLength = detail.description?length>
    <#assign limit = 85>
    <#if descLength gt limit>
      <p class="intro-text">
        <span>${detail.description?substring(0,limit)}</span>
        <span id="j-more-ellipse">... </span>
        <span id="j-read-more" class="tc-main-purple">更多</span>
        <span class="hide" id="j-nested-text">${detail.description?substring(limit, descLength)}<span class="tc-main-purple" id="j-close-more">收起</span></span>
      </p>
    <#else>
      <p class="intro-text">${detail.description!''}</p>
    </#if>
  </div>
  <div class="name-fav cf">
    <#if detail.favoritesId != 0>
      <div class="fav fr" id="j-fav" data-id="${detail.favoritesId}">    
        <i class="icon i-faved j-fav-icon"></i>
        <p class="j-fav-text" style="color:#d37eff">已收藏</p>
      </div>
    <#else>
      <div class="fav fr" id="j-fav" data-id="${detail.id}"> 
        <i class="icon i-fav j-fav-icon"></i>
        <p class="j-fav-text">收藏</p>
      </div>
    </#if>  
    <p class="designer-name">${detail.name}</p> 
  </div>
</div>

<div>
  <p class="new-works">最新作品</p>
</div>

<div class="works cf" data-designer-id="${detail.id}" id="j-designer-works">
</div>
<div class="loading-box">
	<span class="loading-icon"></span>
</div>
</#if>
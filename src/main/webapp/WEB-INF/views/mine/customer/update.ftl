<#include "update_config.ftl"> 

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>个人信息</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="address-input-wrapper">
  <span class="input-title">昵称</span>
  <input id="j-name" type="text" disabled="disabled" class="no-border input-area" value="${user.name}">
</div>
<#if customer.sex == 1>
<div class="address-input-wrapper">
  <span class="input-title">性别</span>
  <div class="sex-box" id="j-sex">
	  <input type="radio" name="sex" value="0" />男
	  <input type="radio" name="sex" value="1" checked = "checked" />女
  </div>
</div>
<#else>
<div class="address-input-wrapper">
  <span class="input-title">性别</span>
  <div class="sex-box" id="j-sex">
	  <input type="radio" name="sex" value="0" checked = "checked" />男
	  <input type="radio" name="sex" value="1"  />女
  </div>
</div>
</#if>

<div class="address-input-wrapper">
  <span class="input-title">地区</span>
  <input id="j-area" type="text" class="no-border input-area" value="${customer.area!}" placeholder="未填写">
  <span class="arrow-icon icon"></span>	
</div>


<div class="address-input-wrapper">
  <span class="input-title">真实姓名</span>
  <input id="j-realname" type="text" class="no-border input-area" value="${customer.realname!}" placeholder="未填写">
  <span class="arrow-icon icon"></span>	
</div>

<div class="address-input-wrapper">
  <span class="input-title">生日</span>
  <input id="j-birthday" type="date" value="${customer.birthday?string("yyyy-MM-dd")}" class="no-border input-area" placeholder="未填写">
  <span class="arrow-icon icon"></span>
</div>



<div class="address-input-wrapper">
  <span class="input-title">邮箱</span>
  <input id="j-mail" type="text" class="no-border input-area" value="${customer.mail!}" placeholder="未填写">
  <span class="arrow-icon icon"></span>	
</div>

<div class="address-input-wrapper">
  <span class="input-title">所在行业</span>
  <input id="j-industry" type="text" class="no-border input-area" value="${customer.industry!}" placeholder="未填写">
  <span class="arrow-icon icon"></span>	
</div>


<div class="update-customer-btn" data-id="0" id="j-confirm">保存</div>


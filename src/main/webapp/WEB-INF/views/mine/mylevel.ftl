<#include "../config.ftl">  
<#-- 设置当前页面的基本设置 start-->
<title>我的特权</title>
<content tag="javascript">/topic/temp1</content>
<content tag="css">/home/topic</content>

<#-- 页面内容部分 start -->
<#assign url = "/index">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span style="position:relative;left:12px;vertical-align:-3px;">我的特权</span>
  <i class="icon i-nav-bar fr"></i>
</div>
<!--
<div class="image first-image">
  <img class="bg-img" src="/static/images/topic/level/mylevel1.jpg" alt="image1" />
</div>
<div class="image first-image">
  <img class="bg-img" src="/static/images/topic/level/mylevel2.jpg" alt="image1" />
</div>
<div class="image first-image">
  <img class="bg-img" src="/static/images/topic/level/mylevel3.jpg" alt="image1" />
</div>
<div class="image first-image">
  <img class="bg-img" src="/static/images/topic/level/mylevel4.jpg" alt="image1" />
</div>
-->

<div class="user-head">
	<img src="/static/images/avatar2.png" class="user-photo">
	<p class="user-name">${user.name}</p>
</div>
<div class="level-box">
	<div class="level-head">
		<div class="lead-head-cont">
			<a href="javascript:;">
				<i class="icon level-icon0"></i>
				<span class="level-text">普通</span>
			</a>
			<a href="javascript:;">
				<i class="icon level-icon1"></i>
				<span class="level-text">金牌</span>
				<em class="level-line"></em>
			</a>
			<a href="javascript:;">
				<i class="icon level-icon2"></i>
				<span class="level-text">铂金</span>
				<em class="level-line"></em>
			</a>
			<a href="javascript:;">
				<i class="icon level-icon3"></i>
				<span class="level-text">钻石</span>
				<em class="level-line"></em>
			</a>
		</div>
	</div>
	<div id="customer-level" class="level-cont" data-level="${customer.level}">
		<p class="level-title">
			<span class="title-line"></span>
			<span class="title-mid">我的特权</span>
			<span class="title-line"></span>
		</p>
		<div class="level1-cont">
			<p class="level-conditions">升级条件：注册即可获得普通会员资格<br>会员特权：</p>
			<p>1.享受全场免运费服务</p>
			<p>2.（付费）可享受修改衣服服务</p>
			<p>3.可以累计积分；并在活动期间享受积分换代金券的机会</p>
			<p>4.可以参与绘事后素定期举办的线下活动（付费）</p>
		</div>
		<div class="level2-cont">
			<p class="level-conditions">升级条件：一年内累计消费金额达20万元以上（含）<br>会员特权：</p>
			<p>1.可以享受金牌会员所能购买的产品及服务</p>
			<p>2.享受全场免运费服务</p>
			<p>3.可以参与金牌会员级别所能参与的商品拍卖</p>
			<p>4.（付费）可享受修改衣服服务。只限购买之前与客服进行沟通</p>
			<p>5.不定期举办个别产品针对金牌会员的优惠活动</p>
			<p>6.享有优先购物权——对国内少见的优秀产品或者其它比较紧俏的产品具有优先购买权</p>
			<p>7.可以累计积分；并在活动期间享受积分换代金券的机会</p>
		</div>
		<div class="level3-cont">
			<p class="level-conditions">升级条件：一年内累计消费金额达50万元以上（含）<br>会员特权：</p>
			<p>1.可以享受铂金会员所能购买的产品及服务</p>
			<p>2.享受全场免运费服务</p>
			<p>3.可以参与铂金会员级别所能参与的商品拍卖</p>
			<p>4.（付费）可享受修改衣服服务。只限购买之前与客服进行沟通</p>
			<p>5.不定期举办个别产品针对铂金会员的优惠活动</p>
			<p>6.享有优先购物权——对国内少见的优秀产品或者其它比较紧俏的产品具有优先购买权</p>
			<p>7.会员生日会收到惊喜精美专属礼品</p>
			<p>8.会员终身享有绘事后素所有商品9.8折优惠</p>
			<p>9.可以累计积分；并在活动期间享受积分换代金券的机会</p>
		</div>
		<div class="level4-cont">
			<p class="level-conditions">升级条件：一年内累计消费金额达100万元以上（含）<br>会员特权：</p>
			<p>1.可以享受钻石会员所能购买的产品及服务</p>
			<p>2.享受全场免运费服务</p>
			<p>3.可以参与钻石会员级别所能参与的商品拍卖</p>
			<p>4.可享受修改衣服服务。北京客户可享受上门量体服务</p>
			<p>5.不定期举办个别产品针对钻石会员的优惠活动</p>
			<p>6.享有优先购物权——对国内少见的优秀产品或者其它比较紧俏的产品具有优先购买权</p>
			<p>7.每年10次以上的VIP线下活动，包括并不限于乐享会，时尚PARTY，紫菲私人搭配分享会</p>
			<p>8.会员生日会收到惊喜精美专属礼品</p>
			<p>9.享有绘事后素高管定期提供的沟通服务；享有客服专员定期回访征询意见服务；并在回访期间收到神秘大礼</p>
			<p>10.终身享有绘事后素所有商品9.5折优惠</p>
			<p>11.钻石会员邀请注册会员直接升级为金牌会员</p>
			<p>12. 可以累计积分；并在活动期间享受积分换代金券的机会</p>
		</div>
	</div>
</div>


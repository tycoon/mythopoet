<#include "add_config.ftl">  

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>收货地址</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="address-input-wrapper">
  <span class="input-title">收货人</span>
  <input id="j-name" type="text" class="no-border input-area" size="30">
</div>

<div class="address-input-wrapper">
  <span class="input-title">手机号码</span>
  <input id="j-phone" type="text" class="no-border input-area" size="30">
</div>

<div class="address-input-wrapper">
  <span class="input-title">省/市</span>
  <input id="j-province" type="text" class="no-border input-area" size="30">
</div>

<div class="address-input-wrapper">
  <span class="input-title">城市</span>
  <input id="j-city" type="text" class="no-border input-area" size="30">
</div>
<div class="address-input-wrapper">
  <span class="input-title">区县</span>
  <input id="j-county" type="text" class="no-border input-area" size="30">
</div>


<div class="address-input-wrapper">
  <span class="input-title">详细地址</span>
  <input id="j-detail" type="text" class="no-border input-area" size="35">
</div>

<p class="err-msg" id="j-err"></p>

<p class="set-as-default">
  <#-- <i class="icon i-check-circle-border"></i> -->
  <i class="icon i-check-circle-border" id="j-set-default"></i>
  <span class="tc-light-gray">设置为默认收货地址</span>
</p>

<div class="add-address-btn" data-id="0" id="j-confirm">确定</div>

<#include "mine_config.ftl">  

<div class="banner">
  个人中心
  <i id="j-settings" class="icon i-settings fr"></i>
  <div class="setting-dropdown" id="j-setting-dropdown">
    <a style="color:#666" href="" id="j-logout">退出登录</a>  
  </div>
</div>

<#if user??>
<div class="avatar-wrapper">
 	<a href="/customer/info/${user.id}" >
  	<img class="user-avatar" src="${user.avatar!'/static/images/avatar2.png'}"></a>
     <#if customer??>
 	<p class="user-info"><i class="icon i-customer-level${customer.level}"></i><span class="username-text">${user.name!''}</span></p>
  <span id="mine-level" data-level="${customer.level}"></span>
  <#else>
  <p class="user-info"><span class="username-text">${user.name!''}</span></p>
</#if>
  
  <div class="status-wrapper cf">
    <a class="status" href="/order/page?status=0">
      <i class="icon i-to-pay"></i>
      <p>待付款
        <#if waitPayCount?exists && (waitPayCount>0) >
        <span class="order-count">${waitPayCount}</span>
        </#if>
      </p>
    </a>
    <a class="status" href="/order/page?status=1">
      <i class="icon i-to-deliver"></i>
      <p>待发货
        <#if waitRecCount?exists && (waitDelCount>0) >        
          <span class="order-count">${waitDelCount}</span>
        </#if>
      </p>
    </a>
    <a class="status" href="/order/page?status=2">
      <i class="icon i-to-receive"></i>
      <p>待收货
        <#if waitRecCount?exists && (waitRecCount>0) >        
          <span class="order-count">${waitRecCount}</span>
        </#if>
      </p>
    </a>
  </div>
</div>
</#if>

<div class="mine-nav">
  <a class="nav-item" href="/order/page">
    <i class="icon i-order"></i>
    <span>我的订单</span>
  </a>
  <a class="nav-item" href="/favorites">
    <i class="icon i-my-fav"></i>
    <span>我的收藏</span>
  </a>
  <a class="nav-item" href="/address">
    <i class="icon i-address"></i>
    <span>收货地址</span>
  </a>
  <div class="div-box"></div>
  <a class="nav-item" href="/customer/level/${user.id}">
    <i class="icon i-my-privilege"></i>
    <span>我的特权</span>
  </a>
    <a class="nav-item" href="/sale/coupon/">
    <i class="icon i-my-coupon"></i>
    <span>我的红包</span>
  </a>
  <a class="nav-item" href="/customer/getvip/${user.id}">
    <i class="icon i-vip-active"></i>
    <span>VIP卡激活</span>
  </a>
  <a class="nav-item" href="/order/refund">
    <i class="icon i-after-sell"></i>
    <span>售后服务</span>
  </a>
</div>

<#include "../widgets/footer.ftl">

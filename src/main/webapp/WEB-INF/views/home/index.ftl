﻿<#include "index_config.ftl">

<div class="index-body">
  <div class="tabs fixed-top-tabs cf">
    <div class="nav-item  selected">
      <a class="nav-name" href="/">
        精品专题
      </a>  
    </div>
    <div class="nav-item">
      <a class="nav-name" href="/new">
        个性潮品
      </a>
    </div>
  </div>

  <div class="main-content">   
    <div>
      <ul class="slides" id="j-slide-container">
        <#if slides ??>
        <#list slides as slide>
          <li><a class="slide" href="${slide.url}">
            <img src="${slide.pic}">
          </a></li>
        </#list>
        </#if>
      </ul>     
    </div>

	<div id="j-topic-1028">
    	<a class="section" href="/topic/37"> 
          <img class="topic" src="/static/images/topic/cover/20151203/001.jpg">
     </a>
        
    </div>
    <div id="j-topic-1028">
    	<a class="section" href="/topic/36"> 
          <img class="topic" src="/static/images/topic/cover/20151202/001.jpg">
     </a>
        
    </div>
    <div id="j-topic-container-h-s">
    	<a class="section" href="/topic/34"> 
          <img class="topic" src="/static/images/topic/cover/20151125/001.jpg">
        </a>
        <a class="section" href="/topic/33"> 
          <img class="topic" src="/static/images/topic/cover/20151125/002.jpg">
        </a>
    </div>
    <div id="j-topic-1028">
    	<a class="section" href="/topic/31"> 
          <img class="topic" src="/static/images/topic/cover/20151118/001.jpg">
     </a>
     </div>
     <div id="j-topic-1028">
    	<a class="section" href="/topic/32"> 
          <img class="topic" src="/static/images/topic/cover/20151118/002.jpg">
     </a>
     </div>
    <div id="j-topic-container-h-s">
    	<a class="section" href="/topic/29"> 
          <img class="topic" src="/static/images/topic/cover/20151104/003.jpg">
        </a>
        <a class="section" href="/topic/30"> 
          <img class="topic" src="/static/images/topic/cover/20151104/004.jpg">
        </a>
    </div>
   
    <div id="j-topic-1028">
    	<a class="section" href="/topic/23"> 
          <img class="topic" src="/static/images/topic/cover/20151028/003.jpg">
     </a>
        
    </div>
    <div id="j-topic-container-h-s">
    	<a class="section" href="/topic/26"> 
          <img class="topic" src="/static/images/topic/cover/20151020/001.jpg">
        </a>
        <a class="section" href="/topic/25"> 
          <img class="topic" src="/static/images/topic/cover/20151020/002.jpg">
        </a>
    </div>
    
    <div id="j-topic-1028">
    	<a class="section" href="/topic/22"> 
          <img class="topic" src="/static/images/topic/cover/20151028/002.jpg">
     </a>
        
    </div>
    <div class="aa">
      <div class="bb">
      <a href="/topic/20"><img src="/static/images/topic/cover/20150921/001.jpg"/></a>
      </div>
      <div class="cc">
            <div class="cc-1">
            <a href="/topic/21"><img src="/static/images/topic/cover/20150921/002.jpg"/></a>
            </div>
            <div style=" clear:both"></div>
            <div class="cc-2">
            <a href="/topic/18"><img src="/static/images/topic/cover/20150921/003.jpg"/></a>
            </div>
      </div>
	</div>
    <div id="j-topic-0914">
    	<a class="section" href="/topic/19"> 
          <img class="topic" src="/static/images/topic/cover/20150914/002.jpg">
        </a>
        
    </div>
    
    <div class="aa">
	  <div class="cc">
        <div class="cc-1">
        	<a href="/topic/17"><img src="/static/images/topic/cover/20150908/001.gif"/></a>
        </div>
        <div style=" clear:both"></div>
        <div class="cc-2">
        	<a href="/topic/14"><img src="/static/images/topic/cover/20150908/002.jpg"/></a>
        </div>
	  </div>
      <div class="bb">
      	<a href="/topic/16"><img src="/static/images/topic/cover/20150908/003.jpg"/></a>
      </div>
      
	</div>
	 
    <div id="j-topic-0831">
        <a class="section" href="/topic/15"> 
          <img class="topic" src="/static/images/topic/cover/20150831/005.jpg">
        </a>
    </div>
    <div class="aa">
      <div class="bb">
      <a href="/topic/13"><img src="/static/images/topic/cover/20150831/001.jpg"/></a>
      </div>
      <div class="cc">
            <div class="cc-1">
            <a href="/topic/12"><img src="/static/images/topic/cover/20150831/002.jpg"/></a>
            </div>
            <div style=" clear:both"></div>
            <div class="cc-2">
            <a href="/topic/11"><img src="/static/images/topic/cover/20150831/003.jpg"/></a>
            </div>
      </div>
	</div>
    
    <div id="j-topic-container">
    	<a class="section" href="/topic/9"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1440405543786.gif">
        </a>
    </div>
    <div id="j-topic-container-h-s">
    	<a class="section" href="/topic/3"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1438239559157.gif">
        </a>
        <a class="section" href="/topic/5"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1438239614583.gif">
        </a>
    </div>
    <div id="j-history-container">
    	<a class="section" href="/topic/2"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1438314388341.gif">
        </a>
        <a class="section" href="/topic/8"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1439211908940.gif">
        </a>
         <a class="section" href="/topic/4"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1435920486386.jpg">
        </a>
        
        
    
    	<a class="section" href="/topic/1"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1435920508236.jpg">
        </a>
        <a class="section" href="/topic/7"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1438597218555.jpg">
        </a>
         <a class="section" href="/topic/6"> 
          <img class="topic" src="http://7xjd0u.com2.z0.glb.qiniucdn.com/topic/pic/1438597244431.jpg">
        </a>
    </div>

  </div>

  <#include "../widgets/footer.ftl">
</div>
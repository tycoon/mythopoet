<#include "new_config.ftl">

<div class="tabs fixed-top-tabs cf">
  <div class="nav-item" >
    <a class="nav-name" href="/">
      精品专题
    </a>  
  </div>
  <div class="nav-item selected">
    <a class="nav-name" href="/new">
      个性潮品
    </a>
  </div>
</div>

<div class="vertical-tabs">
  <div class="nav-item selected j-nav" data-type="new" data-tag="1" data-cid="1">
    <div class="nav-name">
      <p class="zh">新品女装</p>
      <#-- <p class="en">NEW</p> -->
    </div>  
  </div>
  <div class="nav-item j-nav bordered" data-type="new" data-tag="2" data-cid="2">
    <div class="nav-name">
      <p class="zh">新品男装</p>
      <#-- <p class="en">NEW</p> -->
    </div>  
  </div>
  <div class="nav-item j-nav" data-type="hot" data-tag="1" data-cid="3">
    <div class="nav-name">
      <p class="zh">热销女装</p>
      <#-- <p class="en">CAKES</p> -->
    </div>  
  </div> 
  <div class="nav-item j-nav bordered" data-type="hot" data-tag="2" data-cid="4">
    <div class="nav-name">
      <p class="zh">热销男装</p>
      <#-- <p class="en">CAKES</p> -->
    </div>  
  </div> 

  <div class="nav-item j-nav" data-type="discount" data-tag="1" data-cid="5">
    <div class="nav-name">
      <p class="zh">折扣女装</p>
      <#-- <p class="en">DEAL</p> -->
    </div>  
  </div>    
  <div class="nav-item j-nav" data-type="discount" data-tag="2" data-cid="6">
    <div class="nav-name">
      <p class="zh">折扣男装</p>
      <#-- <p class="en">DEAL</p> -->
    </div>  
  </div>    
</div>

<div class="main-content cf" id="j-item-container">  
</div>

<#include "../widgets/footer.ftl">
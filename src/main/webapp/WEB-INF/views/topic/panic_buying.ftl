<#include "panic_buying_config.ftl">

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>限时抢购</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="time-tab">
	<a href="javascript:;" id="flash-buy-1">
		<i class="icon icon-time " id="icon-time-1"></i>
		<span>10:00</span>
	</a>
	<a href="javascript:;" id="flash-buy-2">
		<i class="icon icon-time cur" id="icon-time-2"></i>
		<span>14:00</span>
	</a>
	<a href="javascript:;" id="flash-buy-3">
		<i class="icon icon-time " id="icon-time-3"></i>
		<span>20:00</span>
	</a>
</div>

<div id="flash-pro-cont-1">
	<div class="time time1">
		<span class="time-text"></span><span class="timing"></span>
	</div>
	<ul class="flash-menu" id="flash-menu-1 style="display:none;" >
	</ul>
	<ul class="flash-menu" id="flash-menu-2" ">
	</ul>
	<ul class="flash-menu" id="flash-menu-3" style="display:none;">
	</ul>
</div>



<#include "topic.ftl">
<#-- 设置当前页面的基本设置 start-->
<title>情迷の原宿</title>
 
<#-- 页面内容部分 start -->
<#assign url = "/index">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>情迷の原宿</span>
  <i class="icon i-nav-bar fr"></i>
</div>
<div class="image" style="margin-top: 45px;">
<div class="image zero-image">
   <blockquote>
     <blockquote>
       <p><img class="bg-img" id="j-image-first" src="/static/images/topic/top/2015/30/0.jpg" alt="image"> 
       </p>
     </blockquote>
   </blockquote>
 </div>

<div class="image one-image">
  <img class="bg-img" src="/static/images/topic/top/2015/30/1.jpg" alt="image">
  <a href="/item/detail/1813"  style="top: 76%;left: 4%;width: 16%;height:18%;"></a>
  <a href="/item/detail/1814"  style="top: 77%;left: 30%;width: 17%;height:15%;"></a> 
  <a href="/item/detail/1717"  style="top: 80%;left: 55%;width: 18%;height:11%;"></a>
  <a href="/item/detail/1815"  style="top: 80%;left: 79%;width: 17%;height:10%;"></a>
  </div>

  <div class="image two-image">
  <img class="bg-img" src="/static/images/topic/top/2015/30/2.jpg" alt="image">
  <a href="/item/detail/1323"  style="top: 75%;left: 2%;width: 20%;height:15%;"></a>
  <a href="/item/detail/1760"  style="top: 78%;left: 27%;width: 20%;height:11%;"></a>  
   <a href="/item/detail/1515"  style="top: 78%;left: 52%;width: 20%;height:11%;"></a>
  <a href="/item/detail/1816"  style="top: 75%;left: 80%;width: 17%;height:18%;"></a>
 
  </div>

  <div class="image three-image">
  <img class="bg-img" src="/static/images/topic/top/2015/30/3.jpg" alt="image">						
  <a href="/item/detail/1645"  style="top: 75%;left: 2%;width: 20%;height:18%;"></a>
  <a href="/item/detail/1497"  style="top: 80%;left: 27%;width: 22%;height:10%;"></a>  
  <a href="/item/detail/1437"  style="top: 75%;left: 55%;width: 17%;height:18%;"></a>
  <a href="/item/detail/326"  style="top: 78%;left: 80%;width: 18%;height:12%;"></a>
  </div>

  <div class="image four-image">
  <img class="bg-img" src="/static/images/topic/top/2015/30/4.jpg" alt="image">						
  <a href="/item/detail/1689"  style="top: 75%;left: 2%;width: 20%;height:18%;"></a>
  <a href="/item/detail/1812"  style="top: 80%;left: 29%;width: 17%;height:10%;"></a>  
  <a href="/item/detail/500"  style="top: 79%;left: 54%;width: 19%;height:10%;"></a>
  <a href="/item/detail/1668"  style="top: 80%;left: 80%;width: 18%;height:12%;"></a>
  </div>
 	  
</div>
<#include "topic_footer.ftl">  
  

 





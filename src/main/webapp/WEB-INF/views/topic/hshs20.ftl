<#include "topic.ftl">
<#-- 设置当前页面的基本设置 start-->
<title>公子佳人</title>

<#-- 页面内容部分 start -->
<#assign url = "/index">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>公子佳人</span>
  <i class="icon i-nav-bar fr"></i>
</div>
<div class="image" style="margin-top: 45px;"> 
 <div class="image zero-image">
  <img class="bg-img" id="j-image-first" src="/static/images/topic/top/2015/20/20_01.jpg" alt="image">
  
  </div>
  <div class="image one-image">
  <img class="bg-img" src="/static/images/topic/top/2015/20/20_02.jpg" alt="image">
  <a href="/item/detail/865"  style="top: 55%;left: 23%;width: 35%;height:22%;"></a>
  <a href="/item/detail/1205" style="top: 55%;left: 63%;width: 35%;height:22%;"></a>
  <a href="/item/detail/326" style="top: 80%;left: 23%;width: 35%;height:20%;"></a>
  <a href="/item/detail/1501" style="top: 80%;left: 63%;width: 35%;height:20%;"></a>
   
  </div>
  <div class="image two-image">
  <img class="bg-img" src="/static/images/topic/top/2015/20/20_03.jpg" alt="image">
  <a href="/item/detail/1548" style="top: 50%;left: 0%;width: 35%;height:24%;"></a>
  <a href="/item/detail/731" style="top: 50%;left:40%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1416" style="top: 75%;left: 0%;width: 35%;height:24%;"></a>
  <a href="/item/detail/674" style="top: 75%;left: 40%;width: 35%;height:24%;"></a>
  </div>
  <div class="image three-image">
  <img class="bg-img" src="/static/images/topic/top/2015/20/20_04.jpg" alt="image">						
  <a href="/item/detail/832" style="top: 50%;left: 23%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1331" style="top: 50%;left: 63%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1419" style="top: 75%;left: 23%;width: 35%;height:24%;"></a>
  <a href="/item/detail/849" style="top: 75%;left: 63%;width: 35%;height:24%;"></a>
  </div>
  <div class="image four-image">
  <img class="bg-img" src="/static/images/topic/top/2015/20/20_05.jpg" alt="image">						
  <a href="/item/detail/1623" style="top: 50%;left: 0%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1505" style="top: 50%;left:40%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1488" style="top: 75%;left: 0%;width: 35%;height:24%;"></a>
  <a href="/item/detail/1337" style="top: 75%;left: 40%;width: 35%;height:24%;"></a>
  
  </div>
  

</div>
<#include "topic_footer.ftl">  
  

 





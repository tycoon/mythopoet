<#include "topic.ftl">
<#-- 设置当前页面的基本设置 start-->
<title>英伦“轻型者”</title>
 
<#-- 页面内容部分 start -->
<#assign url = "/index">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>英伦“轻型者”</span>
  <i class="icon i-nav-bar fr"></i>
</div>
<div class="image" style="margin-top: 45px;">
<div class="image zero-image">
   <blockquote>
     <blockquote>
       <p><img class="bg-img" id="j-image-first" src="/static/images/topic/top/2015/29/0.jpg" alt="image"> 
       </p>
     </blockquote>
   </blockquote>
 </div>

<div class="image one-image">
  <img class="bg-img" src="/static/images/topic/top/2015/29/1.jpg" alt="image">
  <a href="/item/detail/1754"  style="top: 42%;left: 65%;width: 34%;height:38%;"></a>
  <a href="/item/detail/1109"  style="top: 60%;left: 45%;width: 20%;height:30%;"></a> 
  <a href="/item/detail/1687"  style="top: 53%;left: 40%;width: 24%;height:6%;"></a>
  <a href="/item/detail/953"  style="top: 81%;left: 72%;width: 25%;height:12%;"></a>
  </div>

  <div class="image two-image">
  <img class="bg-img" src="/static/images/topic/top/2015/29/2.jpg" alt="image">
  <a href="/item/detail/1758"  style="top: 12%;left: 3%;width: 35%;height:48%;"></a>
  <a href="/item/detail/1563"  style="top: 30%;left: 40%;width: 10%;height:19%;"></a>  
  <a href="/item/detail/956"  style="top: 65%;left: 5%;width: 25%;height:14%;"></a>
 <a href="/item/detail/1660"  style="top: 51%;left: 33%;width: 20%;height:39%;"></a>
 
  </div>

  <div class="image three-image">
  <img class="bg-img" src="/static/images/topic/top/2015/29/3.jpg" alt="image">						
  <a href="/item/detail/1755"  style="top: 0%;left: 57%;width: 40%;height:60%;"></a>
  <a href="/item/detail/318"  style="top: 50%;left: 38%;width: 19%;height:12%;"></a>  
  <a href="/item/detail/1152"  style="top: 64%;left: 45%;width: 20%;height:15%;"></a>
  <a href="/item/detail/1770"  style="top: 65%;left: 70%;width: 28%;height:19%;"></a>
  </div>

  <div class="image four-image">
  <img class="bg-img" src="/static/images/topic/top/2015/29/4.jpg" alt="image">						
  <a href="/item/detail/1756"  style="top: 7%;left: 4%;width: 35%;height:63%;"></a>
  <a href="/item/detail/878"  style="top: 35%;left: 40%;width: 17%;height:30%;"></a>  
  <a href="/item/detail/457"  style="top: 70%;left: 3%;width: 27%;height:15%;"></a>
  <a href="/item/detail/1775"  style="top: 73%;left: 30%;width: 30%;height:20%;"></a>
  </div>

  <div class="image four-image">
  <img class="bg-img" src="/static/images/topic/top/2015/29/5.jpg" alt="image">						
  <a href="/item/detail/1757"  style="top: 5%;left: 59%;width: 40%;height:50%;"></a>
  <a href="/item/detail/475"  style="top: 38%;left: 42%;width: 19%;height:10%;"></a>  
  <a href="/item/detail/250"  style="top: 49%;left: 44%;width: 25%;height:40%;"></a>
  <a href="/item/detail/950"  style="top: 63%;left: 70%;width: 27%;height:15%;"></a>
  </div>
 	  
</div>
<#include "topic_footer.ftl">  
  

 





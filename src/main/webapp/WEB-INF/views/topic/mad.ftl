<#include "../config.ftl">
<#-- 设置当前页面的基本设置 start-->
<title>水墨印花</title>
<content tag="javascript">/topic/temp1</content>
<content tag="css">/home/topic</content>

<#-- 页面内容部分 start -->
<#assign url = "/index">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>水墨印花</span>
  <i class="icon i-nav-bar fr"></i>
</div>
<div class="image first-image">
<img class="bg-img" id="j-image-first" src="/static/images/topic/top/mad/shuimo.gif"  alt="image1" />

<#--图片链接顺序    从左到右，从上到下 -->
<#--part1  -->
<a href="/item/detail/100" style="top: 22%;left: 6%;width: 34%;height: 18%;"></a>
<a href="/item/detail/319" style="top: 27%;left: 45%;width: 13%;height: 3%;"></a>
<a href="/item/detail/329" style="top: 25%;left: 66%;width: 30%;height: 7%;"></a>
<a href="/item/detail/322" style="top: 33%;left: 45%;width: 30%;height: 4%;"></a>
<a href="/item/detail/328" style="top: 33%;left: 80%;width: 15%;height: 7%;"></a>

<#--part2 -->
<#--xiang lian -->
<a href="/item/detail/323" style="top: 50%;left: 10%;width: 20%;height: 13%;"></a>
<#--xie  -->
<a href="/item/detail/320" style="top: 65%;left: 0%;width: 25%;height: 6%;"></a>
<#--qunzi  -->
<a href="/item/detail/97" style="top: 50%;left: 36%;width: 27%;height: 20%;"></a>
<#--jiezhi  -->
<a href="/item/detail/324" style="top: 52%;left: 66%;width: 25%;height: 3%;"></a>
<#--jiezhi  -->
<a href="/item/detail/321" style="top: 58%;left: 66%;width: 25%;height: 4%;"></a>


<#--part3 -->
<#--yao dai -->
<a href="/item/detail/326" style="top: 85%;left: 12%;width: 20%;height: 4%;"></a>
<#--xie -->
<a href="/item/detail/325" style="top: 91.5%;left: 9%;width: 23.5%;height: 5%;"></a>
<#--qunzi  -->
<a href="/item/detail/93" style="top: 80%;left: 37.5%;width: 27%;height: 18%;"></a>

<#--er zhui  -->
<a href="/item/detail/330" style="top: 79%;left: 70%;width: 26%;height: 9%;"></a>
<#--jiezhi  -->
<a href="/item/detail/327" style="top: 91%;left: 69%;width: 25%;height: 5%;"></a>

</div>

<#include "ticket_config.ftl">  

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>使用优惠</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="ticket-type from-bank j-ticket-type" data-type-id="1">
    <div class="disCount">
      <p>使用代金券<span class="xztb"></span></p>
      <span class="check-box" id="check-button"></span>
      <i class="icon i-check-tick"></i>
    </div>
    <div class="yzm">
       <input type="text" placeholder="请输入代金券激活码" id="t-pword" class="disCountPword"/>
       <input type="button" value="验证" id="button" class="verifyBtn" >
    </div>
</div>


<div class="ticket-type from-bank j-ticket-type" data-type-id="2" id="member-discount" style="display:none">
    <div class="disCount">
      <p>使用会员专享折扣<span class="xztb"></span></p>
      <span class="check-box" id="check-button2"></span>
      <i class="icon i-check-tick"></i>
    </div>
</div>

<p class="ticket-tips">说明：折扣和代金券不可以同时使用</p>


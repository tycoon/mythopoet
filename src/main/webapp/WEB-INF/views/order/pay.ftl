<#include "pay_config.ftl">  

<div class="banner">
  <a class="fl" href="/order/page" id="j-back"><i class="icon i-back"></i></a>
  <span>订单详情</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<#if order??>

    <#if errMsg??>
        <span style="color: red">${errMsg}</span>
    </#if>
  <div class="address">
    
      <#assign address = order.cneeAddress >
      <i class="i-address icon"></i>
      <div class="address-text" id="j-address" data-id="${address.id}">
        <p class="receiver"><span class="tc-light-gray">收货人：</span><span class="receiver-name">${address.name}</span> <span class="receiver-phone fr">${address.phone}</span></p>
        <p>${address.province+address.city+address.county+address.detail}</p>
      </div>
   
  </div>


<p class="order-view-id tc-light-gray">订单编号：${order.viewId}</p>
<#assign itemList = order.orderDetailViewList >
<div class="item-wrapper">
  <#list itemList as item>
  <a class="item-link" href="/item/detail/${item.itemId}">
    <div class="info-wrapper cf"> 
      <img class="item-thumb fl" src="${item.pic?split(",")[0]}">
      <div class="item-info">
        <p class="item-name">${item.itemSku.name}</p>
        <p class="tc-light-gray">规格：${item.propsValue}</p>
        <p>数量：${item.count}</p>
        <p class="item-price">
          <#if item.itemSku.discount?? &&  item.itemSku.discount lt 1>
            <span id="item-price" data-price="${item.price}" class="produce-price price discount-price">¥${item.price?string(",##0.00")}</span>          
          </br>
            <span class="produce-price line-through">¥${item.originPrice?string(",##0.00")}</span>
          <#else>
            <span id="item-price" data-price="${item.price}" class="produce-price price">¥${item.price?string(",##0.00")}</span>
          </#if>
        </p>
      </div>
    </div>
  </a>
  </#list>
<#--   <div class="trans-fee">
    <p>运费：<span class="fr tc-dark-gray tc-bold">¥0.00</span></p>
  </div>
 -->
</div>

<#if order.status == 0>
<div class="pay-wrapper cf" id="j-pay-type-weixin" data-isWeixin="${isWeixin?string('true','false')}">
  <p class="tc-light-gray pay-title">请选择以下支付方式</p>
  <#if isWeixin>
  <div class="pay-type j-pay-type" data-type-id="0">
    <i class="icon i-wechat-pay fl"></i>
    <div class="pay-text">
      <p class="wechat-pay">微信支付</p>
      <p class="tc-light-gray">方便快捷，及时到账</p>
    </div>
    <span class="check-box"></span>
    <i class="icon i-check-tick"></i>
  </div>
  </#if>
	  <div class="pay-type from-bank j-pay-type" data-type-id="1">
	    <i class="icon i-ali-pay fl"></i>
	    <div class="pay-text">
	      <p class="wechat-pay">支付宝</p>
	      <p class="tc-light-gray">方便快捷，及时到账</p>
	    </div>
	    <span class="check-box"></span>
	    <i class="icon i-check-tick"></i>
	  </div>
  <div class="pay-type from-bank j-pay-type" data-type-id="5">
    <i class="icon i-bank-card-pay fl"></i>
    <div class="pay-text">
      <p class="wechat-pay">线下支付</p>
      <p class="tc-light-gray" id="j-read-more">点击查看详情<i class="icon i-arrow-down read-more" id="j-arrow"></i> </p>
    </div>    
    <div class="reminder" id="j-reminder">
      <p style="font-weight:700;padding-bottom:2px;margin-bottom:2px;">注意事项：如果超出微信支付限额请通过以下账号银行转账方式，并注明转账人姓名等信息</p>
      <p>企业名称：北京绘事后素文化有限公司</p>
      <p>银行账号：0200 2113 0920 0013 613</p>
      <p>大额支付号：1021 0000 2020</p>
      <p>支行信息：工商银行北京金台路支行</p>
      <p>客服电话：010-65008611</p>
    </div>
    <span class="check-box"></span>
    <i class="icon i-check-tick"></i>
  </div>
</div>
</#if>


    <div class="remark">
      <p class="tc-light-gray">订单备注:</p>
      <p>${order.remark}</p>
    </div>


<div class="order-pay cf">
  <#if order.status == 0>
    <a class="fr pay-btn" id="j-pay" data-id="${order.id!'0'}">马上支付</a>
  </#if>
  <div class="order-info <#if order.status == 0>order-to-pay</#if>">
    <p class="cf">
      <span class="fl">共<span class="total-count">${order.totalCount}</span>件商品</span>
      <span class="fr">实付款：¥<span id="number-price" data-price="${order.amount}" class="total-price bold-num">${order.amount}</span>元</span>
    </p>
  </div>
</div>
</#if>
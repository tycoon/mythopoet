<#include "login_config.ftl">
<div class="banner">
  <a class="fl" href="javascript:void(0)"  id="j-back"><i class="icon i-back"></i></a>
  <span>登录</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="login-content">
  <img class="logo" src="/static/images/logo2.png"> 
  <p class="err-msg" id="j-err"></p>
  <div class="login-form">
    <div class="user-input">
      <i class="icon i-user"></i><input id="j-user" class="no-border" type="text" placeholder="请输入用户名/手机号">
    </div>
    <div class="psw-input">
      <i class="icon i-psw"></i><input id="j-psw" type="password" placeholder="密码">
    </div>
    <div class="login-btn" id="j-login-btn">登录</div>
  </div>

  <div class="links cf">
    <a class="fr" href="/user/password" style="color:#707070">忘记密码？</a>
    <p class="fl" style="color:#707070">还没有账号？<a href="/user/register" style="color:#333"><strong>30秒注册</strong></a></p>
  </div>

<#--   <div class="third-party-login">
    <p><i class="icon i-login-other"></i></p>
    <p class="other-icons"><i class="icon i-wechat"></i></p>
  </div>
 -->
</div>

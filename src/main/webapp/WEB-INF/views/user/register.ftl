<#include "register_config.ftl">

<div class="banner">
  <a class="fl" href="/"><i class="icon i-back"></i></a>
  <span>注册</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div>
  <ul class="slides" id="j-slide-container">
      <li><a class="slide" href="#">
        <img src="/static/images/reg1.gif">
      </a></li>
      <li><a class="slide" href="/topic/4">
        <img src="/static/images/reg2.jpg">
      </a></li>
     
  </ul>     
</div>

<div class="login-content">

	
 <!--  <img class="logo" src="../../static/images/logo2.png">   -->
  <div class="login-form">
    <div class="user-input">
      <i class="icon i-user"></i><input  id="j-user-input" class="no-border" type="text" placeholder="用户名">
    </div>
    <div class="phone-input">
      <input  id="j-phone-input" class="no-border" type="text" placeholder="手机号码">
    </div>
    <div class="psw-input">
      </i><input type="tel" id="j-captcha-input" placeholder="请输入验证码">
      <div class="send-btn" id="j-get-captcha">获取验证码</div>
    </div>
    <div class="psw-input">
      <i class="icon i-psw"></i><input type="password" id="j-password" placeholder="6-24位英文与数字组合作为密码">
    </div>
    <div class="psw-input">
      <i class="icon i-psw"></i><input id="j-repassword" type="password" placeholder="请再次输入你的密码">
    </div>
    <p class="err-msg" id="j-err-msg"></p>
    <div class="login-btn" id="j-register">注册</div>
  </div>
  <#-- <p>已同意绘事后素注册协议</p> -->
  <p class="login-direct">已有账号请<a href="/user/login">直接登录</a></p>
  <div class="third-party-login">
  </div>
</div>
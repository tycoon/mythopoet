<#include "psw_config.ftl">
<div class="banner">
  <a class="fl" href="javascript:void(0)" id="j-back"><i class="icon i-back"></i></a>
  <span>找回密码</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<div class="login-content">
  <img class="logo" src="/static/images/logo-title.png"> 
  <p class="err-msg" id="j-err"></p>
  <div class="login-form">
    <div class="user-input">
      <i class="icon i-user"></i><input id="j-phone-input" class="no-border" type="text" placeholder="请输入您的注册手机号码">
    </div>
    <div class="psw-input">
      </i><input type="tel" id="j-captcha-input" placeholder="请输入验证码">
      <div class="send-btn" id="j-get-captcha">获取验证码</div>
    </div>
    <div class="psw-input">
      <i class="icon i-psw"></i><input id="j-password" type="password" placeholder="新密码">
    </div>
    <div class="psw-input">
      <input id="j-repassword" type="password" placeholder="确认新密码">
    </div>

    <div class="login-btn" id="j-get-btn">找回密码</div>
  </div>

  <div class="links cf">
    <a class="fl" href="psw.html" style="color:#707070">忘记密码？</a>
    <p class="fr" style="color:#707070">还没有账号？<a href="/user/register" style="color:#333">30秒注册</a></p>
  </div>

<#--   <div class="third-party-login">
    <p><i class="icon i-login-other"></i></p>
    <p class="other-icons"><i class="icon i-wechat"></i></p>
  </div>
 -->
</div>

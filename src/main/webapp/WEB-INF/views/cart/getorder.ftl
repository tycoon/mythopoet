<#include "getorder_config.ftl">

<div class="banner">
  <a class="fl" id="j-back"><i class="icon i-back"></i></a>
  <span>确认订单</span>
  <i class="icon i-nav-bar fr"></i>
</div>


<div id="to-address">
	<i class="i-address icon"></i>
	<div class="address-text" id="j-address" data-id="">
        <p class="receiver">
        	<span class="tc-light-gray">收货人：</span>
        	<span id="go-name" class="go-name"></span>
        	<span id="go-phone" class="go-phone fr">电话</span>
        </p>
        <span id="go-address" class="go-address"></span>
	</div>
</div>

<div id="j-item-container">
</div>

<div class="item-total-info">
	<p class="item-total">
		<span class="total-conut-wrap">共<span class="total-count" id="go-count"></span>件商品</span>
		<span>合计：¥<span id="go-price" data-price="" class="total-price bold-num"></span>元</span>
	</p>

	<input id="go-remark" type="text" class="no-border input-area" placeholder="备注说明......" size="30">

</div>


<div id="to-ticket" class="getorder-to-ticket">使用优惠 <b id="dataShow" class="data getorder-data"></b><span class="icon i-advance i-getorder-to-ticket"></span></div>

<div class="order-pay cf">

    <a class="fr pay-btn" id="j-pay" data-id="">提交订单</a>
  
  <div class="order-info order-to-pay">
    <p class="cf">
      <span class="fl limit-desc"></span>
      <span class="fr">实付款：¥<span id="go-allprice" data-price="" class="total-price bold-num"></span>元</span>
    </p>
  </div>
</div>


<#include "cart_config.ftl">  

<div class="banner">
  <span class="fl edit-btn" id="j-edit">编辑</span>
  <span>购物车</span>
  <i class="icon i-nav-bar fr"></i>
</div>

<!-- 首先判断如果购物车为空，显示 -->
<div class="empty-cart hide" id="j-empty-cart">
  <div class="go-buy">
    <i class="icon i-cart-empty"></i>
  </div>
  <div class="go-buy">
    <a href="/" class="border-btn" id="j-go">赶快去逛逛</a>
  </div>
</div>

<!-- 如果购物车不为空 -->
<div id="j-not-empty" class="hide content">

  
  <div class="item-wrapper cart-items" id="j-item-container"></div>
    
     
  
  <div class="order-pay cf">
    <div class="fr pay-btn" id="j-get-order">结算<span id="j-total-count"></span></div>
    <div class="order-info">
      <p class="cf">
        <span class="fl" id="j-select-all">
        	<i class="icon i-not-check-circle-border"></i>
        	全选</span>
        <span class="fr">合计：<span	 class="yuan">¥</span><span class="total-price bold-num" id="j-total-price">0</span>元</span>
      </p>
    </div>
  </div>
</div>


<#-- <#include "../widgets/footer.ftl"> -->
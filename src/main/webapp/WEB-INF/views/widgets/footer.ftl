<div class="footer">
  <a class="nav-item <#if url=="/home">selected</#if>" href="/">
    <i class="icon i-home<#if url=="/home">-selected</#if>"></i>
    <p>首页</p>
  </a>
  <a class="nav-item <#if url=="/find">selected</#if>" href="/single/list?tagId=1">
    <i class="icon i-find<#if url=="/find">-selected</#if>"></i>
    <p>发现</p>
  </a>
  <a href="/cart" class="nav-item <#if url=="/cart">selected</#if>">
    <i class="icon i-cart<#if url=="/cart">-selected</#if>"></i>
    <p>购物车</p>
  </a>
  <a class="nav-item <#if url=="/mine">selected</#if>" href="/mine">
    <i class="icon i-mine<#if url=="/mine">-selected</#if>"></i>
    <p>我的</p>
  </a>  
</div>
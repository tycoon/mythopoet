require.config({
	baseUrl : VM.STATIC_URL
});

require(
		[ 'module/general', 'util/utils', 'util/template', 'tpl/find/itemTpl' ],
		function(general, util, template, itemTpl) {
			
			 
			var jQcontainer = $("#j-container");
			var key = jQcontainer.data("key");
			var pageSize = 20;
			var itemCurrentPage = 1, itemNextPage = 1;
			//alert("key:"+key);
			function getWorks(pageNum) {
				$.post('/search', {
					pageNum : pageNum,
					pageSize : pageSize,
					key : key
				}, function(ret) {
					if (ret.code == 0) {
						itemCurrentPage = pageNum;
						if (ret.data.length == pageSize) {
							itemNextPage++;
						}
						jQcontainer.append(template(itemTpl, ret));
					} else {
						alert(ret.msg)
					}
				});
			}
			;

			function loadMore() {
				
				if (itemCurrentPage < itemNextPage) {
					getWorks(itemNextPage);
				}
				$(window).bind('scroll', bindScroll);
			}

			var lastScrollTop = 0;
			function bindScroll() {
				var st = $(this).scrollTop();

				if (st > lastScrollTop) {
					if ($(window).scrollTop() + $(window).height() > $(document)
							.height() - 10) {
						$(window).unbind('scroll');
						loadMore();
					}
				} else {
					// upscroll code
				}
				lastScrollTop = st;
			}

			$(window).scroll(bindScroll);

			$(document).ready(function() {

				if (key == '') {
					alert("没有输入关键字");
					return;
				} else {
					getWorks(1);
				}
			});

		})
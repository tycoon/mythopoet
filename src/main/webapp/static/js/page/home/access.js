require.config({ 
	baseUrl : VM.STATIC_URL
});
require([ 'util/utils', 'module/cookie' ], function(util, cookie) {

	var source = util.query().c;
	var url = window.location.toString();
	var flag = false;
	if (source != null) {
		cookie.set('source', source, 2);
		flag = true;

	} else {
		source = cookie.get('source');
		flag = true;
	}

	if (flag) {
		if (source != null && source != '' && typeof source != "undefined") {
			$.ajax({
				type : 'post',
				url : '/access',
				dataType : 'json',
				data : {
					'source' : source,
					'url' : url
				},
				success : function(ret) {
				}
			});

		}
	}

})
require.config({
  baseUrl: VM.STATIC_URL
});
require(['module/cookie','module/general'], function(general, cookie){

var beginTime1 = new Date(2015,10,13,10);	
var endTime1 =   new Date(2015,10,13,13);

var beginTime2 = new Date(2015,10,13,14); 
var endTime2 =   new Date(2015,10,13,17);

var beginTime3 = new Date(2015,10,13,20); 
var endTime3 =   new Date(2015,10,13,23,59);


//var beginTime4 = new Date(2015,10,10,20,12); 
//var endTime4 =   new Date(2015,10,10,20,49);

//post参数
var stime1 = '2015-11-11 10:00:00 000';      
var stime2 = '2015-11-11 14:00:00 000';
var stime3 = '2015-11-11 20:00:00 000';
//var stime4 = '2015-11-10 20:00:00 000';
//列表
var oUl1 = $('#flash-menu-1');
var oUl2 = $('#flash-menu-2');
var oUl3 = $('#flash-menu-3');
//var oUl4 = $('#flash-menu-4');

var tipText = ''; // 计时文字
var btnText1 = '';
var btnText2 = '';
var btnText3 = '';
//var btnText4 = '';

var curTab =1;

var start1 = false;
var start2 = false;
var start3 = false;
//var start4 = false;
//----------1----------
if(beginTime1>new Date()){
	start1 = false;
	btnText1 = "还未开始";
}
if(beginTime1<new Date() && new Date() < endTime1){
	start1 = true;
	btnText1 = "立即抢购"
}
if(new Date() > endTime1){
	start1 = false;
	btnText1 = "已结束";
}
//----------2----------
if(beginTime2>new Date()){
	start2 = false;
	btnText2 = "还未开始";
}
if(beginTime2<new Date() && new Date() < endTime2){
	start2 = true;
	btnText2 = "立即抢购";
}
if(new Date() > endTime2){
	start2 = false;
	btnText2 = "已结束";
}
//----------3----------
if(beginTime3>new Date()){
	start3 = false;
	btnText3 = "还未开始";
}
if(beginTime3<new Date() && new Date() < endTime3){
	start3 = true;
	btnText3 = "立即抢购";
}
if(new Date() > endTime3){
	start3 = false;
	btnText3 = "已结束";
}
//----------4----------
/*
if(beginTime4>new Date()){
	start4 = false;
	btnText4 = "还未开始";
}
if(beginTime4<new Date() && new Date() < endTime4){
	start4 = true;
	btnText4 = "立即抢购";
}
if(new Date() > endTime4){
	start4 = false;
	btnText4 = "已结束";
}
*/

//页面刚加载时输出数据(只输出一次)
queryFlash(stime1,oUl1,btnText1,start1); // 默认十点开抢的数据
queryFlash(stime2,oUl2,btnText2,start2); 
queryFlash(stime3,oUl3,btnText3,start3); 
//queryFlash(stime4,oUl4,btnText4,start4);

var timer = setInterval(fnTime,1000); //默认开启第一次抢购的定时器

function fn1(){
	queryFlash(stime1,oUl1,btnText1,start1);
}

//输出数据
function prolist(id,pic,brandName,name,price,originPrice,totalSkuCount,$obj,pLink){
	var show = 'none';
	if(totalSkuCount==0){
		show = 'block';
	}
	var li = 
	'<li class="flash-list">'+'\n'+
	'	<a href="/item/detail/'+ id + '" class="flash-pro-thumb"><img src="' + pic.split(',')[0] + '" alt=""></a>'+'\n'+
	'	<div class="flash-pro-desc">'+'\n'+
	'		<p class="flash-brand">' + brandName + '</p>'+'\n'+
	'		<p class="flash-name">' + name + '</p>'+'\n'+
	'		<p>'+'\n'+
	'			<span class="flash-price">￥' + price + '</span>'+'\n'+
	'			<span class="flash-ori-price"><del>￥' + originPrice + '</del></span>'+'\n'+
	'		</p>'+'\n'+
	//'		<p class="flash-count">仅剩<span class="c_count">'+totalSkuCount+'</span>件</p>'+'\n'+
			pLink +'\n'+
	'	</div>'+'\n'+
	'	<div class="mask-box" style="display:'+show+'"><div class="mask"></div><span class="mask-text">已售罄</span></div>'+'\n'+
	'</li>'+'\n'
	;
	$obj.append(li);
};

//查询数据
function queryFlash(stime,$obj,buystatus,start){
	var sLi = '';
	$.post('/flashSale/list', {
		'stime':stime
	}, function(ret) {
		if (ret.code == 0) {
			console.log(ret.data);
			$.each(ret.data,function(i,element){
				var pLink = '<a href="#" class="buy-flash-now" >'+buystatus+'</a>'
				if(start) pLink = '<a href="/item/detail/'+ element.id + '" class="buy-flash-now active" >'+buystatus+'</a>'
				prolist(element.id,element.pic,element.brandName,element.name,element.price,element.originPrice,element.totalSkuCount,$obj,pLink);
			});
		}
	});
};


//倒计时
function fnTime() {
	if(curTab==0){
		t1 = beginTime1.getTime() - new Date().getTime();
		t2 = endTime1.getTime() - new Date().getTime();
	}else if(curTab==1){
		t1 = beginTime2.getTime() - new Date().getTime();
		t2 = endTime2.getTime() - new Date().getTime();
	}else if(curTab==2){
		t1 = beginTime3.getTime() - new Date().getTime();
		t2 = endTime3.getTime() - new Date().getTime();
	}
	/*
	else if(curTab==3){
		t1 = beginTime4.getTime() - new Date().getTime();
		t2 = endTime4.getTime() - new Date().getTime();
	}
	*/
	
	console.log(curTab,t1,t2);
	
	if ( t1 > 0 ) { // 还没开始
		
		tipText = Math.floor(t1/86400000) + '天' + Math.floor(t1%86400000/3600000 ) + '小时' + Math.floor(t1%86400000%3600000/60000) + '分钟' + Math.floor(t1%60000/1000) + '秒';
		$('.time').html('<span class="time-text">距离开始</span><span class="timing">'+tipText+'</span>');
		return;
	}
	else if( t1 < 0 && t2 >0 ) // 正在抢购
	{
		tipText = Math.floor(t2/86400000) + '天' + Math.floor(t2%86400000/3600000 ) + '小时' + Math.floor(t2%86400000%3600000/60000) + '分钟' + Math.floor(t2%60000/1000) + '秒';
		$('.time').html('<span class="time-text">距离结束</span><span class="timing">'+tipText+'</span>');
		return;
		
	}
	else if( -1000 < t2 < 0 ){
		var url = window.location.href;
		window.location.href = url;
		return;
	}
	else if( t2 < -1000 ){ //结束
		$(".time").html('抢购已经结束');
		clearInterval(timer);
	}
	
};

var $Tab = $('.time-tab').find('a');
$Tab.on('click',function(){
	clearInterval(timer);//清除定时器
	timer = null;
	$(this).parents('.time-tab').find('i').removeClass('cur');
	$(this).find('i').addClass('cur');
	$('#flash-pro-cont-1').find('.flash-menu').hide();
	$('#flash-pro-cont-1').find('.flash-menu').eq($(this).index()).show();
	curTab = $(this).index();
	timer = setInterval(fnTime,1000); //重启定时器
});



});//END



	

require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/cookie'], function(cookie){
  var jQsettings = $('#j-settings');
  var jQsettingsDropDown = $('#j-setting-dropdown');
  jQsettings.bind('click', function(){
    jQsettingsDropDown.slideToggle(200);  
  });

  var jQclevel = $("#mine-level");
  var level = jQclevel.data("level");
  if(level == 0){
  	  jQclevel.html("普通会员");
  }else if(level == 1){
	  jQclevel.html("金牌会员");
  }else if(level == 2){
	  jQclevel.html("铂金会员");
  }else if(level == 3){
	  jQclevel.html("钻石会员");
  }

  
  var jQlogout = $("#j-logout");
  jQlogout.bind('click', function(e){
  	e.stopPropagation();
  	cookie.delete("uid");
  	cookie.delete("userId");
    cookie.delete('token');
  	window.location.href = "/user/login"
  })
  
 });


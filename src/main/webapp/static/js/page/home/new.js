require.config({
  baseUrl: VM.STATIC_URL
});

require(['util/template', 'tpl/clothTpl', 'util/utils', 'lib/jquery.lazyload'],
  function(template, itemTpl, util, lazyload){

  var pageSize = 201;
  var currentPage = 1, nextPage=1;
  var jQcontainer = $('#j-item-container');
  var tagId = 1;
  var cid=util.query().cid || 1;
  var itemId=util.query().id || -1;

  function getStrLength(str) {  
    var cArr = str.match(/[^\x00-\xff]/ig);  
    return str.length + (cArr == null ? 0 : cArr.length);  
  }  
  function getItem(type, tagId, cid, pageNum){
    if(pageNum == 1){
      jQcontainer.append('<img id="j-loading" class="loading-spinner" src="/static/images/spinner.gif">');
    }
    $.post('/item/list/'+type, {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){
        if(pageNum == 1){
          jQcontainer.empty();
        }
        var dt = ret.data;
        for(var i=0, l=dt.length; i<l; i++){
          var name = dt[i].name;
          var sub = util.subString(name, 20);
          dt[i].name = sub;
          dt[i].cid = cid;

        }
        if(ret.data.length == pageSize){
          nextPage = currentPage+1;
        }
        jQcontainer.append(template(itemTpl, ret));
        $("img.lazy").lazyload({
            threshold : 200
        });
        // 定位之前位置
        if(itemId != -1){
          var top = $("#cloth" + itemId).offset().top;
          $('body').scrollTop(top);
        }
      }else{
        alert(ret.msg)
      }
    });
  }

  // getItem('new', 1, currentPage);

  var jQnavBtn = $(".j-nav");

  jQnavBtn.bind('click', function(){
    var jQself = $(this);
    var type = jQself.data('type');
    var tagId = jQself.data('tag');
    var cid = jQself.data('cid');

    jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type, tagId, cid, 1);   
  });

  var index = cid-1;
  $(jQnavBtn[index]).click();

  function loadMore(){
    console.log("More loaded");
    if(currentPage<nextPage){
      getItem(nextPage);
    }else{
      alert("没有更多了")
    }
    // $("body").append("<div>");
    $(window).bind('scroll', bindScroll);
  }

  function bindScroll(){
    if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
      $(window).unbind('scroll');
      loadMore();
    }
  }
  // $(window).scroll(bindScroll);

})
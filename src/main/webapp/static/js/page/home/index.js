require.config({
	baseUrl : VM.STATIC_URL
});
require([ 'lib/slick', 'util/template', 'tpl/slideTpl', 'tpl/topicTpl',
		'module/cookie', 'lib/jweixin-1.0.0' ], function(us, template,
		slideTpl, topicTpl, cookie, wx) {
	var isWeixin = $("#j-query-weixin-input").val();
	//alert(isWeixin);
	// 轮播
	var jQslides = $("#j-slide-container")
	jQslides.slick({
		autoplaySpeed : 3000,
		dots : true,
		loop : true,
		autoplay : true,
		pauseOnDotsHover : false,
		pauseOnHover : false,
		arrows : false
	});
	jQslides.on('swipe', function(event, slick, direction) {
		console.log(slick, direction);
		slick.autoPlay();
	});

})
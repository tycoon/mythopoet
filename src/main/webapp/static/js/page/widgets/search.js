require.config({
  baseUrl: VM.STATIC_URL
});
define(['util/template', 'tpl/find/alphaTpl'], function(template,  alphaTpl){
  var jQgoSearch = $('#j-go-search');
  var jQsearchBtn = $('#j-search-btn');
  var jQsearchMask = $('#j-search-mask');
  var jQindex = $("#j-alpha-index");
  var jQsearchInput = $("#j-search-input");
  var jQsearchFrom = $("#j-search-form");
  var jQcloseSearch = $("#j-close-serach");
  var jQmag = $("#j-search-magnify");


  jQcloseSearch.bind('click', function(){
    jQsearchMask.fadeOut(200);
  })

  jQgoSearch.bind('click', function(){
    jQsearchMask.fadeIn(200);
    // jQsearchInput.focus();
  });
  jQsearchFrom.bind('click', function(e){
    e.stopPropagation();
  });

  var search = function(){
    var searchKey = jQsearchInput.val();
    if(searchKey == ''){
      alert("请输入关键字");
      return;
    }else{
      window.location.href = "/search/index?key="+searchKey;
    }    
  }
  jQsearchInput.bind('keydown', function(e){
    if(e.keyCode==13){
      search();
    }
  });
  jQmag.bind('click', function(){
    search();
  })
  jQsearchBtn.bind('click', function(e){
    search()
  });

  var setAlpha = function(tagId){
    $.post('/designer', {
      pageNum: 1,
      pageSize: 200,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){
        var ds = ret.data;
        $('#j-alpha-list').remove();
        jQsearchMask.append(alphaTpl);
        for(var i=0, l=ds.length; i< l; i++){
          var init = ds[i].initial.toUpperCase();
          switch(init){
            case '#':
              $('#num').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;
            case 'A':
              $('#A').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;
            case 'B':
              $('#B').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'C':
              $('#C').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'D':
              $('#D').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');             
              break;  
            case 'E':
              $('#E').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');              
              break;  
            case 'F':
              $('#F').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');              
              break;  
            case 'G':
              $('#G').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'H':
              $('#H').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'I':
              $('#I').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'J':
              $('#J').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'K':
              $('#K').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');              
              break; 
            case 'L':
              $('#L').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'M':
              $('#M').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'N':
              $('#N').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'O':
              $('#O').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;      
            case 'P':
              $('#P').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'Q':
              $('#Q').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'R':
              $('#R').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'S':
              $('#S').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;  
            case 'T':
              $('#T').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'U':
              $('#U').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'V':
              $('#V').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'W':
              $('#W').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'X':
              $('#X').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'Y':
              $('#Y').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break; 
            case 'Z':
              $('#Z').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'?tagId='+tagId+'"">'+ds[i].name+'<a/></li>');
              break;             
          }
        }
      }else{
        alert(ret.msg)
      }
    });
  };


  return {
  	'setAlpha' : setAlpha
  }
})
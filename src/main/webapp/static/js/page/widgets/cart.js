define([], function(){
	function Cart(option){
		this.itemList = option.itemList || [];
		this.cartDetailIds = option.cartDetailIds || '';
		this.totalCount = option.count || 0;
		this.totalPrice = option.totalPrice || 0;
		this.remark = option.remark || '';
		this.mode == option.mode || 'select';  //select or edit
	};

	Cart.prototype.selectToCart = function(item) {
		this.itemList.push(item);
		//更新购物车总件数
		this.totalCount++;
		this.updateTotalCount();	
		// 更新购物车总价
		this.totalPrice += item.totalPrice();		
	};
	Cart.prototype.unselectToCart = function(item) {
		this.cartDetailIds.remove(function(el) { 
			return el.id === item.id; 
		});
		this.totalCount--;
		this.updateTotalCount();					
	};

	Cart.prototype.updateTotalCount = function(totalCount) {
		var total = this.totalCount || totalCount;
		var jQtotalCount = $("#j-total-count");
		jQtotalCount.text(total);		
	};
	Cart.prototype.updateTotalPrice = function(totalPrice) {
		var total = this.totalPrice || totalPrice;
		var jQtotalPrice = $("#j-total-price");
		jQtotalPrice.text(total);		
	};

	Cart.prototype.reCaculate = function() {
		this.totalPrice = 0;
		for(var i=0, l=this.itemList.length; i<l; i++ ){
			this.totalPrice += this.itemList[i].totalPrice();
		}
		this.updateTotalPrice();	
	};


})
define([], function(){
	function Item(option){
		this.id = option.cartId || 0;
		this.price = option.price || 0;
		this.count = option.count || 0;	
		this.stock = option.stock || 100;
		this.totalPrice = function(){
			return (this.count*this.price)
		};	
	};


	Item.prototype.updateCount = function(count) {
		this.count = count;
	}
	Item.prototype.addCount = function(count) {
		this.count++;
	};
	Item.prototype.minuseCount = function(count) {
		if(count<=1){
			this.count = 1;
		}else{
			this.count--;
		}
	};

	return {
		Item : Item
	}
})
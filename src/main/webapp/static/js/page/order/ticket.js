require.config({ 
	baseUrl : VM.STATIC_URL
});
require([ 'module/general', 'util/utils', 'module/cookie' ], function(general,
		util, cookie) {
	var from = util.query().from;
	var ids = util.query().ids;
	var fr = util.query().fr;
	var itemId = util.query().i;
	var count = util.query().c;
	var props = util.query().p;
	var textPword = $("#t-pword");
	var jQself = $(".j-ticket-type");
	var ticketType = 0;
	var btn = $("#button");
	var cbtn = $("#check-button");
	var cbtn2 = $("#check-button2");
	
	$.post('/customer/getlevel',{},function(ret){
		if(ret.code==0){
			var level = ret.data;
			if(level<=1){
				$('#member-discount').hide();
			}else{
				$('#member-discount').show();
			}
		}
	});
	
	

	cbtn2.bind('click', function() {
		if(fr == 1){
			window.location.href = '/cart/selectedcart' + '?fr=1&sign=2&i='+itemId+'&c='+count+'&p='+props;
		}else{
			window.location.href = '/cart/selectedcart' + '?fr=2&sign=2&ids='+ids;
		}		
	});

	$(".j-ticket-type").bind('click', function() {
		var jQself = $(this);
		jQself.find(".i-check-tick").show();
		jQself.siblings(".j-ticket-type").find(".i-check-tick").hide();
	});

	btn.bind('click', function() {
		if(fr == 1){
			$.post('/sale/ticket/check', {
				pword : textPword.val()
			}, function(ret) {
				if (ret.code == 0) {
					window.location.href = '/cart/selectedcart' + '?fr=1&pword=' + textPword.val() + '&sign=1&i='+itemId+'&c='+count+'&p='+props;
				} else if (ret.code > 0) {
					alert(ret.msg);
				}
			});
			$(".j-ticket-type").find(".i-check-tick").show().end().siblings(
					".j-ticket-type").find(".i-check-tick").hide();
		}else{
			$.post('/sale/ticket/check', {
				pword : textPword.val()
			}, function(ret) {
				if (ret.code == 0) {
					window.location.href = '/cart/selectedcart' + '?fr=2&pword=' + textPword.val() + '&sign=1&ids='+ids;
				} else if (ret.code > 0) {
					alert(ret.msg);
				}
			});
			$(".j-ticket-type").find(".i-check-tick").show().end().siblings(
					".j-ticket-type").find(".i-check-tick").hide();
		}
	})

});

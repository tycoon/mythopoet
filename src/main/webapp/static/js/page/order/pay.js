require.config({  
  baseUrl: VM.STATIC_URL
});

require(['module/general', 'module/cookie', 'module/note', 'lib/jweixin-1.0.0','util/utils'],
	function(general, cookie, note, wx ,util){
	
	var userId = cookie.get("userId");
	var jQpayBtn = $("#j-pay");
	var orderId = jQpayBtn.data("id");
	var jQweixinBtn = $("#j-pay-type-weixin");
	var isWeixin = jQpayBtn.data("isWeixin");
	var numberPriceBtn = $("#number-price");
	var price = numberPriceBtn.data("price");
	
	var currentUrl = window.location.href;
	var APP_ID = "wx51473b3f12948341";
	var payType = 0;
    
  	var pword = util.query().pword;
  	var sign = util.query().sign;
  	   if(sign == 2){
	  		$.post('/sale/discount', {
			},function(ret){
				if(ret.code == 0){
					if(ret.data.discount < 1){
						$("#dataShow").html(ret.data.discount*100+"折");
						numberPriceBtn.html(price*ret.data.discount);
						numberPriceBtn.data("price",price*ret.data.discount);
					}
					
				}
			});  
  	   }else if(sign == 1){
  		 if(pword != ''){
      	   alert("pwrod="+pword);
      	   $.post('/sale/ticket/check', {
  	    		 pword: pword
  	 		},function(ret){
  	 			if(ret.code == 0){
  	 				if(price-ret.data.buyLimit>=0){
  	 					$("#dataShow").html(ret.data.cost);
  		 				$("#number-price").html(price-ret.data.cost);
  		 				numberPriceBtn.data("price",price-ret.data.cost);
  	 				}else{
  	 					alert("订单金额满"+ret.data.buyLimit+"元可以使用此券！");
  	 				}	
  	 			}
  	 		});  
  		} 
  	   }
  	
       
    if(isWeixin=='true'){
	    var loading = note.show('正在获取支付信息……', {duration: 30000});
		//认证js sdk
		$.post('/auth/ticket', {
			currentUrl: currentUrl,
			userId: userId
		}, function(ret){
			if(ret.code == 1001){
				// window.location.href = ret.data;
			}else if(ret.code ==1){
				alert(ret.msg)
			}else if(ret.code ==0){
				// console.log(ret.data);
				var ticket = ret.data;
				wx.config({
				    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				    appId: ticket.appId, // 必填，公众号的唯一标识
				    timestamp: ticket.timestamp, // 必填，生成签名的时间戳
				    nonceStr: ticket.noncestr, // 必填，生成签名的随机串
				    signature: ticket.sign,// 必填，签名，见附录1
				    jsApiList: ['wx.chooseWXPay'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});	
				// loading.destroy();		
			}
			loading.destroy();		
		});
    } 
	$(".j-pay-type").bind('click', function(){
		var jQself = $(this);
		payType = parseInt(jQself.data("type-id"));
		jQself.find(".i-check-tick").show();
		jQself.siblings(".j-pay-type").find(".i-check-tick").hide();
	});
	$('#j-read-more').bind('click', function(e){
		e.stopPropagation();
		var jQicon = $(this).find('#j-arrow');
		if(jQicon.hasClass('i-arrow-down')){
			jQicon.removeClass('i-arrow-down').addClass('i-arrow-up')
		}else{
			jQicon.removeClass('i-arrow-up').addClass('i-arrow-down')	
		}
		//$('#j-reminder').slideToggle();
		$('#j-reminder').toggle();
	});	

	jQpayBtn.bind('click', function(){
		if(userId == ''){
	      alert("请先登录");
	      var redirect = window.location.pathname;
	      window.location.href = '/user/login?redirect='+redirect;
	      return;		
  		}else if(orderId == 0){
			alert("正在获取支付信息，请稍候再试");
			return;
		}else{
			
			$.post('/order/'+orderId+'/updatePayType', {
				payType: payType
			},function(ret){
				if(ret.code != 0){
					alert(ret.msg)
				}

			});

			if(payType == 0){
				$.post('/pay/prepay', {
					userId : userId,
					orderId : orderId, 
					payType : payType
				},function(ret){
					if(ret.code == 0){
						// 调用微信支付
						// alert("预支付回调成功");
						// console.log(ret.data);
						var payInfo = ret.data;
						function onBridgeReady(){
							WeixinJSBridge.invoke('getBrandWCPayRequest', {
								appId: payInfo.appId,
							    timeStamp: payInfo.timeStamp,
							    nonceStr: payInfo.nonceStr, // 支付签名随机串，不长于 32 位
							    package: payInfo.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
							    signType: payInfo.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
							    paySign: payInfo.paySign, // 支付签名
							}, function (res) {
								//alert("v1:"+res.err_msg.indexOf("ok"));
								if(res.err_msg.indexOf("ok")>0) {
						    		alert("支付成功，可以在我的订单中查看");
						    		window.location.href = '/pay/notify/'+orderId+"?pword="+pword;
								}
							});
						}
						if (typeof WeixinJSBridge == "undefined"){
						   if( document.addEventListener ){
						       document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
						   }else if (document.attachEvent){
						       document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
						       document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
						   }
						}else{
						   onBridgeReady();
						}

					}else if(ret.code ==1){
						// 错误信息
						alert(ret.data)
					}else if(ret.code ==1001){
						// 调用微信授权
						window.location.href = ret.data;
					}else{
						alert(ret.msg)
					}
				})				
			}else if(payType==1){
				$.post('/alipay/prepay', {
					userId : userId,
					orderId : orderId, 
					payType : payType
				},function(ret){
					if(ret.code == 0){
						window.location.href = '/alicallback/pay?orderId='+orderId+'&userId='+userId;
					}else if(ret.code ==1){
						// 错误信息
						alert(ret.msg)
					}
				})
			}else if(payType==5){
				alert("您已选择银行转账方式，请耐心等待我们确认，并到我的订单中关注订单状态变更。我们会在工作日24小时之内与您联系，如果您有任何问题，请联系010-65015330。");
				window.location.href = '/order/page';					    			
			}

		}
	}); 

});

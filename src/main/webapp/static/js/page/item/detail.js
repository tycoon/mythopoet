require.config({ 
  baseUrl: VM.STATIC_URL
});

require(['module/general', 'util/utils', 'lib/slick', 'module/cookie', 'module/note'], 
  function(general, util, slick, cookie, note){
  var uid = cookie.get("uid");
  var userId = cookie.get("userId");
  var itemId = $("#j-item-wrapper").data("id");
  var skuList = [];
  var skuMap = {};   //全局skustyle映射
  var skuArray = [];
  var propTotalLevel = 0;
  var jQselectInfo = $("#j-select-info");

  var jQback = $('#j-back');
  var cid = util.query().cid || -1;
  var id = util.query().id || -1;
  var page = util.query().page || -1;
  var tagId = util.query().tagId || 1;

  //如果有cid，来自new
  if(cid != -1){
    jQback.unbind();
    jQback.attr('href', '/new?cid='+cid+'&id='+id);
  }else if(page != -1){
    var location = '';
    var from = util.query().from || '';
    // 来自所有单品
    if(from == 'item'){
      location = '/single/list?page='+page+'&id='+id+'&tagId='+tagId;
      jQback.unbind();
      jQback.attr('href', location);
    }else if(from == 'category'){
      var categoryId = util.query().categoryId || 1;
      location = '/category/'+categoryId+'/items?page='+page+'&id='+id+'&tagId='+tagId;
      jQback.unbind();
      jQback.attr('href', location);
    }

  }

  function skuProp(options){
    this.id = options.id || 0;
    this.label = options.label || 0;
    this.level = options.level || 0; //0,1,2,3...
    this.identify = options.level+'-'+options.id || '0-0';
    this.dom = '<li data-id="'+this.id+'"'+
               'data-identify="'+this.identify+'"'+
               'data-level="'+this.level+'">'+
                this.label+'</li>';
    this.getChildren = function(selectedParentId){
      var childLevel = this.level+1;
      // var parentLevel = (this.level-1>=0)?this.level-1 : 0;
      var parentLevel = this.level-1;
      var children = [];
      if(childLevel < propTotalLevel){
        for(var i=0; i<skuList.length; i++){
          if(skuList[i].itemPropValueList[this.level].id == this.id){
            var childId = skuList[i].itemPropValueList[childLevel].id;
            if(this.level > 0 ){
              if(selectedParentId == skuList[i].itemPropValueList[parentLevel].id){
                children.push(skuMap[childLevel+'-'+childId]);
              }
            }else{
              children.push(skuMap[childLevel+'-'+childId]);
            }
          }
        }        
      }
      return children;
    };
    this.setChildren = function(child){
      //child为一个skuProp对象
      this.child.push(child);
    };
    this.drawDom = function(){
    }
  }
  $.post('/item', {
    id: itemId
  }, function(ret){
    var test = {};
    test.startTime = new Date().getTime();
    var item = ret.data;
    skuList = item.itemSkuViewList; //已有的sku商品
    var propList = item.itemPropList; //商品有的属性 
    if(propList == null){
      // todo:加文案
      return;
    }
    propTotalLevel = propList.length;

    // 遍历商品属性
    for(var i=0; i<propTotalLevel;i++){
      skuArray.push([]);
      var jQsection = '<div class="select-section">'+
                      '  <p class="select-title">'+propList[i].content+'</p>'+
                      '  <ul class="select-list j-prop-container cf"></ul>'+
                      '</div>';
      jQselectInfo.append(jQsection);
    }
    var jQcount = '<div class="select-section">'+
              ' <p class="select-title">数量</p>'+
              '<div class="count-box cf">'+
              '<div class="minuse-btn" id="j-minuse-count">－</div>'+
              '<div><input class="no-border count-input" type="text" id="j-count" value="1"></div>'+
              '<div class="plus-btn" id="j-add-count">＋</div>'+
              '</div>'+
              '</div>' 
    jQselectInfo.append(jQcount);
    // 遍历每个sku商品
    for(var i=0, l=skuList.length; i<l; i++){
      //遍历该sku商品的每个sku属性
      for(var j=0, len = skuList[i].itemPropValueList.length; j<len; j++){
        var options = {};
        options.id = skuList[i].itemPropValueList[j].id;
        options.label = skuList[i].itemPropValueList[j].content;
        options.level = j;
        var tmp = new skuProp(options);
        if(!skuMap.hasOwnProperty(tmp.identify)){
          skuMap[tmp.identify] = tmp;
          skuArray[j].push(tmp);
        }          
      }

    }
    test.endTime = new Date().getTime();
    console.log(skuMap);
    console.log(skuArray);
    // 循环执行时间
    // console.log(test.endTime-test.startTime);
    // 初始化时只绘制第一层
    skuArray[0].sort(function(a,b){
      var reg = /^-?[1-9]*(\.\d*)?$|^-?0(\.\d*)?$/;
      var intReg = /^\d+$/;
      if(reg.test(a.label) || intReg.test(a.label) ){
        return parseFloat(a.label) > parseFloat(b.label)?1:-1
      }else{
        sortMap = {
          'S': 0,
          'M': 1,
          'L': 2,
          'XL': 3,
          'XXL': 4
        }
        var aLabel = sortMap[a.label.toUpperCase()];
        var bLabel = sortMap[b.label.toUpperCase()];
        return aLabel > bLabel?1:-1
      }

    });    
    var jQfirstUl = jQselectInfo.find('.j-prop-container').first();
    for(var k=0; k<skuArray[0].length; k++){
      var d = $(skuArray[0][k].dom);
      jQfirstUl.append(d);
      if(k ==0){
        d.click();
      }
    }   
  });
  var jQselectMask = $('#j-select-mask');
  var jQshowMask = $('.j-show-mask');
  var jQselectArea = $('#j-select-area');  

  jQshowMask.bind('click', function(){
    jQselectMask.fadeIn(200);
  });

  jQselectMask.bind('click', function(){
    $(this).fadeOut("fast");
  });

  jQselectArea.bind('click', function(e){
    e.stopPropagation();
  });

  // 下单流程
  //todo:初始化
  var count=1;
  jQselectInfo.delegate('li', 'click', function(){
    var jQself = $(this);
    jQself.addClass('selected').siblings().removeClass('selected');
    var jQpropContainers = jQself.parents().find('.j-prop-container');
    var id = jQself.data("id");
    var key = jQself.data("identify");
    var level = jQself.data("level");
    var me = skuMap[key];
    var thisContainer = jQpropContainers[level];
    var nextContainer, prevContainer, parentId=0;
    if(level<(propTotalLevel-1)){
      nextContainer = $(jQpropContainers[level+1]);
      nextContainer.empty();
      if(level>0){
        prevContainer = $(jQpropContainers[level-1]);
        parentId = prevContainer.find('.selected').data("id");
      }
      var children = me.getChildren(parentId);
      for(var i=0; i<children.length; i++){
        var c = $(children[i].dom);
        nextContainer.append(c);
        if(i==0){
          c.click();
        }
      }
    }else if(level==(propTotalLevel-1)){
      //设置库存和价格
      var allPropContainers = $('.j-prop-container');
      var propArray=[];
      for(var i=0, l=allPropContainers.length; i< l; i++){
        var propId = $(allPropContainers[i]).find('.selected').data("id");
        propArray.push(propId);
      }
      prop = propArray.join(':');
      console.log(prop);
      for(var i=0; i<skuList.length; i++){
        if(skuList[i].props == prop){
          var price = skuList[i].price;
          var stock = skuList[i].count;
          $("#j-sku-stock").text(stock);
          // $("#j-sku-price").text(price);
        }
      }  
    }
  });

  var jQminuse = $('#j-minuse-count');
  var jQadd = $('#j-add-count');
  var jQcount = $('#j-count');
  jQselectInfo.delegate('#j-add-count', 'click', function(){
    var jQcount = $('#j-count');
    count = parseInt(jQcount.val());
    count++;
    jQcount.val(count);
  });

  jQselectInfo.delegate('#j-minuse-count', 'click', function(){
    var jQcount = $('#j-count');
    count = parseInt(jQcount.val());
    if(count > 1){
      count--;
    }else{
      count = 1;
    }
    jQcount.val(count);
  });

  // 加入购物车及马上下单
  var jQaddToCart = $('#j-add-to-cart');
  var jQbuyNow = $('#j-buy-now');
  var jQconfirm = $('#j-confirm');
  jQaddToCart.bind('click', function(){
    jQconfirm.data('type', 'cart');
  });

  jQbuyNow.bind('click', function(){
    jQconfirm.data('type', 'pay');
  });

  jQconfirm.bind('click', function(){
    if(userId == ''){
      var redirect = window.location.pathname;
      window.location.href = '/user/login?redirect='+redirect;
      return;
    }
    var type = $(this).data('type');
    var prop = "1:4:6", propArray=[];
    var jQcount = $('#j-count');
    var count = jQcount.val();
    var allPropContainers = $('.j-prop-container');
    for(var i=0, l=allPropContainers.length; i< l; i++){
      var propId = $(allPropContainers[i]).find('.selected').data("id");
      propArray.push(propId);
    }
    prop = propArray.join(':');
    // 加入购物车
    if(type == 'cart'){
      $.post('/cart/'+userId+'/addItem', {
        props : prop,
        itemId : itemId,
        count : count
      },function(ret){
        if(ret.code ==0){
          note.show("已经加入购物车");
          jQselectMask.fadeOut("fast");
        }else{
          alert(ret.msg);
        }
      })
    }else{
    	window.location.href= '/cart/selectedcart?i='+itemId+'&c='+count+'&p='+prop+'&fr=1';
//      // 立即购买
//      $.post('/order/add', {
//        "itemId":itemId,
//        "count":count,
//        "props":prop,
//      },function(ret){
//        if(ret.code == 0){
//          var orderId = ret.data.id;
//          var url = '/pay/order/'+orderId+'?showwxpaytitle=1';
//          window.location.href = url;          
//        }else{
//          alert(ret.msg);
//        }
//      });
    }
  });

  // 收藏单品
  var jQfav = $("#j-fav");
  var jQfavIcon = $("#j-fav-icon");
  var jQfavText = $("#j-fav-text");
  var favType = 0; //商品
  var itemId = $("#j-item-wrapper").data("id");
  jQfav.bind('click', function(){
    if(userId == ''){
      alert("请先登录");
      var redirect = window.location.pathname;
      window.location.href = '/user/login?redirect='+redirect;
      return;
    }
    if(jQfavIcon.hasClass("i-fav")){
      $.post('/favorites/add', {
        "userId":uid,
        "type": favType,
        "tid":itemId
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-fav").addClass("i-faved");
          jQfavText.text("已收藏").css({"color": "#d37eff"});
          jQfav.data("id", ret.data.id);
        }else{
          alert(ret.msg)
        }
      })
    }else{
      var fid = jQfav.data("id");
      $.post('/favorites/'+fid+'/delete', {
        "userId":uid,
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-faved").addClass("i-fav");
          jQfavText.text("收藏").css({"color": "#868686"});  
        }else{
          alert(ret.msg);
        }
      })
    
    }
  });


  // 图片滑动
  var jQslides = $('#j-slides');
  jQslides.slick({
    dots: true,
    autoplay: false,
    arrows: false
  }).show();
  $("#j-desc").show();

  $(".j-product-img").bind('click', function(){
    var imgUrl = $(this).attr("src");
    // alert(imgUrl);
    window.location.href = imgUrl;
  })
})
require.config({ 
  baseUrl: VM.STATIC_URL
});

require(['module/general'], function(general) {
	var jQclevelBtn = $("#customer-level");
	var level = jQclevelBtn.data("level");
	var $levelCont = $('.level-cont').find('div');
	var $levelTab = $('.lead-head-cont').find('a');
	var $levelTag = $('.title-mid');
	var $levelLine = $('.level-line');
	var $levelText = $('.level-text');
	var $levelCondi = $('.level-conditions');
	// body...
	
	(function(){
		//alert(level);
		if(level==0){
			$levelTab.eq(0).addClass('current');
			$levelCont.eq(0).show();
			$levelCondi.eq(0).hide();
			$levelText.eq(0).addClass('active');
		}else if(level==1){
			$levelTab.eq(1).addClass('current');
			$levelCont.eq(1).show();
			$levelCondi.eq(1).hide();
			$levelText.eq(1).addClass('active');
		}else if(level==2){
			$levelTab.eq(2).addClass('current');
			$levelCont.eq(2).show();
			$levelCondi.eq(2).hide();
			$levelText.eq(2).addClass('active');
		}else{
			$levelTab.eq(3).addClass('current');
			$levelCont.eq(3).show();
			$levelCondi.eq(3).hide();
			$levelText.eq(3).addClass('active');
		}
		for(var i=0;i<level;i++){
			$levelLine.eq(i).addClass('active');
		}
	})();
	
	
	$levelTab.on("click",function(){
		var $levelText = $(this).find('span').html();
		$(this).addClass('current').siblings().removeClass('current');
		$levelCont.hide();
		$levelCont.eq($(this).index()).show();
		if($(this).index()==level){
			$levelTag.html('我的特权');
		}else{
			$levelTag.html($levelText+'会员');
		}
	});
	
	
})
require.config({ 
	baseUrl : VM.STATIC_URL
});

require(['module/general','module/cookie',  'lib/jweixin-1.0.0'], function(general ,cookie,wx) {

	var isWeixin = $("#j-query-weixin-input").val();
	$(function() {
		$.extend({
			fn1 : function() {
				jQtoshop.empty();
			}
		});
		setInterval("$.fn1()", 20000);
	});
	
	var userId = cookie.get("userId");
	if (userId != ''){
		var currentUrl = window.location.href;
		if(isWeixin=='true'){
			$.post('/share/ticket', {
				userId : userId,
				currentUrl : currentUrl
			}, function(ret) {
				if (ret.code == 1001) {
					//alert(ret.msg)
					window.location.href = ret.data;
				} else if (ret.code == 1) {
					alert(ret.msg)
				} else if (ret.code == 0) {
					var ticket = ret.data;
					wx.config({
						debug : false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
						appId : ticket.appId, // 必填，公众号的唯一标识
						timestamp : ticket.timestamp, // 必填，生成签名的时间戳
						nonceStr : ticket.noncestr, // 必填，生成签名的随机串
						signature : ticket.sign,// 必填，签名，见附录1
						jsApiList : [ 'onMenuShareTimeline', 'onMenuShareAppMessage' ]
					// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
					});
				}
			});
		}
		var s_imgUrl = "http://testh5.hshs-fashion.com:9090"
				+ $('#j-image-first').attr("src");
		var s_title = $('title').text();
		var s_desc = "";
		//alert(s_title+s_imgUrl);
		wx.ready(function() {
	
			wx.onMenuShareAppMessage({
				title : s_title,
				desc : s_desc,
				link : currentUrl,
				imgUrl : s_imgUrl
			});
			wx.onMenuShareTimeline({
				title : s_title,
				link : currentUrl,
				imgUrl : s_imgUrl
			});
	
		});
	}

})
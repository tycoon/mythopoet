require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/general','module/cookie', 'util/template', 'tpl/find/itemTpl', 'util/utils'], 
  function(general,cookie, template, itemTpl, util){
  var uid = cookie.get("uid");
  var userId = cookie.get("userId");
  var jQmain = $("#j-main");

  var jQback = $('#j-back');

  var id = util.query().id || -1;
  var page = util.query().page || 1;
  var tagId = util.query().tagId || 1;

  var jQworkList = $("#j-designer-works");
  var designerId = jQworkList.data("designer-id");

  jQback.unbind();
  jQback.attr('href', '/designer/list/?tagId='+tagId+'&page='+page+'&id='+id);


  jQmain.delegate('.j-alpha','click',function(){
    var jQself = $(this);
    jQself.parent().find('.j-designers').slideToggle();
    var jQarrow = jQself.find('.j-arrow');
    if(jQarrow.hasClass('i-arrow-down')){
      jQarrow.removeClass('i-arrow-down').addClass('i-arrow-up');
    }else{
      jQarrow.removeClass('i-arrow-up').addClass('i-arrow-down');
    }
  });

  var jQfav = $("#j-fav");
  var jQfavIcon = jQfav.find(".j-fav-icon");
  var jQfavText = jQfav.find(".j-fav-text");
  var favType = 1; //设计师
  jQfav.bind('click', function(){
    if(uid == ''){
      alert("请先登录");
      var redirect = window.location.pathname;
      window.location.href = '/user/login?redirect='+redirect;
      return;    
    }
    if(jQfavIcon.hasClass("i-fav")){
      var designerId = jQfav.data("id");
      $.post('/favorites/add', {
        "userId":uid,
        "type": favType,
        "tid":designerId
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-fav").addClass("i-faved");
          jQfavText.text("已收藏").css({"color": "#d37eff"});
          jQfav.data("id", ret.data.id);
        }else{
          alert(ret.msg)
        }
      })
    }else{
      var fid = jQfav.data("id");
      $.post('/favorites/'+fid+'/delete', {
        "userId":uid,
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-faved").addClass("i-fav");
          jQfavText.text("收藏").css({"color": "#868686"});  
        }else{
          alert(ret.msg);
        }
      })   
    }
  });

  var jQloading = $(".loading-box");
  var pageSize = 20;
  var flag = true;
  var itemCurrentPage = 1, itemNextPage=1;

  function getWorks(pageNum){
    $.post('/designer/'+designerId+'/items', {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId   
    }, function(ret){
      if(ret.code ==0){
    	  if(ret.data.length==0){
      		flag = false;
      	}
        itemCurrentPage = pageNum;
        for(var i=0, l=ret.data.length; i<l; i++){
          var id = ret.data[i].id;
          ret.data[i].tid = id;
        }
        if(ret.data.length == pageSize){
          itemNextPage++;
        }   
        jQloading.hide();
        jQworkList.append(template(itemTpl, ret));
      }else{
        alert(ret.msg)
      }
    });
  };
  

  function loadMore(){
    console.log("More loaded");
    if(itemCurrentPage<itemNextPage && flag){
      jQloading.show();
      getWorks(itemNextPage);
    }
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);




  $('#j-read-more').bind('click', function(){
    $('#j-nested-text').show();
    $('#j-more-ellipse').hide();
    $('#j-close-more').show();
    $(this).hide();
  });
  $('#j-close-more').bind('click', function(){
    $('#j-nested-text').hide();
    $('#j-more-ellipse').show();
    $('#j-read-more').show();
    $(this).hide();
  });  
  
    //页面加载时请求
  $(document).ready(function(){
    getWorks(1);    
  });

//  var jQtabNav = $(".i-nav-bar");
//  if(jQtabNav.length>0){
//    jQtabNav.bind('click', function(e){
//      e.stopPropagation();
//      var jQnav = $('.j-tab-nav');
//      if(jQnav.length>0){
//        jQnav.remove();
//      }else{
//        $('body').append('<ul class="j-tab-nav tab-nav">'+
//          '<a href="/"><li>首页</li></a>'+
//          '<a href="/designer/list"><li>发现</li></a>'+
//          '<a href="/cart"><li>购物车</li></a>'+
//          '<a href="/mine"><li>我的</li></a></ul>')
//      }
//    })
//  }

  $('body').bind('click', function(){
     $('.j-tab-nav').remove();
  });
  $(".banner").bind('click', function(e){
    e.stopPropagation();
  });

  /*var tagId = util.query().tagId||1;
  $('#j-back').bind('click', function(){
    window.location.href = '/designer/list?tagId='+tagId;
  });*/


  // var jQdesignerWork = $("#j-designer-works");


  
})
require.config({
  baseUrl: VM.STATIC_URL
});
require(['util/template', 'tpl/cateTpl', 'page/widgets/search', 'util/utils','tpl/find/itemTpl'],
  function(template, cateTpl, search, util,itemTpl){
  var jQmain = $("#j-main")
  var jQgender = $("#j-gender-select");

  // 男女装
  jQgender.delegate('.not-selected', 'click', function(){
    //撤销当前
    var current = jQgender.find('.selected').data('gender');
    if(current == 'her'){
      $('#j-him').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-her").animate({'left' : '10px'}).removeClass('selected').addClass('not-selected');  
    }else{
      $('#j-her').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-him").animate({'left' : '80%'}).removeClass('selected').addClass('not-selected');          
    }
  });

  var pageSize = 20;
  var flag = true;
  var categoryCurrentPage = parseInt(util.query().page) || 1;
  var categoryNextPage = categoryPrevPage = categoryCurrentPage;
  var jQcontainer = $('#j-main');
  var jQloding = $('.loading-box');
  var tagId = util.query().tagId || 1;
  var id = util.query().id || -1;
  var jQitemLast;


  if(tagId == 2){
    $('.not-selected').click()
  }

  function getItem(type, tagId, pageNum, from){
    if(pageNum == 1 && from){
      jQcontainer.empty();
    }
    if(from){
      //忘了是干嘛的啊？？
      categoryCurrentPage = pageNum;
    }
    
    $.post('/'+type, {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){ 
        // 如果为下拉
        ret['categoryCurrentPage'] = pageNum;
        ret['tagId'] = tagId;
        ret['from'] = 'item';

        if(from){

          if(ret.data.length==0){
            flag = false;
          }
          categoryCurrentPage = pageNum;
          if(ret.data.length==pageSize){
            categoryNextPage++;
          }
          jQcontainer.append(template(itemTpl, ret));
          var jQitemIndex= $("#item" + id);
          if(jQitemIndex.length>0){
            var top = jQitemIndex.offset().top-50;
            $('body').scrollTop(top);
          }
          jQloding.hide();           
        }else{
          jQcontainer.prepend(template(itemTpl, ret)); 
          var lastTop = jQitemLast.offset().top;
          $('body').scrollTop(lastTop);         
        }
      }else{
        alert(ret.msg)
      }
    });
  };

  //初始化
  getItem('single', tagId, categoryCurrentPage, 1);
  $(".j-nav").bind('click', function(){
  	jQself = $(this);
  	type = jQself.data("type");
  	jQself.addClass('selected').siblings().removeClass('selected');
  	getItem(type, tagId, 1);
    window.location.href = '/'+type+'/list?tagId=' + tagId;
  });

  $('.j-tag').bind('click',function(){
    jQself = $(this);
    var tag = jQself.data("tag-id"); 
    tagId = tag;
    // jQself.addClass('selected').siblings().removeClass('selected');
    categoryCurrentPage = categoryNextPage = categoryPrevPage = 1;
    getItem('single', tagId, categoryCurrentPage, 1);
    search.setAlpha(tagId);
  });
  search.setAlpha(tagId);

  // load more
  function loadMore(){
    console.log("More loaded");
    if(categoryCurrentPage<categoryNextPage && flag){
      jQloding.show();
      getItem('single', tagId, categoryNextPage, 1);
    }      
    $(window).bind('scroll', bindScroll);
  }
  function loadPrev(){
    console.log("More loaded");
    if(categoryPrevPage >= 1){
      jQitemLast = jQcontainer.find('.item').first();
      getItem('single', tagId, categoryPrevPage);
    }      
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
      if( $(window).scrollTop() == 0){
        if(categoryPrevPage>1){
          categoryPrevPage = categoryPrevPage-1;
          loadPrev();
        }
      }
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);


})
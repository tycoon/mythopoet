require.config({
  baseUrl: VM.STATIC_URL
});
require(['module/swipe','util/template', 'tpl/cateTpl', 'tpl/find/designerTpl', 'tpl/find/alphaTpl', 'lib/touchSwipe'],
  function(swipe, template, cateTpl, designerTpl, alphaTpl, swipe){
  var jQmain = $("#j-main")
  var jQgoSearch = $('#j-go-search');
  var jQsearchBtn = $('#j-search-btn');
  var jQsearchMask = $('#j-search-mask');
  var jQindex = $("#j-alpha-index");
  var jQsearchInput = $("#j-search-input");
  var jQsearchFrom = $("#j-search-form");
  var jQgender = $("#j-gender-select");
  var jQslideDown = $("#j-slide-down");
  var jQcloseSearch = $("#j-close-serach");
  var jQmag = $("#j-search-magnify");

  // 男女装
  jQgender.delegate('.not-selected', 'click', function(){
    //撤销当前
    var current = jQgender.find('.selected').data('gender');
    if(current == 'her'){
      $('#j-him').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-her").animate({'left' : '10px'}).removeClass('selected').addClass('not-selected');  
    }else{
      $('#j-her').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-him").animate({'left' : '80%'}).removeClass('selected').addClass('not-selected');          
    }
  });

  jQcloseSearch.bind('click', function(){
    jQsearchMask.fadeOut(200);
  })

  jQgoSearch.bind('click', function(){
    jQsearchMask.fadeIn(200);
    // jQsearchInput.focus();
  });

  // jQsearchMask.bind('click', function(){
  //   $(this).fadeOut(200);
  // });

  jQsearchFrom.bind('click', function(e){
    e.stopPropagation();
  });
  var search = function(){
    var searchKey = jQsearchInput.val();
    if(searchKey == ''){
      alert("请输入关键字");
      return;
    }else{
      window.location.href = "/search?key="+searchKey;
    }    
  }
  jQsearchInput.bind('keydown', function(e){
    if(e.keyCode==13){
      search();
    }
  });
  jQmag.bind('click', function(){
    search();
  })
  jQsearchBtn.bind('click', function(e){
    search()
  });

  var pageSize = 20;
  var designerCurrentPage = 1, categoryCurrentPage = 1;
  var designerNextPage=1, categoryNextPage=1;
  var jQcontainer = $('#j-main');
  var tagId = 1, type = 'designer';

  function getItem(type, tagId, pageNum){
    if(pageNum == 1){
      jQcontainer.empty();
    }
    if(type == 'designer'){
      designerCurrentPage = pageNum;
    }else{
      categoryCurrentPage = pageNum;
    }
    $.post('/'+type, {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){
        if(type == 'designer'){
          for(var i=0, l=ret.data.length; i<l; i++){
            var id = ret.data[i].id;
            var urlWithTag = id+'?tagId='+tagId;
            ret.data[i].tid = id;
            ret.data[i].urlWithTag = urlWithTag;
          }
          if(ret.data.length == pageSize){
            designerNextPage = designerCurrentPage+1;
          }
          jQcontainer.append(template(designerTpl, ret));
        }else{
          if(ret.data.length == pageSize){
            categoryNextPage = categoryCurrentPage+1;
          }
          ret.data.reverse();
          jQcontainer.append(template(cateTpl, ret));
        }

      }else{
        alert(ret.msg)
      }
    });
  };

  var setAlpha = function(tagId){
    $.post('/designer', {
      pageNum: 1,
      pageSize: 200,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){
        var ds = ret.data;
        $('#j-alpha-list').remove();
        jQsearchMask.append(alphaTpl);
        for(var i=0, l=ds.length; i< l; i++){
          var init = ds[i].initial.toUpperCase();
          switch(init){
            case '#':
              $('#num').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;
            case 'A':
              $('#A').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;
            case 'B':
              $('#B').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'C':
              $('#C').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'D':
              $('#D').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'E':
              $('#E').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'F':
              $('#F').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'G':
              $('#G').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'H':
              $('#H').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'I':
              $('#I').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'J':
              $('#J').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'K':
              $('#K').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'L':
              $('#L').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'M':
              $('#M').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'N':
              $('#N').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'O':
              $('#O').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;      
            case 'P':
              $('#P').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'Q':
              $('#Q').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'R':
              $('#R').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'S':
              $('#S').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;  
            case 'T':
              $('#T').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'U':
              $('#U').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'V':
              $('#V').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'W':
              $('#W').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'X':
              $('#X').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'Y':
              $('#Y').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break; 
            case 'Z':
              $('#Z').find('.index-list').css({'borderLeft': 'solid 1px white'}).append('<li><a href="/designer/detail/'+ds[i].id+'">'+ds[i].name+'<a/></li>');
              break;             
          }
        }
      }else{
        alert(ret.msg)
      }
    });
  };

  //初始化
  getItem('designer', tagId, 1);
  $(".j-nav").bind('click', function(){
    jQself = $(this);
    type = jQself.data("type"); 
    if(type == 'designer'){
      jQindex.show();
    }else{
      jQindex.hide();
    }
    jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type, tagId, 1);
    if(type == 'designer'){
      designerCurrentPage = 1; 
      designerNextPage = 1
    }else{
      categoryCurrentPage = 1;
      categoryNextPage = 1;
    }
  });

  $('.j-tag').bind('click',function(){
    jQself = $(this);
    var tag = jQself.data("tag-id"); 
    tagId = tag;
    // jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type, tagId, 1);
    setAlpha(tagId);
  });
  setAlpha(tagId);

  // load more
  function loadMore(){
    console.log("More loaded");
    if(type == 'designer'){
      if(designerCurrentPage<designerNextPage){
        // designerNextPage = designerCurrentPage+1;
        getItem(type, tagId, designerNextPage);
      }
    }else{
      if(categoryCurrentPage<categoryNextPage){
        getItem(type, tagId, categoryNextPage);
      }      
    }
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);


})
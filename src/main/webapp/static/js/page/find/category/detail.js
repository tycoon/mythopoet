require.config({
  baseUrl: VM.STATIC_URL
});
require(['module/general','module/cookie', 'util/template', 'tpl/find/itemTpl', 'util/utils'], 
		  function(general,cookie, template, itemTpl, util){	

  $('body').bind('click', function(){
     $('.j-tab-nav').remove();
  });
  $(".banner").bind('click', function(e){
    e.stopPropagation();
  });

  var tagId = parseInt(util.query().tagId) || 1;

  var pageSize = 20;
  var flag = true;
  var categoryCurrentPage = parseInt(util.query().page) || 1;
  var categoryNextPage = categoryPrevPage = categoryCurrentPage;
  var id = util.query().id || -1;
  var jQitemLast;

  var jQcontainer = $("#j-contatiner");
  var jQloading = $(".loading-box");
  var categoryId = jQcontainer.data("category-id");

  var jQback = $('#j-back');
  jQback.unbind();
  var location = '/category/list?tagId='+tagId+'&cateid='+categoryId;
  jQback.attr('href', location);

  function getItem(pageNum, from){
	  
    $.post('/category/'+categoryId+'/itemss', {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId
    }, function(ret){
      if(pageNum == 1 && from){
        jQcontainer.empty();
      }
      if(from){
        //忘了是干嘛的啊？？
        categoryCurrentPage = pageNum;
      }

      if(ret.code ==0){
      	ret['categoryCurrentPage'] = pageNum;
        ret['categoryId'] = categoryId;
        ret['from'] = 'category';
        ret['tagId'] = tagId;


        if(from){

          if(ret.data.length==0){
            flag = false;
          }
          categoryCurrentPage = pageNum;
          if(ret.data.length==pageSize){
            categoryNextPage++;
          }
          jQcontainer.append(template(itemTpl, ret));
          var jQitemIndex= $("#item" + id);
          if(jQitemIndex.length>0){
            var top = jQitemIndex.offset().top-50;
            $('body').scrollTop(top);
          }
          jQloading.hide();           
        }else{
          jQcontainer.prepend(template(itemTpl, ret)); 
          var lastTop = jQitemLast.offset().top;
          $('body').scrollTop(lastTop);         
        }
      }else{
        alert(ret.msg)
      }
    });
  };

  // load more
  function loadMore(){
    console.log("More loaded");
    if(categoryCurrentPage<categoryNextPage && flag){
      jQloading.show();
      getItem(categoryNextPage, 1);
    }      
    $(window).bind('scroll', bindScroll);
  }
  function loadPrev(){
    console.log("More loaded");
    if(categoryPrevPage >= 1){
      jQitemLast = jQcontainer.find('.item').first();
      getItem(categoryPrevPage);
    }      
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
      if( $(window).scrollTop() == 0){
        if(categoryPrevPage>1){
          categoryPrevPage = categoryPrevPage-1;
          loadPrev();
        }
      }
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);



  //页面加载时请求
  $(document).ready(function(){
	  getItem(categoryCurrentPage, 1);    
  });
})
require.config({
  baseUrl: VM.STATIC_URL
});
require(['util/template', 'tpl/cateTpl', 'page/widgets/search', 'util/utils'],
  function(template, cateTpl, search, util){
  var jQmain = $("#j-main")
  var jQgender = $("#j-gender-select");

  // 男女装
  jQgender.delegate('.not-selected', 'click', function(){
    //撤销当前
    var current = jQgender.find('.selected').data('gender');
    if(current == 'her'){
      $('#j-him').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-her").animate({'left' : '10px'}).removeClass('selected').addClass('not-selected');  
    }else{
      $('#j-her').animate({'left' : '50%'}).removeClass('not-selected').addClass('selected');
      $("#j-him").animate({'left' : '80%'}).removeClass('selected').addClass('not-selected');          
    }
  });

  var pageSize = 20;
  var flag = true;
  var designerCurrentPage = 1, categoryCurrentPage = 1;
  var designerNextPage=1, categoryNextPage=1;
  var jQcontainer = $('#j-main');
  var jQloding = $('.loading-box');
  var tagId = util.query().tagId || 1;
  var cateId = util.query().cateid || -1;


  if(tagId == 2){
    $('.not-selected').click()
  }

  function getItem(type, tagId, pageNum){
    if(pageNum == 1){
      jQcontainer.empty();
    }

    categoryCurrentPage = pageNum;
    
    $.post('/'+type, {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId
    }, function(ret){
      if(ret.code ==0){
    	  if(ret.data.length==0){
      		flag = false;
          }
		  for(var i=0, l=ret.data.length; i<l; i++){
	          ret.data[i].tagId = tagId;
	        }
	        if(ret.data.length == pageSize){
	          categoryNextPage = categoryCurrentPage+1;
	        }
	        ret.data.reverse();
	        jQloding.hide();
	        jQcontainer.append(template(cateTpl, ret));
          if(cateId != -1){
            var jQitemIndex= $("#cate" + cateId);
            if(jQitemIndex.length>0){
              var top = jQitemIndex.offset().top-80;
              $('body').scrollTop(top);
            }
          }
      }else{
        alert(ret.msg)
      }
    });
  };

  //初始化
  getItem('category', tagId, 1);
  $(".j-nav").bind('click', function(){
    jQself = $(this);
    type = jQself.data("type"); 
    jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type, tagId, 1);

    window.location.href = '/'+type+'/list?tagId=' + tagId;
  });

  $('.j-tag').bind('click',function(){
    jQself = $(this);
    var tag = jQself.data("tag-id"); 
    tagId = tag;
    // jQself.addClass('selected').siblings().removeClass('selected');
    getItem('category', tagId, 1);
    search.setAlpha(tagId);
  });
  search.setAlpha(tagId);

  // load more
  function loadMore(){
    console.log("More loaded");
    if(categoryCurrentPage<categoryNextPage && flag){
      jQloding.show();
      getItem('category', tagId, categoryNextPage);
    }      
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);


})
require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/general','module/cookie', 'util/template', 'tpl/find/itemTpl', 'util/utils'], 
  function(general,cookie, template, itemTpl, util){
  var uid = cookie.get("uid");
  var userId = cookie.get("userId");
  var jQmain = $("#j-main");
  jQmain.delegate('.j-alpha','click',function(){
    var jQself = $(this);
    jQself.parent().find('.j-designers').slideToggle();
    var jQarrow = jQself.find('.j-arrow');
    if(jQarrow.hasClass('i-arrow-down')){
      jQarrow.removeClass('i-arrow-down').addClass('i-arrow-up');
    }else{
      jQarrow.removeClass('i-arrow-up').addClass('i-arrow-down');
    }
  });

  var jQfav = $("#j-fav");
  var jQfavIcon = jQfav.find(".j-fav-icon");
  var jQfavText = jQfav.find(".j-fav-text");
  var favType = 1; //设计师
  jQfav.bind('click', function(){
    if(uid == ''){
      alert("请先登录");
      var redirect = window.location.pathname;
      window.location.href = '/user/login?redirect='+redirect;
      return;    
    }
    if(jQfavIcon.hasClass("i-fav")){
      var designerId = jQfav.data("id");
      $.post('/favorites/add', {
        "userId":uid,
        "type": favType,
        "tid":designerId
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-fav").addClass("i-faved");
          jQfavText.text("已收藏").css({"color": "#d37eff"});
          jQfav.data("id", ret.data.id);
        }else{
          alert(ret.msg)
        }
      })
    }else{
      var fid = jQfav.data("id");
      $.post('/favorites/'+fid+'/delete', {
        "userId":uid,
      },function(ret){
        if(ret.code == 0){
          jQfavIcon.removeClass("i-faved").addClass("i-fav");
          jQfavText.text("收藏").css({"color": "#868686"});  
        }else{
          alert(ret.msg);
        }
      })   
    }
  });

  var jQworkList = $("#j-designer-works");
  var designerId = jQworkList.data("designer-id");
  var pageSize = 20;
  var tagId = util.query().tagId;
  function getWorks(pageNum){
    $.post('/designer/'+designerId+'/items', {
      pageNum: pageNum,
      pageSize: pageSize,
      tagId: tagId   
    }, function(ret){
      if(ret.code ==0){
        for(var i=0, l=ret.data.length; i<l; i++){
          var id = ret.data[i].id;
          ret.data[i].tid = id;
        }
        jQworkList.append(template(itemTpl, ret));     
      }else{
        alert(ret.msg)
      }
    });
  };
  

  $('#j-read-more').bind('click', function(){
    $('#j-nested-text').show();
    $('#j-more-ellipse').hide();
    $('#j-close-more').show();
    $(this).hide();
  });
  $('#j-close-more').bind('click', function(){
    $('#j-nested-text').hide();
    $('#j-more-ellipse').show();
    $('#j-read-more').show();
    $(this).hide();
  });  
  
  	//页面加载时请求
	$(document).ready(function(){
		getWorks(1);		
	});
	
})
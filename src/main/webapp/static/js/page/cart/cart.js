require.config({ 
	baseUrl : VM.STATIC_URL
});
require([ 'module/general', 'module/cookie', 'util/template',
		'tpl/order/singleTpl', 'page/widgets/item', 'util/utils' ], function(
		general, cookie, template, itemTpl, CartItem, util) {
	var jQempty = $('#j-empty-cart');
	var uid = cookie.get('userId');
	var itemCount = 0;

	if (uid == '') {
		$("#j-go").attr('href', "/user/login").text("请先登录");
		window.location.href = '/user/login';
		jQempty.show();
		return;
	}
	var pword = util.query().pword;
	var sign = util.query().sign;
	var offsale = 1;
	var cost = 0;
	var buyLimit = 0;
	var amount = 0;
	if (sign == 2) {
		$.post('/sale/discount', {}, function(ret) {
			if (ret.code == 0) {
				if (ret.data.discount < 1) {
					$("#dataShow").html(ret.data.discount * 100 + "折");
					offsale = ret.data.discount;
				}

			}
		});
	} else if (sign == 1) {
		if (pword != '') {
			//alert("pwrod=" + pword);
			$.post('/sale/ticket/check', {
				pword : pword
			}, function(ret) {
				if (ret.code == 0) {
					cost = ret.data.cost;
					buyLimit = ret.data.buyLimit;
					$("#dataShow").html('满'+buyLimit+'减'+ret.data.cost);
				}
			});
		}
	}

	var getTotalPrice = function(price) {
		var ret = price;
		if (sign == 2) {

			ret = (price * offsale).toFixed(2);

		} else if (sign == 1) {

			if (price - buyLimit >= 0) {
				ret = price -cost;
			} else {
				ret = price;
			}

		}
		amount = ret;

		return ret;

	}

	var jQcontainer = $("#j-item-container");
	var jQnotEmpty = $("#j-not-empty");
	var allItems = [];
	var selectItems = [];
	var getCart = function() {
		jQcontainer.empty();
		$.post('/cart/' + uid, function(ret) {
			if (ret.code == 0) {
				if (ret.data.length == 0) {
					jQempty.show();
				} else {
					var items = ret.data;
					for (var i = 0, l = items.length; i < l; i++) {
						var itemData = items[i];
						var item = new CartItem.Item({
							id : itemData.id,
							price : itemData.cartPrice,
							count : itemData.count,
							stock : itemData.skuCount
						});
						allItems.push(item);
					}
					console.log(allItems);
					jQcontainer.append(template(itemTpl, ret));
					jQnotEmpty.show();
					itemCount = ret.data.length;
				}
			} else {
				alert(ret.msg);
			}
		});
	}
	getCart();

	// 勾选
	var selectedCount = 0, totalPrice = 0;
	var jQtotalCount = $("#j-total-count");
	var jQtotalPrice = $("#j-total-price");
	var jQselectAll = $("#j-select-all");
	var editStatus = false;
	jQcontainer.delegate('.j-select-icon', 'click', function(e) {
		var jQselectIcon = $(this);
		var id = jQselectIcon.data('id');
		var jQitem = jQselectIcon.closest('.j-item');
		if (!editStatus) {
			e.stopPropagation();
		}
		// 选择商品
		var price = parseFloat(jQitem.find('.j-price').text());
		var count = parseInt(jQitem.find('.j-count').text());
		if (jQselectIcon.hasClass("i-not-check-circle-border-cart")) {
			jQselectIcon.removeClass("i-not-check-circle-border-cart")
					.addClass("i-check-circle-border-cart");
			selectItems.push(id);
			selectedCount++;
			jQtotalCount.text('(' + selectedCount + ')');
			totalPrice += (price * count);
			jQtotalPrice.text(getTotalPrice(totalPrice));
		} else if (jQselectIcon.hasClass("i-check-circle-border-cart")) {
			// 取消选择
			jQselectIcon.removeClass("i-check-circle-border-cart").addClass(
					"i-not-check-circle-border-cart");
			selectedCount--;
			totalPrice -= (price * count);
			var dx = selectItems.indexOf(id);
			if (dx > -1) {
				selectItems.splice(dx, 1);
			}
			if (selectedCount <= 0) {
				selectedCount = 0;
				jQtotalCount.text('');
				totalPrice = 0;
			} else {
				jQtotalCount.text('(' + selectedCount + ')');
			}
			jQtotalPrice.text(getTotalPrice(totalPrice));
		}

	});
	jQcontainer.delegate('.j-item', 'click', function() {
		var jQself = $(this);
		var jQselectIcon = jQself.find('.j-select-icon');
		var id = jQself.data('id');
		var itemId = jQself.data('item-id');
		if (editStatus) {
			// 编辑状态删除
			jQselectIcon.removeClass('i-delete').addClass('i-delete-red');
			var ifDelete = confirm("确定要删除该商品？");
			if (ifDelete) {
				$.post('/cart/' + uid + '/update', {
					cartDetailId : id,
					count : 0,
				}, function(ret) {
					if (ret.code == 0) {
						alert("删除成功！");
						jQself.remove();
						// getCart();
					} else {
						alert(ret.msg);
						jQselectIcon.removeClass('i-delete-red').addClass(
								'i-delete');
					}
				})
			} else {
				jQselectIcon.removeClass('i-delete-red').addClass('i-delete');
			}
			return;
		} else {
			window.location.href = "/item/detail/" + itemId;
		}
	});

	jQcontainer.delegate('.j-item', 'click', function() {

	})

	// 增减数量
	jQcontainer.delegate('.j-minuse', 'click', function(e) {
		e.stopPropagation();
		var jQself = $(this);
		var jQcount = jQself.parent().find('.j-count');
		var jQprice = jQself.closest('.item-info').find('.j-price');
		var price = parseFloat(jQprice.text());
		id = jQself.closest(".j-item").data("id");
		var currentCount = parseInt(jQcount.text());
		if (currentCount <= 1) {
			currentCount = 1;
			return;
		}
		$.post('/cart/' + uid + '/update', {
			cartDetailId : id,
			count : (currentCount - 1),
		}, function(ret) {
			if (ret.code == 0) {
				currentCount--;
				jQcount.text(currentCount);
				// todo: 如果美选中的时候减
				if (selectItems.indexOf(id) > -1) {
					totalPrice -= price;
					jQtotalPrice.text(getTotalPrice(totalPrice));
				}
				// getCart();
			} else {
				alert(ret.msg);
			}
		});
	});
	jQcontainer.delegate('.j-add', 'click', function(e) {
		e.stopPropagation();
		var jQself = $(this);
		var jQcount = jQself.parent().find('.j-count');
		var jQprice = jQself.closest('.item-info').find('.j-price');
		var price = parseFloat(jQprice.text());
		id = jQself.closest(".j-item").data("id");
		var currentCount = parseInt(jQcount.text());
		$.post('/cart/' + uid + '/update', {
			cartDetailId : id,
			count : (currentCount + 1),
		}, function(ret) {
			if (ret.code == 0) {
				currentCount++;
				jQcount.text(currentCount);
				if (selectItems.indexOf(id) > -1) {
					totalPrice += price;
					jQtotalPrice.text(getTotalPrice(totalPrice));
				}
				// getCart();
			} else {
				alert(ret.msg);
			}
		});
	});

	// 全选
	jQselectAll.bind('click', function() {
		if (editStatus) {
			return;
		}
		var jQselfIcon = $(this).find('.icon');
		var jQselects = $('.j-select-icon');
		if (selectedCount == itemCount) {
			// 取消全选
			jQselfIcon.removeClass("i-check-circle-border").addClass(
					"i-not-check-circle-border");
			jQselects.removeClass("i-check-circle-border-cart").addClass(
					"i-not-check-circle-border-cart");
			selectedCount = 0;
			totalPrice = 0;
			jQtotalCount.text('');
			jQtotalPrice.text(getTotalPrice(totalPrice));
		} else if (selectedCount < itemCount) {
			// 全选
			jQselfIcon.removeClass("i-not-check-circle-border").addClass(
					"i-check-circle-border");
			jQselects.removeClass("i-not-check-circle-border-cart").addClass(
					"i-check-circle-border-cart");
			selectedCount = itemCount;
			totalPrice = 0;
			var jQprice = $('.j-price');
			for (var i = 0; i < itemCount; i++) {
				var p = parseFloat($(jQprice[i]).text());
				var c = parseInt($(jQprice[i]).closest('.j-item').find(
						'.j-count').text());
				totalPrice += p * c;
			}
			jQtotalCount.text('(' + selectedCount + ')');
			jQtotalPrice.text(getTotalPrice(totalPrice));
		}
	});

	// 编辑／完成按钮
	var jQeditBtn = $("#j-edit");
	jQeditBtn.bind('click', function() {
		var jQselects = $(".j-select-icon");
		var jQself = $(this);
		if (editStatus) {
			// 进入非编辑状态，需要编辑
			editStatus = false;
			jQselects.removeClass("i-delete").addClass(
					"i-not-check-circle-border-cart");
			jQself.text("编辑");
		} else {
			// 进入编辑状态，需要完成
			editStatus = true;
			jQselects.removeClass("i-not-check-circle-border-cart")
					.removeClass("i-check-circle-border-cart").addClass(
							"i-delete");
			jQself.text("完成");
			// 取消全选
			jQselectAll.find('.icon').removeClass("i-check-circle-border-cart")
					.addClass("i-not-check-circle-border-cart");
			selectedCount = 0;
			totalPrice = 0;
			jQtotalCount.text('');
			jQtotalPrice.text(getTotalPrice(totalPrice));
		}
	});

	// 结算
	var jQpay = $("#j-get-order");
	jQpay.bind('click', function() {
		var jQselects = $('.j-select-icon');
		var idArray = [];
		for (var i = 0; i < jQselects.length; i++) {
			if ($(jQselects[i]).hasClass('i-check-circle-border-cart')) {
				idArray.push($(jQselects[i]).data('id'));
			}
		}
		if (idArray.length == 0) {
			alert("请从购物车中选取商品")
		} else {
			var ids = idArray.join(',');
			window.location.href= '/cart/selectedcart?ids='+ids+'&fr=2';
				
		}
	})
})
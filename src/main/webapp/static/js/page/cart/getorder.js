require.config({
	baseUrl : VM.STATIC_URL
});
require([ 'module/general', 'module/cookie', 'util/template',
		'tpl/order/getOrder', 'page/widgets/item', 'util/utils' ], function(
		general, cookie, template, itemTpl, CartItem, util) {
	var jQempty = $('#j-empty-cart');
	var uid = cookie.get('userId');
	var itemCount = 0;
	var ids = util.query().ids;
	if (uid == '') {
		$("#j-go").attr('href', "/user/login").text("请先登录");
		window.location.href = '/user/login';
		jQempty.show();
		return;
	}
	var addressSign = -1;
	var pword = util.query().pword;
	var sign = util.query().sign;
	var from = util.query().fr;
	var itemId = util.query().i;
	var count = util.query().c;
	var props = util.query().p;
	var offsale = 1;
	var cost = 0;
	var buyLimit = 0;
	var amount = 0;
	var discountAmount = 0;
	var discountType = 0;
	if (sign == 2) {
		discountType = 1;
		$.post('/sale/discount', {}, function(ret) {
			if (ret.code == 0) {
				if (ret.data.discount < 1) {
					$("#dataShow").html(ret.data.discount * 100 + "折");
					offsale = ret.data.discount;
				}

			}
		});
	} else if (sign == 1) {
		discountType = 2;
		if (pword != '') {
			//alert("pwrod=" + pword);
			$.post('/sale/ticket/check', {
				pword : pword
			}, function(ret) {
				if (ret.code == 0) {
					cost = ret.data.cost;
					buyLimit = ret.data.buyLimit;
					$("#dataShow").html('立减'+ret.data.cost+'（订单满'+buyLimit+'元可用）');
				}
			});
		}
	}
	
	var getTotalPrice = function(price) {
		var ret = price;
		if (sign == 2) {

			ret = (price * offsale).toFixed(2);
			console.log("price"+price);
			console.log("ret"+ret);
			discountAmount = (price - ret).toFixed(2);
		} else if (sign == 1) {

			if (price - buyLimit >= 0) {
				ret = price -cost;
				discountAmount = price - ret;
			} else {
				ret = price;
			}

		}/*else{
			if(price >= 3000 && price < 5000){
				price -= 200;
				$('.limit-desc').html('满3000减200');
			}
			if(price >= 5000 && price < 10000){
				price -= 300;
				$('.limit-desc').html('满5000减300');
			}
			if(price >= 10000){
				price -= 1000;
				$('.limit-desc').html('满10000减1000');
			}
			ret = price;
		}  黑五满减下线   */
		amount = ret;
		console.log(ret);
		return ret;

	}

	
	
	var jQcontainer = $("#j-item-container");
	var jQaddress = $("#go-address");
	var jQadname = $("#go-name");
	var jQadphone = $("#go-phone");
	var jQadcount = $("#go-count");
	var jQadprice = $("#go-price");
	var jQallprice = $("#go-allprice");
	console.log(jQcontainer);
	//var jQnotEmpty = $("#j-not-empty");
	var allItems = [];
	var selectItems = [];
	var allcount = 0;
	var allprice = 0;
	var addressId = util.query().aId;
	if(addressId == "undefined"){
		addressId = 0
	}
	
	var getCart = function() {
		//jQcontainer.empty();
		if(from == 1){
			$.post('/cart/getSorder', {
				itemId : itemId,
				count : count,
				props : props
			}, function(ret) {
				if (ret.code == 0) {
					if (ret.data.length == 0) {
						jQempty.show();
					} else {
						jQcontainer.append(template(itemTpl, ret));
						for(var i = 0 ; i < ret.data.length;i++){
							allcount += ret.data[i].count;
							allprice += ret.data[i].cartPrice * ret.data[i].count;
						}
						if(typeof ret.data[0].cneeAddress != "undefined"){
							jQadname.html(ret.data[0].cneeAddress.name);
							jQadphone.html(ret.data[0].cneeAddress.phone);
							jQaddress.html(ret.data[0].cneeAddress.province+ret.data[0].cneeAddress.city+ret.data[0].cneeAddress.county+ret.data[0].cneeAddress.detail);
							addressSign = 0;
						}else{
							$('#to-address').html("请填写收货地址");
							addressSign = -1;
						}
						jQadprice.html(allprice);
						jQadcount.html(allcount);
						jQallprice.html(getTotalPrice(allprice));
						amount=getTotalPrice(allprice);
						//jQnotEmpty.show();
						//ßitemCount = ret.data.length;
					}
				} else {
					alert(ret.msg);
				}
			});
		}else{
			$.post('/cart/getorder', {
				cartDetailIds : ids,
				addressId : addressId
			}, function(ret) {
				if (ret.code == 0) {
					if (ret.data.length == 0) {
						jQempty.show();
					} else {
						jQcontainer.append(template(itemTpl, ret));
						for(var i = 0 ; i < ret.data.length;i++){
							allcount += ret.data[i].count;
							allprice += ret.data[i].cartPrice * ret.data[i].count;
						}
						if(typeof ret.data[0].cneeAddress != "undefined"){
							jQadname.html(ret.data[0].cneeAddress.name);
							jQadphone.html(ret.data[0].cneeAddress.phone);
							jQaddress.html(ret.data[0].cneeAddress.province+ret.data[0].cneeAddress.city+ret.data[0].cneeAddress.county+ret.data[0].cneeAddress.detail);
							addressSign = 0;
						}else{
							$('#to-address').html("请填写收货地址");
							addressSign = -1;
						}
						jQadprice.html(allprice);
						jQadcount.html(allcount);
						jQallprice.html(getTotalPrice(allprice));
						amount=getTotalPrice(allprice);
						//jQnotEmpty.show();
						//ßitemCount = ret.data.length;
					}
				} else {
					alert(ret.msg);
				}
			});
		}
		
	}
	getCart();
	
	console.log(allprice);
	var totiket = $("#to-ticket");
	totiket.bind('click',function(){
		if(from == 1){
			var url = '/sale/ticket?fr=1&from=cart&i='+itemId+'&c='+count+'&p='+props;
			window.location.href = url;
		}else{
			var url = '/sale/ticket?from=cart&fr=2&ids='+ids;
			window.location.href = url;
		}		
	})

	var toaddress = $("#to-address");
	toaddress.bind('click',function(){
		if(from == 1){
			var url = '/address?from=order&fr=1&i='+itemId+'&c='+count+'&p='+props;
			window.location.href = url;
		}else{
			var url = '/address?from=order&fr=2&ids='+ids;
			window.location.href = url;
		}		
	})

	
	
	// 结算
	var jQpay = $("#j-pay");
	jQpay.bind('click', function() {
		if(addressSign == -1){
			alert('请填写收货地址');
			return;
		}
		var remark = $("#go-remark").val();
		var ids = util.query().ids;
		var jQselects = $('.j-select-icon');
		var idArray = [];
		for (var i = 0; i < jQselects.length; i++) {
			if ($(jQselects[i]).hasClass('i-check-circle-border-cart')) {
				idArray.push($(jQselects[i]).data('id'));
			}
		}
		if(from == 1){
		      $.post('/order/add', {
		          itemId:itemId,
		          count:count,
		          props:props,
		          amount : amount,
		          remark : remark,
		          discountType : discountType,
		          discountAmount :discountAmount
		        },function(ret){
		          if(ret.code == 0){
		            var orderId = ret.data.id;
		            var url = '/pay/order/'+orderId+'?showwxpaytitle=1';
		            window.location.href = url;          
		          }else{
		            alert(ret.msg);
		          }
		        });
		}else{
			$.post('/order/submit', {
				cartDetailIds : ids,
				amount : amount,
				pword : pword,
				remark : remark,
				discountType : discountType,
		        discountAmount :discountAmount
			}, function(ret) {
				if (ret.code == 0) {
					var orderId = ret.data.id;
					var url = '/pay/order/' + orderId + '?showwxpaytitle=1';
					window.location.href = url;
				} else {
					alert(ret.msg);
				}
			});
		}
	})
})
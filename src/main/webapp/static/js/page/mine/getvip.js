require.config({ 
  baseUrl: VM.STATIC_URL
});
require(['module/general','util/utils', 'module/cookie'], function(general, util, cookie){

	var jQgetVip = $("#j-confirm");
	var jQvCodeInput = $("#j-code");
	var uid = cookie.get("uid");
	var re = new RegExp("^[0-9]*$");  

	jQgetVip.bind('click', function(){
		var vCode = jQvCodeInput.val();

		if(vCode == ''){
			alert("请输入激活码");
			return;
		}
		else{
			if(!re.test(vCode)){
				alert("请输入正确的激活码");
			}else{
				$.post('/customer/exchangecode', {
					"userId": uid,
					"code": vCode
				}, function(ret){
					if(ret.code == 0){
							alert(ret.msg);
							window.location.href = '/mine';
						}else{
							alert(ret.msg);
						}		
				});
			}
		}
	});

});
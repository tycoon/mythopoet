require.config({ 
  baseUrl: VM.STATIC_URL
});
require(['module/general','util/utils', 'module/cookie'], function(general, util, cookie){

	$.post('/sale/myTicket', {
	}, function(ret){
		if(ret.code == 0){
			console.log(ret.data);
			parseJsonhtml(ret.data)
		}
	});

	function parseJsonhtml(data){
		var HTML = '';
		$.each(data,function(i,element){
			var html = 
				'<li>'+'\n'+
				'	<div class="coupon-cont">'+'\n'+
				'		<div class="coupon-col-1">'+'\n'+
				'			<p class="coupon-sums">￥'+element.cost+'</p>'+'\n'+
				'			<p class="coupon-condition">满'+element.buyLimit+'可用</P>'+'\n'+
				'		</div>'+'\n'+
				'		<div class="coupon-col-2">'+'\n'+
				'			<p class="coupon-id"><span>激活码：</span><input type="text" value="'+element.pword+'" class="couponId" readonly></p>'+'\n'+
				'			<p class="coupon-time">有效期至'+format(element.expDate,"yyyy-MM-dd")+'</p>'+'\n'+
				'		</div>'+'\n'+
				'	</div>'+'\n'+
				'</li>'
				;
			HTML += html;
		});
		$('.my-coupon-list').append($(HTML));
		$('.copy-btn').on('click',copyVal);
	};
	
	//时间转换
	 var format = function(time, format){
		  var t = new Date(time);
		  var tf = function(i){return (i < 10 ? '0' : '') + i};
		  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
		   switch(a){
		   case 'yyyy':
		   return tf(t.getFullYear());
		   break;
		   case 'MM':
		   return tf(t.getMonth() + 1);
		   break;
		   case 'mm':
		   return tf(t.getMinutes());
		   break;
		   case 'dd':
		   return tf(t.getDate());
		   break;
		   case 'HH':
		   return tf(t.getHours());
		   break;
		   case 'ss':
		   return tf(t.getSeconds());
		   break;
		   };
		  });
		 };
		  //console.log(format(1396178344662, 'yyyy-MM-dd HH:mm:ss')); 
		 
		 //
		 function copyVal(){
			 var Url2=document.getElementById("biao1");
			 console.log($(this).parents('li').find('.couponId'));
			 var input = $(this).parents('li').find('.couponId');
			 input.select(); // 选择对象
			 document.execCommand("Copy"); // 执行浏览器复制命令
			 alert("已复制好，可贴粘。");
		 };
	

});
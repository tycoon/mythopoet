require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/general', 'module/cookie', 'util/utils','util/template', 'tpl/order/statusOrder'],
  function(general, cookie, util, template, itemTpl){

  var uid = cookie.get("userId");
  var status = util.query().status;
  if(status == ''){
    status = -1;
  }
  var pageSize = 10;
  var currentPage = 1, nextPage=1;
  var jQcontainer = $('#j-item-container');
  var statusMap = {
    '0': '待付款',
    '1': '待发货',
    '2': '待收货',
    '4': '已完成',
    '9': '已取消'
  }
  var formatTime = function(timestamp){
    var unixTimestamp = new Date(timestamp * 1000) ;
      // return unixTimestamp.toLocaleString();
    var d = new Date(timestamp);
    var jstimestamp = (d.getFullYear())+"-"+(d.getMonth()+1)+"-"+
    (d.getDate())+" "+(d.getHours())+":"+(d.getMinutes())+":"
    +(d.getSeconds());
    return jstimestamp;
  }

  function getItem(status, pageNum){
  	// type=0商品 1设计师
    currentPage=pageNum;
    if(pageNum == 1){
      jQcontainer.empty();
    }
    $.post('/order/list', {
      status: status,
      pageNum: pageNum,
      pageSize: pageSize,
    }, function(ret){
      if(ret.code ==0){
      	if(ret.data == null){
          if(pageNum==1){
      		  jQcontainer.append('<div style="padding-top:40px;"><p class="none-reminder" style="text-align: center;margin-bottom:10px;">您还没有该类型订单</p>'+
      		  '<div class="go-buy" style="text-align: center;"><a href="/" class="border-btn" id="j-go">赶快去逛逛</a></div></div>');     
          }	
      	}else{
          if(ret.data.length>0){
            for(var i=0; i<ret.data.length; i++){
              var t = formatTime(ret.data[i].utime);
              var s = statusMap[ret.data[i].status];
              ret.data[i].t = t;
              ret.data[i].s = s;
              ret.data[i].uid = uid;
            }
            if(ret.data.length == pageSize){
              nextPage = currentPage+1;
            }
          	jQcontainer.append(template(itemTpl, ret));
          }
      	}
      }else{
      	alert(ret.msg)
      }
    })
  }

  var jQnavBtn = $(".j-nav");
  jQnavBtn.bind('click', function(){
    var jQself = $(this);
    var type = jQself.data('type');
    jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type , 1);   
  });

  if(status == -1){
    $(jQnavBtn[0]).click()
  }else if(status == 0){
    $(jQnavBtn[1]).click()
  }else if(status == 1){
    $(jQnavBtn[2]).click()
  }else if(status == 2){
    $(jQnavBtn[3]).click()
  }else if(status == 4){
    $(jQnavBtn[4]).click()
  }else{
    $(jQnavBtn[0]).click()    
  }

  // load more
  function loadMore(){
    console.log("More loaded");
    if(currentPage<nextPage){
      getItem(status, nextPage);
    }else{
      // alert("没有更多了")
    }
    $(window).bind('scroll', bindScroll);
  }

  var lastScrollTop=0;
  function bindScroll(){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        $(window).unbind('scroll');
        loadMore();
      }
    } else {
      // upscroll code
    }
    lastScrollTop = st;
  }

  $(window).scroll(bindScroll);

})
require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/general','module/cookie','util/template', 'tpl/itemTpl', 'tpl/designerTpl'],
  function(general,cookie, template, itemTpl, designerTpl){

  var uid = cookie.get("uid");
  var pageSize = 12;
  var currentPage = 1;
  var jQcontainer = $('#j-item-container');

  function getItem(type, pageNum){
  	// type=0商品 1设计师
    if(pageNum == 1){
      jQcontainer.empty();
    }
    $.post('/favorites/list', {
      userId: uid,
      pageNum: pageNum,
      pageSize: pageSize,
      type: type
    }, function(ret){
      if(ret.code ==0){
      	if(ret.data == null){
      		if(type == 0){
      			jQcontainer.append('<div style="text-align:center;margin-top:20px;"><p class="none-reminder">您还没有收藏的商品</p>'+
      			'<div style="margin-top:10px;" class="go-buy"><a href="/" class="border-btn" id="j-go">赶快去逛逛</a></div></div>');
      		}else{
      			jQcontainer.append('<div style="text-align:center;margin-top:20px;"><p class="none-reminder">您还没有收藏的设计师</p>'+
      			'<div style="margin-top:10px;" class="go-buy"><a href="/designer/list" class="border-btn" id="j-go">赶快去逛逛</a></div></div>');
      		}     	
      	}else{
          if(type == 0){
            jQcontainer.removeClass('designers').addClass('items');
            jQcontainer.append(template(itemTpl, ret));
          }else{
            for(var i=0, l=ret.data.length; i<l; i++){
              var bn = ret.data[i].itemBrandName;
              ret.data[i].branName = bn;
            }
            jQcontainer.removeClass('items').addClass('designers');
            jQcontainer.append(template(designerTpl, ret));
          }
      	}
      }else{
      	alert(ret.msg)
      }
    })
  }
  getItem(0, 1);

  var jQnavBtn = $(".j-nav");
  jQnavBtn.bind('click', function(){
    var jQself = $(this);
    var type = jQself.data('type');
    jQself.addClass('selected').siblings().removeClass('selected');
    getItem(type , 1);   
  });
  
})
require.config({ 
  baseUrl: VM.STATIC_URL
});
require(['module/general','util/utils', 'module/cookie'], function(general, util, cookie){

	//todo:util?
	function toggleClass(jQdom, firstClass, secondClass){
		if( jQdom.hasClass(firstClass) ){
			jQdom.removeClass(firstClass).addClass(secondClass)
		}else{
			jQdom.removeClass(secondClass).addClass(firstClass)
		}

	}
	var from = util.query().from;
  	var orderId = util.query().id;

	var jQsetDefault = $("#j-set-default");
	jQsetDefault.bind('click', function(){
		toggleClass($(this), "i-not-check-circle-border", "i-check-circle-border")
	});

	var jQconfirmAdd = $("#j-confirm");
	var jQnameInput = $("#j-name");
	var jQphoneInput = $("#j-phone");
	var jQprovinceInput = $("#j-province");
	var jQcityInput = $("#j-city");
	var jQcountyInput = $("#j-county");
	var jQdetailInput = $("#j-detail");
	var jQerr = $("#j-err");
	var uid = cookie.get("uid");

	jQconfirmAdd.bind('click', function(){
		var name = jQnameInput.val();
		var phone = jQphoneInput.val();
		var province = jQprovinceInput.val();
		var city = jQcityInput.val();
		var county = jQcountyInput.val();
		var detail = jQdetailInput.val();
		var addressId = $(this).data("id");
		var isDefault = jQsetDefault.hasClass("i-check-circle-border") ? true :false;

		if(name == ''){
			jQerr.text("收货人为必填项");return;
		}else if(phone == ''){
			jQerr.text("联系电话为必填项");return;
		}else if(!util.checkMobile(phone)){
			jQerr.text("请输入正确的联系电话");return;
		}else if(detail == ''){
			jQerr.text("详细地址为必填项");return;
		}else{
			jQerr.text("");
			$.post('/address/update', {
				"userId": uid,
				"name": name,
				"phone": phone,
				"province": province,
				"city": city,
				"county": county,
				"detail": detail,
				"isDefault": isDefault,
				"addressId": addressId
			}, function(ret){
				if(ret.code == 0){
					if(addressId == 0){
						alert("添加地址成功");
					}else{
						alert("更新地址成功");
					}
					if(from == 'order'){
						var fr = util.query().fr;
						var itemId = util.query().i;
						var count = util.query().c;
						var props = util.query().p;
						var ids = util.query().ids;
						if(fr == 1){
				            window.location.href = '/cart/selectedcart' + '?fr=1&i='+itemId+'&c='+count+'&p='+props;
						}else{
				            window.location.href = '/cart/selectedcart' + '?fr=2&ids='+ids;
						}

					}else{
						window.location.href = "/address";
					}
				}else{
					alert(ret.msg);
				}
			});
		}
	})

	$('.address-input-wrapper').bind('click', function(){
		var jQinput = $(this).find('input');
		jQinput.focus();
	})


})
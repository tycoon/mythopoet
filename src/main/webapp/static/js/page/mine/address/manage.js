require.config({ 
  baseUrl: VM.STATIC_URL
});
require(['module/general', 'util/utils', 'module/cookie','util/template', 'tpl/addressTpl', 'lib/touchSwipe'], 
  function(general, util, cookie,template, addressTpl, touchswipe){
	var uid = cookie.get("uid");
	var pageSize = 12;
  var currentPage = 1;
  var jQcontainer = $('#j-address-container');
  var from = util.query().from;
  var orderId = util.query().id;

	function getAddress(pageNum){
    if(pageNum == 1){
      jQcontainer.empty();
    }
    $.post('/address/list', {
    	userId: uid,
      pageNum: pageNum,
      pageSize: pageSize,
    }, function(ret){
    	console.log(ret);
      if(ret.code ==0){
        jQcontainer.append(template(addressTpl, ret));
        $(".j-address").swipe( {
          swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            var jQself = $(this);
            if(direction == 'left'){
              jQself.find('.j-delete-address').show();
              jQself.find('.j-text').css({'left': '-70px'});             
            }else if(direction == 'right'){
              jQself.find('.j-delete-address').hide();
              jQself.find('.j-text').css({'left': '0px'});                
            }

          },
          //Default is 75px, set to 0 for demo so any distance triggers swipe
           threshold: 50
        });
      }
    });
	}
	getAddress(1);

  if(from == 'order'){
	  var url;
      var fr = util.query().fr;  
      var itemId = util.query().i;
		var count = util.query().c;
		var props = util.query().p;
  	  var ids = util.query().ids;
    jQcontainer.delegate('.address', 'click', function(){
      var addressId = $(this).data('id');
      if(fr == 1){		
    		window.location.href = '/cart/selectedcart?fr=1&i='+itemId+'&c='+count+'&p='+props+'&aId='+addressId;   		
      }else{
          window.location.href = '/cart/selectedcart?fr=2&ids='+ids+'&aId='+addressId;  
      }    
    });
    if(fr == 1){
    	url = "/address/add?from=order&fr=1&i="+itemId+'&c='+count+'&p='+props;
    }else{
    	 url = "/address/add?from=order&fr=2&ids="+ids;
    }
    $("#j-add-address").attr('href', url);
  }
  // 删除地址
  jQcontainer.delegate('.j-delete-address', 'click', function(e){
    e.stopPropagation();
    var jQself = $(this);
    var del = confirm("是否确定删除");
    if(del){
      var id = jQself.data("id");
      $.post('/address/delete/'+id, {
        "userId":uid
      },function(ret){
        if(ret.code == 0){
          alert("删除地址成功！");
          jQself.closest('.address').remove();
        }else{
          alert(ret.msg)
        }
      })      
    }
  });

  // 更新地址
  jQcontainer.delegate('.j-edit-address', 'click', function(e){
    e.stopPropagation();
    var jQself = $(this);
    var id = jQself.data("id");
    var fr = util.query().fr;  
    var itemId = util.query().i;
	var count = util.query().c;
	var props = util.query().p;
	var ids = util.query().ids;
    var url;
    if(from == 'order'){
    	if(fr == 1){
    		url = '/address/update/'+uid+'/'+id+'?from=order&fr=1&i='+itemId+'&c='+count+'&p='+props;
    	}else{
    		url = '/address/update/'+uid+'/'+id+'?from=order&fr=2&ids='+ids;
    	}
      
    }else{
      url = '/address/update/'+uid+'/'+id;
    }
    window.location.href = url;
  });

  // 更新地址为默认地址
  jQcontainer.delegate('.j-set-default', 'click', function(e){
    e.stopPropagation();
    var jQself = $(this);
    var id = jQself.data("id");
    $.post('/address/default/'+id, {
      "userId":uid
    },function(ret){
      if(ret.code == 0){
        alert("设置默认地址成功！");
        getAddress(1);
      }else{
        alert(ret.msg)
      }
    })
  });


})
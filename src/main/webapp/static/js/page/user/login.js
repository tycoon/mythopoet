require.config({
  baseUrl: VM.STATIC_URL
});
require(['module/general','module/cookie', 'util/utils'], function(general,cookie,util){
  // $.post("/user/")
  var jQusrInput = $('#j-user');
  var jQpswInput = $('#j-psw');
  var jQloginBtn = $('#j-login-btn');
  var jQerr = $('#j-err');

  jQloginBtn.bind('click', function(){
    var usr = jQusrInput.val();   
    var psw = jQpswInput.val();
    if(usr == ''){
      jQerr.text("用户名不能为空");
      return;
    }else if(psw == ''){
      jQerr.text("密码不能为空");
      return;
    }
    $.ajax({
      type: 'post',
      url: '/user/login', 
      dataType: 'json',
      data: { 'phone': usr,
              'password': psw
            },
      success: function(ret){
        if(ret.code == 0){
          cookie.set('uid', ret.data.id, 10000);
          cookie.set('userId', ret.data.id, 10000);
          cookie.set('token', ret.data.token, 10000);
          var redirect = util.query().redirect;
          if(redirect){
            window.location.href = redirect;
          }else{
            window.location.href = "/";
          }
        }else{
          alert(ret.msg)
        }
      }      
    });
  })

})
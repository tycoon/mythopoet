require.config({
  baseUrl: VM.STATIC_URL
});

require(['module/general'], function(general){
  var jQcaptchBtn = $('#j-get-captcha');
  var jQcaptchInput = $('#j-captcha-input');
  var jQphoneInput = $('#j-phone-input');
  var jQpswInput = $('#j-password');
  var jQrepswInput = $('#j-repassword');
  var jQgetBtn = $('#j-get-btn');
  var jQerrMsg = $('#j-err-msg');

  jQcaptchBtn.bind('click', function(){
    var phone = jQphoneInput.val();
    var jQself = $(this);
    if(phone == ''){
      jQerrMsg.text('请输入手机号')
    }else{
      jQerrMsg.text('');
      if(jQself.data('clicked') == 1){
        return;
      }
      $.post('/user/captcha', {
        phone: phone
      }, function(ret){
        if(ret.code == 0){
          console.log(ret);
          jQself.addClass('btn-disable');
          jQself.text("获取验证码(60)");
          jQself.data('clicked', 1);
          var leftTime = 60;
          var timer;
          timer = setInterval(function(){
            if(leftTime>1){
              leftTime--;
              jQself.text("获取验证码("+leftTime+")");
            }else if(leftTime ==1){
              jQself.removeClass('btn-disable');
              jQself.text("获取验证码");
              leftTime = 60;
              jQself.data('clicked',0);
              clearInterval(timer);
            }
          },1000)
          // alert("用于测试：您的验证码是"+ret.data);
        }else{
          alert(ret.msg)
        }
      });     
    }
  });

  var validReg = function(phone, captcha, psw, repsw){
    if(phone == ''){
      jQerrMsg.text('请输入手机号');
      return false;
    }else if(captcha == ''){
      jQerrMsg.text('请输入验证码');
      return false;
    }else if(psw == '' || repsw == ''){
      jQerrMsg.text('请输入密码');
      return false;
    }else if(psw != repsw){
      jQerrMsg.text('密码不一致');
      return false;
    }
    return true;
  }

  jQgetBtn.bind('click', function(){
    var phone = jQphoneInput.val();
    var captcha = jQcaptchInput.val();
    var psw = jQpswInput.val();
    var repsw = jQrepswInput.val();

    if(validReg(phone, captcha, psw, repsw) == false){
      return;
    }
    
    $.post('/user/pwd/forgot', {
      phone: phone,
      captcha: captcha,
      password: psw,
      repassword: repsw,
    },function(ret){
      if(ret.code == 0){
        console.log(ret);
        alert("修改密码成功，请直接登录"); //todo:或直接进入首页选择？
        window.location.href = "/user/login";
      }else{
        alert(ret.msg)
      }      
    });
  })

})
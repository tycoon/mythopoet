require.config({
  baseUrl: VM.STATIC_URL
});

require(['lib/slick','module/general', 'util/utils','tpl/slideTpl',], function(us,general, util,slideTpl){

  var jQcaptchBtn = $('#j-get-captcha');
  var jQcaptchInput = $('#j-captcha-input');
  var jQphoneInput = $('#j-phone-input');
  var jQnameInput = $("#j-user-input");
  var jQpswInput = $('#j-password');
  var jQrepswInput = $('#j-repassword');
  var jQregBtn = $('#j-register');
  var jQerrMsg = $('#j-err-msg');

  jQcaptchBtn.bind('click', function(){
    var phone = jQphoneInput.val();
    var jQself = $(this);
    if(phone == ''){
      jQerrMsg.text('请输入手机号')
    }else if(!util.checkMobile(phone)){
      jQerrMsg.text('请输入正确的手机号');
      alert("请输入正确的手机号");
    }else{
      jQerrMsg.text('');
      if(jQself.data('clicked') == 1){
        return;
      }
      $.post('/user/captcha', {
        phone: phone
      }, function(ret){
        if(ret.code == 0){
          console.log(ret);
          jQself.addClass('btn-disable');
          jQself.text("获取验证码(60)");
          jQself.data('clicked', 1);
          var leftTime = 60;
          var timer;
          timer = setInterval(function(){
            if(leftTime>1){
              leftTime--;
              jQself.text("获取验证码("+leftTime+")");
            }else if(leftTime ==1){
              jQself.removeClass('btn-disable');
              jQself.text("获取验证码");
              leftTime = 60;
              jQself.data('clicked',0);
              clearInterval(timer);
            }
          },1000)
          // alert("用于测试：您的验证码是"+ret.data);
        }else{
          alert(ret.msg)
        }
      });     
    }
  });

  var validReg = function(phone, name, captcha, psw, repsw){
    if(phone == ''){
      jQerrMsg.text('请输入手机号');
      return false;
    }else if (name == ''){
      jQerrMsg.text('请输入用户名');
      return false;
    }else if(!util.checkMobile(phone)){
      jQerrMsg.text('请输入正确的手机号');
      return false;
    }else if(captcha == ''){
      jQerrMsg.text('请输入验证码');
      return false;
    }else if(psw == '' || repsw == ''){
      jQerrMsg.text('请输入密码');
      return false;
    }else if(psw != repsw){
      jQerrMsg.text('密码不一致');
      return false;
    }
    return true;
  }

  jQregBtn.bind('click', function(){
    var phone = jQphoneInput.val();
    var captcha = jQcaptchInput.val();
    var psw = jQpswInput.val();
    var repsw = jQrepswInput.val();
    var name = jQnameInput.val();

    if(validReg(phone, name, captcha, psw, repsw) == false){
      return;
    }
    
    $.post('/user/reg', {
      phone: phone,
      captcha: captcha,
      name: name,
      password: psw,
      repassword: repsw,
    },function(ret){
      if(ret.code == 0){
        console.log(ret);
        alert("注册成功，请直接登录"); //todo:或直接进入首页选择？
        window.location.href = "/user/login";
      }else{
        alert(ret.msg)
      }      
    });
  })
  
  var jQslides = $("#j-slide-container")
	jQslides.slick({
		autoplaySpeed : 3000,
		dots : true,
		loop : true,
		autoplay : true,
		pauseOnDotsHover : false,
		pauseOnHover : false,
		arrows : false
	});
	jQslides.on('swipe', function(event, slick, direction) {
		console.log(slick, direction);
		slick.autoPlay();
	});
  
});
define([],function(){
  return  '<% for(var i in data){ %>'+
          '  <a href="/pay/order/<%= data[i].id %>?showwxpaytitle=1">'+
          '  <div class="item-wrapper">'+ 
          '    <div class="order-status">'+ 
          '      <p><span><%= data[i].t %></span><span class="fr status-<%= data[i].status %>"><%= data[i].s %></span></p>'+
          '    </div>'+
          '    <div class="order-view-id">'+      
          '      <p class="other-info">'+
          '        <span class="other-info-span">订单编号：</span>'+
          '        <span class="other-info-span"><%= data[i].viewId %></span>'+
          '      </p>'+    
          '    </div>'+
          '    <% for(var j in data[i].orderDetailViewList){ %>'+
          '    <div class="info-wrapper">'+
      	  '      <img class="item-thumb fl" src="<%= data[i].orderDetailViewList[j].pic.split(",")[0]  %>">'+
      	  '      <div class="item-info">'+
      	  '        <p class="item-name"><%= data[i].orderDetailViewList[j].brandName %></p>'+
      	  '        <p class="tc-light-gray"><%= data[i].orderDetailViewList[j].propsValue%></p>'+
      	  '        <p>数量：<%= data[i].orderDetailViewList[j].count %></p>'+
      	  '        <p class="item-price bold-num">¥<%= data[i].orderDetailViewList[j].price%></p>'+
      	  '      </div>'+
      	  '    </div>'+
      	  '    <%}%>'+
      	  '    <div class="trans-fee">'+
      	  '      <p class="other-info"><span class="other-info-span">共<%= data[i].totalCount %>件商品</span>'+
          // '      <span class="tc-dark-gray tc-bold other-info-span">运费：¥8.00</span>'+
          '      <span class="other-info-span">实付：¥ <%= data[i].amount %></span></p>'+
      	  '    </div>'+
      	  '  </div>'+
          '  </a>'+
          '<%}%>'	                                                                  
})
define([],function(){
  return  '<% for(var i in data){ %>'+
          // '<a href = "/item/detail/<%= data[i].itemId %>" class="item-link-wrapper">'+
          '<div class="info-wrapper j-item" data-id="<%= data[i].id %>" data-item-id="<%= data[i].itemId %>">'+ 
          '  <div class="fl item-left">'+
          '    <span>'+
          '      <i data-id="<%= data[i].id %>" class="icon j-select-icon i-not-check-circle-border-cart"></i>'+
          '    </span>'+
          '    <img class="item-thumb" src="<%= data[i].pic.split(",")[0] %>">'+
          '  </div>'+
          '  <div class="item-info">'+
          '    <p class="item-name"><%= data[i].brandName %></p>'+
          '    <p class="tc-light-gray">规格：<%= data[i].propsValue %></p>'+
          '    <div class="count-box cf">'+
          '      <div class="minuse-btn j-minuse">－</div>'+
          // '      <div><input class="no-border count-input j-count" type="text" value="<%= data[i].count %>"></div>'+
           '      <div><span class="no-border count-input j-count"><%= data[i].count %></span></div>'+
          '      <div class="plus-btn j-add">＋</div>'+
          '    </div>'+ 
          '    <p class="item-price">'+
          // '      <% if(data[i].originPrice != data[i].currentPrice){ %>'+          
          // '         <span class="yuan" style="color:#dd4570">¥</span><span class="bold-num price discount-price j-price"><%= data[i].currentPrice.toFixed(2) %></span>'+
          // '         </br><span class="line-through price">¥<%= data[i].originPrice.toFixed(2) %></span>'+
          // '      <% }else{ %>'+  
          '         <span class="yuan">¥</span><span class="bold-num price no-discount j-price"><%= data[i].currentPrice.toFixed(2) %></span>'+          
          // '      <% } %>'+   
          '    </p>'+    
          '  </div>'+
          '</div>'+
          // '</a>'+
          '<%}%>'
})
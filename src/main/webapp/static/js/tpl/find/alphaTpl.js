define([], function() {
	var alpha = ['#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
				 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
				 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
				]
	var tpl = '<div class="cf designer-list" id="j-alpha-list">'+
			  '  <div id="num" class="index-designer-wrapper">'+
			  '    <p class="designer-index">#</p>'+
      		  '    <ul class="index-list">'+
      		  '    </ul>'+
    		  '  </div>';
	for(var i=1, l=alpha.length; i<l; i++){
		tpl += '  <div id="'+alpha[i]+'" class="index-designer-wrapper">'+
			   '    <p class="designer-index">'+alpha[i]+'</p>'+
      		   '    <ul class="index-list">'+
      		   '    </ul>'+
    		   '  </div>';
	}
	tpl += '</div>'
	return tpl;
})
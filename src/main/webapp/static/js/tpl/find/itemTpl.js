define([],function(){
  return  '<% for(var i in data){ %>'+
          '  <div class="item">'+  
          '    <a class="item-wrapper" href="/item/detail/<%= data[i].id %>?page=<%= categoryCurrentPage %>&from=<%= from %>&id=<%= data[i].id %>&tagId=<%= tagId %>&categoryId=<%= categoryId %>" id="item<%= data[i].id %>">'+
          '      <img class="item-img" src="<%= data[i].pic.split(",")[0] %>">'+
          '      <p class="item-name brand-name"><%= data[i].brandName %></p>'+
          '      <% if(data[i].discount < 1){ %>'+
          '        <p class="item-price price discount-price">¥<%= data[i].price.toFixed(2) %></p>'+  
          '        <p class="item-price line-through price">¥<%= data[i].originPrice.toFixed(2) %></p>'+         
          '      <% }else{ %>'+  
          '        <p class="item-price price no-discount">¥<%= data[i].price.toFixed(2) %></p>'+ 
          '      <% } %>'+    
          '    </a>'+
          '  </div>'+
          '<%}%>'
})
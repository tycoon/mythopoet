define([],function(){
  return  '<% for(var i in data){ %>'+
          '  <div class="item">'+  
          '    <a class="item-wrapper" href="/item/detail/<%= data[i].tid %>">'+
          '      <img class="item-img" src="<%= data[i].pic.split(",")[0] %>">'+
          '      <p class="item-name brand-name"><%= data[i].itemBrandName %></p>'+
          '      <p class="item-price price">¥<%= data[i].itemPrice.toFixed(2) %></p>'+  
          // '      <p class="item-price line-through">¥<%= data[i].originPrice.toFixed(2) %></p>'+         
          '    </a>'+
          '  </div>'+
          '<%}%>'
})
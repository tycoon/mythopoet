define([],function(){
  return  '<% for(var i in data){ %>'+
          '  <% if(data[i].type == 1 && i==0){ %>'+
          '    <div class="address selected add-first" data-id="<%= data[i].id %>">'+
          '  <%}else if(i==0){%>'+
          '    <div class="address add-first" data-id="<%= data[i].id %>">'+
          '  <%}else if(data[i].type == 1){%>'+
          '    <div class="address selected" data-id="<%= data[i].id %>">'+
          '  <%}else{%>'+
          '    <div class="address" data-id="<%= data[i].id %>">'+
          '  <%}%>'+
  				'	   <div class="manipulate">'+
          '    <% if(data[i].type == 1){ %>'+
    			'		   <i class="icon i-check-circle-border"></i>'+
          '      <span class="tc-light-gray">默认地址</span>'+
          '    <%}else{%>'+
    			'      <span data-id="<%= data[i].id %>" class="tc-light-gray j-set-default">设为默认</span>'+
          '    <%}%>'+
    			'		   <span class="fr">'+
      		'			   <span data-id="<%= data[i].id %>" class="j-edit-address">编辑</span>'+
      		// '			   <span data-id="<%= data[i].id %>" class="j-delete-address">删除</span>'+
    			'		   </span>'+
  				'	   </div>'+
  				'	   <div class="address-text j-address">'+
          '      <div class="j-text">'+
    			'		     <p class="receiver"><span class="receiver-name"><%= data[i].name %></span><span class="receiver-phone fr"><%= data[i].phone %></span></p>'+
    			'		     <p><%= data[i].province %>'+'<%= data[i].city %>'+'<%= data[i].detail %></p>'+
          '      </div>'+
          '      <div data-id="<%= data[i].id %>" class="j-delete-address delete-btn hide">删除</div>'+
  				'	   </div>'+
					'  </div>'+
          '<%}%>'
})
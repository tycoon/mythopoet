define([],function(){
  return  '<% for(var i in data){ %>'+
          '  <div class="cloth">'+  
          '    <a class="cloth-wrapper" href="/item/detail/<%= data[i].id %>?from=new&cid=<%= data[i].cid %>&id=<%= data[i].id %>" id="cloth<%= data[i].id %>">'+
          '      <img class="cloth-img lazy" data-original="<%= data[i].pic.split(",")[0] %>">'+
          '      <p class="cloth-name brand-name"><%= data[i].brandName %></p>'+
          '      <% if(data[i].discount < 1){ %>'+
          '        <p class="cloth-price price discount-price">¥<%= data[i].price.toFixed(2) %></p>'+ 
          '        <p class="cloth-price price line-through">¥<%= data[i].originPrice.toFixed(2) %></p>'+
          '      <% }else{ %>'+  
          '        <p class="cloth-price price no-discount">¥<%= data[i].price.toFixed(2) %></p>'+ 
          '      <% } %>'+     
          '    </a>'+
          '  </div>'+
          '<%}%>'
})
define([],function(){
	var QueryString = function () {
	  // This function is anonymous, is executed immediately and 
	  // the return value is assigned to QueryString!
	  var query_string = {};
	  var query = window.location.search.substring(1);
	  var vars = query.split("&");
	  for (var i=0;i<vars.length;i++) {
	    var pair = vars[i].split("=");
	      // If first entry with this name
	    if (typeof query_string[pair[0]] === "undefined") {
	      query_string[pair[0]] = pair[1];
	      // If second entry with this name
	    } else if (typeof query_string[pair[0]] === "string") {
	      var arr = [ query_string[pair[0]], pair[1] ];
	      query_string[pair[0]] = arr;
	      // If third or later entry with this name
	    } else {
	      query_string[pair[0]].push(pair[1]);
	    }
	  } 
	    return query_string;
	}


	var toggleClass= function(jQdom, firstClass, secondClass){
		if( jQdom.hasClass(firstClass) ){
			jQdom.removeClass(firstClass).addClass(secondClass)
		}else{
			jQdom.removeClass(secondClass).addClass(firstClass)
		}
	}
	
	var sub=function(str,n){ 
		var r=/[^\x00-\xff]/g; 
		if(str.replace(r,"mm").length<=n){return str;} 
		var m=Math.floor(n/2); 
		for(var i=m;i<str.length;i++){ 
			if(str.substr(0,i).replace(r,"mm").length>=n){ 
				return str.substr(0,i)+"..."; 
			} 
		} 
		return str; 
	} 

	var checkMobile = function(str) {
    	var re = /^1\d{10}$/;
	    if (re.test(str)) {
	        return true;
	    } else {
	        return false;	    
	    }
	}
	return{
		query: QueryString,
		toggleClass: toggleClass,
		subString: sub,
		checkMobile : checkMobile
	}
})
define([],function(){
	var jQtabNav = $(".i-nav-bar");
	if(jQtabNav.length>0){
		jQtabNav.bind('click', function(e){
			e.stopPropagation();
			var jQnav = $('.j-tab-nav');
			if(jQnav.length>0){
				jQnav.remove();
			}else{
				$('body').append('<ul class="j-tab-nav tab-nav">'+
					'<a href="/"><li>首页</li></a>'+
					'<a href="/single/list"><li>发现</li></a>'+
					'<a href="/cart"><li>购物车</li></a>'+
					'<a href="/mine"><li>我的</li></a></ul>')
			}
		})
	}

	$('body').bind('click', function(){
		 $('.j-tab-nav').remove();
	});
	$(".banner").bind('click', function(e){
		e.stopPropagation();
	});

	var jQback = $("#j-back");
	if(jQback.length>0){
	  jQback.bind('click', function(){
	    if(history.length<1){
	    	window.location.href = '/'
	    }else{
	    	history.go(-1)
	    }
	  });
	}

})
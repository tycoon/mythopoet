define([], function(){
	function Note(text, option){
		this.text = text || '';
		if(option){
			this.duration = option.duration;
		}else{
			this.duration = 1000;
		}
		this.dom = $('<div class="note-wrapper hide"><p class="note">'+this.text+'</p></div>');
	}
	Note.prototype.show = function() {
		$('body').append(this.dom);
		this.dom.fadeIn();
		var jQdom = this.dom;
		var duration = this.duration;
		setTimeout(function(){
			jQdom.fadeOut('slow', function(){
				jQdom.remove();
			});			
		}, duration);
		return this.dom;
	};
	Note.prototype.destroy = function() {
		var jQdom = this.dom;
		jQdom.fadeOut('slow', function(){
			jQdom.remove();
		});			
		return;
	};

	var noteShow = function(text, option){
		var n = new Note(text, option);
		n.show();
		return n;
	};
	return{
		'show': noteShow
	}
})